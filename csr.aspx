﻿<%@ Page Title="Corporate Social Responsibility | Patel Integrated Logistics Ltd." Language="C#" MasterPageFile="~/HouseOfPatel.master" AutoEventWireup="true" CodeFile="csr.aspx.cs" Inherits="csr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="inner_page_container">

        <div class="blue_banner">
            <div class="container ">
                 <div class="row text-uppercase iepf_head">Corporate Social Responsibility <span>(</span>CSR<span>)</span></div> 
	                
            </div>
        </div>


        <div class="container text_format  iepf_wrp fade_anim">
            
            <asp:Literal ID="ltrCsrContent" runat="server"></asp:Literal>

<%--     <p> The world works with balance.</p>

 <p>Right from the phenomena of life and death, breathing in and breathing out, good and evil, to the simple play of giving and receiving, we realize that they are all two sides of the same coin, working together to manifest something greater.</p>

 <p> CSR is an activity that makes sure we establish this balance, by giving back to the world, as much as we receive, as a renowned multi-national business.</p>

 <p> Corporate Social Responsibility signifies the responsibility that a corporate holds towards its society, to build the society, just like the society together builds the business.</p>

 <p> They say, “Every action causes an equal or opposite reaction…” We believe in going above and beyond our actions with the pure joy of getting good reaction, from not only our customers, but also the underprivileged people in the world, that could use our support in any little heartful way.</p>

 <p> We do our work with poise and commitment.</p>

 <p> We donated a Blood-Donation Bus to TATA Memorial Hospital, utilizing 2% of both the financial years 15’ to 16’ and 16’ to 17’ needed to be utilized for CSR.</p>

            <img src="images/csr.png" class="img-responsive" / style="margin-bottom:20px; border:1px solid #f1f1f1;">


 <p> This added as a catalyst to save lives, and fuel the futures of as many humans that will make the future in this world.</p>

 <p> Recently we paid Rs.7 lacs to Rotary Club of Bombay Bandra towards part contribution (Blood Donation) to TATA Memorial Cancer Hospital.</p>

 <p> The world helps us making it big as a commercial platform, and we try our little best to make the platform of the world a little bigger.</p>

 <p> The people give us a purpose to work for them, and we use that purpose to serve them.</p>

 <p> We are here to give true justice to our motto, “Trusted People. Sensible Solutions.”, by coming up with real life solutions to surreal problems in the Nation.</p>--%>

            
  
        </div>

        <div class="clear"></div>
    </div>

      <script src="js/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>

    <script src="js/slick.min.js"></script>

    <script src="js/rem.min.js"></script>

    <script src="js/pushy.min.js"></script>

    <script src="js/general.js"></script>

    <script src="js/css3-animate-it.js"></script>

</asp:Content>



