﻿<%@ Page Title="Leaders of Supply Chain & Logistics Management in Logistics Industry in India" Language="C#" MasterPageFile="~/HouseOfPatel.master" AutoEventWireup="true" CodeFile="about_us.aspx.cs" Inherits="about_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <section>

        <div class="container-fluid">

            <asp:Literal ID="ltrlBanner" runat="server"></asp:Literal>

  
            <asp:Literal ID="ltrUpperAboutus" runat="server"></asp:Literal>

            <asp:Literal ID="ltrlQualityPolicy" runat="server"></asp:Literal>
   

            <div class="row grey_bg board_commities_wrp">

                <div class="wrapper">

                    <ul class="nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#bod" aria-controls="bod" role="tab" data-toggle="tab">Board & committees</a></li>
                        <li role="presentation"><a href="#coc" aria-controls="coc" role="tab" data-toggle="tab">composition of committees </a></li>
                    </ul>

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active bod_slider animatedParent" id="bod">

                            <asp:Repeater ID="rptboardcommittee" runat="server">
                                <ItemTemplate>
                                    <div>

                                <div class="bod_slider_content text-center animated fadeInLeftShort">

                                    <a data-fancybox data-src="#hidden-content-<%#Eval("Id") %>" href="javascript:;">

                                        <div class="bod_pic img-circle">
                                            <img src="uploads/bod/<%#Eval("Image").ToString().Trim() %>" alt=""></div>

                                        <div style="display: none;" id="hidden-content-<%#Eval("Id") %>" class="hidden-content">

                                            <div class="bod_pic img-circle">
                                                <img src="uploads/bod/<%#Eval("Image").ToString().Trim() %>" alt="" class="img-responsive"></div>

                                            <div class="nd">
                                                <h2><%#Eval("Prefix").ToString().Trim() %><%#Eval("Name").ToString().Trim() %></h2>
                                                <p><%#Eval("Position").ToString().Trim() %></p>
                                            </div>

                                            <p><%#Eval("Description") %></p>

                                        </div>

                                    </a>

                                    <h2><%#Eval("Prefix").ToString().Trim() %><%#Eval("Name").ToString().Trim() %></h2>
                                    <p><%#Eval("Position").ToString().Trim() %></p>

                                </div>

                            </div>
                                </ItemTemplate>
                            </asp:Repeater>

                        </div>

             <asp:Literal ID="ltrlCompositionCommittee" runat="server"></asp:Literal>

                    </div>

                </div>
            </div>

            <div class="row wrapper ptw_wrp">

                <div class="col-md-4">

                    <div class="title">
                        <h1>policies</h1>
                    </div>

                    <div class="policy_wrp">

                        <ul class="news_flash_wrap scrollbox">

                            <asp:Repeater ID="rptPrivacyPolicy" runat="server">
                                <ItemTemplate>
                               <li><a href="uploads/Privacy_policies/<%#Eval("Pdf") %>" target="_blank"><%#Eval("Title")%></a></li>

                                </ItemTemplate>
                            </asp:Repeater>              
                        </ul>

                    </div>

                </div>


                <asp:Literal ID="ltrlTestimonial" runat="server"></asp:Literal>


                <asp:Literal ID="ltrlWhyUs" runat="server"></asp:Literal>

         
            </div>

        </div>

    </section>

    <script src="js/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>

    <script src="js/slick.min.js"></script>

    <script src="js/rem.min.js"></script>

    <script src="js/jquery.fancybox.min.js"></script>

    <script src="js/enscroll-0.6.1.min.js"></script>

    <script src="js/general.js"></script>

    <script src="js/css3-animate-it.js"></script>

    <script>

        $(".bod_slider").slick({ infinite: !0, slidesToShow: 4, slidesToScroll: 4, arrows: !1, dots: !0, autoplay: !0, autoplaySpeed: 8e3, speed: 2e3, responsive: [{ breakpoint: 991, settings: { slidesToShow: 3, slidesToScroll: 3, infinite: !0, dots: !0 } }, { breakpoint: 767, settings: { slidesToShow: 2, slidesToScroll: 2, infinite: !0, dots: !1 } }, { breakpoint: 500, settings: { slidesToShow: 1, slidesToScroll: 1, infinite: !0, dots: 1 } }] });


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href") // activated tab
            if (target == "#coc") {

                $('.coc_slider').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                    autoplay: true,
                    autoplaySpeed: 8000,
                    speed: 2000,
                    arrows: false,
                    adaptiveHeight: true


                });
            }
            else {
                $('.coc_slider').slick('unslick');
            }
        });

        jQuery(".read_btn").click(function (e) { jQuery("#open").is(":visible") ? ($(this).text("Read More..."), jQuery("#open").slideUp()) : ($(this).text("Read Less..."), jQuery("#open").slideDown()) });

        $(".tab_content").hide(), $(".tab_content:first").show(), $("ul.tabs li").click(function () {
            $(".tab_content").hide();
            var a = $(this).attr("rel");
            $("#" + a).fadeIn(), $("ul.tabs li").removeClass("active"), $(this).addClass("active"), $(".tab_drawer_heading").removeClass("d_active"), $(".tab_drawer_heading[rel^='" + a + "']").addClass("d_active"); if (jQuery(window).width() < 767) { $('html,body').animate({ scrollTop: $("div.tab_container h3[rel^='" + a + "']").offset().top - 35 }, 1000) }
        }), $(".tab_drawer_heading").click(function () {
            $(".tab_content").hide();
            var a = $(this).attr("rel");
            $("#" + a).fadeIn(), $(".tab_drawer_heading").removeClass("d_active"), $(this).addClass("d_active"), $("ul.tabs li").removeClass("active"), $("ul.tabs li[rel^='" + a + "']").addClass("active"); if (jQuery(window).width() < 767) { $('html,body').animate({ scrollTop: $("div.tab_container h3[rel^='" + a + "']").offset().top - 35 }, 1000) }
        });


        if ($(window).width() > 992) {
            ! function (l) {
                var a = l(".iframe_wrap");
                a.hide(), l(".theme_box").find("a").on("click", function () {
                    l(".theme_main").hide(), a.fadeIn(300)
                }), l(".scrollbox").enscroll({
                    showOnHover: !0,
                    verticalTrackClass: "track",
                    verticalHandleClass: "handle"
                })
            }(jQuery);
        }

        if (window.location.href.indexOf("know_more") > -1) {
            $("html, body").animate({ scrollTop: $("#know_more").offset().top - 155 }, 1500), !1;
        }
    </script>
</asp:Content>

