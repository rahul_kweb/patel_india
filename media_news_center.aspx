﻿<%@ Page Title="Logistics Sector in India News | Media Advertisements | Patel Integrated Logistics Ltd | Real time Track and Trace" Language="C#" MasterPageFile="~/HouseOfPatel.master" AutoEventWireup="true" CodeFile="media_news_center.aspx.cs" Inherits="media_news_center" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="shortcut icon" href="images/fav_icon.png" />
    <link rel="stylesheet" href="css/jquery.fancybox.min.css" />
    <!-- css starts -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animations.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/nav-layout.min.css">

    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/inner_page.css">
    <section>

        <div class="container-fluid">

                <asp:Literal ID="ltrlBanner" runat="server"></asp:Literal>

        

            <div class="row">

                <div class="wrapper media_center_wrp">

                    <div class="inner_title">

                        <!--<h1>MEDIA & NEWS</h1>-->

                        <h2>PRESS COVERAGE</h2>

                    </div>

                    <div class="media_center_slider_wrp">
                        <asp:Repeater ID="rptPressCoverage" runat="server" Visible="true">
                            <ItemTemplate>
                                <div>

                                    <div class="media_center_slider_content">
                                        <a href="uploads/press_coverage/<%#Eval("Format").ToString().Trim()=="1"?Eval("pdf"):Eval("InnerImage")%>" data-fancybox="images">
                                            <img src="uploads/press_coverage/<%#Eval("PressCoverageimg").ToString().Trim() %>" class="img-responsive" alt=""></a>
                                    </div>
                                  
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                      
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 events_news_wrp">

                        <div class="col-md-6 col-sm-6 col-xs-12">

                            <div class="row events_wrp">

                                <div class="inner_title">
                                    <h2>RECENT NEWS</h2>
                                </div>

                                <div class="events_slider">


                                    <asp:Repeater ID="rptNewsVideos" runat="server">
                                        <ItemTemplate>
                                            <div class="events_slider_content">
                                                <a data-fancybox href='https://www.youtube.com/watch?v=<%#Eval("NewsVideosLink")%>'>


                                                    <img src="https://img.youtube.com/vi/<%#Eval("NewsVideosLink")%>/hqdefault.jpg" class="img-responsive" alt="">
                                                    <div class="youtube_button">
                                                        <img src="images/youtube_icon.png">
                                                    </div>
                                                </a>


                                                </a>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                 
                                </div>

                            </div>

                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                            <div class="row news_wrp">

                                <!--<div class="inner_title"><h2>Recent News</h2></div>-->

                                <ul class="news_flash_wrap scrollbox">

                                    <asp:Repeater ID="rptPdf" runat="server">
                                        <ItemTemplate>

                                    <li><a href="uploads/press_coverage/<%#Eval("Pdf") %>" target="_blank"><%#Eval("Title") %></a></li>
<%--                                    <li><a href="images/pdf/news/Article_TIMESONCETHEGSTBill.pdf" target="_blank">Article as how we are in for EXCITING TIMES ONCE THE GST Bill is passed</a></li>--%>

                                        </ItemTemplate>
                                        </asp:Repeater>
                                </ul>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>
    <script src="js/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>

    <script src="js/slick.min.js"></script>

    <script src="js/rem.min.js"></script>

    <script src="js/jquery.fancybox.min.js"></script>

    <script src="js/enscroll-0.6.1.min.js"></script>

    <script src="js/general.js"></script>

    <script src="js/css3-animate-it.js"></script>

    <script>

        $('.media_center_slider_wrp').slick({
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 3,
            dots: false,
            arrows: true,
            speed: 2000,
            responsive: [
             {
                 breakpoint: 768,
                 settings: {
                     slidesToShow: 2,
                     slidesToScroll: 1,
                 }
             },
             {
                 breakpoint: 480,
                 settings: {
                     slidesToShow: 1,
                     slidesToScroll: 1,
                 }
             }
            ]
        });
        $('.events_slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            speed: 2000,

        });

        if ($(window).width() > 992) {
            ! function (l) {
                var a = l(".iframe_wrap");
                a.hide(), l(".theme_box").find("a").on("click", function () {
                    l(".theme_main").hide(), a.fadeIn(300)
                }), l(".scrollbox").enscroll({
                    showOnHover: !0,
                    verticalTrackClass: "track",
                    verticalHandleClass: "handle"
                })
            }(jQuery);
        }


    </script>

</asp:Content>

