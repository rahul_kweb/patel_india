﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HouseOfPatel.master" AutoEventWireup="true" CodeFile="sitemap.aspx.cs" Inherits="sitemap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

           <div class="inner_page_container">

        <div class="blue_banner">
            <div class="container">
                 Sitemap 
	        </div>
        </div>


        <div class="clear"></div>

        <div class="container text_format fade_anim">

                
            <div class="col-xs-12 sitemap_wrp">

            <div class="row">
            
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <h2>Our Services</h2>
                    <ul>
                       <%-- <li><a href="patel_roadways.aspx">Patel Roadways</a></li>
                        <li><a href="patel_express.aspx">Patel Express</a></li>--%>
                        <li><a href="patel_airfreight.aspx">Patel Airfreight</a></li>
                        <li><a href="patel_warehouse.aspx">Patel Warehouse</a></li>
                    </ul>
                </div>

                <div class="col-md-3 col-sm-4 col-xs-12">
                    <h2>Investor Relation</h2>
                    <ul>
                        <li><a href="#" class="review_head">Investor Information <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            
                            <ul class="review_content">
                                <li><a href="share_holding_pattern.aspx">share holding pattern</a></li>
                                <li><a href="quaterly_result.aspx">quaterly results</a></li>
                                <li><a href="annual_reports.aspx">annual reports</a></li>
                                <li><a href="corporate_announcement.aspx">corporate announcements</a></li>
                                <li><a href="images/pdf/Chairman_speech.pdf" target="_blank"> chairman speech</a></li>
                                <li><a href="unclaimed_dividend.aspx">unclaimed dividend</a></li>
                                <li><a href="images/pdf/Fixed Deposit.pdf" target="_blank"> fixed deposit</a></li>
                                  <li><a href="iepf.aspx">IEPF</a></li>

                            </ul>
                        
                        </li>
                        <li><a href="contacts_for_investor.aspx">Contacts For Investors</a></li>
                        <li><a href="images/pdf/CodeofConduct.pdf" target="_blank">Code Of Conduct</a></li>
                        <li><a href="#" class="review_head">Insider Trading Code <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                        
                        <ul class="review_content">
                                <li><a href="images/pdf/PILL-Code-of-Conduct-for-Insider-Trading.pdf" target="_blank">Pill Code Of counduct for insider trading</a></li>
                                <li><a href="images/pdf/PILL-Code-of-Fair-Disclosure.pdf" target="_blank">Pill code of fair disclosure</a></li>
                            </ul></li>
                        <li><a href="images/pdf/NoticeoftheClosureofTradingWindow_n.pdf" target="_blank">Closure Of Trading Window</a></li>
                    </ul>
                </div>


                <div class="col-md-3 col-sm-4 col-xs-12">
                    <h2>Quick Links</h2>
                    <ul>
                        <li><a href="about_us.aspx">About Us</a></li>
                        <li><a href="careers.aspx">Careers</a></li>
                        <li><a href="#" class="review_head">CSR <i class="fa fa-angle-down" aria-hidden="true"></i> </a>
                             <ul class="review_content">
                                <li><a href="csr.aspx"> Corporate Social Responsibility </a></li>
                                <li><a href="images/pdf/csr_policy.pdf" target="_blank"> Patel CSR Policy</a></li>
                            </ul>
                        </li>
                        <li><a href="media_news_center.aspx">Media & News Center</a></li>
                         
                    </ul>
                </div>

                <div class="col-md-3 col-sm-4 col-xs-12">
                    <h2>Support</h2>
                    <ul>
                        <li><a href="location.aspx">Locations</a></li>
                        <li><a href="contact_us.aspx">Contact Us</a></li>
                    
                   </ul>
                </div>

                
                <div class="clearfix"></div>
          
                </div>

              
            </div>

        </div>

        <div class="clear"></div>
    </div>

   <script src="js/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>

    <script src="js/slick.min.js"></script>

    <script src="js/rem.min.js"></script>

    <script src="js/pushy.min.js"></script>

    <script src="js/general.js"></script>

    <script src="js/css3-animate-it.js"></script>

    <script src="js/equate.js"></script>

    <script>
        jQuery(document).ready(function (e) { jQuery(window).width() > 0 && (jQuery(".review_content").hide(), jQuery(".review_head").click(function (e) { 0 == jQuery(this).next(".review_content").is(":visible") ? (jQuery(".review_content").slideUp(), jQuery(this).next(".review_content").slideDown(), jQuery(".review_head").children(".fa").addClass("fa-angle-down"), jQuery(this).children(".fa").removeClass("fa-angle-down"), jQuery(this).children(".fa").addClass("fa-angle-up")) : (jQuery(".review_content").slideUp(), jQuery(".review_head").children(".fa").addClass("fa-angle-down")) })) });

    </script>
</asp:Content>

