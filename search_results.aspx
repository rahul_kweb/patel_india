﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HouseOfPatel.master" AutoEventWireup="true" CodeFile="search_results.aspx.cs" Inherits="search_results" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style>
        ul.search_result li {
            float: none;
            list-style: none;
        }
    </style>

      

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

        <div class="inner_page_container">

        <div class="blue_banner">
            <div class="container">
                 Search Result For : 
	                <span style="display: inline-block; text-transform: none;">
                        <asp:Label ID="lblTittle" runat="server"></asp:Label>
                    </span>
            </div>
        </div>
        <div class="clear"></div>

        <div class="container text_format fade_anim">

            <p id="pError" runat="server" visible="false" align="center">No matching result found.<br>
                Please modify your search criteria and try searching again.</p>

            <ul class="search_result">
            
                <asp:Repeater ID="rptSearch" runat="server">
                    <ItemTemplate>
                        <li>
                    <a href='/<%#Eval("Page_Name") %>' class="search_hd"><%#Eval("Heading") %></a>
                               <asp:Literal ID="litDesc" runat="server" Text='<%#Eval("DESCRIPTION") %>'></asp:Literal>
                   
                </li>
                    </ItemTemplate>
                </asp:Repeater>
               
              
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

   <script src="js/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>

    <script src="js/slick.min.js"></script>

    <script src="js/rem.min.js"></script>

    <script src="js/pushy.min.js"></script>

    <script src="js/general.js"></script>

    <script src="js/css3-animate-it.js"></script>

</asp:Content>

