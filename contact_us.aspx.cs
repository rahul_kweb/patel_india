﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class contact_us : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindContactUs();
        }
    }
    //protected void submit_Click(object sender, EventArgs e)
    //{
    //    using (SqlCommand cmd = new SqlCommand("Proc_ContactUs"))
    //    {
    //        cmd.CommandType = CommandType.StoredProcedure;
    //        cmd.Parameters.AddWithValue("@PARA", "ADD");
    //        cmd.Parameters.AddWithValue("@FirstNAME", txtFirstName.Text.Trim());
    //        cmd.Parameters.AddWithValue("@LastNAME", txtLastName.Text.Trim());
    //        cmd.Parameters.AddWithValue("@JobTitle", txtJobTitle.Text.Trim());
    //        cmd.Parameters.AddWithValue("@CompanyName",txtComapanyName.Text.Trim());
    //        cmd.Parameters.AddWithValue("@City",ddlCity.SelectedItem.Text.Trim());
    //        cmd.Parameters.AddWithValue("@Pincode",txtPinCode.Text.Trim());
    //        cmd.Parameters.AddWithValue("@Address1",txtAddress1.Text.Trim());
    //        cmd.Parameters.AddWithValue("@Address2",txtAddress2.Text.Trim());
    //        cmd.Parameters.AddWithValue("@EMAIL",txtEmail.Text.Trim());
    //        cmd.Parameters.AddWithValue("@Mobile",txtMobNo.Text.Trim());
    //        cmd.Parameters.AddWithValue("@TimeToContact",ddlDateTime.Text.Trim());
    //        cmd.Parameters.AddWithValue("@Industry",ddlIndustry.Text.Trim());
    //        cmd.Parameters.AddWithValue("@Requirement",txtRequirement.Text.Trim());
    //        cmd.Parameters.AddWithValue("@StorageVolume",txtStorageValue.Text.Trim());
    //        cmd.Parameters.AddWithValue("@ServiceProvider",txtServiceProvider.Text.Trim());

    //        if (utility.Execute(cmd))
    //        {
    //            //ContactUsMail mailconf = new ContactUsMail();
    //            //mailconf.EmailContactUs(txtFirstName.Text, txtLastName.Text,txtJobTitle.Text, txtComapanyName.Text, ddlCity.SelectedItem.Text,txtPinCode.Text,txtAddress1.Text,txtAddress2.Text,txtEmail.Text,txtMobNo.Text,ddlDateTime.SelectedItem.Text,ddlIndustry.SelectedItem.Text,txtStorageValue.Text,txtServiceProvider.Text);

    //            //Reset();

    //            //Form.Visible = true;

    //            //string script = "window.onload = function() { InstantShowPopUp(); };";
    //            //ClientScript.RegisterStartupScript(this.GetType(), "InstantShowPopUp", script, true);
    //            //MyMessageBox2.ShowSuccess("Thank you for contacting us.we will get back to you soon!.");


    //        }
    //        else
    //        {
    //            Form.Visible = true;

    //        }


    //    }
    //}

    public void Reset()
    {
        txtFirstName.Text = string.Empty;
        txtLastName.Text = string.Empty;
        txtJobTitle.Text = string.Empty;
        txtComapanyName.Text = string.Empty;
        ddlCity.SelectedValue ="0";
        txtPinCode.Text = string.Empty;
        txtAddress1.Text = string.Empty;
        txtAddress2.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtMobNo.Text = string.Empty;
        ddlDateTime.SelectedValue = "0";
        ddlIndustry.SelectedValue = "0";
        txtRequirement.Text = string.Empty;
        txtStorageValue.Text = string.Empty;
        txtServiceProvider.Text = string.Empty;


    }

    public void BindContactUs()
    {
        DataTable dt = new DataTable();
        try
        {
            dt = utility.Display("Exec Proc_CMS 'GET_BY_ID',4");
            if (dt.Rows.Count > 0)
            {
                ltrlContactUs.Text = dt.Rows[0]["CMS_DESC"].ToString();
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;

        }


    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_ContactUs"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@FirstNAME", txtFirstName.Text.Trim());
            cmd.Parameters.AddWithValue("@LastNAME", txtLastName.Text.Trim());
            cmd.Parameters.AddWithValue("@JobTitle", txtJobTitle.Text.Trim());
            cmd.Parameters.AddWithValue("@CompanyName", txtComapanyName.Text.Trim());
            cmd.Parameters.AddWithValue("@City", ddlCity.SelectedItem.Text.Trim());
            cmd.Parameters.AddWithValue("@Pincode", txtPinCode.Text.Trim());
            cmd.Parameters.AddWithValue("@Address1", txtAddress1.Text.Trim());
            cmd.Parameters.AddWithValue("@Address2", txtAddress2.Text.Trim());
            cmd.Parameters.AddWithValue("@EMAIL", txtEmail.Text.Trim());
            cmd.Parameters.AddWithValue("@Mobile", txtMobNo.Text.Trim());
            cmd.Parameters.AddWithValue("@TimeToContact", ddlDateTime.Text.Trim());
            cmd.Parameters.AddWithValue("@Industry", ddlIndustry.Text.Trim());
            cmd.Parameters.AddWithValue("@Requirement", txtRequirement.Text.Trim());
            cmd.Parameters.AddWithValue("@StorageVolume", txtStorageValue.Text.Trim());
            cmd.Parameters.AddWithValue("@ServiceProvider", txtServiceProvider.Text.Trim());

            if (utility.Execute(cmd))
            {
                ContactUsMail mailconf = new ContactUsMail();
                mailconf.EmailContactUs(txtFirstName.Text, txtLastName.Text, txtJobTitle.Text, txtComapanyName.Text, ddlCity.SelectedItem.Text, txtPinCode.Text, txtAddress1.Text, txtAddress2.Text, txtEmail.Text, txtMobNo.Text, ddlDateTime.SelectedItem.Text, ddlIndustry.SelectedItem.Text, txtRequirement.Text, txtStorageValue.Text, txtServiceProvider.Text);

                Reset();

                Form.Visible = true;

                string script = "window.onload = function() { InstantShowPopUp(); };";
                ClientScript.RegisterStartupScript(this.GetType(), "InstantShowPopUp", script, true);
                MyMessageBox2.ShowSuccess("Thank you for contacting us.we will get back to you soon!.");


            }
            else
            {
                Form.Visible = true;

            }


        }
    }
}