﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class careers : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bindrepeater();
            BindDropDownList();
            BindInnerBanner();
            BindCareerContent();
        }
    }


    protected void submit_Click(object sender, EventArgs e)
    {
       

                using (SqlCommand cmd = new SqlCommand("Proc_Careers"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PARA", "ADD");
                    cmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text.Trim());
                    cmd.Parameters.AddWithValue("@LastName", txtLastName.Text.Trim());
                    cmd.Parameters.AddWithValue("@ContactNo", txtContact.Text.Trim());
                    cmd.Parameters.AddWithValue("@JobProfile", ddlJobProfile.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text.Trim());




                    if (utility.Execute(cmd))
                    {
                        CareerMail mailconf = new CareerMail();
                        mailconf.EmailCareersMail(txtFirstName.Text, txtLastName.Text, txtContact.Text, ddlJobProfile.SelectedItem.Text, txtEmail.Text);

                        Reset();


                        string script = "window.onload = function() { InstantShowPopUp(); };";
                        ClientScript.RegisterStartupScript(this.GetType(), "InstantShowPopUp", script, true);

                        //Page.ClientScript.RegisterStartupScript(this.GetType(), "InstantShowPopUp", "InstantShowPopUp()", true);
                        //MyMessageBox1.ShowSuccess("Successfully Submitted");
                        //Form.Visible = false;
                        //pnlMsg.Visible = true;

                    }
                    else
                    {
                        //MyMessageBox1.ShowWarning("Unable to Submit");
                        //Form.Visible = true;
                        //pnlMsg.Visible = false;


                    }
                }
            }
        
    




    //public bool CheckSave()
    //{
    //    bool isOK = true;
    //    string message = string.Empty;

    //    if (ddlJobProfile.SelectedValue == "0")
    //    {
    //        isOK = false;
    //        message += "JobProfile,";
    //    }

    //    if (txtFirstName.Text.Trim().Equals(string.Empty))
    //    {
    //        isOK = false;
    //        message += "First Name, ";
    //    }

    //    if (txtLastName.Text.Trim().Equals(string.Empty))
    //    {
    //        isOK = false;
    //        message += "Last Name, ";
    //    }
    //    if (txtContact.Text.Trim().Equals(string.Empty))
    //    {
    //        isOK = false;
    //        message += "Contact No, ";
    //    }

    //    if (txtEmail.Text.Trim().Equals(string.Empty))
    //    {
    //        isOK = false;
    //        message += "Email, ";
    //    }

    //    if (message.Length > 0)
    //    {
    //        message = message.Substring(0, message.Length - 2);
    //    }
    //    if (!isOK)
    //    {
    //        MyMessageBox1.ShowError("Please fill following fields <br />" + message);
    //    }
    //    return isOK;
    //}


    public void Reset()
    {

        txtFirstName.Text = string.Empty;
        txtLastName.Text = string.Empty;
        txtContact.Text = string.Empty;
        ddlJobProfile.SelectedIndex = -1;
        txtEmail.Text = string.Empty;
    }

    public void Bindrepeater()
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CurrentOpening 'FORNT_GET'");
        if (dt.Rows.Count > 0)
        {
            rptCurrentOpening.DataSource = dt;
            rptCurrentOpening.DataBind();
        }
        else
        {

            rptCurrentOpening.DataSource = null;
            rptCurrentOpening.DataBind();
        }
    }


    private void BindDropDownList()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC Proc_CurrentOpening 'FORNT_GET_JOBPROFILE_FOR_CAREER'");
        ddlJobProfile.DataSource = dt;
        ddlJobProfile.DataBind();

        ddlJobProfile.Items.Insert(0, new ListItem("--Select Job Profile--", "0"));
    }

    public void BindInnerBanner()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner'GET_BY_ID',6");

        if (dt.Rows.Count > 0)
        {


            StringBuilder strbuild = new StringBuilder();
            strbuild.Append("<div class=\"row inner_banner_wrp\" style=\"background:url(uploads/Images/" + dt.Rows[0]["InnerBannerImage"].ToString() + ") no-repeat center; background-size:cover;\">");
            strbuild.Append("<div class=\"inner_page_title\">");
            strbuild.Append("<div class=\"inner_page_title_txt text-center\">");
            strbuild.Append("<h1>Careers</h1>");
            strbuild.Append("<ul class=\"breadcrumb\">");
            strbuild.Append("<li><a href=\"index.aspx\">Home</a></li>");
            strbuild.Append("<li>Careers</li>");
            strbuild.Append("</ul>");
            strbuild.Append("</div>");
            strbuild.Append("</div>");
            strbuild.Append("</div>");

            ltrlBanner.Text = strbuild.ToString();
        }
    }


    public void BindCareerContent()
    {
        DataTable dt = new DataTable();
        try
        {
            dt = utility.Display("Exec Proc_CMS'GET_BY_ID',2");
            if (dt.Rows.Count > 0)
            {
                ltrlCareerContent.Text = dt.Rows[0]["CMS_DESC"].ToString();
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;

        }


    }

}