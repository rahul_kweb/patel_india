﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class location : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //BindPatelRoadWays();
            BindPatelAirFright();
            BindRegisterOffice();
        }
    }

    //protected void BindPatelRoadWays()
    //{
    //    int CategoryId = 1;
    //    DataTable dt = new DataTable();
    //    dt = utility.Display("Exec Proc_Location 'FRONT_GET_ZONE_BY_CAT',0,'','','','','','','','','','" + CategoryId + "'");

    //    StringBuilder sb = new StringBuilder();
    //    StringBuilder sbContent = new StringBuilder();

    //    if (dt.Rows.Count > 0)
    //    {
    //        sb.Append("<ul class=\"resp-tabs-list\">");

    //        sbContent.Append("<div class=\"resp-tabs-container lodehide\" id=\"abovediv\">");
    //        foreach (DataRow dr in dt.Rows)
    //        {
    //            sb.Append("<li><a class=\"abovediv\">" + dr["Zone"].ToString() + "</a></li>");


    //            DataTable dtContent = new DataTable();
    //            dtContent = utility.Display("Exec Proc_Location 'FRONT_GET_DATA_BY_CAT_ZONE',0,'','','','','','','','','','" + CategoryId + "','" + dr["ZoneId"].ToString() + "'");

    //            sbContent.Append("<div>");
    //            foreach (DataRow drContent in dtContent.Rows)
    //            {
    //                sbContent.Append("<div class=\"col-md-4 col-sm-6 col-xs-12 office_address_txt equate\">");
    //                sbContent.Append("<span class=\"bl_clr uppercase\"><i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i>" + drContent["City"].ToString() + "</span>");

    //                if (drContent["Branch_Location_Code"].ToString().Trim() != string.Empty && drContent["Branch_Location_Code"].ToString().Trim() != "-")
    //                {
    //                    sbContent.Append("<p><span class=\"bl_clr\">Branch Code :</span>" + drContent["Branch_Location_Code"].ToString().Trim() + "</p>");
    //                }

    //                if (drContent["BranchType"].ToString().Trim()!= string.Empty && drContent["BranchType"].ToString().Trim()!= "-")
    //                {
    //                    sbContent.Append("<p><span class=\"bl_clr\">Branch Type : </span>" + drContent["BranchType"].ToString().Trim() + "</p>");
    //                }

    //                if (drContent["Ownership"].ToString().Trim()!= string.Empty && drContent["Ownership"].ToString().Trim()!= "-")
    //                {
    //                    sbContent.Append("<p><span class=\"bl_clr\">Ownership : </span>" + drContent["Ownership"].ToString().Trim() + "</p>");
    //                }

    //                if (drContent["ManagerName"].ToString().Trim()!= string.Empty && drContent["ManagerName"].ToString().Trim()!= "-")
    //                {
    //                    sbContent.Append("<p><span class=\"bl_clr\">Contact Person : </span>" + drContent["ManagerName"].ToString().Trim() + " </p>");
    //                }

    //                sbContent.Append("<p><span class=\"bl_clr\">Add : </span> " + drContent["Address"].ToString().Trim() + "</p>");

    //                if (drContent["Telephone"].ToString().Trim()!= string.Empty && drContent["Telephone"].ToString().Trim()!= "-")
    //                {
    //                    sbContent.Append("<p><span class=\"bl_clr\">Tel. No. : </span>" + drContent["Telephone"].ToString().Trim() + "</p>");
    //                }

    //                if (drContent["Fax"].ToString().Trim()!= string.Empty && drContent["Fax"].ToString().Trim()!= "-")
    //                {
    //                    sbContent.Append("<p><span class=\"bl_clr\">Fax. No. : </span>" + drContent["Fax"].ToString().Trim() + "</p>");
    //                }

    //                if (drContent["Mobile"].ToString().Trim()!= string.Empty && drContent["Mobile"].ToString().Trim()!= "-")
    //                {
    //                    sbContent.Append("<p><span class=\"bl_clr\">Mob. No. : </span>" + drContent["Mobile"].ToString().Trim() + "</p>");
    //                }

    //                if (drContent["Email"].ToString().Trim() != string.Empty)
    //                {
    //                    //sbContent.Append("<p><span class=\"bl_clr\">Email id : </span>" + drContent["Email"].ToString().Trim() + "</p>");

    //                    //sbContent.Append("<p><span class=\"bl_clr\">Email id : </span> <a href=mailto:'" + drContent["Email"].ToString().Trim() + "'> " + drContent["Email"].ToString().Trim() + "</a> </p>");
    //                    sbContent.Append("<p><span class=\"bl_clr\">Email id : </span> <a href=mailto:'" + drContent["Email"].ToString().Trim() + "'> " + drContent["Email"].ToString().Trim() + "</a> <br> <a href=mailto:'" + drContent["Email2"].ToString().Trim() + "'> " + drContent["Email2"].ToString().Trim() + "</a> <br> <a href=mailto:'" + drContent["Email3"].ToString().Trim() + "'> " + drContent["Email3"].ToString().Trim() + "</a></p>");

    //                }
    //                sbContent.Append("</div>");
    //            }
    //            sbContent.Append("</div>");

    //        }
    //        sb.Append("</ul>");
                        
    //        sbContent.Append("</div>");

    //        sb.Append(sbContent.ToString());
    //    }



    //    ltrPatelRoadWays.Text = sb.ToString();
    //}

    protected void BindPatelAirFright()
    {
        int CategoryId = 2;
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_Location 'FRONT_GET_ZONE_BY_CAT',0,'','','','','','','','','','" + CategoryId + "'");

        StringBuilder sb = new StringBuilder();
        StringBuilder sbContent = new StringBuilder();

        if (dt.Rows.Count > 0)
        {
            sb.Append("<ul class=\"resp-tabs-list\">");

            sbContent.Append("<div class=\"resp-tabs-container kalpesh lodehide\" id=\"belowdiv\">");
            foreach (DataRow dr in dt.Rows)
            {
                sb.Append("<li><a class=\"belowdiv\">" + dr["Zone"].ToString() + "</a></li>"); 
                
                DataTable dtContent = new DataTable();
                dtContent = utility.Display("Exec Proc_Location 'FRONT_GET_DATA_BY_CAT_ZONE',0,'','','','','','','','','','" + CategoryId + "','" + dr["ZoneId"].ToString() + "'");

                sbContent.Append("<div>"); 
                foreach (DataRow drContent in dtContent.Rows)
                {
                   
                    sbContent.Append("<div class=\"col-md-4 col-sm-6 col-xs-12 office_address_txt equate\">");
                    sbContent.Append("<span class=\"bl_clr uppercase\"><i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i>" + drContent["City"].ToString() + "</span>");

                    if (drContent["Branch_Location_Code"].ToString().Trim() != string.Empty && drContent["Branch_Location_Code"].ToString().Trim() != "-")
                    {
                        sbContent.Append("<p><span class=\"bl_clr\">Branch Code :</span>" + drContent["Branch_Location_Code"].ToString().Trim() + "</p>");
                    }

                    if (drContent["BranchType"].ToString().Trim() != string.Empty && drContent["BranchType"].ToString().Trim() != "-")
                    {
                        sbContent.Append("<p><span class=\"bl_clr\">Branch Type : </span>" + drContent["BranchType"].ToString().Trim() + "</p>");
                    }

                    if (drContent["Ownership"].ToString().Trim() != string.Empty && drContent["Ownership"].ToString().Trim() != "-")
                    {
                        sbContent.Append("<p><span class=\"bl_clr\">Ownership : </span>" + drContent["Ownership"].ToString().Trim() + "</p>");
                    }

                    if (drContent["ManagerName"].ToString().Trim() != string.Empty && drContent["ManagerName"].ToString().Trim() != "-")
                    {
                        sbContent.Append("<p><span class=\"bl_clr\">Contact Person : </span>" + drContent["ManagerName"].ToString().Trim() + " </p>");
                    }

                    sbContent.Append("<p><span class=\"bl_clr\">Add : </span> " + drContent["Address"].ToString().Trim() + "</p>");

                    if (drContent["Telephone"].ToString().Trim() != string.Empty && drContent["Telephone"].ToString().Trim() != "-")
                    {
                        sbContent.Append("<p><span class=\"bl_clr\">Tel. No. : </span>" + drContent["Telephone"].ToString().Trim() + "</p>");
                    }

                    if (drContent["Fax"].ToString().Trim() != string.Empty && drContent["Fax"].ToString().Trim() != "-")
                    {
                        sbContent.Append("<p><span class=\"bl_clr\">Fax. No. : </span>" + drContent["Fax"].ToString().Trim() + "</p>");
                    }

                    if (drContent["Mobile"].ToString().Trim() != string.Empty && drContent["Mobile"].ToString().Trim() != "-")
                    {
                        sbContent.Append("<p><span class=\"bl_clr\">Mob. No. : </span>" + drContent["Mobile"].ToString().Trim() + "</p>");
                    }

                    if (drContent["Email"].ToString().Trim() != string.Empty)
                    {
                        //sbContent.Append("<p><span class=\"bl_clr\">Email id : </span>" + drContent["Email"].ToString().Trim() + "</p>");
                        sbContent.Append("<p><span class=\"bl_clr\">Email id : </span> <a href=mailto:'" + drContent["Email"].ToString().Trim() + "'> " + drContent["Email"].ToString().Trim() + "</a> <br> <a href=mailto:'" + drContent["Email2"].ToString().Trim() + "'> " + drContent["Email2"].ToString().Trim() + "</a> <br> <a href=mailto:'" + drContent["Email3"].ToString().Trim() + "'> " + drContent["Email3"].ToString().Trim() + "</a></p>");


                    }

                    sbContent.Append("</div>");
                }
                sbContent.Append("</div>");

            }
            sb.Append("</ul>");

            sbContent.Append("</div>");

            sb.Append(sbContent.ToString());
        }

        ltrPatelAirfrieght.Text = sb.ToString();
    }

    //[WebMethod]
    //public static string SearchPatel(string Value)
    //{
    //    Utility utility = new Utility();
    //    int CategoryId = 1;
    //    DataTable dt = new DataTable();
    //    dt = utility.Display("Exec Proc_Location 'FRONT_SEARCH_LOCATION_GET_ZONE_BY_CAT',0,'','','','','','','','','','" + CategoryId + "',0,'" + Value + "'");

    //    StringBuilder sb = new StringBuilder();
    //    StringBuilder sbContent = new StringBuilder();

    //    if (dt.Rows.Count > 0)
    //    {
    //        sb.Append("<ul class=\"resp-tabs-list\">");

    //        sbContent.Append("<div class=\"resp-tabs-container lodehide\" id=\"abovediv\">");
    //        foreach (DataRow dr in dt.Rows)
    //        {
    //            sb.Append("<li><a class=\"abovediv\">" + dr["Zone"].ToString() + "</a></li>");


    //            DataTable dtContent = new DataTable();
    //            dtContent = utility.Display("Exec Proc_Location 'FRONT_SEARCH_LOCATION',0,'','','','','','','','','','" + CategoryId + "','" + dr["ZoneId"].ToString() + "','" + Value + "'");

    //            sbContent.Append("<div>");
    //            foreach (DataRow drContent in dtContent.Rows)
    //            {
    //                sbContent.Append("<div class=\"col-md-4 col-sm-6 col-xs-12 office_address_txt equate\">");
    //                sbContent.Append("<span class=\"bl_clr uppercase\"><i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i>" + drContent["City"].ToString() + "</span>");

    //                if (drContent["Branch_Location_Code"].ToString().Trim() != string.Empty && drContent["Branch_Location_Code"].ToString().Trim() != "-")
    //                {
    //                    sbContent.Append("<p><span class=\"bl_clr\">Branch Code :</span>" + drContent["Branch_Location_Code"].ToString().Trim() + "</p>");
    //                }

    //                if (drContent["BranchType"].ToString().Trim() != string.Empty && drContent["BranchType"].ToString().Trim() != "-")
    //                {
    //                    sbContent.Append("<p><span class=\"bl_clr\">Branch Type : </span>" + drContent["BranchType"].ToString().Trim() + "</p>");
    //                }

    //                if (drContent["Ownership"].ToString().Trim() != string.Empty && drContent["Ownership"].ToString().Trim() != "-")
    //                {
    //                    sbContent.Append("<p><span class=\"bl_clr\">Ownership : </span>" + drContent["Ownership"].ToString().Trim() + "</p>");
    //                }

    //                if (drContent["ManagerName"].ToString().Trim() != string.Empty && drContent["ManagerName"].ToString().Trim() != "-")
    //                {
    //                    sbContent.Append("<p><span class=\"bl_clr\">Contact Person : </span>" + drContent["ManagerName"].ToString().Trim() + " </p>");
    //                }

    //                sbContent.Append("<p><span class=\"bl_clr\">Add : </span> " + drContent["Address"].ToString().Trim() + "</p>");

    //                if (drContent["Telephone"].ToString().Trim() != string.Empty && drContent["Telephone"].ToString().Trim() != "-")
    //                {
    //                    sbContent.Append("<p><span class=\"bl_clr\">Tel. No. : </span>" + drContent["Telephone"].ToString().Trim() + "</p>");
    //                }

    //                if (drContent["Fax"].ToString().Trim() != string.Empty && drContent["Fax"].ToString().Trim() != "-")
    //                {
    //                    sbContent.Append("<p><span class=\"bl_clr\">Fax. No. : </span>" + drContent["Fax"].ToString().Trim() + "</p>");
    //                }

    //                if (drContent["Mobile"].ToString().Trim() != string.Empty && drContent["Mobile"].ToString().Trim() != "-")
    //                {
    //                    sbContent.Append("<p><span class=\"bl_clr\">Mob. No. : </span>" + drContent["Mobile"].ToString().Trim() + "</p>");
    //                }

    //                if (drContent["Email"].ToString().Trim() != string.Empty)
    //                {
    //                    //sbContent.Append("<p><span class=\"bl_clr\">Email id : </span>" + drContent["Email"].ToString().Trim() + "</p>");
    //                    sbContent.Append("<p><span class=\"bl_clr\">Email id : </span> <a href=mailto:'" + drContent["Email"].ToString().Trim() + "'> " + drContent["Email"].ToString().Trim() + "</a> <br> <a href=mailto:'" + drContent["Email2"].ToString().Trim() + "'> " + drContent["Email2"].ToString().Trim() + "</a> <br> <a href=mailto:'" + drContent["Email3"].ToString().Trim() + "'> " + drContent["Email3"].ToString().Trim() + "</a></p>");


    //                }
    //                sbContent.Append("</div>");
    //            }
    //            sbContent.Append("</div>");

    //        }
    //        sb.Append("</ul>");

    //        sbContent.Append("</div>");

    //        sb.Append(sbContent.ToString());
    //    }
    //    else
    //    {
    //        sb.Append("<h4 style=\"text-align:center;color:red\">Result not found.</h4>"); 
    //    }
    //    //ltrPatelRoadWays.Text = sb.ToString();
    //    return sb.ToString();
    //}

    [WebMethod]
    public static string SearchPatelAirfried(string Value)
    {
        Utility utility = new Utility();
        int CategoryId = 2;
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_Location 'FRONT_SEARCH_LOCATION_GET_ZONE_BY_CAT',0,'','','','','','','','','','" + CategoryId + "',0,'" + Value + "'");

        StringBuilder sb = new StringBuilder();
        StringBuilder sbContent = new StringBuilder();

        if (dt.Rows.Count > 0)
        {
            sb.Append("<ul class=\"resp-tabs-list\">");

            sbContent.Append("<div class=\"resp-tabs-container kalpesh lodehide\" id=\"belowdiv\">");
            foreach (DataRow dr in dt.Rows)
            {
                sb.Append("<li><a class=\"belowdiv\">" + dr["Zone"].ToString() + "</a></li>");


                DataTable dtContent = new DataTable();
                dtContent = utility.Display("Exec Proc_Location 'FRONT_SEARCH_LOCATION',0,'','','','','','','','','','" + CategoryId + "','" + dr["ZoneId"].ToString() + "','" + Value + "'");

                sbContent.Append("<div>");
                foreach (DataRow drContent in dtContent.Rows)
                {
                    sbContent.Append("<div class=\"col-md-4 col-sm-6 col-xs-12 office_address_txt equate\">");
                    sbContent.Append("<span class=\"bl_clr uppercase\"><i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i>" + drContent["City"].ToString() + "</span>");

                    if (drContent["Branch_Location_Code"].ToString().Trim() != string.Empty && drContent["Branch_Location_Code"].ToString().Trim() != "-")
                    {
                        sbContent.Append("<p><span class=\"bl_clr\">Branch Code :</span>" + drContent["Branch_Location_Code"].ToString().Trim() + "</p>");
                    }

                    if (drContent["BranchType"].ToString().Trim() != string.Empty && drContent["BranchType"].ToString().Trim() != "-")
                    {
                        sbContent.Append("<p><span class=\"bl_clr\">Branch Type : </span>" + drContent["BranchType"].ToString().Trim() + "</p>");
                    }

                    if (drContent["Ownership"].ToString().Trim() != string.Empty && drContent["Ownership"].ToString().Trim() != "-")
                    {
                        sbContent.Append("<p><span class=\"bl_clr\">Ownership : </span>" + drContent["Ownership"].ToString().Trim() + "</p>");
                    }

                    if (drContent["ManagerName"].ToString().Trim() != string.Empty && drContent["ManagerName"].ToString().Trim() != "-")
                    {
                        sbContent.Append("<p><span class=\"bl_clr\">Contact Person : </span>" + drContent["ManagerName"].ToString().Trim() + " </p>");
                    }

                    sbContent.Append("<p><span class=\"bl_clr\">Add : </span> " + drContent["Address"].ToString().Trim() + "</p>");

                    if (drContent["Telephone"].ToString().Trim() != string.Empty && drContent["Telephone"].ToString().Trim() != "-")
                    {
                        sbContent.Append("<p><span class=\"bl_clr\">Tel. No. : </span>" + drContent["Telephone"].ToString().Trim() + "</p>");
                    }

                    if (drContent["Fax"].ToString().Trim() != string.Empty && drContent["Fax"].ToString().Trim() != "-")
                    {
                        sbContent.Append("<p><span class=\"bl_clr\">Fax. No. : </span>" + drContent["Fax"].ToString().Trim() + "</p>");
                    }

                    if (drContent["Mobile"].ToString().Trim() != string.Empty && drContent["Mobile"].ToString().Trim() != "-")
                    {
                        sbContent.Append("<p><span class=\"bl_clr\">Mob. No. : </span>" + drContent["Mobile"].ToString().Trim() + "</p>");
                    }

                    if (drContent["Email"].ToString().Trim() != string.Empty )
                    {
                        ////sbContent.Append("<p><span class=\"bl_clr\">Email id : </span>" + drContent["Email"].ToString().Trim() + "</p>");
                        sbContent.Append("<p><span class=\"bl_clr\">Email id : </span> <a href=mailto:'" + drContent["Email"].ToString().Trim() + "'> " + drContent["Email"].ToString().Trim() + "</a> <br> <a href=mailto:'" + drContent["Email2"].ToString().Trim() + "'> " + drContent["Email2"].ToString().Trim() + "</a> <br> <a href=mailto:'" + drContent["Email3"].ToString().Trim() + "'> " + drContent["Email3"].ToString().Trim() + "</a></p>");


                    }
                    sbContent.Append("</div>");
                }
                sbContent.Append("</div>");

            }
            sb.Append("</ul>");

            sbContent.Append("</div>");

            sb.Append(sbContent.ToString());
        }
        else
        {
            sb.Append("<h4 style=\"text-align:center;color:red\">Result not found.</h4>");
        }
        //ltrPatelRoadWays.Text = sb.ToString();
        return sb.ToString();
    }

    public void BindRegisterOffice()
    {
        DataTable dt = new DataTable();
     
        try
        {
              dt = utility.Display("Exec Proc_CMS 'GET_BY_ID',9");
            if (dt.Rows.Count > 0)
            {
                ltrlRegOffice.Text = dt.Rows[0]["CMS_DESC"].ToString();
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

}