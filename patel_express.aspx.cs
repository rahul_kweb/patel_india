﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class patel_express : System.Web.UI.Page
{
    Utility utility = new Utility();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bindcontent();
            BindInnerBanner();
        }
    }

    public void Bindcontent()
    {
        try
        {
            dt = utility.Display("EXEC Proc_OurServices 'GET_BY_ID','2'");
            if (dt.Rows.Count > 0)
            {
                ltrlcontent.Text = dt.Rows[0]["OurServicesDescription"].ToString();
                imagereview.Src = string.Format("~/uploads/OurServicesLogo/{0}", dt.Rows[0]["OurServicesLogo"].ToString());
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    public void BindInnerBanner()
    {
        dt = utility.Display("Exec Proc_InnerBanner'GET_BY_ID',3");

        if (dt.Rows.Count > 0)
        {


            StringBuilder strbuild = new StringBuilder();
            strbuild.Append("<div class=\"row inner_banner_wrp\" style=\"background:url(uploads/Images/" + dt.Rows[0]["InnerBannerImage"].ToString() + ") no-repeat center; background-size:cover;\">");
            strbuild.Append("<div class=\"inner_page_title\">");
            strbuild.Append("<div class=\"inner_page_title_txt text-center\">");
            strbuild.Append("<h1>Patel Express</h1>");
            strbuild.Append("<ul class=\"breadcrumb\">");
            strbuild.Append("<li><a href=\"index.aspx\">Home</a></li>");
            strbuild.Append("<li>Our Services</li>");
            strbuild.Append("</ul>");
            strbuild.Append("</div>");
            strbuild.Append("</div>");
            strbuild.Append("</div>");

            ltrlBanner.Text = strbuild.ToString();
        }
    }
}