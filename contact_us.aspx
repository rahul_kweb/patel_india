﻿<%@ Page Title="Patel Integrated Logistics Ltd – Top Logistics Services Provider | India & International" Language="C#" MasterPageFile="~/HouseOfPatel.master" AutoEventWireup="true" CodeFile="contact_us.aspx.cs" Inherits="contact_us" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="css/bootstrap-select.min.css">
    <style>
        .Error {
            border-color: blue;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <uc1:MyMessageBox ID="MyMessageBox2" runat="server" />
    <link rel="shortcut icon" href="images/fav_icon.png">


    <section>



        <div class="container-fluid">

            <div class="row inner_banner_wrp map_wrp"></div>

            <div class="row contact_wrp wrapper">

                <asp:Literal ID="ltrlContactUs" runat="server"></asp:Literal>



                <div class="col-md-8 col-sm-7 col-xs-12 contact_right">

                    <div class="inner_title">
                        <h1>Get in Touch</h1>
                    </div>

                    <form class="row" runat="server">


                        <div class="col-xs-12 col-sm-6 form-group">
                            <%--                            <input type="text" placeholder="First Name" class="form-control fade_anim" />--%>
                            <asp:TextBox ID="txtFirstName" CssClass="form-control fade_anim" placeholder="First Name" ValidationGroup="Contact" runat="server"></asp:TextBox>

                        </div>

                        <div class="col-xs-12 col-sm-6 form-group">
                            <%--                            <input type="text" placeholder="Last Name" class="form-control fade_anim" />--%>
                            <asp:TextBox ID="txtLastName" CssClass="form-control fade_anim" placeholder="Last Name" ValidationGroup="Contact" runat="server"></asp:TextBox>
                        </div>


                        <div class="col-xs-12 col-sm-6 form-group">
                            <%--                            <input type="text" placeholder="Job Title" class="form-control fade_anim" />--%>
                            <asp:TextBox ID="txtJobTitle" class="form-control fade_anim" placeholder="Job Title" ValidationGroup="Contact" runat="server"></asp:TextBox>
                        </div>

                        <div class="col-xs-12 col-sm-6 form-group">
                            <%--  <input type="text" placeholder="Company Name" class="form-control fade_anim" />--%>
                            <asp:TextBox ID="txtComapanyName" CssClass="form-control fade_anim" placeholder="Company Name" ValidationGroup="Contact" runat="server"></asp:TextBox>

                        </div>

                        <div class="col-xs-12 col-sm-6 form-group">
                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker form-control no_padding select_dropdown_height" ValidationGroup="Contact">
                                <asp:ListItem Value="0">Select City</asp:ListItem>
                                <asp:ListItem value="AGARTALA">AGARTALA</asp:ListItem>
                                <asp:ListItem value="AGRA">AGRA</asp:ListItem>
                                <asp:ListItem value="AHEMADNAGAR">AHMEDNAGAR</asp:ListItem>
                                <asp:ListItem value="AHMEDABAD">AHMEDABAD</asp:ListItem>
                                <asp:ListItem value="AIZAWL">AIZAWL</asp:ListItem>
                                <asp:ListItem value="AIZWAL">AIZWAL</asp:ListItem>
                                <asp:ListItem value="AJMER">AJMER</asp:ListItem>
                                <asp:ListItem value="AKOLA">AKOLA</asp:ListItem>
                                <asp:ListItem value="ALIGARH">ALIGARH</asp:ListItem>
                                <asp:ListItem value="ALIPURDUAR">ALIPURDUAR</asp:ListItem>
                                <asp:ListItem value="ALLAHABAD">ALLAHABAD</asp:ListItem>
                                <asp:ListItem value="ALLEPPEY">ALLEPPEY</asp:ListItem>
                                <asp:ListItem value="ALWAR">ALWAR</asp:ListItem>
                                <asp:ListItem value="ALWAYE">ALWAYE</asp:ListItem>
                                <asp:ListItem value="AMARAVATI">AMARAVATI</asp:ListItem>
                                <asp:ListItem value="AMB">AMB</asp:ListItem>
                                <asp:ListItem value="AMBALA">AMBALA</asp:ListItem>
                                <asp:ListItem value="AMBERNATH">AMBERNATH</asp:ListItem>
                                <asp:ListItem value="AMBIKAPUR">AMBIKAPUR</asp:ListItem>
                                <asp:ListItem value="AMBUR">AMBUR</asp:ListItem>
                                <asp:ListItem value="AMDW">AMDW</asp:ListItem>
                                <asp:ListItem value="AMERALI">AMERALI</asp:ListItem>
                                <asp:ListItem value="AMRITSAR">AMRITSAR</asp:ListItem>
                                <asp:ListItem value="ANANTAPUR">ANANTAPUR</asp:ListItem>
                                <asp:ListItem value="ANANTPR">ANANTPR</asp:ListItem>
                                <asp:ListItem value="ANDHERI">ANDHERI</asp:ListItem>
                                <asp:ListItem value="ANGUL">ANGUL</asp:ListItem>
                                <asp:ListItem value="ANKLESHWAR">ANKLESHWAR</asp:ListItem>
                                <asp:ListItem value="ANKLESWAR">ANKLESWAR</asp:ListItem>
                                <asp:ListItem value="ANNUR">ANNUR</asp:ListItem>
                                <asp:ListItem value="ARAH">ARAH</asp:ListItem>
                                <asp:ListItem value="ARANI">ARANI</asp:ListItem>
                                <asp:ListItem value="ASANSOL">ASANSOL</asp:ListItem>
                                <asp:ListItem value="AURANGABAD">AURANGABAD</asp:ListItem>
                                <asp:ListItem value="AVINASHI">AVINASHI</asp:ListItem>
                                <asp:ListItem value="AZAMGARH">AZAMGARH</asp:ListItem>
                                <asp:ListItem value="BADDI">BADDI</asp:ListItem>
                                <asp:ListItem value="BALASORE">BALASORE</asp:ListItem>
                                <asp:ListItem value="BALLABHGARH">BALLABHGARH</asp:ListItem>
                                <asp:ListItem value="BANGALORE">BANGALORE</asp:ListItem>
                                <asp:ListItem value="BANGALORE RURAL">BANGALORE RURAL</asp:ListItem>
                                <asp:ListItem value="BANGKOK">BANGKOK</asp:ListItem>
                                <asp:ListItem value="BANGLORE">BANGLORE</asp:ListItem>
                                <asp:ListItem value="BANKURA">BANKURA</asp:ListItem>
                                <asp:ListItem value="BANSWARA">BANSWARA</asp:ListItem>
                                <asp:ListItem value="BARAMATI">BARAMATI</asp:ListItem>
                                <asp:ListItem value="BAREILLY">BAREILLY</asp:ListItem>
                                <asp:ListItem value="BARGARH">BARGARH</asp:ListItem>
                                <asp:ListItem value="BARIPADA">BARIPADA</asp:ListItem>
                                <asp:ListItem value="BARMER">BARMER</asp:ListItem>
                                <asp:ListItem value="BARNALA">BARNALA</asp:ListItem>
                                <asp:ListItem value="BARODA">BARODA</asp:ListItem>
                                <asp:ListItem value="BASTI">BASTI</asp:ListItem>
                                <asp:ListItem value="BATALA">BATALA</asp:ListItem>
                                <asp:ListItem value="BATHINDA">BATHINDA</asp:ListItem>
                                <asp:ListItem value="BAWAL">BAWAL</asp:ListItem>
                                <asp:ListItem value="BEED">BEED</asp:ListItem>
                                <asp:ListItem value="BEGUSARAI">BEGUSARAI</asp:ListItem>
                                <asp:ListItem value="BEHRAGORA">BEHRAGORA</asp:ListItem>
                                <asp:ListItem value="BEIJING">BEIJING</asp:ListItem>
                                <asp:ListItem value="BELGAUM">BELGAUM</asp:ListItem>
                                <asp:ListItem value="BELLARY">BELLARY</asp:ListItem>
                                <asp:ListItem value="BENAPOLE">BENAPOLE</asp:ListItem>
                                <asp:ListItem value="BERHAMPUR">BERHAMPUR</asp:ListItem>
                                <asp:ListItem value="BETUL, BHOPAL">BETUL, BHOPAL</asp:ListItem>
                                <asp:ListItem value="BHAGAHALPUR">BHAGAHALPUR</asp:ListItem>
                                <asp:ListItem value="BHAGALKOT">BHAGALKOT</asp:ListItem>
                                <asp:ListItem value="BHAGWANPUR">BHAGWANPUR</asp:ListItem>
                                <asp:ListItem value="BHANDARA">BHANDARA</asp:ListItem>
                                <asp:ListItem value="BHARATPUR">BHARATPUR</asp:ListItem>
                                <asp:ListItem value="BHARUCH">BHARUCH</asp:ListItem>
                                <asp:ListItem value="BHATINDA">BHATINDA</asp:ListItem>
                                <asp:ListItem value="BHATKAL">BHATKAL</asp:ListItem>
                                <asp:ListItem value="BHAVNAGAR">BHAVNAGAR</asp:ListItem>
                                <asp:ListItem value="BHIALI">BHIALI</asp:ListItem>
                                <asp:ListItem value="BHILAI">BHILAI</asp:ListItem>
                                <asp:ListItem value="BHILWARA">BHILWARA</asp:ListItem>
                                <asp:ListItem value="BHIWANDI">BHIWANDI</asp:ListItem>
                                <asp:ListItem value="BHOPAL">BHOPAL</asp:ListItem>
                                <asp:ListItem value="BHUBANESHWAR">BHUBANESHWAR</asp:ListItem>
                                <asp:ListItem value="BHUJ-KUTCH">BHUJ-KUTCH</asp:ListItem>
                                <asp:ListItem value="BIDADI HOBALI">BIDADI HOBALI</asp:ListItem>
                                <asp:ListItem value="BIHARSARIF">BIHARSARIF</asp:ListItem>
                                <asp:ListItem value="BIJAPUR">BIJAPUR</asp:ListItem>
                                <asp:ListItem value="BIJAY NAGAR">BIJAY NAGAR</asp:ListItem>
                                <asp:ListItem value="BIKANER">BIKANER</asp:ListItem>
                                <asp:ListItem value="BILASPUR">BILASPUR</asp:ListItem>
                                <asp:ListItem value="BILASPURE">BILASPURE</asp:ListItem>
                                <asp:ListItem value="BIRATNAGAR">BIRATNAGAR</asp:ListItem>
                                <asp:ListItem value="BIRGUNJ">BIRGUNJ</asp:ListItem>
                                <asp:ListItem value="BMC">BMC</asp:ListItem>
                                <asp:ListItem value="BOKARO">BOKARO</asp:ListItem>
                                <asp:ListItem value="BOLLARAM">BOLLARAM</asp:ListItem>
                                <asp:ListItem value="BOLPUR">BOLPUR</asp:ListItem>
                                <asp:ListItem value="BONGAIGAON">BONGAIGAON</asp:ListItem>
                                <asp:ListItem value="BORIVALI">BORIVALI</asp:ListItem>
                                <asp:ListItem value="BUNDI">BUNDI</asp:ListItem>
                                <asp:ListItem value="BURDWAN">BURDWAN</asp:ListItem>
                                <asp:ListItem value="BURHANPUR">BURHANPUR</asp:ListItem>
                                <asp:ListItem value="BUTIBORI">BUTIBORI</asp:ListItem>
                                <asp:ListItem value="BUTTIBORI">BUTTIBORI</asp:ListItem>
                                <asp:ListItem value="CHAKKAN">CHAKKAN</asp:ListItem>
                                <asp:ListItem value="CHAMPA">CHAMPA</asp:ListItem>
                                <asp:ListItem value="CHANDHIGARH">CHANDHIGARH</asp:ListItem>
                                <asp:ListItem value="CHANDIGARH">CHANDIGARH</asp:ListItem>
                                <asp:ListItem value="CHANDRAPUR">CHANDRAPUR</asp:ListItem>
                                <asp:ListItem value="CHAPRA">CHAPRA</asp:ListItem>
                                <asp:ListItem value="CHATRAL">CHATRAL</asp:ListItem>
                                <asp:ListItem value="CHEMBUR">CHEMBUR</asp:ListItem>
                                <asp:ListItem value="CHENGALPATTU">CHENGALPATTU</asp:ListItem>
                                <asp:ListItem value="CHENNAI">CHENNAI</asp:ListItem>
                                <asp:ListItem value="CHHATARPUR">CHHATARPUR</asp:ListItem>
                                <asp:ListItem value="CHIKMANGALUR">CHIKMANGALUR</asp:ListItem>
                                <asp:ListItem value="CHILUKALURIPET">CHILAKALURIPET</asp:ListItem>
                                <asp:ListItem value="CHINDWARA">CHINDWARA</asp:ListItem>
                                <asp:ListItem value="CHIPLUN">CHIPLUN</asp:ListItem>
                                <asp:ListItem value="CHIRALA">CHIRALA</asp:ListItem>
                                <asp:ListItem value="CHIRLE">CHIRLE</asp:ListItem>
                                <asp:ListItem value="CHITTOOR">CHITTOOR</asp:ListItem>
                                <asp:ListItem value="CHITTOR">CHITTOR</asp:ListItem>
                                <asp:ListItem value="CHITTORGARH">CHITTORGARH</asp:ListItem>
                                <asp:ListItem value="CHURU">CHURU</asp:ListItem>
                                <asp:ListItem value="CLOSED">CLOSED</asp:ListItem>
                                <asp:ListItem value="COCHIN-ALWAYE">COCHIN-ALWAYE</asp:ListItem>
                                <asp:ListItem value="COIMBATORE">COIMBATORE</asp:ListItem>
                                <asp:ListItem value="COLOMBO">COLOMBO</asp:ListItem>
                                <asp:ListItem value="COMBTORE">COMBTORE</asp:ListItem>
                                <asp:ListItem value="CONOOR">CONOOR</asp:ListItem>
                                <asp:ListItem value="COOCHBIHAR">COOCHBIHAR</asp:ListItem>
                                <asp:ListItem value="CUDDALORE">CUDDALORE</asp:ListItem>
                                <asp:ListItem value="CUDDAPAH">CUDDAPAH</asp:ListItem>
                                <asp:ListItem value="CUTTACK">CUTTACK</asp:ListItem>
                                <asp:ListItem value="DALTANGANJ">DALTANGANJ</asp:ListItem>
                                <asp:ListItem value="DAMAN">DAMAN</asp:ListItem>
                                <asp:ListItem value="DAMOH">DAMOH</asp:ListItem>
                                <asp:ListItem value="DANAPUR">DANAPUR</asp:ListItem>
                                <asp:ListItem value="DARJEELING">DARJEELING</asp:ListItem>
                                <asp:ListItem value="DAUND PUNE">DAUND PUNE</asp:ListItem>
                                <asp:ListItem value="DAUSA">DAUSA</asp:ListItem>
                                <asp:ListItem value="DAVANGERE">DAVANGERE</asp:ListItem>
                                <asp:ListItem value="DEHRADUN">DEHRADUN</asp:ListItem>
                                <asp:ListItem value="DELHI">DELHI</asp:ListItem>
                                <asp:ListItem value="DEOGHAR">DEOGHAR</asp:ListItem>
                                <asp:ListItem value="DEORIA">DEORIA</asp:ListItem>
                                <asp:ListItem value="DERABASSI">DERABASSI</asp:ListItem>
                                <asp:ListItem value="DEWAS">DEWAS</asp:ListItem>
                                <asp:ListItem value="DHANBAD">DHANBAD</asp:ListItem>
                                <asp:ListItem value="DHARAMSHALA">DHARAMSHALA</asp:ListItem>
                                <asp:ListItem value="DHARBHANGA">DHARBHANGA</asp:ListItem>
                                <asp:ListItem value="DHARUHERA">DHARUHERA</asp:ListItem>
                                <asp:ListItem value="DHARWAD">DHARWAD</asp:ListItem>
                                <asp:ListItem value="DHENKAMAL">DHENKAMAL</asp:ListItem>
                                <asp:ListItem value="DHULIA">DHULIA</asp:ListItem>
                                <asp:ListItem value="DIBRUGARH">DIBRUGARH</asp:ListItem>
                                <asp:ListItem value="DIMAPUR">DIMAPUR</asp:ListItem>
                                <asp:ListItem value="DINDIGUL">DINDIGUL</asp:ListItem>
                                <asp:ListItem value="DOBBASPET">DOBBASPET</asp:ListItem>
                                <asp:ListItem value="DOMBEVELLI">DOMBEVELLI</asp:ListItem>
                                <asp:ListItem value="DUBAI">DUBAI</asp:ListItem>
                                <asp:ListItem value="DUMKA">DUMKA</asp:ListItem>
                                <asp:ListItem value="DUNGARPUR">DUNGARPUR</asp:ListItem>
                                <asp:ListItem value="DURGAPUR">DURGAPUR</asp:ListItem>
                                <asp:ListItem value="EAST SINGHBHUM">EAST SINGHBHUM</asp:ListItem>
                                <asp:ListItem value="ELURU">ELURU</asp:ListItem>
                                <asp:ListItem value="ERNAKULAM">ERNAKULAM</asp:ListItem>
                                <asp:ListItem value="ERODE">ERODE</asp:ListItem>
                                <asp:ListItem value="ETAWAH">ETAWAH</asp:ListItem>
                                <asp:ListItem value="FAIZABAD">FAIZABAD</asp:ListItem>
                                <asp:ListItem value="FALTA">FALTA</asp:ListItem>
                                <asp:ListItem value="FARIDABAD">FARIDABAD</asp:ListItem>
                                <asp:ListItem value="FATEHGARH SAHIB">FATEHGARH SAHIB</asp:ListItem>
                                <asp:ListItem value="FEROZPUR">FEROZPUR</asp:ListItem>
                                <asp:ListItem value="FIROZABAD">FIROZABAD</asp:ListItem>
                                <asp:ListItem value="GAJRAULA">GAJRAULA</asp:ListItem>
                                <asp:ListItem value="GANDHI NAGAR">GANDHI NAGAR</asp:ListItem>
                                <asp:ListItem value="GANDHIDHAM">GANDHIDHAM</asp:ListItem>
                                <asp:ListItem value="GANDHINAGAR">GANDHINAGAR</asp:ListItem>
                                <asp:ListItem value="GANGTOK">GANGTOK</asp:ListItem>
                                <asp:ListItem value="GAYA">GAYA</asp:ListItem>
                                <asp:ListItem value="GHARONDA">GHARONDA</asp:ListItem>
                                <asp:ListItem value="GHAZIABAD">GHAZIABAD</asp:ListItem>
                                <asp:ListItem value="GHUMARWIN">GHUMARWIN</asp:ListItem>
                                <asp:ListItem value="GIRIDIH">GIRIDIH</asp:ListItem>
                                <asp:ListItem value="GOA">GOA</asp:ListItem>
                                <asp:ListItem value="GODHRA">GODHRA</asp:ListItem>
                                <asp:ListItem value="GOI">GOI</asp:ListItem>
                                <asp:ListItem value="GONDA">GONDA</asp:ListItem>
                                <asp:ListItem value="GOPALGUNJ">GOPALGUNJ</asp:ListItem>
                                <asp:ListItem value="GOPALPUR">GOPALPUR</asp:ListItem>
                                <asp:ListItem value="GORAKHPUR">GORAKHPUR</asp:ListItem>
                                <asp:ListItem value="GREATER NOIDA">GREATER NOIDA</asp:ListItem>
                                <asp:ListItem value="GULBARGA">GULBARGA</asp:ListItem>
                                <asp:ListItem value="GUMMUDIPUNDI">GUMMUDIPUNDI</asp:ListItem>
                                <asp:ListItem value="GUNA">GUNA</asp:ListItem>
                                <asp:ListItem value="GUNTAKAL">GUNTAKAL</asp:ListItem>
                                <asp:ListItem value="GUNTUR">GUNTUR</asp:ListItem>
                                <asp:ListItem value="GURGAON">GURGAON</asp:ListItem>
                                <asp:ListItem value="GUWAHATI">GUWAHATI</asp:ListItem>
                                <asp:ListItem value="GWALIOR">GWALIOR</asp:ListItem>
                                <asp:ListItem value="HALDAWANI">HALDAWANI</asp:ListItem>
                                <asp:ListItem value="HALDIA">HALDIA</asp:ListItem>
                                <asp:ListItem value="HALDWANI">HALDWANI</asp:ListItem>
                                <asp:ListItem value="HALOL">HALOL</asp:ListItem>
                                <asp:ListItem value="HANGZHOU">HANGZHOU</asp:ListItem>
                                <asp:ListItem value="HARIDWAR">HARIDWAR</asp:ListItem>
                                <asp:ListItem value="HARIHAR">HARIHAR</asp:ListItem>
                                <asp:ListItem value="HARPUR">HARPUR</asp:ListItem>
                                <asp:ListItem value="HASIMARA">HASIMARA</asp:ListItem>
                                <asp:ListItem value="HASSAN">HASSAN</asp:ListItem>
                                <asp:ListItem value="HATHRAS">HATHRAS</asp:ListItem>
                                <asp:ListItem value="HAVERI">HAVERI</asp:ListItem>
                                <asp:ListItem value="HAZARIBAGH">HAZARIBAGH</asp:ListItem>
                                <asp:ListItem value="HAZIPUR">HAZIPUR</asp:ListItem>
                                <asp:ListItem value="HIMMATNAGAR">HIMMATNAGAR</asp:ListItem>
                                <asp:ListItem value="HINDUPUR">HINDUPUR</asp:ListItem>
                                <asp:ListItem value="HISAR">HISAR</asp:ListItem>
                                <asp:ListItem value="HONGKONG">HONGKONG</asp:ListItem>
                                <asp:ListItem value="HOOGHLY">HOOGHLY</asp:ListItem>
                                <asp:ListItem value="HOSHIARPUR">HOSHIARPUR</asp:ListItem>
                                <asp:ListItem value="HOSKOTE">HOSKOTE</asp:ListItem>
                                <asp:ListItem value="HOSPET">HOSPET</asp:ListItem>
                                <asp:ListItem value="HOSUR">HOSUR</asp:ListItem>
                                <asp:ListItem value="HOWRAH">HOWRAH</asp:ListItem>
                                <asp:ListItem value="HUBLI">HUBLI</asp:ListItem>
                                <asp:ListItem value="HYDERABAD">Hyderabad</asp:ListItem>
                                <asp:ListItem value="ICHAPURAM">ICHAPURAM</asp:ListItem>
                                <asp:ListItem value="IGATPUR">IGATPUR</asp:ListItem>
                                <asp:ListItem value="IMPHAL">IMPHAL</asp:ListItem>
                                
                                <asp:ListItem value="INDORE">INDORE</asp:ListItem>
                                <asp:ListItem value="INVALID">INVALID</asp:ListItem>
                                <asp:ListItem value="ITANAGAR">ITANAGAR</asp:ListItem>
                                <asp:ListItem value="ITARSI">ITARSI</asp:ListItem>
                                <asp:ListItem value="JABALPUR">JABALPUR</asp:ListItem>
                                <asp:ListItem value="JAGDALPUR">JAGDALPUR</asp:ListItem>
                                <asp:ListItem value="JAGDISHPUR">JAGDISHPUR</asp:ListItem>
                                <asp:ListItem value="JAINPUR">JAINPUR</asp:ListItem>
                                <asp:ListItem value="JAIPUR">JAIPUR</asp:ListItem>
                                <asp:ListItem value="JAJPUR">JAJPUR</asp:ListItem>
                                <asp:ListItem value="JALANDHAR">JALANDHAR</asp:ListItem>
                                <asp:ListItem value="JALGAON">JALGAON</asp:ListItem>
                                <asp:ListItem value="JALNA">JALNA</asp:ListItem>
                                <asp:ListItem value="JALORE">JALORE</asp:ListItem>
                                <asp:ListItem value="JALPAIGURI">JALPAIGURI</asp:ListItem>
                                <asp:ListItem value="JAMMU">JAMMU</asp:ListItem>
                                <asp:ListItem value="JAMMU CITY">JAMMU CITY</asp:ListItem>
                                <asp:ListItem value="JAMNAGAR">JAMNAGAR</asp:ListItem>
                                <asp:ListItem value="JAMSHEDPUR">JAMSHEDPUR</asp:ListItem>
                                <asp:ListItem value="JEJURI">JEJURI</asp:ListItem>
                                <asp:ListItem value="JETPUR">JETPUR</asp:ListItem>
                                <asp:ListItem value="JHANSI">JHANSI</asp:ListItem>
                                <asp:ListItem value="JHARSAGUDA">JHARSAGUDA</asp:ListItem>
                                <asp:ListItem value="JHARSUGUDA">JHARSUGUDA</asp:ListItem>
                                <asp:ListItem value="JHUNJHUNU">JHUNJHUNU</asp:ListItem>
                                <asp:ListItem value="JODHPUR">JODHPUR</asp:ListItem>
                                <asp:ListItem value="JOGBANI">JOGBANI</asp:ListItem>
                                <asp:ListItem value="JORHAT">JORHAT</asp:ListItem>
                                <asp:ListItem value="JUNAGADH">JUNAGADH</asp:ListItem>
                                <asp:ListItem value="KAITHAL">KAITHAL</asp:ListItem>
                                <asp:ListItem value="KAKINADA">KAKINADA</asp:ListItem>
                                <asp:ListItem value="KALA AMB">KALA AMB</asp:ListItem>
                                <asp:ListItem value="KALAMBOLI">KALAMBOLI</asp:ListItem>
                                <asp:ListItem value="KALIMPONG DARJEELING">KALIMPONG DARJEELING</asp:ListItem>
                                <asp:ListItem value="KALYAN">KALYAN</asp:ListItem>
                                <asp:ListItem value="KALYANI">KALYANI</asp:ListItem>
                                <asp:ListItem value="KAMAN">KAMAN</asp:ListItem>
                                <asp:ListItem value="KANCHIPURAM">KANCHIPURAM</asp:ListItem>
                                <asp:ListItem value="KANNAUJ">KANNAUJ</asp:ListItem>
                                <asp:ListItem value="KANNUR">KANNUR</asp:ListItem>
                                <asp:ListItem value="KANPUR">KANPUR</asp:ListItem>
                                <asp:ListItem value="KANYAKUMARI">KANYAKUMARI</asp:ListItem>
                                <asp:ListItem value="KAPURTHALA">KAPURTHALA</asp:ListItem>
                                <asp:ListItem value="KARAD">KARAD</asp:ListItem>
                                <asp:ListItem value="KARAIKAL">KARAIKAL</asp:ListItem>
                                <asp:ListItem value="KARIMNAGAR">KARIMNAGAR</asp:ListItem>
                                <asp:ListItem value="KARNAL">KARNAL</asp:ListItem>
                                <asp:ListItem value="KARUR">KARUR</asp:ListItem>
                                <asp:ListItem value="KARWAR">KARWAR</asp:ListItem>
                                <asp:ListItem value="KASARAGOD">KASARAGOD</asp:ListItem>
                                <asp:ListItem value="KASHIPUR">KASHIPUR</asp:ListItem>
                                <asp:ListItem value="KATHMANDU">KATHMANDU</asp:ListItem>
                                <asp:ListItem value="KATIHAR">KATIHAR</asp:ListItem>
                                <asp:ListItem value="KATMANDU">KATMANDU</asp:ListItem>
                                <asp:ListItem value="KATNI">KATNI</asp:ListItem>
                                <asp:ListItem value="KHAMGAON">KHAMGAON</asp:ListItem>
                                <asp:ListItem value="KHAPOLI">KHAPOLI</asp:ListItem>
                                <asp:ListItem value="KHARAGPUR">KHARAGPUR</asp:ListItem>
                                <asp:ListItem value="KHARGHAR">KHARGHAR</asp:ListItem>
                                <asp:ListItem value="KHURDA">KHURDA</asp:ListItem>
                                <asp:ListItem value="KHURDHA">KHURDHA</asp:ListItem>
                                <asp:ListItem value="KHURJA">KHURJA</asp:ListItem>
                                <asp:ListItem value="KISHANGANJ">KISHANGANJ</asp:ListItem>
                                <asp:ListItem value="KOHIMA">KOHIMA</asp:ListItem>
                                <asp:ListItem value="KOLAHPUR">KOLAHPUR</asp:ListItem>
                                <asp:ListItem value="KOLAR">KOLAR</asp:ListItem>
                                <asp:ListItem value="KOLENCHERI">KOLENCHERI</asp:ListItem>
                                <asp:ListItem value="KOLHAPUR">KOLHAPUR</asp:ListItem>
                                <asp:ListItem value="KOLKATA">KOLKATA</asp:ListItem>
                                <asp:ListItem value="KOLKATTA">KOLKATTA</asp:ListItem>
                                <asp:ListItem value="KOLLAM">KOLLAM</asp:ListItem>
                                <asp:ListItem value="KONDAPUR">KONDAPUR</asp:ListItem>
                                <asp:ListItem value="KORBA">KORBA</asp:ListItem>
                                <asp:ListItem value="KOTA">KOTA</asp:ListItem>
                                <asp:ListItem value="KOTTARAKARA">KOTTARAKARA</asp:ListItem>
                                <asp:ListItem value="KOTTAYAM">KOTTAYAM</asp:ListItem>
                                <asp:ListItem value="KOYILANDI">KOYILANDI</asp:ListItem>
                                <asp:ListItem value="KOZHIKODE">KOZHIKODE</asp:ListItem>
                                <asp:ListItem value="KRISHNA DIST">KRISHNA DIST</asp:ListItem>
                                <asp:ListItem value="KRISHNAGIRI">KRISHNAGIRI</asp:ListItem>
                                <asp:ListItem value="KUALA LUMPUR">KUALA LUMPUR</asp:ListItem>
                                <asp:ListItem value="KUDAL">KUDAL</asp:ListItem>
                                <asp:ListItem value="KUDAS">KUDAS</asp:ListItem>
                                <asp:ListItem value="KULLU">KULLU</asp:ListItem>
                                <asp:ListItem value="KUMBAKONAM">KUMBAKONAM</asp:ListItem>
                                <asp:ListItem value="KURNOOL">KURNOOL</asp:ListItem>
                                <asp:ListItem value="KURUKSHETRA">KURUKSHETRA</asp:ListItem>
                                <asp:ListItem value="KUTCH">KUTCH</asp:ListItem>
                                <asp:ListItem value="LAL TAPPAR">LAL TAPPAR</asp:ListItem>
                                <asp:ListItem value="LATUR">LATUR</asp:ListItem>
                                <asp:ListItem value="LKATTA">LKATTA</asp:ListItem>
                                
                                <asp:ListItem value="LTT MUMBAI">LTT MUMBAI</asp:ListItem>
                                <asp:ListItem value="LUCKNOW">LUCKNOW</asp:ListItem>
                                <asp:ListItem value="LUDHIAN">LUDHIAN</asp:ListItem>
                                <asp:ListItem value="LUDHIANA">LUDHIANA</asp:ListItem>
                                <asp:ListItem value="LUDHINA">LUDHINA</asp:ListItem>
                                <asp:ListItem value="MADHO SINGH">MADHO SINGH</asp:ListItem>
                                <asp:ListItem value="MADHUBANI">MADHUBANI</asp:ListItem>
                                <asp:ListItem value="MADHYAMGRAM">MADHYAMGRAM</asp:ListItem>
                                <asp:ListItem value="MADURAI">MADURAI</asp:ListItem>
                                <asp:ListItem value="MAHABOOB NAGAR">MAHABOOB NAGAR</asp:ListItem>
                                <asp:ListItem value="MAHAD">MAHAD</asp:ListItem>
                                <asp:ListItem value="MAHIM">MAHIM</asp:ListItem>
                                <asp:ListItem value="MAHIPALPUR">MAHIPALPUR</asp:ListItem>
                                <asp:ListItem value="MALDA">MALDA</asp:ListItem>
                                <asp:ListItem value="MALERKOTLA">MALERKOTLA</asp:ListItem>
                                <asp:ListItem value="MANDI">MANDI</asp:ListItem>
                                <asp:ListItem value="MANDI GOVINDGARH">MANDI GOVINDGARH</asp:ListItem>
                                <asp:ListItem value="MANDI GOVINDGARH	">MANDI GOVINDGARH	</asp:ListItem>
                                <asp:ListItem value="MANDIDEEP">MANDIDEEP</asp:ListItem>
                                <asp:ListItem value="MANDVI">MANDVI</asp:ListItem>
                                <asp:ListItem value="MANDYA">MANDYA</asp:ListItem>
                                <asp:ListItem value="MANGALORE">MANGALORE</asp:ListItem>
                                <asp:ListItem value="MANIPAL">MANIPAL</asp:ListItem>
                                <asp:ListItem value="MANKAPUR">MANKAPUR</asp:ListItem>
                                <asp:ListItem value="MARGOA">MARGOA</asp:ListItem>
                                <asp:ListItem value="MARTHANDAM">MARTHANDAM</asp:ListItem>
                                <asp:ListItem value="MATHURA">MATHURA</asp:ListItem>
                                <asp:ListItem value="MAUNATH BAHNJAN">MAUNATH BAHNJAN</asp:ListItem>
                                <asp:ListItem value="MAX GROUP NAGAR">MAX GROUP NAGAR</asp:ListItem>
                                <asp:ListItem value="MEERUT">MEERUT</asp:ListItem>
                                <asp:ListItem value="MEHSANA">MEHSANA</asp:ListItem>
                                <asp:ListItem value="MIRA BHAYANDER">MIRA BHAYANDER</asp:ListItem>
                                <asp:ListItem value="MIRA ROAD">MIRA ROAD</asp:ListItem>
                                <asp:ListItem value="MIRA ROAD ( EAST )">MIRA ROAD ( EAST )</asp:ListItem>
                                <asp:ListItem value="MIRZAPUR">MIRZAPUR</asp:ListItem>
                                <asp:ListItem value="MODI NAGAR">MODI NAGAR</asp:ListItem>
                                <asp:ListItem value="MOGA">MOGA</asp:ListItem>
                                <asp:ListItem value="MOHALI">MOHALI</asp:ListItem>
                                <asp:ListItem value="MORADABAD">MORADABAD</asp:ListItem>
                                <asp:ListItem value="MORBI">MORBI</asp:ListItem>
                                <asp:ListItem value="MOTIHARI">MOTIHARI</asp:ListItem>
                                <asp:ListItem value="MUMBAI">MUMBAI</asp:ListItem>
                                <asp:ListItem value="MUMBAICITY">MUMBAICITY</asp:ListItem>
                                <asp:ListItem value="MUNGER">MUNGER</asp:ListItem>
                                <asp:ListItem value="MURBAD">MURBAD</asp:ListItem>
                                <asp:ListItem value="MURSHIDABAD">MURSHIDABAD</asp:ListItem>
                                <asp:ListItem value="MUZAFFARNAGAR">MUZAFFARNAGAR</asp:ListItem>
                                <asp:ListItem value="MUZAFFARPUR">MUZAFFARPUR</asp:ListItem>
                                <asp:ListItem value="MYSORE">MYSORE</asp:ListItem>
                                <asp:ListItem value="NA">NA</asp:ListItem>
                                <asp:ListItem value="NAGAON">NAGAON</asp:ListItem>
                                <asp:ListItem value="NAGERCOIL">NAGERCOIL</asp:ListItem>
                                <asp:ListItem value="NAGPUR">NAGPUR</asp:ListItem>
                                <asp:ListItem value="NAGUR">NAGUR</asp:ListItem>
                                <asp:ListItem value="NAJAFGARGH">NAJAFGARGH</asp:ListItem>
                                
                                <asp:ListItem value="NALAGARH">NALAGARH</asp:ListItem>
                                <asp:ListItem value="NALASOPARA">NALASOPARA</asp:ListItem>
                                <asp:ListItem value="NAMMAKKAL">NAMMAKKAL</asp:ListItem>
                                <asp:ListItem value="NANDED">NANDED</asp:ListItem>
                                <asp:ListItem value="NANJANGUD">NANJANGUD</asp:ListItem>
                                <asp:ListItem value="NAPUR">NAPUR</asp:ListItem>
                                <asp:ListItem value="NARSAPUR">NARSAPUR</asp:ListItem>
                                <asp:ListItem value="NASIK">NASIK</asp:ListItem>
                                <asp:ListItem value="NATHDWARA">NATHDWARA</asp:ListItem>
                                <asp:ListItem value="NAVI MUMBAI">NAVI MUMBAI</asp:ListItem>
                                <asp:ListItem value="NAVSARI">NAVSARI</asp:ListItem>
                                <asp:ListItem value="NAWADA">NAWADA</asp:ListItem>
                                <asp:ListItem value="NEEMRANA">NEEMRANA</asp:ListItem>
                                <asp:ListItem value="NELLORE">NELLORE</asp:ListItem>
                                <asp:ListItem value="NEPAL">NEPAL</asp:ListItem>
                                <asp:ListItem value="NEW  DELHI">NEW  DELHI</asp:ListItem>
                                <asp:ListItem value="NEW CODE-HYDN">NEW CODE-HYDN</asp:ListItem>
                                <asp:ListItem value="NEW CODE-PNL">NEW CODE-PNL</asp:ListItem>
                                <asp:ListItem value="NEW CODE-RLE">NEW CODE-RLE</asp:ListItem>
                                <asp:ListItem value="NEW DELHI">NEW DELHI</asp:ListItem>
                                <asp:ListItem value="NEW DELHI GATE">NEW DELHI GATE</asp:ListItem>
                                <asp:ListItem value="NEW JALPAIGURI">NEW JALPAIGURI</asp:ListItem>
                                <asp:ListItem value="NILOKHERI">NILOKHERI</asp:ListItem>
                                <asp:ListItem value="NIZAMABAD">NIZAMABAD</asp:ListItem>
                                <asp:ListItem value="NOIDA">NOIDA</asp:ListItem>
                                <asp:ListItem value="NORTH LAKHIMPUR">NORTH LAKHIMPUR</asp:ListItem>
                                <asp:ListItem value="ONGOLE">ONGOLE</asp:ListItem>
                                <asp:ListItem value="OOTY">OOTY</asp:ListItem>
                                <asp:ListItem value="OSMANABAD">OSMANABAD</asp:ListItem>
                                <asp:ListItem value="PALANPUR">PALANPUR</asp:ListItem>
                                <asp:ListItem value="PALASA">PALASA</asp:ListItem>
                                <asp:ListItem value="PALGHAR">PALGHAR</asp:ListItem>
                                <asp:ListItem value="PALGHAT">PALGHAT</asp:ListItem>
                                <asp:ListItem value="PALI">PALI</asp:ListItem>
                                <asp:ListItem value="PALLADAM">PALLADAM</asp:ListItem>
                                <asp:ListItem value="PALWAL">PALWAL</asp:ListItem>
                                <asp:ListItem value="PANCHKULA">PANCHKULA</asp:ListItem>
                                <asp:ListItem value="PANCKULA">PANCKULA</asp:ListItem>
                                <asp:ListItem value="PANIPAT">PANIPAT</asp:ListItem>
                                <asp:ListItem value="PANJIM">PANJIM</asp:ListItem>
                                <asp:ListItem value="PANVEL">PANVEL</asp:ListItem>
                                <asp:ListItem value="PARADEEP">PARADEEP</asp:ListItem>
                                <asp:ListItem value="PARASALA">PARASALA</asp:ListItem>
                                <asp:ListItem value="PARAWADA">PARAWADA</asp:ListItem>
                                <asp:ListItem value="PARBHANI">PARBHANI</asp:ListItem>
                                <asp:ListItem value="PARWANO">PARWANO</asp:ListItem>
                                <asp:ListItem value="PARWANOO">PARWANOO</asp:ListItem>
                                <asp:ListItem value="PATALGANGA">PATALGANGA</asp:ListItem>
                                <asp:ListItem value="PATHANKOT">PATHANKOT</asp:ListItem>
                                <asp:ListItem value="PATIALA">PATIALA</asp:ListItem>
                                <asp:ListItem value="PATNA">PATNA</asp:ListItem>
                                <asp:ListItem value="PENJERLA">PENJERLA</asp:ListItem>
                                <asp:ListItem value="PERINTALMANNA">PERINTALMANNA</asp:ListItem>
                                <asp:ListItem value="PERUMBALUR">PERUMBALUR</asp:ListItem>
                                <asp:ListItem value="PERUNDURAI">PERUNDURAI</asp:ListItem>
                                <asp:ListItem value="PHAGWARA">PHAGWARA</asp:ListItem>
                                <asp:ListItem value="PHURSUNGI">PHURSUNGI</asp:ListItem>
                                <asp:ListItem value="PHUTSHILLONG">PHUTSHILLONG</asp:ListItem>
                                <asp:ListItem value="PILANI">PILANI</asp:ListItem>
                                <asp:ListItem value="PIRANGUT">PIRANGUT</asp:ListItem>
                                <asp:ListItem value="PITAMPUT">PITAMPUT</asp:ListItem>
                                <asp:ListItem value="PITHAMPUR">PITHAMPUR</asp:ListItem>
                                <asp:ListItem value="PNAVEL">PNAVEL</asp:ListItem>
                                <asp:ListItem value="POLLACHI">POLLACHI</asp:ListItem>
                                <asp:ListItem value="PONDA">PONDA</asp:ListItem>
                                <asp:ListItem value="PONDICHERRY">PONDICHERRY</asp:ListItem>
                                <asp:ListItem value="PORT BLAIR">PORT BLAIR</asp:ListItem>
                                <asp:ListItem value="PUNE">PUNE</asp:ListItem>
                                <asp:ListItem value="PURI">PURI</asp:ListItem>
                                <asp:ListItem value="PURNEA">PURNEA</asp:ListItem>
                                <asp:ListItem value="PUTTUR">PUTTUR</asp:ListItem>
                                <asp:ListItem value="RABALE">RABALE</asp:ListItem>
                                <asp:ListItem value="RAE BARIELI">RAE BARIELI</asp:ListItem>
                                <asp:ListItem value="RAI BARELI">RAI BARELI</asp:ListItem>
                                <asp:ListItem value="RAICHUR">RAICHUR</asp:ListItem>
                                <asp:ListItem value="RAIGANJ">RAIGANJ</asp:ListItem>
                                <asp:ListItem value="RAIGARH">RAIGARH</asp:ListItem>
                                <asp:ListItem value="RAIPUR">RAIPUR</asp:ListItem>
                                <asp:ListItem value="RAJKOT">RAJKOT</asp:ListItem>
                                <asp:ListItem value="RAJMUNDARY">RAJMUNDARY</asp:ListItem>
                                <asp:ListItem value="RAJPURA">RAJPURA</asp:ListItem>
                                <asp:ListItem value="RAMGARH">RAMGARH</asp:ListItem>
                                <asp:ListItem value="RAMPUR">RAMPUR</asp:ListItem>
                                <asp:ListItem value="RANCHI">RANCHI</asp:ListItem>
                                <asp:ListItem value="RANEBENNUR">RANEBENNUR</asp:ListItem>
                                <asp:ListItem value="RANGPO">RANGPO</asp:ListItem>
                                <asp:ListItem value="RANIPET">RANIPET</asp:ListItem>
                                <asp:ListItem value="RANJANGAON">RANJANGAON</asp:ListItem>
                                <asp:ListItem value="RANJANGOAN-PUNE">RANJANGOAN-PUNE</asp:ListItem>
                                <asp:ListItem value="RATANGARH">RATANGARH</asp:ListItem>
                                <asp:ListItem value="RATLAM">RATLAM</asp:ListItem>
                                <asp:ListItem value="RATNAGIRI">RATNAGIRI</asp:ListItem>
                                <asp:ListItem value="RAUDRAPUR">RAUDRAPUR</asp:ListItem>
                                <asp:ListItem value="RAXUAL">RAXUAL</asp:ListItem>
                                <asp:ListItem value="RAYAGADA">RAYAGADA</asp:ListItem>
                                <asp:ListItem value="RAYGAD">RAYGAD</asp:ListItem>
                                <asp:ListItem value="RENUKOOT">RENUKOOT</asp:ListItem>
                                <asp:ListItem value="REWA">REWA</asp:ListItem>
                                <asp:ListItem value="REWARI">REWARI</asp:ListItem>
                                <asp:ListItem value="ROHA">ROHA</asp:ListItem>
                                <asp:ListItem value="ROHTAK">ROHTAK</asp:ListItem>
                                <asp:ListItem value="ROORKEE">ROORKEE</asp:ListItem>
                                <asp:ListItem value="ROPAR">ROPAR</asp:ListItem>
                                <asp:ListItem value="ROURKELA">ROURKELA</asp:ListItem>
                                <asp:ListItem value="RUDRAPRAYAG">RUDRAPRAYAG</asp:ListItem>
                                <asp:ListItem value="RUDRAPUR">RUDRAPUR</asp:ListItem>
                                <asp:ListItem value="RUDRAPUR ,">RUDRAPUR ,</asp:ListItem>
                                <asp:ListItem value="SAGAR">SAGAR</asp:ListItem>
                                <asp:ListItem value="SAHA">SAHA</asp:ListItem>
                                <asp:ListItem value="SAHARANPUR">SAHARANPUR</asp:ListItem>
                                <asp:ListItem value="SAHIBABAD">SAHIBABAD</asp:ListItem>
                                <asp:ListItem value="SALEM">SALEM</asp:ListItem>
                                <asp:ListItem value="SAMASTIPUR">SAMASTIPUR</asp:ListItem>
                                <asp:ListItem value="SAMBHA">SAMBHA</asp:ListItem>
                                <asp:ListItem value="SAMBHALPUR">SAMBHALPUR</asp:ListItem>
                                <asp:ListItem value="SAMUDRAGARH">SAMUDRAGARH</asp:ListItem>
                                <asp:ListItem value="SANEWAL">SANEWAL</asp:ListItem>
                                <asp:ListItem value="SANGALI">SANGALI</asp:ListItem>
                                <asp:ListItem value="SANGAMNER">SANGAMNER</asp:ListItem>
                                <asp:ListItem value="SANGRUR">SANGRUR</asp:ListItem>
                                <asp:ListItem value="SANKESHWAR">SANKESHWAR</asp:ListItem>
                                <asp:ListItem value="SARDARSAHAR">SARDARSAHAR</asp:ListItem>
                                <asp:ListItem value="SATARA">SATARA</asp:ListItem>
                                <asp:ListItem value="SATNA">SATNA</asp:ListItem>
                                <asp:ListItem value="SAWANTWADI">SAWANTWADI</asp:ListItem>
                                <asp:ListItem value="SBD">SBD</asp:ListItem>
                                <asp:ListItem value="SECUNDEARABD">SECUNDEARABD</asp:ListItem>
            
                                <asp:ListItem value="SEHORE">SEHORE</asp:ListItem>
                                <asp:ListItem value="SELAQUI">SELAQUI</asp:ListItem>
                                <asp:ListItem value="SHADNAGAR">SHADNAGAR</asp:ListItem>
                                <asp:ListItem value="SHAHAPUR">SHAHAPUR</asp:ListItem>
                                <asp:ListItem value="SHAHJAHANPUR">SHAHJAHANPUR</asp:ListItem>
                                <asp:ListItem value="SHALIMAR">SHALIMAR</asp:ListItem>
                                <asp:ListItem value="SHANGAI">SHANGAI</asp:ListItem>
                                <asp:ListItem value="SHANTIPUR">SHANTIPUR</asp:ListItem>
                                <asp:ListItem value="SHILLONG">SHILLONG</asp:ListItem>
                                <asp:ListItem value="SHIMOGA">SHIMOGA</asp:ListItem>
                                <asp:ListItem value="SHIRWAL">SHIRWAL</asp:ListItem>
                                <asp:ListItem value="SHIVPURI">SHIVPURI</asp:ListItem>
                                <asp:ListItem value="SHOLAPR">SHOLAPR</asp:ListItem>
                                <asp:ListItem value="SHOLAPUR">SHOLAPUR</asp:ListItem>
                                <asp:ListItem value="SIKAR">SIKAR</asp:ListItem>
                                <asp:ListItem value="SIKKIM">SIKKIM</asp:ListItem>
                                <asp:ListItem value="SIKNDRABAD">SIKNDRABAD</asp:ListItem>
                                <asp:ListItem value="SILCHAR">SILCHAR</asp:ListItem>
                                <asp:ListItem value="SILIGURI">SILIGURI</asp:ListItem>
                                <asp:ListItem value="SILVASA">SILVASA</asp:ListItem>
                                <asp:ListItem value="SIMLA">SIMLA</asp:ListItem>
                                <asp:ListItem value="SINGAPORE">SINGAPORE</asp:ListItem>
                                <asp:ListItem value="SINGAPORE 199591">SINGAPORE 199591</asp:ListItem>
                                <asp:ListItem value="SINGAPORE 486444">SINGAPORE 486444</asp:ListItem>
                                <asp:ListItem value="SINGAPORE TERRITORY">SINGAPORE TERRITORY</asp:ListItem>
                                <asp:ListItem value="SINGRAULI">SINGRAULI</asp:ListItem>
                                <asp:ListItem value="SIROHI">SIROHI</asp:ListItem>
                                <asp:ListItem value="SITAPUR">SITAPUR</asp:ListItem>
                                <asp:ListItem value="SITARGANJ">SITARGANJ</asp:ListItem>
                                <asp:ListItem value="SIVAKASI">SIVAKASI</asp:ListItem>
                                <asp:ListItem value="SIWAN">SIWAN</asp:ListItem>
                                <asp:ListItem value="SLIGURI">SLIGURI</asp:ListItem>
                                <asp:ListItem value="SOLAN">SOLAN</asp:ListItem>
                                <asp:ListItem value="SONIPAT">SONIPAT</asp:ListItem>
                                <asp:ListItem value="SRIGANGANAGAR">SRIGANGANAGAR</asp:ListItem>
                                <asp:ListItem value="SRIKAKULAM">SRIKAKULAM</asp:ListItem>
                                <asp:ListItem value="SRINAGAR">SRINAGAR</asp:ListItem>
                                <asp:ListItem value="SRIPERUMBADUR">SRIPERUMBADUR</asp:ListItem>
                      
                                <asp:ListItem value="SULTANPUR">SULTANPUR</asp:ListItem>
                                <asp:ListItem value="SUNDER NAGAR">SUNDER NAGAR</asp:ListItem>
                                <asp:ListItem value="SURAT">SURAT</asp:ListItem>
                                <asp:ListItem value="SURENDRANAGAR">SURENDRANAGAR</asp:ListItem>
                                <asp:ListItem value="SURYAPET">SURYAPET</asp:ListItem>
                                <asp:ListItem value="TALOJA">TALOJA</asp:ListItem>
                                <asp:ListItem value="TARAPUR">TARAPUR</asp:ListItem>
                                <asp:ListItem value="TARDEO">TARDEO</asp:ListItem>
                                <asp:ListItem value="TEHERPUR">TEHERPUR</asp:ListItem>
                                <asp:ListItem value="TEMBHURNI">TEMBHURNI</asp:ListItem>
                                <asp:ListItem value="TENALI">TENALI</asp:ListItem>
                                <asp:ListItem value="TEZPUR">TEZPUR</asp:ListItem>
                                <asp:ListItem value="THAILAND">THAILAND</asp:ListItem>
                                <asp:ListItem value="THANE">THANE</asp:ListItem>
                                <asp:ListItem value="THANJAVUR">THANJAVUR</asp:ListItem>
                                <asp:ListItem value="THE">THE</asp:ListItem>
                                <asp:ListItem value="THENI">THENI</asp:ListItem>
                                <asp:ListItem value="THIRUNELVELI">THIRUNELVELI</asp:ListItem>
                                <asp:ListItem value="TIKIAPARA">TIKIAPARA</asp:ListItem>
                                <asp:ListItem value="TINSUKIA">TINSUKIA</asp:ListItem>
                                <asp:ListItem value="TIRUNCHENGODE">TIRUNCHENGODE</asp:ListItem>
                                <asp:ListItem value="TIRUPATHI">TIRUPATHI</asp:ListItem>
                                <asp:ListItem value="TIRUPUR">TIRUPUR</asp:ListItem>
                                <asp:ListItem value="TONK">TONK</asp:ListItem>
                                <asp:ListItem value="TRICHUR">TRICHUR</asp:ListItem>
                                <asp:ListItem value="TRICHY">TRICHY</asp:ListItem>
                                <asp:ListItem value="TRIVALLUR">TRIVALLUR</asp:ListItem>
                                <asp:ListItem value="TRIVANDRUM">TRIVANDRUM</asp:ListItem>
                                <asp:ListItem value="TRONICA CITY">TRONICA CITY</asp:ListItem>
                                <asp:ListItem value="TUMKUR">TUMKUR</asp:ListItem>
                                <asp:ListItem value="TUTICORIN">TUTICORIN</asp:ListItem>
                                <asp:ListItem value="UDAIPUR">UDAIPUR</asp:ListItem>
                                <asp:ListItem value="UDRAPURR">UDRAPURR</asp:ListItem>
                                <asp:ListItem value="UJJAIN">UJJAIN</asp:ListItem>
                                <asp:ListItem value="UMBERGAM">UMBERGAM</asp:ListItem>
                                <asp:ListItem value="UNA">UNA</asp:ListItem>
                                <asp:ListItem value="UNNAO">UNNAO</asp:ListItem>
                                <asp:ListItem value="VADODARA">VADODARA</asp:ListItem>
                                <asp:ListItem value="VALLABH VIDYANAGAR">VALLABH VIDYANAGAR</asp:ListItem>
                                <asp:ListItem value="VALSAD">VALSAD</asp:ListItem>
                                <asp:ListItem value="VAPI">VAPI</asp:ListItem>
                                <asp:ListItem value="VARANASI">VARANASI</asp:ListItem>
                                <asp:ListItem value="VASAI">VASAI</asp:ListItem>
                                <asp:ListItem value="VASAI E">VASAI E</asp:ListItem>
                                <asp:ListItem value="VASHI">VASHI</asp:ListItem>
                                <asp:ListItem value="VELLORE">VELLORE</asp:ListItem>
                                <asp:ListItem value="VERAWAL">VERAWAL</asp:ListItem>
                                <asp:ListItem value="VIDISHA">VIDISHA</asp:ListItem>
                                <asp:ListItem value="VIJAYANAGRAM">VIJAYANAGRAM</asp:ListItem>
                                <asp:ListItem value="VIJAYAWADA">VIJAYAWADA</asp:ListItem>
                                <asp:ListItem value="VILLUPURAM">VILLUPURAM</asp:ListItem>
                                <asp:ListItem value="VISAKHAPATANAM">VISAKHAPATANAM</asp:ListItem>
                                <asp:ListItem value="VOLOMBO">VOLOMBO</asp:ListItem>
                                <asp:ListItem value="WARANGAL">WARANGAL</asp:ListItem>
                                <asp:ListItem value="WAYANAD">WAYANAD</asp:ListItem>
                                <asp:ListItem value="WAZIRPURE">WAZIRPURE</asp:ListItem>
                                <asp:ListItem value="WEST GODAVARI">WEST GODAVARI</asp:ListItem>
                                <asp:ListItem value="XIAMEN">XIAMEN</asp:ListItem>
                                <asp:ListItem value="YAMUNA NAGAR">YAMUNA NAGAR</asp:ListItem>
                                <asp:ListItem value="YAVAI">YAVAI</asp:ListItem>
                                <asp:ListItem value="YAVATMAL">YAVATMAL</asp:ListItem>
                                <asp:ListItem value="ZIRAKPUR">ZIRAKPUR</asp:ListItem>
                            </asp:DropDownList>

                     
                        </div>

                        <div class="col-xs-12 col-sm-6 form-group">
                            <%--  <input type="text" placeholder="Pincode" class="form-control fade_anim" />--%>
                            <asp:TextBox ID="txtPinCode" CssClass="form-control fade_anim" placeholder="Pincode" runat="server" ValidationGroup="Contact"></asp:TextBox>
                        </div>

                        <div class="col-sm-12 form-group">
                            <%--                            <textarea placeholder="Address Line 1" class="form-control fade_anim"> </textarea>--%>
                            <asp:TextBox ID="txtAddress1" CssClass="form-control fade_anim" placeholder="Address Line 1" TextMode="MultiLine" runat="server" ValidationGroup="Contact"></asp:TextBox>
                        </div>

                        <div class="col-xs-12 col-sm-12 form-group">
                            <asp:TextBox ID="txtAddress2" CssClass="form-control fade_anim" placeholder="Address Line 2" TextMode="MultiLine" runat="server" ValidationGroup="Contact"></asp:TextBox>
                        </div>

                        <div class="col-xs-12 col-sm-6 form-group">
                            <%-- <input type="text" placeholder="Email Address" class="form-control fade_anim" />--%>
                            <asp:TextBox ID="txtEmail" CssClass="form-control fade_anim" placeholder="Email Address" runat="server" ValidationGroup="Contact"></asp:TextBox>

                        </div>

                        <div class="col-xs-12 col-sm-6 form-group">
                            <%--                            <input type="number" placeholder="Mobile No." class="form-control fade_anim" />--%>
                            <asp:TextBox ID="txtMobNo" CssClass="form-control fade_anim" placeholder="Mobile No." runat="server" ValidationGroup="Contact"></asp:TextBox>

                        </div>

                        <div class="col-xs-12 col-sm-6 form-group">
                            <%--                            <input type="text" placeholder="Select right date & time to contact you" class="date form_datetime form-control" readonly>--%>
                            <asp:DropDownList ID="ddlDateTime" runat="server" class="selectpicker no_padding select_dropdown_height form-control" ValidationGroup="Contact">
                                <asp:ListItem Value="0"> When is the right time to contact you </asp:ListItem>
                                <asp:ListItem> 8 AM to 10 AM </asp:ListItem>
                                <asp:ListItem>10 AM to 12 PM</asp:ListItem>
                                <asp:ListItem>12 PM to 2 PM</asp:ListItem>
                                <asp:ListItem>2 PM to 6 PM</asp:ListItem>
                                <asp:ListItem>6 PM to 8 PM</asp:ListItem>
                                <asp:ListItem>8 PM to 10 PM</asp:ListItem>
                            </asp:DropDownList>
                        </div>


                        <div class="col-xs-12 col-sm-6 form-group">

                            <%--<select class="selectpicker form-control no_padding select_dropdown_height" standard title="Select Industry">
                                <option value="31,AGRICULTURE">AGRICULTURE</option>
                                <option value="32,AUTOMOTIVE">AUTOMOTIVE</option>
                                <option value="32,AVIATION">AVIATION</option>
                                <option value="36,BANKING">BANKING</option>
                                <option value="33,BIOTECHNOLOGY">BIOTECHNOLOGY</option>
                                <option value="37,CEMENT">CEMENT</option>
                                <option value="37,CONSTRUCTION">CONSTRUCTION</option>
                                <option value="34,CONSUMER MARKETS">CONSUMER MARKETS</option>
                                <option value="36,EDUCATION AND TRAINING">EDUCATION AND TRAINING</option>
                                <option value="38,ECOMMERCE">ECOMMERCE</option>
                                <option value="35,ELECTRONICS">ELECTRONICS</option>
                                <option value="37,ENERGY">ENERGY</option>
                                <option value="37,ENGINEERING">ENGINEERING</option>
                                <option value="31,FAST-MOVING CONSUMER GOODS(FMCG)">FAST-MOVING CONSUMER GOODS(FMCG)</option>
                                <option value="36,FINANCIAL SERVICES">FINANCIAL SERVICES</option>
                                <option value="40,GEMS AND JEWELLERY">GEMS AND JEWELLERY</option>
                                <option value="33,HEALTHCARE AND SOCIAL SERVICES">HEALTHCARE AND SOCIAL SERVICES </option>
                                <option value="37,INFRASTRUCTURE">INFRASTRUCTURE</option>
                                <option value="36,INSURANCE">INSURANCE</option>
                                <option value="39,IT &amp; ITES">IT &amp; ITES</option>
                                <option value="36,LEGAL">LEGAL</option>
                                <option value="36,LOGISTICS &amp; TRANSPORTATION">LOGISTICS &amp; TRANSPORTATION</option>
                                <option value="36,MANUFACTURING">MANUFACTURING</option>
                                <option value="36,MEDIA AND ENTERTAINMENT">MEDIA AND ENTERTAINMENT</option>
                                <option value="37,OIL AND GAS">OIL AND GAS </option>
                                <option value="38,ONLINE">ONLINE</option>
                                <option value="33,PHARMACEUTICALS">PHARMACEUTICALS</option>
                                <option value="36,PUBLIC ADMINISTRATION &amp; GOVT">PUBLIC ADMINISTRATION &amp; GOVT</option>
                                <option value="36,RESEARCH AND DEVELOPMENT">REAL ESTATE</option>
                                <option value="36,">RESEARCH AND DEVELOPMENT</option>
                                <option value="34,RETAIL">RETAIL</option>
                                <option value="36,SCIENCE AND TECHNOLOGY">SCIENCE AND TECHNOLOGY</option>
                                <option value="35,SEMICONDUCTOR">SEMICONDUCTOR</option>
                                <option value="37,STEEL">STEEL</option>
                                <option value="39,TELECOMMUNICATIONS">TELECOMMUNICATIONS</option>
                                <option value="40,TEXTILES">TEXTILES</option>
                                <option value="36,TOURISM AND HOSPITALITY">TOURISM AND HOSPITALITY</option>
                                <option value="36,TRAVEL">TRAVEL</option>
                                <option value="34,URBAN MARKET">URBAN MARKET</option>
                                <option value="37,UTILITIES">UTILITIES</option>
                                <option value="34,WHOLESALE TRADE">WHOLESALE TRADE</option>
                                <option value="36,OTHERS">OTHERS</option>

                            </select>--%>

                            <asp:DropDownList ID="ddlIndustry" class="selectpicker form-control no_padding select_dropdown_height" runat="server" ValidationGroup="Contact">
                                <asp:ListItem Value="0">Select Industry</asp:ListItem>
                                <asp:ListItem>AGRICULTURE</asp:ListItem>
                                <asp:ListItem>AUTOMOTIVE</asp:ListItem>
                                <asp:ListItem>AVIATION</asp:ListItem>
                                <asp:ListItem>BANKING</asp:ListItem>
                                <asp:ListItem>BIOTECHNOLOGY</asp:ListItem>
                                <asp:ListItem>CEMENT</asp:ListItem>
                                <asp:ListItem>CONSTRUCTION</asp:ListItem>
                                <asp:ListItem>CONSUMER MARKETS</asp:ListItem>
                                <asp:ListItem>EDUCATION AND TRAINING</asp:ListItem>
                                <asp:ListItem>ECOMMERCE</asp:ListItem>
                                <asp:ListItem>ELECTRONICS</asp:ListItem>
                                <asp:ListItem>ENERGY</asp:ListItem>
                                <asp:ListItem>ENGINEERING</asp:ListItem>
                                <asp:ListItem>FAST-MOVING CONSUMER GOODS(FMCG)</asp:ListItem>
                                <asp:ListItem>FINANCIAL SERVICES</asp:ListItem>
                                <asp:ListItem>GEMS AND JEWELLERY</asp:ListItem>
                                <asp:ListItem>HEALTHCARE AND SOCIAL SERVICES</asp:ListItem>
                                <asp:ListItem>INFRASTRUCTURE</asp:ListItem>
                                <asp:ListItem>INSURANCE</asp:ListItem>
                                <asp:ListItem>IT &amp; ITES</asp:ListItem>
                                <asp:ListItem>LEGAL</asp:ListItem>
                                <asp:ListItem>LOGISTICS &amp; TRANSPORTATION</asp:ListItem>
                                <asp:ListItem>MANUFACTURING</asp:ListItem>
                                <asp:ListItem>MEDIA AND ENTERTAINMENT</asp:ListItem>
                                <asp:ListItem>OIL AND GAS</asp:ListItem>
                                <asp:ListItem>ONLINE</asp:ListItem>
                                <asp:ListItem>PHARMACEUTICALS</asp:ListItem>
                                <asp:ListItem>PUBLIC ADMINISTRATION &amp; GOVT</asp:ListItem>
                                <asp:ListItem>REAL ESTATE</asp:ListItem>
                                <asp:ListItem>RESEARCH AND DEVELOPMENT</asp:ListItem>
                                <asp:ListItem>RETAIL</asp:ListItem>
                                <asp:ListItem>SCIENCE AND TECHNOLOGY</asp:ListItem>
                                <asp:ListItem>SEMICONDUCTOR</asp:ListItem>
                                <asp:ListItem>STEEL</asp:ListItem>
                                <asp:ListItem>TELECOMMUNICATIONS</asp:ListItem>
                                <asp:ListItem>TEXTILES</asp:ListItem>
                                <asp:ListItem>TOURISM AND HOSPITALITY</asp:ListItem>
                                <asp:ListItem>TRAVEL</asp:ListItem>
                                <asp:ListItem>URBAN MARKET</asp:ListItem>
                                <asp:ListItem>UTILITIES</asp:ListItem>
                                <asp:ListItem>WHOLESALE TRADE</asp:ListItem>
                                <asp:ListItem>OTHERS</asp:ListItem>
                            </asp:DropDownList>
                        </div>


                        <div class="col-xs-12 col-sm-12 form-group">
                            <%--                            <textarea placeholder="Address Line 2" class="form-control fade_anim"> </textarea>>--%>
                            <asp:TextBox ID="txtRequirement" CssClass="form-control fade_anim" placeholder="Your Requirement" TextMode="MultiLine" ValidationGroup="Contact" runat="server"></asp:TextBox>
                        </div>

                        <div class="col-xs-12 form-group">
                            <%--                            <input type="text" placeholder="What is your monthly distribution or/and storage volume requirement in tonnes *" class="form-control fade_anim" />--%>
                            <asp:TextBox ID="txtStorageValue" CssClass="form-control fade_anim" placeholder="What is your monthly distribution or/and storage volume requirement in tonnes *" runat="server" ValidationGroup="Contact"></asp:TextBox>

                        </div>

                        <div class="col-xs-12 form-group">
                            <%--                            <input type="text" placeholder="Which service provider are you using currently" class="form-control fade_anim" />--%>

                            <asp:TextBox ID="txtServiceProvider" CssClass="form-control fade_anim" placeholder="Which service provider are you using currently" runat="server" ValidationGroup="Contact"></asp:TextBox>

                        </div>

                        <div class="col-xs-12 form-group">
                            <asp:Button ID="btnSubmit" Text="Submit" CssClass="btn btn-primary fade_anim" OnClientClick="javascript:return Checkfields_contact();" OnClick="btnSubmit_Click" runat="server" ValidationGroup="Contact" />
                        </div>

                    </form>

                </div>

            </div>

        </div>

    </section>


    <script src="js/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>

    <script src="js/rem.min.js"></script>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQbt8fBXZu0RHPqPCRM3GPMRklOt-9Als&callback=initMap"></script>

    <script src="js/mapmarker.jquery.js"></script>

    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>


    <script src="js/general.js"></script>

    <script src="js/css3-animate-it.js"></script>

    <script type="text/javascript">
        $(document).ready(function () { var a = { markers: [{ latitude: "19.078047", longitude: " 72.829500", icon: "images/pointer.png", baloon_text: "<strong> PATEL HOUSE </strong> <p> 48, Gazdar Bandh, North Avenue Road,<br> Santacruz (West), Mumbai, India – 400 054 </p>"}] }; $(".map_wrp").mapmarker({ zoom: 19, center: "19.078047, 72.829500", markers: a }) });
    </script>

    <script>
        function InstantShowPopUp() {
            //$.fancybox.open('<div class="fade_anim"><div class="popup_display">Thank you!</div><p style="color: cadetblue;">Thank you for contacting us, <br /> we will get back to you soon!.</p></div>');
            $.fancybox.open('<div class="popup_display"><div class="popup_title"> </div> <strong><p><br/>Thank you for contacting us, <br /> we will get back to you soon!.</p></strong></div>');
        };




    </script>

    <script type="text/javascript">
        function Checkfields_contact() {
            var FName = $('#<%=txtFirstName.ClientID%>').val();
            var Lastname = $('#<%=txtLastName.ClientID%>').val();
            var JobTitle = $('#<%=txtJobTitle.ClientID %>').val();
            var Companyname = $('#<%=txtComapanyName.ClientID%>').val();
            var ddlCity = $('#<%=ddlCity.ClientID%>').val();
            var Pincode = $('#<%=txtPinCode.ClientID%>').val();
            var Address1 = $('#<%=txtAddress1.ClientID%>').val();
            var Address2 = $('#<%=txtAddress2.ClientID%>').val();
            var Email = $('#<%=txtEmail.ClientID%>').val();
            var MobNo = $('#<%=txtMobNo.ClientID%>').val();
            var ddlDateTime = $('#<%=ddlDateTime.ClientID%>').val();
            var ddlIndustry = $('#<%=ddlIndustry.ClientID%>').val();
            var Requirement = $('#<%=txtRequirement.ClientID%>').val();
            var StorageVolume = $('#<%=txtStorageValue.ClientID%>').val();
            var ServiceProvide = $('#<%=txtServiceProvider.ClientID%>').val();
            var Blank = false;
            var message = "";

            if (FName == '') {

               <%-- $('#<%=txtFirstName.ClientID %>').css('border-color', 'red');--%>
                message = " " + "First Name";
                Blank = true;
            }
            else {
              <%--  $('#<%=txtFirstName.ClientID %>').css('border-color', '');--%>
            }


            if (Lastname == '') {
                message += " ,  " + "Last Name";
             <%--   $('#<%=txtLastName.ClientID %>').css('border-color', 'red');--%>
                Blank = true;
            }
            else {
            <%--    $('#<%=txtLastName.ClientID %>').css('border-color', '');--%>
            }

            if (JobTitle == '') {
                message += " ,  " + "Job Title ";
               <%-- $('#<%=txtJobTitle.ClientID %>').css('border-color', 'red');--%>
                Blank = true;
            }
            else {
          <%--   $('#<%=txtJobTitle.ClientID %>').css('border-color', '');--%>
            }


            if (Companyname == '') {
                message += " ,  " + "Company Name ";
             <%--$('#<%=txtComapanyName.ClientID %>').css('border-color', 'red');--%>
                Blank = true;
            }
            else {
               <%--$('#<%=txtComapanyName.ClientID %>').css('border-color', '');--%>
            }

            if (ddlCity == "0") {

              <%-- $('#<%=ddlCity.ClientID %>').css('border-color', 'red');--%>
                message += " , " + "City";

                Blank = true;
            }
            else {
       <%--         $('#<%=ddlCity.ClientID %>').css('border-color', '');--%>
            }

            if (Pincode == '') {
                message += " ,  " + "Pincode";
             <%--   $('#<%=txtPinCode.ClientID %>').css('border-color', 'red');--%>
                Blank = true;
            }
            else {
              <%--  $('#<%=txtPinCode.ClientID %>').css('border-color', '');--%>
            }


            if (Address1 == '') {
                message += " ,  " + "Address1";
              <%--  $('#<%=txtAddress1.ClientID %>').css('border-color', 'red');--%>
                Blank = true;
            }
            else {
               <%-- $('#<%=txtAddress1.ClientID %>').css('border-color', '');--%>
            }

            if (Address2 == '') {
                message += " ,  " + "Address2";
           <%--     $('#<%=txtAddress2.ClientID %>').css('border-color', 'red');--%>
                Blank = true;
            }
            else {
            <%--    $('#<%=txtAddress2.ClientID %>').css('border-color', '');--%>
            }

            if (Email == '') {
                message += " ,  " + "Email";
        <%--        $('#<%=txtEmail.ClientID %>').css('border-color', 'red');--%>
                Blank = true;
            }
            else {

                var emailext = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (emailext.test(Email) == false) {
                    message += " ,  " + "Valid Email";
                    <%--$('#<%=txtEmail.ClientID %>').css('border-color', 'red');--%>
                    Blank = true;
                }
                else {

                 <%--   $('#<%=txtEmail.ClientID %>').css('border-color', '');--%>
                }
            }


            if (MobNo == '') {
                message += " ,  " + "Mobile Number";
             <%--   $('#<%=txtMobNo.ClientID %>').css('border-color', 'red');--%>
                Blank = true;
            }
            else {
                var mob = /^[1-9]{1}[0-9]{9}$/;
                console.log(mob.test(MobNo));

                if (mob.test(MobNo) == false) {
                    message += " ,  " + "Valid Mobile Number";
                   <%-- $('#<%=txtMobNo.ClientID %>').css('border-color', 'red');--%>
                    Blank = true;
                }
                else {
            <%--        $('#<%=txtMobNo.ClientID %>').css('border-color', '');--%>
                }
            }

            if (ddlDateTime == '0') {
                message += " ,  " + "Contact Time";
          <%--      $('#<%=ddlDateTime.ClientID %>').css('border-color', 'red');--%>
                Blank = true;
            }
            else {
         <%--   $('#<%=ddlDateTime.ClientID %>').css('border-color', '');--%>
            }


            if (ddlIndustry == '0') {
                message += " ,  " + "Industry";
         <%--   $('#<%=ddlIndustry.ClientID %>').css('border-color', 'red');--%>
                Blank = true;
            }
            else {
             <%--  $('#<%=ddlIndustry.ClientID %>').css('border-color', '');--%>
            }

            if (Requirement == '') {
                message += " ,  " + "Requirement";
              <%-- $('#<%=txtRequirement.ClientID %>').css('border-color', 'red');--%>
                Blank = true;
            }
            else {
              <%--  $('#<%=txtRequirement.ClientID %>').css('border-color', '');--%>
            }

            if (StorageVolume == '') {
                message += " ,  " + "Storage Volume";
            <%--    $('#<%=txtStorageValue.ClientID %>').css('border-color', 'red');--%>
                Blank = true;
            }
            else {
               <%-- $('#<%=txtStorageValue.ClientID %>').css('border-color', '');--%>
            }


            if (ServiceProvide == '') {
                message += " ,  " + "Service Provider";
             <%--   $('#<%=txtServiceProvider.ClientID %>').css('border-color', 'red');--%>
                Blank = true;
            }
            else {
           <%-- $('#<%=txtServiceProvider.ClientID %>').css('border-color', '');--%>
            }

            if (Blank) {
                alert(message + " " + "Required.");
                return false;

            }
            else {
                return true;

            }
        }

    </script>
</asp:Content>


