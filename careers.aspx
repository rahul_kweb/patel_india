﻿<%@ Page Title="Jobs in Logistics Sector in India | Transport & Logistics Industry | Best Logistics Management Technology System in India | 3PL & 4PL Logistics Service Provider, Supply Chain Management & Transportation in India" Language="C#" MasterPageFile="~/HouseOfPatel.master" AutoEventWireup="true" CodeFile="careers.aspx.cs" Inherits="careers" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="shortcut icon" href="images/fav_icon.png">

    <link rel="stylesheet" href="css/bootstrap-select.min.css">

    <section>

        <div class="container-fluid">

             <asp:Literal ID="ltrlBanner" runat="server"></asp:Literal>

            <asp:Literal ID="ltrlCareerContent" runat="server"></asp:Literal>


            <div class="row current_opening_wrp wrapper" id="join_team">

                <div class="inner_title text-center">
                    <h1>CURRENT OPENING</h1>
                </div>

                <asp:Repeater ID="rptCurrentOpening" runat="server">
                    <ItemTemplate>
                        <div class="col-xs-12 col-sm-12 no_padding">

                            <div class="col-xs-12 co_tab_wrp no_padding">

                                <div class="co_tab_head">

                                    <ul class="pull-left">
                                        <li><i class="fa fa-user-o" aria-hidden="true"></i><%#Eval("JobTitle") %></li>
                                        <%--  <li><i class="fa fa-map-marker" aria-hidden="true"></i><%#Eval("Location") %></li>--%>
                                        <li><i class="fa fa-clock-o" aria-hidden="true"></i><%#Eval("JobType") %></li>
                                        <li><i class="fa fa-graduation-cap" aria-hidden="true"></i><%#Eval("Qualification") %></li>
                                    </ul>

                                    <i class="fa fa-angle-double-down" aria-hidden="true"></i>

                                    <div class="clearfix"></div>

                                </div>

                                <div class="co_tab_content">

                                    <table width="100%">

                                        <tr>
                                            <td>Job Profile</td>

                                            <td><%#Eval("JobProfile") %></td>

                                        </tr>

                                        <tr>
                                            <td>Qualification</td>

                                            <td><%#Eval("Qualification") %></td>

                                        </tr>

                                        <tr>
                                            <td>Candidate Profile</td>

                                            <td><%#Eval("CandidateProfile") %></td>

                                        </tr>

                                        <tr>

                                            <td>Experience</td>

                                            <td><%#Eval("Experience") %></td>

                                        </tr>

                                        <tr>
                                            <td>Location</td>

                                            <td><%#Eval("Location") %></td>

                                        </tr>

                                    </table>

                                    <a data-fancybox data-src="#apply_form" href="javascript:;">Apply</a>

                                </div>

                            </div>

                        </div>
                    </ItemTemplate>
                </asp:Repeater>

       

                <div style="display: none;" id="apply_form" class="hidden-content">

                    <form id="Form" runat="server">
                        <div class="inner_title text-center">
                            <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
                            <h2>FILL BELOW DETAILS</h2>
                        </div>

                     


                        <div class="form-group">
                            <asp:TextBox ID="txtFirstName" class="form-control" runat="server" placeholder="First Name"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Your FirstName" ControlToValidate="txtFirstName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtLastName" class="form-control" runat="server" placeholder="Last Name"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Enter Your LastName" ControlToValidate="txtLastName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtContact" Class="form-control" runat="server" placeholder="Contact No." MaxLength="12"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please Enter Your Contact No" ControlToValidate="txtContact" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="contact"  ErrorMessage="Enter valid contact No" ValidationExpression="[0-9]{10}"  ControlToValidate="txtContact" runat="server" ForeColor="Red"></asp:RegularExpressionValidator>
                            
                        </div>
                        <div class="form-group">
                            <asp:DropDownList ID="ddlJobProfile" class="selectpicker form-control no_padding" runat="server" DataTextField="JobTitle" DataValueField="JobId">
                                <%--<asp:ListItem Selected="True" Value="0">Job Profile</asp:ListItem>--%>
                                <%--<asp:ListItem Value="1">Customer Service Executive</asp:ListItem>
                                <asp:ListItem Value="2">Sales Executive</asp:ListItem>
                                <asp:ListItem Value="3">Software Developer</asp:ListItem>--%>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please Select JobProfile" ControlToValidate="ddlJobProfile" InitialValue="0" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtEmail" class="form-control" runat="server" placeholder="Email"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please Enter Email ID " ControlToValidate="txtEmail" ForeColor="Red"></asp:RequiredFieldValidator>

                            <asp:RegularExpressionValidator ID="Email" ErrorMessage="Enter Valid Email Address" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w{2,4}([-.]\w{2,4})*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w{2,4}([-.]\w{2,4})*)*" ControlToValidate="txtEmail" runat="server" ForeColor="Red"></asp:RegularExpressionValidator>

                        </div>
                        <asp:Button ID="btnsubmit" runat="server" Text="SUBMIT" class="btn btn-primary fade_anim" OnClick="submit_Click" />
                    </form>

                      <asp:PANel ID="pnlMsg" runat="server" Visible="false">
                        <div>
                            Thanks for Apply.
                        </div>
                    </asp:PANel>
                </div>

                <!-- popup display -->
            </div>
        </div>
    </section>


    <script src="js/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>

    <script src="js/slick.min.js"></script>

    <script src="js/rem.min.js"></script>

    <script src="js/jquery.fancybox.min.js"></script>

    <script src="js/enscroll-0.6.1.min.js"></script>

    <script src="js/general.js"></script>

    <script src="js/css3-animate-it.js"></script>

    <script>

        $(".bod_slider").slick({ infinite: !0, slidesToShow: 4, slidesToScroll: 4, arrows: !1, dots: !0, autoplay: !0, autoplaySpeed: 8e3, speed: 2e3, responsive: [{ breakpoint: 991, settings: { slidesToShow: 3, slidesToScroll: 3, infinite: !0, dots: !0 } }, { breakpoint: 767, settings: { slidesToShow: 2, slidesToScroll: 2, infinite: !0, dots: !1 } }, { breakpoint: 500, settings: { slidesToShow: 1, slidesToScroll: 1, infinite: !0, dots: 1 } }] });


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href") // activated tab
            if (target == "#coc") {

                $('.coc_slider').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                    autoplay: true,
                    autoplaySpeed: 8000,
                    speed: 2000,
                    arrows: false,
                    adaptiveHeight: true


                });
            }
            else {
                $('.coc_slider').slick('unslick');
            }
        });

        jQuery(".read_btn").click(function (e) { jQuery("#open").is(":visible") ? ($(this).text("Read More..."), jQuery("#open").slideUp()) : ($(this).text("Read Less..."), jQuery("#open").slideDown()) });

        $(".tab_content").hide(), $(".tab_content:first").show(), $("ul.tabs li").click(function () { $(".tab_content").hide(); var a = $(this).attr("rel"); $("#" + a).fadeIn(), $("ul.tabs li").removeClass("active"), $(this).addClass("active"), $(".tab_drawer_heading").removeClass("d_active"), $(".tab_drawer_heading[rel^='" + a + "']").addClass("d_active") }), $(".tab_drawer_heading").click(function () { $(".tab_content").hide(); var a = $(this).attr("rel"); $("#" + a).fadeIn(), $(".tab_drawer_heading").removeClass("d_active"), $(this).addClass("d_active"), $("ul.tabs li").removeClass("active"), $("ul.tabs li[rel^='" + a + "']").addClass("active") });



        if ($(window).width() > 992) {
            ! function (l) {
                var a = l(".iframe_wrap");
                a.hide(), l(".theme_box").find("a").on("click", function () {
                    l(".theme_main").hide(), a.fadeIn(300)
                }), l(".scrollbox").enscroll({
                    showOnHover: !0,
                    verticalTrackClass: "track",
                    verticalHandleClass: "handle"
                })
            }(jQuery);
        }

    </script>


    <script>
        jQuery(document).ready(function (o) {

            jQuery(".co_tab_content").hide(), jQuery(".co_tab_content").eq(0).show(), jQuery(".co_tab_head").click(function (o) {
                0 == jQuery(this).next(".co_tab_content").is(":visible") ? (jQuery(".co_tab_content").slideUp(), jQuery(this).next(".co_tab_content").slideDown(), jQuery(this).addClass('active'), jQuery(".co_tab_head").children(".fa").addClass("fa-angle-double-down"), jQuery(this).children(".fa").removeClass("fa-angle-double-down"), jQuery(this).children(".fa").addClass("fa-angle-double-up")) : (jQuery(".co_tab_content").slideUp(), jQuery(".co_tab_head").removeClass('active'), jQuery(".co_tab_head").children(".fa").addClass("fa-angle-double-down"))
            })
        });

        $("a.join_team").click(function () { return $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top - 90 }, 1500), !1 });


    </script>

    <script>
        function InstantShowPopUp() {            
            //$.fancybox.open('<div class="fade_anim"><div class="popup_display">Thank you!</div><p style="color: cadetblue;">Thank you for contacting us, <br /> we will get back to you soon!.</p></div>');
            $.fancybox.open('<div class="popup_display"><div class="popup_title"><strong> Thank you! </strong> </div><p>Thank you for contacting us, <br /> we will get back to you soon!.</p></div>');
        };       
    </script>

</asp:Content>

