$(document).ready(function(){
	
	// Variables
	var clickedTab = $(".tab_1 > .active");
	var tabWrapper = $(".tab__content");
	var activeTab = tabWrapper.find(".active");
	var activeTabHeight = activeTab.outerHeight();
	
	// Show tab on page load
	activeTab.show();
	
	// Set height of wrapper on page load
	tabWrapper.height(activeTabHeight);
	
	$(".tab_1 > li").on("click", function() {
		
		// Remove class from active tab
		$(".tab_1 > li").removeClass("active");
		
		// Add class active to clicked tab
		$(this).addClass("active");
		
		// Update clickedTab variable
		clickedTab = $(".tab_1 .active");
		
		// fade out active tab
		activeTab.fadeOut(250, function() {
			
			// Remove active class all tabs
			$(".tab__content > li").removeClass("active");
			
			// Get index of clicked tab
			var clickedTabIndex = clickedTab.index();

			// Add class active to corresponding tab
			$(".tab__content > li").eq(clickedTabIndex).addClass("active");
			
			// update new active tab
			activeTab = $(".tab__content > .active");
			
			// Update variable
			activeTabHeight = activeTab.outerHeight();
			
			// Animate height of wrapper to new tab height
			tabWrapper.stop().delay(10).animate({
				height: activeTabHeight
			}, 200, function() {
				
				// Fade in active tab
				activeTab.delay(50).fadeIn(200);
				
			});
		});
	});
	
	
		// Variables
	var clickedTab_1 = $(".pobc_tabs > .active_1");
	var tabWrapper_1 = $(".pobc_tab__content");
	var activeTab_1 = tabWrapper_1.find(".active_1");
	var activeTabHeight_1 = activeTab_1.outerHeight();
	
	// Show tab on page load
	activeTab_1.show();
	
	// Set height of wrapper on page load
	tabWrapper_1.height(activeTabHeight_1);
	
	$(".pobc_tabs > li").on("click", function() {
		
		// Remove class from active tab
		$(".pobc_tabs > li").removeClass("active_1");
		
		// Add class active to clicked tab
		$(this).addClass("active_1");
		
		// Update clickedTab variable
		clickedTab_1 = $(".pobc_tabs .active_1");
		
		// fade out active tab
		activeTab_1.fadeOut(250, function() {
			
			// Remove active class all tabs
			$(".pobc_tab__content > li").removeClass("active_1");
			
			// Get index of clicked tab
			var clickedTabIndex = clickedTab_1.index();

			// Add class active to corresponding tab
			$(".pobc_tab__content > li").eq(clickedTabIndex).addClass("active_1");
			
			// update new active tab
			activeTab_1 = $(".pobc_tab__content > .active_1");
			
			// Update variable
			activeTabHeight_1 = activeTab_1.outerHeight();
			
			// Animate height of wrapper to new tab height
			tabWrapper_1.stop().delay(10).animate({
				height: activeTabHeight_1
			}, 200, function() {
				
				// Fade in active tab
				activeTab_1.delay(50).fadeIn(200);
				
			});
		});
	});
	
	
	
	

	

});