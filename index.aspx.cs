﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class index : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindHomeBanner();
            BindHomeServices();
        }
    }

    public void BindHomeBanner()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_HomePageBanner 'FRONT_GET'");
        if (dt.Rows.Count > 0)
        {
            rptHomeBanner.DataSource = dt;
            rptHomeBanner.DataBind();
        }
        else
        {
            rptHomeBanner.DataSource = null;
            rptHomeBanner.DataBind();
        }
    }

    public void BindHomeServices()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_HomePageServices 'Front_GET_Services'");
        if (dt.Rows.Count > 0)
        {
            rptHomeServices.DataSource = dt;
            rptHomeServices.DataBind();
        }
        else
        {
            rptHomeServices.DataSource = null;
            rptHomeServices.DataBind();
        }
    }
}