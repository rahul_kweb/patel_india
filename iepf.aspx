﻿<%@ Page Title="Investor Education and Protection Fund | Patel Integrated Logistics Ltd." Language="C#" MasterPageFile="~/HouseOfPatel.master" AutoEventWireup="true" CodeFile="iepf.aspx.cs" Inherits="iepf" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="inner_page_container">

        <div class="blue_banner">
            <div class="container ">
                 <div class="row text-uppercase iepf_head">Investor Education and Protection Fund <span>(</span>IEPF<span>)</span></div> 
	                
            </div>
        </div>


        <div class="container text_format  iepf_wrp fade_anim">
            

       <%--     <h1>Transfer of Unclaimed Dividend to IEPF </h1>
            
            <p>Pursuant to Sections 124(5) of the Companies Act, 2013 read with Rules framed thereunder (the Act), the Company is required to transfer to the Investor Education and Protection Fund (IEPF), the amount of dividend remained unclaimed for a period of seven years from the date of transfer to the Unpaid Dividend Account.</p>

            <p>Ministry of Corporate Affairs has issued Investor Education and Protection Fund Authority (Accounting, Audit, Transfer and Refund) Rules, 2016 as amended by Investor Education and Protection Fund Authority (Accounting, Audit, Transfer and Refund) Amendment Rules, 2017 (IEPF Rules). As per these Rules, the companies must identify and upload details of unclaimed dividend on their website.</p>

            <h1>  Transfer of shares to IEPF </h1>
            
            <p>Pursuant to Section 124(6) of the Act read with Rule 6 of IEPF Rules, the Company is required to transfer shares in respect of which dividend has not been paid/claimed for 7 (seven) consecutive years or more to the Demat Account of IEPF Authority.</p>

            <p>In terms of Rule 6 of IEPF Rules, the Company shall upload the details of such shareholders and the shares due for transfer to IEPF on its website.</p>

            <p>Accordingly, the relevant information relating to the unpaid dividend and shares is uploaded for the information of the shareholders.</p>

            <h1>Application to IEPF Authority for claiming unpaid dividend and shares transferred to IEPF</h1>
            
            <p>Any person whose shares, unclaimed dividend has been transferred to IEPF, may claim the shares under Section 124(6) of the Act or apply for refund under Section 125(3), as the case may be by submitting the application in Form IEPF-5 along with fee specified by the IEPF Authority from time to time in consultation with the Central Government.</p>

            <p>The access link for downloading Form IEPF-5 from the website of IEPF Authority is provided for the easy refund procedure for the shareholders.</p>

            <p> <a href="http://www.iepf.gov.in/IEPFA/refund.html" target="_blank"> http://www.iepf.gov.in/IEPFA/refund.html </a> </p>--%>

            <asp:Literal ID="ltrIepfTextContent" runat="server"></asp:Literal>
            
            <div class="pdf_attached_wrp row">
                <asp:Repeater ID="rptIepfPdf" runat="server">
                    <ItemTemplate>
                          <div class="col-sm-4 col-md-4">
            
           <a href="uploads/Iepf_pdf/<%#Eval("Pdf") %>" target="_blank" class="iepf_pdf_box">
            <i class="fa fa-file-pdf-o" aria-hidden="true"></i> <%#Eval("Title") %></a>
            </div>
                    </ItemTemplate>
                </asp:Repeater>
         <%--   <div class="col-sm-4 col-md-4">
            
           <a href="images/pdf/iepf/Details_of_Nodal_Officer.pdf" target="_blank" class="iepf_pdf_box">
            <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Details of Nodal Officer </a>
            </div>
            <div class="col-sm-4 col-md-4">
            
           <a href="images/pdf/iepf/Equity_Shares_liable_for_credit_to_IEPF_Authority_during_2018_19.pdf" target="_blank" class="iepf_pdf_box">
            <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Equity Shares liable for credit to IEPF Authority during 2018-19 </a></div>
            <div class="col-sm-4 col-md-4">
            
            <a href="images/pdf/iepf/Newspaper_Advertisement.pdf" target="_blank" class="iepf_pdf_box">
            <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Newspaper Advertisement </a></div>--%>

            </div>
            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    
   <script src="js/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>

    <script src="js/slick.min.js"></script>

    <script src="js/rem.min.js"></script>

    <script src="js/pushy.min.js"></script>

    <script src="js/general.js"></script>

    <script src="js/css3-animate-it.js"></script>
</asp:Content>

