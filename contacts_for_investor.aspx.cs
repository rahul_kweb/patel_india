﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class contacts_for_investor : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        BindInnerBanner();
        BindCareerContent();
    }

    public void BindInnerBanner()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner'GET_BY_ID',1005");

        if (dt.Rows.Count > 0)
        {
            StringBuilder strbuild = new StringBuilder();
            strbuild.Append("<div class=\"row inner_banner_wrp\" style=\"background:url(uploads/Images/" + dt.Rows[0]["InnerBannerImage"].ToString() + ") no-repeat center; background-size:cover;\">");
            strbuild.Append("<div class=\"inner_page_title\">");
            strbuild.Append("<div class=\"inner_page_title_txt text-center\">");
            strbuild.Append("<h1>contacts for investor</h1>");
            strbuild.Append("<ul class=\"breadcrumb\">");
            strbuild.Append("<li><a href=\"index.aspx\">Home</a></li>");
            strbuild.Append("<li>Investor Relation</li>");
            strbuild.Append("</ul>");
            strbuild.Append("</div>");
            strbuild.Append("</div>");
            strbuild.Append("</div>");

            ltrlBanner.Text = strbuild.ToString();
        }
    }

    public void BindCareerContent()
    {
        DataTable dt = new DataTable();
        try
        {
            dt = utility.Display("Exec Proc_CMS 'GET_BY_ID',3");
            if (dt.Rows.Count > 0)
            {
                ltrlCareerContent.Text = dt.Rows[0]["CMS_DESC"].ToString();
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;

        }


    }
}