﻿public static class AppKeys
{
    public static string SESSION_ADMIN_USERID_KEY = "admin_id";
    public static string SESSION_ADMIN_USERNAME_KEY = "admin_user";
    public static string SESSION_CARD_INFO_PASSWORD = "card__info_password";

    public static string SESSION_CAPTCHA_KEY = "captcha_key_01";
}