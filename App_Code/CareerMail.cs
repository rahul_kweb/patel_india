﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for CareerMail
/// </summary>
public class CareerMail
{
    //string strFrom = "ho_hr@patel-india.com";
    string Subject = "Current  Opening ";
    //string strMailUserName = "noreply@kwebmaker.com";
    //string strMailPassword = "noreply123";
     
    //string strMailUserName = "prl_autobilling@patel-india.com";
    //string strMailPassword = "EWG$212";

    string strMailUserName = "";
    string strMailPassword = "";

	public CareerMail()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int SendEmail(string Body, string EmailTo)
    {

        int sent = 0;
        try
        {

            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();
            //mailClient.Host = "smtpout.secureserver.net";
            //mailClient.Port = 25;

            mailClient.Host = "mail.patel-india.com";
            mailClient.Port = 587;



            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = false;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            //string strFromMail = strFrom;
            string strFromMail = EmailTo;
            MailAddress fromAddress = new MailAddress(strFromMail, "House Of Patel");
            message.From = fromAddress;
       
            message.Priority = System.Net.Mail.MailPriority.High;

            //emailaddress of house of patel 
            //message.To.Add("ho_hr@patel-india.com");
            message.To.Add("ho_hr@patel-india.com");

            //message.To.Add(EmailTo);
            //message.To.Add("balasaheb.phapale@patel-india.com");
       
            
          
            
            message.Subject = Subject;
         


            message.Body = Body;
            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;

            sent = 1;

        }
        catch (Exception ex)
        {
            sent = 0;
        }

        return sent;
    }

    public bool EmailCareersMail(string FirstName, string LastName, string ContactNo, string JobProfile, string Email)
    {
        bool blnRetVal = false;
        try
        {
            System.Text.StringBuilder strEmailBuilder = new System.Text.StringBuilder();

            strEmailBuilder.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
            strEmailBuilder.AppendFormat("<tr> <td colsPAN=\"2\" align=\"center\" bgcolor=\"#0d1740\"><strong><font color=\"#FFFFFF\"> Careers from Patel Integrated Logistics Ltd. Website</font></strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td width=\"200px\"><strong>First Name : </strong></td><td> " +FirstName + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Last Name : </strong></td> <td> " + LastName + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Contact No : </strong></td> <td> " + ContactNo + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Job Profile : </strong></td> <td> " + JobProfile + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Email Id : </strong></td> <td> " + Email + " </td></tr>");         
            strEmailBuilder.Append("</table>");

            SendEmail(strEmailBuilder.ToString(),Email);


            blnRetVal = true;
        }
        catch (Exception ex)
        {
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }
}