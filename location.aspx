﻿<%@ Page Title="Strongest Logistics Network in India | Patel Integrated Logistics Ltd" Language="C#" MasterPageFile="~/HouseOfPatel.master" AutoEventWireup="true" CodeFile="location.aspx.cs" Inherits="location" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <section>

        <div class="container-fluid">

            <div class="row inner_banner_wrp map_wrp"></div>

            <div class="row location_wrp wrapper">

                <asp:Literal ID="ltrlRegOffice" runat="server"></asp:Literal>

                <%--<div class="col-md-12 col-sm-12 col-xs-12 head_off_address row">
                    <div class="inner_title">
                        <h1>Registered Office</h1>
                    </div>
                    <p>PATEL INTEGRATED LOGISTICS LTD., PATEL HOUSE, 48 Gazdar Bandh, North Avenue Road, Santacruz (West), Mumbai, India – 400 054</p>
                </div>--%>

                <div class="clearfix"></div>

<%--                <div class="col-md-12 col-sm-12 col-xs-12 no_padding">

                    <div id="horizontalTab">

                        <div class="regional_wrp_head">

                            <div class="regional_wrp_head_txt pull-left">
                                <h2>Patel Roadways Branches</h2>
                            </div>

                            <div class="address_search pull-right">

                                <input type="text" id="txtSearhPatelRoadWays" placeholder="Search" class="form-control" /><span class="text-center fade_anim" onclick="SearchPatelRoadWays();"><i class="fa fa-search" aria-hidden="true"></i></span>
                            </div>

                            <div class="clearfix"></div>

                        </div>

                        <div id="divPatelRoadWays">
                            <asp:Literal ID="ltrPatelRoadWays" runat="server"></asp:Literal>
                        </div>


                    </div>--%>

                    <div class="clearfix"></div>

                    <div id="horizontalTab2">

                        <div class="regional_wrp_head">

                            <div class="regional_wrp_head_txt pull-left">
                                <h2>Patel Airfreight Branches</h2>
                            </div>

                            <div class="address_search pull-right">

                                <input type="text" id="txtSearchPatelAirfrieght" placeholder="Search" class="form-control"><span class="text-center fade_anim" onclick="SearchAirAfrieght();"><i class="fa fa-search" aria-hidden="true"></i></span>

                            </div>
                            <div class="clearfix"></div>

                        </div>

                        <div id="divPatelAirfried">
                            <asp:Literal ID="ltrPatelAirfrieght" runat="server"></asp:Literal>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- js starts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/rem.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQbt8fBXZu0RHPqPCRM3GPMRklOt-9Als&callback=initMap"></script>
    <script src="js/mapmarker.jquery.js"></script>
    <script src="js/easy-responsive-tabs.js"></script>
    <script src="js/equate.js"></script>

    <script src="js/general.js"></script>



    <script>
        $(document).ready(function () {

            $('#horizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion           
                width: 'auto', //auto or any width like 600px
                fit: true,   // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#tabInfo');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });
            $('.resp-tab-content-active').css('display', 'none');
            // location div show and hide

            //$("#horizontalTab li").on("click", function () {
            //    //e.preventDefault();
            //    $('a').css("border-bottom", 'none');

            //    $("#horizontalTab li a").css("border-bottom", '3px solid');
            //    $('#abovediv').css('display', 'block');
            //    equalheight('.equate');
            //})

            //$(".belowdiv").on("click", function (e) {
            //    //e.preventDefault();
            //    $('a').css("border-bottom", 'none');
            //    $(this).css("border-bottom", '3px solid');
            //    $('#belowdiv').css('display', 'block');
            //    equalheight('.equate');
            //})

        });
    </script>

    <script>
        $(document).ready(function () {
            $('#horizontalTab2').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion           
                width: 'auto', //auto or any width like 600px
                fit: true,   // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#tabInfo');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }

            });
            $('.resp-tab-content-active').css('display', 'none');
        });
    </script>


    <script>

        //function SearchPatelRoadWays() {
        //    var SearchVal = $("#txtSearhPatelRoadWays").val();
        //    $.ajax({
        //        type: "POST",
        //        url: "location.aspx/SearchPatel",
        //        data: '{Value: "' + SearchVal + '" }',
        //        contentType: "application/json; charset=utf-8",
        //        dataType: "json",
        //        success: OnSuccess,
        //        failure: function (response) {
        //            alert(response.d);
        //        }
        //    });
        //}


        //$('#txtSearhPatelRoadWays').keyup(function (e) {
        //    var SearchVal = $("#txtSearhPatelRoadWays").val();
        //    //console.log(SearchVal);
        //    if (e.keyCode == 8 || e.keyCode == 46) {
        //        $.ajax({
        //            type: "POST",
        //            url: "location.aspx/SearchPatel",
        //            data: '{Value: "' + SearchVal + '" }',
        //            contentType: "application/json; charset=utf-8",
        //            dataType: "json",
        //            success: OnSuccess,
        //            failure: function (response) {
        //                alert(response.d);
        //            }
        //        });
        //    }

        //})



        //function OnSuccess(response) {
        //    $("#divPatelRoadWays").html(response.d);

        //    $('#horizontalTab').easyResponsiveTabs({
        //        type: 'default', //Types: default, vertical, accordion           
        //        width: 'auto', //auto or any width like 600px
        //        fit: true,   // 100% fit in a container
        //        closed: 'accordion', // Start closed if in accordion view
        //        activate: function (event) { // Callback function if tab is switched
        //            var $tab = $(this);
        //            var $info = $('#tabInfo');
        //            var $name = $('span', $info);
        //            $name.text($tab.text());
        //            $info.show();
        //        }
        //    });
        //    $('.resp-tab-content-active').css('display', 'none');
        //}





        function SearchAirAfrieght() {
            var SearchVal = $("#txtSearchPatelAirfrieght").val();
            $.ajax({
                type: "POST",
                url: "location.aspx/SearchPatelAirfried",
                data: '{Value: "' + SearchVal + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess1,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        $('#txtSearchPatelAirfrieght').keyup(function (e) {
            var SearchVal = $("#txtSearchPatelAirfrieght").val();
            //console.log(SearchVal);
            if (e.keyCode == 8 || e.keyCode == 46) {
                $.ajax({
                    type: "POST",
                    url: "location.aspx/SearchPatelAirfried",
                    data: '{Value: "' + SearchVal + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnSuccess1,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

        })



        function OnSuccess1(response) {
            $("#divPatelAirfried").html(response.d);

            $('#horizontalTab2').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion           
                width: 'auto', //auto or any width like 600px
                fit: true,   // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#tabInfo');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });
            $('.resp-tab-content-active').css('display', 'none');
        }

    </script>

  <script type="text/javascript">
      $(document).ready(function () { var a = { markers: [{ latitude: "19.078047", longitude: " 72.829500", icon: "images/pointer.png", baloon_text: "<strong> PATEL HOUSE </strong> <p> 48, Gazdar Bandh, North Avenue Road,<br> Santacruz (West), Mumbai, India – 400 054 </p>"}] }; $(".map_wrp").mapmarker({ zoom: 19, center: "19.078047, 72.829500", markers: a }) });
    </script>

    <script>

        $('#horizontalTab li').click(function () {

            if ($('.resp-tab-content-active').is(':visible')) {
                $('.resp-tab-content-active').slideUp();
                //$('.resp-tabs-list li a:after').css({ 'background': '#fff' });
                //$('.resp-tabs-list li a').css('border-bottom', '3px solid red');
                //$(this).css('border', '3px solid black');
                //$(this).children('a').removeClass("border_next");


            }
            $(this).children('a').toggleClass("border_next");
            $(this).siblings('li').children('a').removeClass("border_next");
            $(this).parents('#horizontalTab').siblings('#horizontalTab2').find('a').removeClass("border_next");
            //console.log($(this).parents('#horizontalTab').siblings());
        })

        $('#horizontalTab2 li').click(function () {

            if ($('.resp-tab-content-active').is(':visible')) {
                $('.resp-tab-content-active').slideUp();
                //$('.resp-tabs-list li a:after').css({ 'background': '#fff' });
                //$('.resp-tabs-list li a').css('border-bottom', '3px solid red');
                //$(this).css('border', '3px solid black');

                //$(this).children('a').removeClass("border_next");

            }

            $(this).children('a').toggleClass("border_next");
            $(this).siblings('li').children('a').removeClass("border_next");
            $(this).parents('#horizontalTab2').siblings('#horizontalTab').find('a').removeClass("border_next");

        })


    </script>
    <!-- js ends -->

</asp:Content>




