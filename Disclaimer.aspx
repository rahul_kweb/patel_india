﻿<%@ Page Title="Disclaimer" Language="C#" MasterPageFile="~/HouseOfPatel.master" AutoEventWireup="true" CodeFile="Disclaimer.aspx.cs" Inherits="Disclaimer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

        <div class="inner_page_container">

        <div class="blue_banner">
            <div class="container">
                 Disclaimer
	                <span style="display: inline-block; text-transform: none;">
                        <asp:Label ID="lblTittle" runat="server"></asp:Label>
                    </span>
            </div>
        </div>
        <div class="clear"></div>

        <div class="container text_format fade_anim disclaimer_wrp">
<p>
    The information and the data contained in this website is only for general information to the public about PATEL INTEGRATED LOGISTICS Limited, its subsidiaries and associates (hereinafter referred to as PATEL INTEGRATED LOGISTICS Limited) and the services that they offer is not in any way binding on PATEL INTEGRTAED LOGISTICS Limited. The information being shown through this website may not be construed as advised from PATEL INTEGRATED LOGISTICS Limited and should not be relied upon in making or desisting from making any decision.
</p>
            <p>
PATEL INTEGRATED LOGISTICS Limited hereby excludes any warranty (express or implied) as to the correctness, accuracy, completeness, timeliness for a particular purpose of the website or any of its contents.
            </p>
            <p>
                By having access to this web site you shall agree that PATEL INTEGRATED LOGISTICS Limited will not be liable for any direct, indirect, consequential, exemplary or remote damages or compensation arising out of the use of the information and data contained in the website or from the access of other material on the internet via web links for this web site.
            </p>
            <p>
                The movement of cargo and courier by multi – modal transport system or any value added services being offered by PATEL INTEGRATED LOGISTICS Limited to the general public will be subject to the conditions of carriage,docket terms & conditions or any change thereof from time to time which may be accessed or seen on request by approaching or contacting the PATEL ROADWAYS / PATEL AIRFREIGHT local office.
            </p>
             <p>
                 Unless otherwise stated, copy right or trade mark or any other similar right in all materials contained in this web site is the property of PATEL INTEGRATED LOGISTICS Limited and the same is owned by PATEL INTEGRATED LOGISTICS Limited. Any access to it by the general public does not imply free license to use it unless permitted by law.
            </p>
            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

   <script src="js/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>

    <script src="js/slick.min.js"></script>

    <script src="js/rem.min.js"></script>

    <script src="js/pushy.min.js"></script>

    <script src="js/general.js"></script>

    <script src="js/css3-animate-it.js"></script>

</asp:Content>

