﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class about_us : System.Web.UI.Page
{
    Utility utility = new Utility();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindInnerBanner();
            BindAboutUsData();
            BindTestimonial();
            BindWhyUs();
            RepeaterPrivacyPolicies();
            ReapeaterBoarCommittee();
            BindCompositionCommittees();
            BindQualityPolicy();
        }

    }

    public void BindInnerBanner()
    {
        dt = utility.Display("Exec Proc_InnerBanner'GET_BY_ID',1");

        if (dt.Rows.Count > 0)
        {


            StringBuilder strbuild = new StringBuilder();
            strbuild.Append("<div class=\"row inner_banner_wrp\" style=\"background:url(uploads/Images/" + dt.Rows[0]["InnerBannerImage"].ToString() + ") no-repeat center; background-size:cover;\">");
            strbuild.Append("<div class=\"inner_page_title\">");
            strbuild.Append("<div class=\"inner_page_title_txt text-center\">");
            strbuild.Append("<h1>About Us</h1>");
            strbuild.Append("<ul class=\"breadcrumb\">");
            strbuild.Append("<li><a href=\"index.aspx\">Home</a></li>");
            strbuild.Append("<li>About Us</li>");
            strbuild.Append("</ul>");
            strbuild.Append("</div>");
            strbuild.Append("</div>");
            strbuild.Append("</div>");

            ltrlBanner.Text = strbuild.ToString();
        }

    }


    public void BindAboutUsData()
    {
        dt = utility.Display("Exec Proc_CMS 'GET_BY_ID',1 ");
        if (dt.Rows.Count > 0)
        {
            ltrUpperAboutus.Text = dt.Rows[0]["CMS_Desc"].ToString();
        }
    }

    public void BindTestimonial()
    {
        dt = utility.Display("Exec Proc_CMS 'GET_BY_ID',5");
        if (dt.Rows.Count > 0)
        {
            ltrlTestimonial.Text = dt.Rows[0]["CMS_Desc"].ToString();
        }
    }


    public void BindWhyUs()
    {
        dt = utility.Display("Exec Proc_CMS 'GET_BY_ID',6");
        if (dt.Rows.Count > 0)
        {
            ltrlWhyUs.Text = dt.Rows[0]["CMS_Desc"].ToString();
        }
    }


    public void RepeaterPrivacyPolicies()
    {
        dt = utility.Display("exec Proc_AboutUsPolicies 'Fornt_Get'");
        if (dt.Rows.Count > 0)
        {
            rptPrivacyPolicy.DataSource = dt;
            rptPrivacyPolicy.DataBind();
        }
        else
        {
            rptPrivacyPolicy.DataSource = null;
            rptPrivacyPolicy.DataBind();
        }

    }


    public void ReapeaterBoarCommittee()
     {
        dt = utility.Display("Exec Proc_AboutUsBoardCommetees 'Fron_Get'");

        if (dt.Rows.Count > 0)
        {
            rptboardcommittee.DataSource = dt;
            rptboardcommittee.DataBind();
        }
        else 
        {
            rptboardcommittee.DataSource = null;
            rptboardcommittee.DataBind();
        }
    }


    public void BindCompositionCommittees()
    {
        dt = utility.Display("Exec Proc_CMS 'GET_BY_ID',7");
        if (dt.Rows.Count > 0)
        {
            ltrlCompositionCommittee.Text = dt.Rows[0]["CMS_Desc"].ToString(); 
        
        }
    }


    public void BindQualityPolicy()
    {
        dt = utility.Display("Exec Proc_CMS 'GET_BY_ID',8");
        if (dt.Rows.Count > 0)
        {
            ltrlQualityPolicy.Text = dt.Rows[0]["CMS_Desc"].ToString();

        }
    }
}