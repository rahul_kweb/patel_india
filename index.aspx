﻿<%@ Page Title="Top 10 Logistics Companies in India | Tracking | Customised logistics & Supply Chain Management for Corporates | First Multimodal Transport in India - Patel Integrated Logistics Ltd" Language="C#" MasterPageFile="~/HouseOfPatel.master" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

 
      
    <section>

        <div class="container-fluid">

            <div class="row animatedParent" style="position: relative;">

                <div class="banner_wrp">
                    <asp:Repeater ID="rptHomeBanner" runat="server" >
                        <ItemTemplate>
                            <div>
                                <div class="banner_content">
                                    <img src="uploads/Images/<%#Eval("HomePageImage")%>" alt="">
                                    <div class="banner_txt">
                                 <%#Eval("HomePageDescription")%>
                                        <a href='/<%#Eval("Url") %>'>Know More</a>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    </div>



                    <%--<div>
                        <div class="banner_content">
                            <img src="images/banner_slider/01.jpg" alt="">
                            <div class="banner_txt">REDEFINING LOGISTICS.<br>
                                <span>With World-Class Service.</span><a href="patel_roadways.aspx">Know More</a></div>
                        </div>
                    </div>

                    <div>
                        <div class="banner_content">
                            <img src="images/banner_slider/04.jpg" alt="">
                            <div class="banner_txt">NO ONE KNOWS INDIA
                                <br>
                                <span>LIKE WE DO.</span><a href="about_us.aspx?know_more">Know More</a></div>
                        </div>
                    </div>

                    <div>
                        <div class="banner_content">
                            <img src="images/banner_slider/02.jpg" alt="">
                            <div class="banner_txt">Perfection. Persistance. Prosper.
                                <br>
                                <span>For a Better India.</span> <a href="about_us.aspx">Know More</a></div>
                        </div>
                    </div>--%>
                

                <div class="track_form animated fadeInDownShort slow">

                    <ul class="nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#track" aria-controls="track" role="tab" data-toggle="tab">Track</a></li>
                        <li role="presentation"><a href="" aria-controls="quote_us" onclick="ShowQuoteUs()">quote us</a></li>

                    </ul>

                    <div class="tab-content">

                        <div role="tabPanel" class="tab-PANe active" id="track">

                            <form>

                                <div class="form-group gc_no">
                                    <input type="text" class="form-control" id="gc_input" placeholder="ENTER YOUR GC NO."/>

                                    <button type="button" class="btn btn-primary fade_anim" id="track_btn" onclick="Show()">Submit</button>

                                    <a target="_blank" class="fade_anim btn-primary pod">View POD </a>

                                </div>

                                <div class="note"><strong>NOTE :</strong> Scanned copy of POD will be available after 48 hrs of Delivery. <br />

                                  <strong> DOCUMENTS REQUIRED AT THE TIME OF BOOKING :</strong>

                                   <%-- <ol>

                                        <li>THREE COPIES OF INVOICES</li>
                                        <li>ONE COPY OF E-WAY BILL</li>
                                    </ol>--%>

                                    <p> Three Copies Of Invoices &nbsp; | &nbsp; One Copy Of E-WAY Bill <%--THREE COPIES OF INVOICES &nbsp; | &nbsp; ONE COPY OF E-WAY BILL--%> </p>
                                      
                                </div>

                            </form>

                        </div>

                    

                    </div>

                </div>

            </div>

        </div>

        <div class="container-fluid">

            <div class="row wrapper">

                <div class="col-xs-12 banner_below_txt text-center no_padding">


                    <strong><span>India's 1<sup>st</sup> multimodal transport operator. </span>
                        <br />
                    </strong>
                    Providing all logistics solutions under one roof.
                
                </div>

            </div>





        </div>

        <div class="container-fluid">

            <div class="row wrapper">

                <div class="col-xs-12 service_wrp" id="enquiry_form">

                    <div class="row">

                        <asp:Repeater ID="rptHomeServices" runat="server">
                            <ItemTemplate>
                                 <div class="col-xs-6 col-sm-6 col-md-3 animatedParent service_box_wrp">


                            <div class="service_box animated fadeInLeftShort">

                                <a href='/<%#Eval("OurServicesUrl") %>'>


                                    <div class="service_logo violet">
                                        <img src="uploads/images/<%#Eval("OurServicesLogo") %>" class="img-responsive" alt="">
                                    </div>

                                    <div class="service_box_pic_wrp">
                                        <img src="uploads/images/<%#Eval("OurServicesImage") %>" alt="">
                                    </div>

                                </a>

                                <div class="service_box_content">

                                    <div class="service_box_content_head">
                                        <a href="patel_roadways.aspx">
                                            <h1><%#Eval("OurServicesTitle") %></h1>
                                        </a>
                                    </div>

                                    <div class="service_box_content_text">
                                        <p><%#Eval("OurServicesDescription") %></p>
                                    </div>

                                    <a href='/<%#Eval("OurServicesUrl") %>'>View Details <span>
                                        <img src="images/right_arrow.png" alt="" class="img-responsive"></span></a>


                                </div>

                            </div>

                        </div>
                            </ItemTemplate>
                        </asp:Repeater>

                    


                    </div>


                </div>

            </div>

            <div class="col-xs-12 no_padding welcome_note text-center animatedParent animateOnce" data-sequence='500'>
             
                
                <div class="welcome_note_content">

                <div class="animated fadeIn fade_style" data-id='1'><span class="coma open_cl">"</span>Trusted</div>
                <div class="animated fadeIn fade_style" data-id='2'>People. </div>
                <div class="animated fadeIn fade_style" data-id='3'>Sensible </div>
                <div class="animated fadeIn fade_style" data-id='4'>Solutions.<span class="coma close_cl">"</span></div>
 
                
                </div>
            </div>

            <div class="col-xs-12 no_padding wheel_wrp" style="display: none;">

                <div class="title text-center">
                    <h1>THE dummy text here</h1>
                </div>

                <div class="wheel_content animatedParent animateOnce" data-appear-top-offset='-300'>

                    <span class="slow wheel animated">
                        <img src="images/wheel.png" alt="" class="img-responsive animated rotateIn delay-250 slow"></span>

                    <div class="box_one animated fadeIn" data-id='1'>
                        adasda
                    </div>

                    <div class="box_two animated fadeIn">
                        adasda
                    </div>

                    <div class="box_three animated fadeIn">
                        adasda
                    </div>

                    <div class="box_four animated fadeIn">
                        adasda
                    </div>

                    <div class="box_five animated fadeIn">
                        adasda
                    </div>

                    <div class="box_six animated fadeIn">
                        adasda
                    </div>

                </div>

            </div>

        </div>
       
        </div>


        
    </section>


    <script src="js/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>

    <script src="js/slick.min.js"></script>

    <script src="js/rem.min.js"></script>

    <script src="js/pushy.min.js"></script>

     <script src="js/jquery.fancybox.min.js"></script>

    <script src="js/general.js"></script>

    <script src="js/css3-animate-it.js"></script>



    <script>

        $(document).ready(function () { $(".banner_wrp").slick({ slidesToShow: 1, slidesToScroll: 1, autoplay: !0, autoplaySpeed: 5e3, speed: 1000, arrows: !1, dots: !0, fade: !0, cssEase: "linear", zIndex: 1e3 }) });

        $("#door_delivery_check").click(function (e) { 1 == $(this).is(":checked") ? $(".door_delivery").slideDown() : $(".door_delivery").slideUp() });

    </script>
    <script type="text/javascript" language="javascript">
        function Show() {

            if ($("#gc_input").val() == "") {
                alert("Please Enter Your GC Number");
            }
            else {
                window.open("http://180.92.165.253/CustomerTracking/CustGCNTrackingNewWebsite.aspx?GCN_No=" + $("#gc_input").val() + "");

            }
        }

        function ShowQuoteUs() {
            window.open("http://180.92.165.253/CustomerTracking/QuoteUsNewWebSite.aspx");

        }
    </script>
    <script>
        $(document).ready(function () {
            $('.pod').click(function (e) {

                e.preventDefault();
                if ($("#gc_input").val() == "") {
                    alert("Please Enter Your POD Number");
                }
                else {
                    window.open("http://180.92.165.254/POD_HO/" + $("#gc_input").val() + ".pdf");

                }

            })
        });


    </script>



    <script>$(window).load(function () {
    //$("#myModal").modal("show");

    $(document).ready(function () {
        $('.fancybox').fancybox();

        var data = sessionStorage.getItem('firstvisit');

        if (data == null) {

            sessionStorage.setItem('firstvisit', 'no');
            $("#myModal").modal("show");

        }
        else if (data = 'no') {
            // alert("sdf");
        }
    });

});

       

    </script>




</asp:Content>

