﻿<%@ Page Title="Full Truck Load, Less Than Truck Load, Bulk Booking in India | PATEL Roadways" Language="C#" MasterPageFile="~/HouseOfPatel.master" AutoEventWireup="true" CodeFile="patel_roadways.aspx.cs" Inherits="patel_roadways" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--    <link rel="shortcut icon" href="images/fav_icon.png">
<!-- css starts -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/animations.css">
<link rel="stylesheet" href="css/slick.css">
<link rel="stylesheet" href="css/slick-theme.css">
<link rel="stylesheet" href="css/nav-layout.min.css">
<link rel="stylesheet" href="css/jquery.fancybox.min.css">
<link rel="stylesheet" href="css/general.css">
<link rel="stylesheet" href="css/inner_page.css">--%>
    <section>

        <div class="container-fluid">

            <asp:Literal ID="ltrlBanner" runat="server"></asp:Literal>

            

            <div class="row services_wrp wrapper">

                <%--<div class="services_wrp_logo"><img src="images/services/service_logo/01.jpg" class="img-responsive" alt=""></div>--%>

                <div class="services_wrp_logo">
                    <img id="imagereview" runat="server" src="" class="img-responsive" /></div>
                <asp:Literal ID="ltrlcontent" runat="server"></asp:Literal>

         
            </div>

        </div>

    </section>



    <script src="js/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>

    <script src="js/rem.min.js"></script>

    <script src="js/general.js"></script>

    <script src="js/css3-animate-it.js"></script>
</asp:Content>

