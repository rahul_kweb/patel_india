﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class iepf : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindIepfTextContent();
            rptBindPdf();
        }
    }


    public void BindIepfTextContent()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'GET_BY_ID',1001");
        if (dt.Rows.Count > 0)
        {
            ltrIepfTextContent.Text = dt.Rows[0]["CMS_Desc"].ToString();
        }
    }

    public void rptBindPdf()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("exec Proc_IepfPdf 'Fornt_Get'");
        if (dt.Rows.Count > 0)
        {
            rptIepfPdf.DataSource = dt;
            rptIepfPdf.DataBind();
        }
        else
        {
            rptIepfPdf.DataSource = null;
            rptIepfPdf.DataBind();
        }
    }
}