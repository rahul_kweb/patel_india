﻿<%@ Page Title="Investor Relations | Logistics Quarterly Results, Dividend, Annual Reports | Patel Integrated Logistics Ltd" Language="C#" MasterPageFile="~/HouseOfPatel.master" AutoEventWireup="true" CodeFile="contacts_for_investor.aspx.cs" Inherits="contacts_for_investor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <section>

        <div class="container-fluid">

            <asp:Literal ID="ltrlBanner" runat="server"></asp:Literal>

<%--            <div class="row inner_banner_wrp" style="background: url(images/inner_banner/investor_relation.jpg) no-repeat center; background-size: cover;">

                <div class="inner_page_title">
                    <div class="inner_page_title_txt text-center">
                        <h1>contacts for investor</h1>
                        <ul class="breadcrumb">
                            <li><a href="index.aspx">Home</a></li>
                            <li>Investor Relation</li>
                        </ul>
                    </div>
                </div>
            </div>--%>

             <asp:Literal ID="ltrlCareerContent" runat="server"></asp:Literal>

     <%--       <div class="row contact_wrp wrapper">


                <div class="col-md6 col-sm-6 col-xs-12 investor_content_wrp fade_anim equate"> 

                    <h1>For Shareholder matters please contact our Share Transfer Agent :</h1>


                    <h2>BIGSHARE SERVICES PVT. LTD</h2>

                    <p>
                        Unit: Patel Integrated Logistics Limited<br>
                        E-2, Ansa Industrial Estate,
                        <br>
                        Sakivihar Road, Saki Naka, Andheri (E.),<br>
                        Mumbai-400072<br>
                        <strong>Tel : </strong>+91 (22) 40430200<br>
                        <strong>Email : </strong><a href="mailto:investor@bigshareonline.com">investor@bigshareonline.com</a><br>
                        <strong>Website : </strong><a href="http://www.bigshareonline.com/" target="_blank">www.bigshareonline.com</a>
                    </p>

                </div>

                <div class="col-md6 col-sm-6 col-xs-12 investor_content_wrp fade_anim equate">

                    <h1>For Compliance and Shareholder Matters Please Contact : </h1>

                    <h2>NITIN B. AKOLKAR</h2>

                    <p>
                        Company Secretary<br>
                        <strong>Tel :</strong> +91 (22) 26050021<br>
                        <strong>Email :</strong> <a href="mailto:pill_investorservices@patel-india.com">pill_investorservices@patel-india.com</a>
                    </p>

                </div>





            </div>--%>

        </div>

    </section>

    <!-- js starts -->

    <script src="js/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>

    <script src="js/rem.min.js"></script>

    <script src="js/equate.js"></script>

    <script src="js/general.js"></script>

    <script src="js/css3-animate-it.js"></script>

    <script type="text/javascript">
        $(document).ready(function () { var a = { markers: [{ latitude: "19.078018", longitude: "72.828999", icon: "images/pointer.png", baloon_text: "<strong> PATEL HOUSE </strong> <p> 48, Gazdar Bandh, North Avenue Road,<br> Santacruz (West), Mumbai, India – 400 054 </p>" }] }; $(".map_wrp").mapmarker({ zoom: 17, center: "19.078018, 72.828999", markers: a }) });
    </script>


    <!-- js ends -->

</asp:Content>

