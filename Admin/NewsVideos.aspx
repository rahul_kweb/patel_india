﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="NewsVideos.aspx.cs" Inherits="Admin_NewsVideos" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" Runat="Server">

    <div class="page-header">
        Videos
    </div>
      <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />

    <div class="form-box" style="width: 500px;">

      <asp:HiddenField ID="hdnId" runat="server" />

      <%--  <asp:Label ID="label1" class="control-label" runat="server">Video Title</asp:Label>
    <asp:TextBox ID="txtVideoTitle" runat="server"></asp:TextBox>--%>

        <asp:Label ID="Label" class="control-label" runat="server">
            <label class="control-label">News Videos</label>
        </asp:Label>
    <br/>
          <asp:TextBox ID="txtNewsVideo" runat="server" CssClass="textbox" Width="400px"></asp:TextBox>
        <br />
        <img alt="" src="../images/youtube_id.jpg" />
        <br />

    <br/>
    <br />
    <br />
    <br />
       
      <asp:Button ID="btnSave" Text="Save" CssClass="button" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="Cancel" CssClass="button" runat="server" OnClick="btnCancel_Click" />
    <br />
    <br />
    <br />
    <br />
   
   </div>
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
            OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging"
            CssClass="mGrid" PageSize="10" AllowPaging="true">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" />
               <%--  <asp:BoundField DataField="VideoTitle" HeaderText="VideoTitle" />--%>
                         <asp:TemplateField HeaderText="News VideoLink" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <object width="136" height="100" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
                            codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0">
                            <param name="allowFullScreen" value="true">
                            <param name="allowscriptaccess" value="always">
                            <param name="src" value="http://www.youtube.com/v/<%# Eval("NewsVideosLink") %>?modestbranding=1;version=3&amp;hl=en_US&amp;rel=0&amp;hd=1&amp;autohide=1&amp;showinfo=0&amp;controls=0">
                            <param name="allowfullscreen" value="true">
                            <param name="wmode" value="opaque">
                            <embed width="136" height="100" type="application/x-shockwave-flash" src="http://www.youtube.com/v/<%# Eval("NewsVideosLink") %>?modestbranding=1;version=3&amp;hl=en_US&amp;rel=0&amp;hd=1&amp;autohide=1&amp;showinfo=0&amp;controls=0"
                                allowfullscreen="true" allowscriptaccess="always" wmode="opaque">
                        </object>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                            CommandName="Select">
                            <i class="icon-edit"></i> Edit
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                            CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>

</div>
</asp:Content>

