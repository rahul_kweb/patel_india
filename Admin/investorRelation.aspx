﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="investorRelation.aspx.cs" Inherits="Admin_brand" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Investor Information
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Investor Relation<span class="required">*</span> :
        </label>
        <asp:DropDownList ID="ddlPageName" runat="server" CssClass="dropdown">
            <asp:ListItem Value="0" Text="Select Page"></asp:ListItem>
            <asp:ListItem>SHARE HOLDING PATTERN</asp:ListItem>
            <asp:ListItem>QUARTERLY RESULTS</asp:ListItem>
            <asp:ListItem>ANNUAL REPORTS</asp:ListItem>
            <asp:ListItem>CORPORATE ANNOUNCEMENTS</asp:ListItem>
            <asp:ListItem>UNCLAIMED DIVIDEND</asp:ListItem>
        </asp:DropDownList>
        <br />
        <div id="divCategory" runat="server">
            <label class="control-label">
                FromYear<span class="required">*</span> :
            </label>
            <asp:DropDownList ID="ddlFromYear" runat="server" CssClass="dropdown">
                <asp:ListItem Value="0" Text="Select"></asp:ListItem>
                <asp:ListItem>2025</asp:ListItem>
                <asp:ListItem>2024</asp:ListItem>
                <asp:ListItem>2023</asp:ListItem>
                <asp:ListItem>2022</asp:ListItem>
                <asp:ListItem>2021</asp:ListItem>
                <asp:ListItem>2020</asp:ListItem>
                <asp:ListItem>2019</asp:ListItem>
                <asp:ListItem>2018</asp:ListItem>
                <asp:ListItem>2017</asp:ListItem>
                <asp:ListItem>2016</asp:ListItem>
                <asp:ListItem>2015</asp:ListItem>
                <asp:ListItem>2014</asp:ListItem>
                <asp:ListItem>2013</asp:ListItem>
                <asp:ListItem>2012</asp:ListItem>
                <asp:ListItem>2011</asp:ListItem>
                <asp:ListItem>2010</asp:ListItem>
                <asp:ListItem>2009</asp:ListItem>
            </asp:DropDownList>
            <br />
        </div>
        <label class="control-label">
            ToYear<span class="required">*</span> :
        </label>
        <asp:DropDownList ID="ddlToYear" runat="server" CssClass="dropdown">
            <asp:ListItem Value="0" Text="Select"></asp:ListItem>
            <asp:ListItem>2025</asp:ListItem>
            <asp:ListItem>2024</asp:ListItem>
            <asp:ListItem>2023</asp:ListItem>
            <asp:ListItem>2022</asp:ListItem>
            <asp:ListItem>2021</asp:ListItem>
            <asp:ListItem>2020</asp:ListItem>
            <asp:ListItem>2019</asp:ListItem>
            <asp:ListItem>2018</asp:ListItem>
            <asp:ListItem>2017</asp:ListItem>
            <asp:ListItem>2016</asp:ListItem>
            <asp:ListItem>2015</asp:ListItem>
            <asp:ListItem>2014</asp:ListItem>
            <asp:ListItem>2013</asp:ListItem>
            <asp:ListItem>2012</asp:ListItem>
            <asp:ListItem>2011</asp:ListItem>
            <asp:ListItem>2010</asp:ListItem>
            <asp:ListItem>2009</asp:ListItem>
        </asp:DropDownList>
        <br />
        <label class="control-label">
            Title<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtTitle" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            PDF<span class="required">*</span> :
        </label>
        <asp:FileUpload ID="FilePdf" CssClass="upload-file" runat="server" />

        <asp:HyperLink ID="PdfPreview" runat="server" Text="View Pdf" Visible="false" Target="_blank"></asp:HyperLink>
        <br />
        <label class="control-label">
            TitleDate<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtTitleDate" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <br />
        <br />
        <br />

        <asp:Button ID="btnSave" Text="Save" CssClass="button" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="Cancel" CssClass="button" runat="server" OnClick="btnCancel_Click" />
        <br />

    </div>


    <div align="center">
        <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
    </div>
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
            OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging"
            CssClass="mGrid" PageSize="10" AllowPaging="true">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="ID" />
                <asp:BoundField DataField="PageName" HeaderText="Page Name" />
                <asp:BoundField DataField="FromYear" HeaderText="FromYear" />
                <asp:BoundField DataField="ToYear" HeaderText="ToYear" />
                <asp:BoundField DataField="Title" HeaderText="Title" />
                <asp:BoundField DataField="TitleDate" HeaderText="TitleDate" />
                <asp:TemplateField HeaderText="Pdf" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <a href='<%# Eval("Pdf", "../uploads/pdf/{0}") %>' target="_blank">View Pdf</a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                            CommandName="Select">
                            <i class="icon-edit"></i> Edit
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                            CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>




    <script src="http://code.jquery.com/jquery.js"></script>
    <%-- <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>--%>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.js" type="text/javascript"></script>

    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <style>
        .ui-datepicker-trigger {
            height: 25px;
            width: 25px;
            margin-left: 3px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id$=txtTitleDate]").datepicker({
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: 'images/calendar.gif',
                dateFormat: 'dd-mm-yy'
            });
        });
    </script>



</asp:Content>
