﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_brand : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        // SAVE IMAGES
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/uploads/pdf/";

        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                if (FilePdf.HasFile)
                {
                    ext = System.IO.Path.GetExtension(FilePdf.FileName).ToLower();
                    if (!utility.IsValidPDFFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Pdf file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Pdf", ext, this, false);
                    FilePdf.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_InvestorRelation"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "ADD");
                    cmd.Parameters.AddWithValue("@PageName", ddlPageName.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@FromYear", ddlFromYear.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@ToYear", ddlToYear.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@Title", txtTitle.Text);
                    cmd.Parameters.AddWithValue("@Pdf", MainImage + ext);

                    cmd.Parameters.AddWithValue("@TitleDate",txtTitleDate.Text);

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {
                if (FilePdf.HasFile)
                {
                    ext = System.IO.Path.GetExtension(FilePdf.FileName).ToLower();
                    if (!utility.IsValidPDFFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Pdf file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Pdf", ext, this, false);
                    FilePdf.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_InvestorRelation"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "UPDATE");
                    cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                    cmd.Parameters.AddWithValue("@PageName", ddlPageName.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@FromYear", ddlFromYear.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@ToYear", ddlToYear.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@Title", txtTitle.Text);
                    cmd.Parameters.AddWithValue("@Pdf", MainImage + ext);

                    cmd.Parameters.AddWithValue("@TitleDate", txtTitleDate.Text);

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        if (ddlPageName.SelectedValue == "0")
        {
            isOK = false;
            message += "pageName, ";
        }
        
        if (ddlFromYear.SelectedValue == "0")
        {
            isOK = false;
            message += "FromYear, ";
        }

        if (ddlToYear.SelectedValue == "0")
        {
            isOK = false;
            message += "ToYear, ";
        }
        if (txtTitle.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Title, ";
        }

        if (txtTitleDate.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "TitleDate, ";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;
        if (ddlPageName.SelectedValue == "0")
        {
            isOK = false;
            message += "page name, ";
        }
       
        if (ddlFromYear.SelectedValue == "0")
        {
            isOK = false;
            message += "FromYear, ";
        }
        if (ddlToYear.SelectedValue == "0")
        {
            isOK = false;
            message += "ToYear, ";
        }
        if (txtTitle.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Title, ";
        }

        if (txtTitleDate.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "TitleDate, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    private void Reset()
    {

        hdnId.Value = string.Empty;

        txtTitle.Text = string.Empty;

        ddlFromYear.SelectedValue = "0";
        ddlPageName.SelectedValue = "0";
        ddlToYear.SelectedValue = "0";
        txtTitleDate.Text = string.Empty;
        btnSave.Text = "Save";
        PdfPreview.Visible = false;

    }
    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            int Id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_InvestorRelation 'GET_BY_ID'," + Id);
            hdnId.Value = dt.Rows[0]["Id"].ToString();

            ddlPageName.SelectedValue= dt.Rows[0]["PageName"].ToString();
            ddlFromYear.SelectedValue = dt.Rows[0]["FromYear"].ToString();
            ddlToYear.SelectedValue = dt.Rows[0]["ToYear"].ToString();

            txtTitle.Text = dt.Rows[0]["Title"].ToString();

            txtTitleDate.Text = dt.Rows[0]["TitleDate"].ToString();
            PdfPreview.Visible = true;
            PdfPreview.NavigateUrl = string.Format("~/uploads/pdf/" + dt.Rows[0]["Pdf"].ToString());

            btnSave.Text = "Update";

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("Proc_InvestorRelation"))
            {
                int Id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Delete");
                cmd.Parameters.AddWithValue("@Id", Id);
                utility.Execute(cmd);
                BindGrid();
                MyMessageBox1.ShowSuccess("Record Deleted Successfully.");
            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Unable to delete record.");
        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    private void BindGrid()
     {
        try
        {
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_InvestorRelation 'GET'");

            if (dt.Rows.Count > 0)
            {
                gdView.Columns[0].Visible = true;
                gdView.DataSource = dt;
                gdView.DataBind();
                gdView.Columns[0].Visible = false;
                lblmsg.Visible = false;

            }
            else
            {
                gdView.DataSource = null;
                gdView.DataBind();
                lblmsg.Visible = true;

            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Some error occurred, While fetching records.");
        }
    }
    
}