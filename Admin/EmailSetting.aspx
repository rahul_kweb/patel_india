﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true"
    CodeFile="EmailSetting.aspx.cs" Inherits="admin_EmailSetting" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Email Details</div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <asp:HiddenField ID="hdnId" runat="server" />
    <div class="form-box">
        <label class="control-label">
            Host<span class="required">*</span> : &nbsp;&nbsp;</label><br />
        <asp:TextBox ID="txtHost" CssClass="textbox" runat="server"></asp:TextBox>
        <br />
        <label class="control-label">
            Port<span class="required">*</span> : &nbsp;&nbsp;</label><br />
        <asp:TextBox ID="txtPort" CssClass="textbox" runat="server"></asp:TextBox>
        <br />
        <label class="control-label">
            User Name<span class="required">*</span> : &nbsp;&nbsp;</label><br />
        <asp:TextBox ID="txtUsername" CssClass="textbox" runat="server"></asp:TextBox>
        <br />
        <label class="control-label">
            Password<span class="required">*</span> : &nbsp;&nbsp;</label><br />
        <asp:TextBox ID="txtPassword" CssClass="textbox" runat="server" TextMode="Password"></asp:TextBox>
        <br />
        <label class="control-label">
            To<span class="required">*</span> : &nbsp;&nbsp;</label><br />
        <asp:TextBox ID="txtTo" CssClass="textbox" runat="server"></asp:TextBox>
        <br />
        <label class="control-label">
            Cc : &nbsp;&nbsp;</label><br />
        <asp:TextBox ID="txtCc" CssClass="textbox" runat="server"></asp:TextBox>
        <br />
        <label class="control-label">
            BCc : &nbsp;&nbsp;</label><br />
        <asp:TextBox ID="txtBCc" CssClass="textbox" runat="server"></asp:TextBox>
        <br />
        <label class="control-label">
            Phone No. : &nbsp;&nbsp;</label><br />
        <asp:TextBox ID="txtPhno" CssClass="textbox" runat="server"></asp:TextBox>
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <%-- <asp:Button ID="btnCancel" Text="Cancel" CssClass="button" runat="server" 
            onclick="btnCancel_Click" />--%>
    </div>
</asp:Content>
