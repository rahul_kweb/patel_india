﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class unclaimed_dividend : System.Web.UI.Page
{
    Utility utility = new Utility();
    string PageName = "UNCLAIMED DIVIDEND ";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRepeater();
            dropdownArchieves();
        }
    }

    public void BindRepeater()
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InvestorRelation 'GET_YEAR_BY_PAGENAME',0,'" + PageName + "'");
        if (dt.Rows.Count > 0)
        {
            rptUnclaimedDividend.DataSource = dt;
            rptUnclaimedDividend.DataBind();
        }
        else
        {
            rptUnclaimedDividend.DataSource = null;
            rptUnclaimedDividend.DataBind();
        }
    }

    protected void rptUnclaimedDividend_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label fromYear = (Label)e.Item.FindControl("hdnFromYear");
            Label toYear = (Label)e.Item.FindControl("hdnToYear");


            DataTable dt = new DataTable();
            dt = utility.Display("Exec Proc_InvestorRelation 'GET_DETAILS_BY_YEAR_AND_PAGENAME',0,'" + PageName + "','" + fromYear.Text + "','" + toYear.Text + "'");
            Repeater rptInside = e.Item.FindControl("rptInside") as Repeater;

            if (dt.Rows.Count > 0)
            {
                rptInside.DataSource = dt;
                rptInside.DataBind();
            }
            else
            {
                rptInside.DataSource = null;
                rptInside.DataBind();
            }

        }
    }

    public void dropdownArchieves()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InvestorRelation 'FOR_DROPDOWNLIST_ARCHIEVES',0,'" + PageName + "' ");

        if (dt.Rows.Count > 0)
        {
            ddlArchieves.DataSource = dt;
            ddlArchieves.DataTextField = "Archive";
            ddlArchieves.DataValueField = "FromYear";
            ddlArchieves.DataBind();

            ddlArchieves.Items.Insert(0, new ListItem("Select Year", ""));
        }
        else
        {
            ddlArchieves.DataSource = null;
            ddlArchieves.Visible = false;
            Label1.Visible = false;
        }
    }

    protected void ddlArchieves_SelectedIndexChanged(object sender, EventArgs e)
    {
        //updateArchive.Update();
        string year = ddlArchieves.SelectedItem.Text;
        string[] arr = new string[2];
        if (year.Contains("Select Year"))
        {
            arr[0] = "0";
            arr[1] = "1";
        }
        else
        {
            arr = year.Split('/');
        }


        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InvestorRelation 'GET_DETAILS_BY_YEAR_AND_PAGENAME',0,'" + PageName + "','" + arr[0].ToString() + "','" + arr[1].ToString() + "'");

        StringBuilder sb = new StringBuilder();
        if (dt.Rows.Count > 0)
        {
            sb.Append("<div class=\"select_box_content output_content\" id=\"Yellow\" style=\"display:block;\">");
            sb.Append("<div class=\"inner_title\">");
            sb.Append("<h2>" + year + "</h2>");

            sb.Append("</div>");
            sb.Append("<ul>");

            foreach (DataRow dr in dt.Rows)
            {
                sb.Append("<li>");
                sb.Append("<i class=\"fa fa-file-pdf-o\" aria-hidden=\"true\"></i><a href=\"uploads/pdf/" + dr["pdf"].ToString() + "\" target=\"_blank\" tabindex=\"0\">" + dr["Title"] + "</a>");
                sb.Append("</li>");
            }

            sb.Append("</ul>");
            sb.Append("</div>");

            ltrArchiveDetails.Text = sb.ToString();


        }
        else
        {
            ltrArchiveDetails.Text = "";

        }
    }
}