﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_HomePageServices : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        // SAVE IMAGES
        string extLogo = string.Empty;
        string MainImageLogo = string.Empty;
        string extImage = string.Empty;
        string MainImage= string.Empty;
        string VirtualPart = "~/uploads/Images/";

        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                if (FileOurServicesLogo.HasFile)
                {
                    extLogo = System.IO.Path.GetExtension(FileOurServicesLogo.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(extLogo))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImageLogo = utility.GetUniqueName(VirtualPart, "Images", extLogo, this, false);
                    FileOurServicesLogo.SaveAs(Server.MapPath(VirtualPart + MainImageLogo + extLogo));
                }

                if (FileOurServicesImage.HasFile)
                {
                    extImage = System.IO.Path.GetExtension(FileOurServicesImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(extLogo))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Images", extImage, this, false);
                    FileOurServicesImage.SaveAs(Server.MapPath(VirtualPart + MainImage + extImage));
                }


                using (SqlCommand cmd = new SqlCommand("Proc_HomePageServices"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "ADD");
                    if (MainImageLogo == "")
                    {
                        cmd.Parameters.AddWithValue("@OurServicesLogo", "");
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@OurServicesLogo", MainImageLogo + extLogo);
                    }

                    if (MainImage == "")
                    {
                        cmd.Parameters.AddWithValue("@OurServicesImage", "");
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@OurServicesImage", MainImage + extImage);
                    }

               
                    cmd.Parameters.AddWithValue("@OurServicesTitle", txtOurServicesTitle.Text.Trim());
                    cmd.Parameters.AddWithValue("@OurServicesDescription", txtOurServicesDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@OurServicesUrl", txtOurServicesUrl.Text.Trim());




                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {

                if (FileOurServicesLogo.HasFile)
                {
                    extLogo = System.IO.Path.GetExtension(FileOurServicesLogo.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(extLogo))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImageLogo = utility.GetUniqueName(VirtualPart, "Images", extLogo, this, false);
                    FileOurServicesLogo.SaveAs(Server.MapPath(VirtualPart + MainImageLogo + extLogo));
                }

                if (FileOurServicesImage.HasFile)
                {
                    extImage = System.IO.Path.GetExtension(FileOurServicesImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(extLogo))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Images", extImage, this, false);
                    FileOurServicesImage.SaveAs(Server.MapPath(VirtualPart + MainImage + extImage));
                }

      
                using (SqlCommand cmd = new SqlCommand("Proc_HomePageServices"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "UPDATE");
                    cmd.Parameters.AddWithValue("@Id", hdnId.Value);

                    if (MainImageLogo == "")
                    {
                        cmd.Parameters.AddWithValue("@OurServicesLogo", "");
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@OurServicesLogo", MainImageLogo + extLogo);
                    }

                    if (MainImage == "")
                    {
                        cmd.Parameters.AddWithValue("@OurServicesImage", "");
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@OurServicesImage", MainImage + extImage);
                    }

            
                    cmd.Parameters.AddWithValue("@OurServicesTitle", txtOurServicesTitle.Text.Trim());
                    cmd.Parameters.AddWithValue("@OurServicesDescription", txtOurServicesDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@OurServicesUrl", txtOurServicesUrl.Text.Trim());
                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;


        if (txtOurServicesTitle.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Title, ";
        }

        if (txtOurServicesDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description,";
        }

        if (txtOurServicesUrl.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Services Url";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;

      
        if (txtOurServicesTitle.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Title, ";
        }

        if (txtOurServicesDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description";
        }

        if (txtOurServicesUrl.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Services Url";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    private void Reset()
    {

        hdnId.Value = string.Empty;

        txtOurServicesTitle.Text = string.Empty;
        txtOurServicesDescription.Text = string.Empty;

        txtOurServicesUrl.Text = string.Empty;
        btnSave.Text = "Save";
        LogoPreview.Visible = false;
        ImagePreview.Visible = false;

    }
    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            int Id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_HomePageServices 'GET_BY_ID'," + Id);
            hdnId.Value = dt.Rows[0]["Id"].ToString();
            txtOurServicesTitle.Text = dt.Rows[0]["OurServicesTitle"].ToString();
            txtOurServicesDescription.Text = dt.Rows[0]["OurServicesDescription"].ToString();
            txtOurServicesUrl.Text = dt.Rows[0]["OurServicesUrl"].ToString();
            LogoPreview.Visible = true;
            LogoPreview.ImageUrl = string.Format("~/uploads/Images/" + dt.Rows[0]["OurServicesLogo"].ToString());
            ImagePreview.Visible = true;
            ImagePreview.ImageUrl = string.Format("~/uploads/Images/" + dt.Rows[0]["OurServicesImage"].ToString());

            btnSave.Text = "Update";

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("Proc_HomePageServices"))
            {
                int Id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "DELETE");
                cmd.Parameters.AddWithValue("@Id", Id);
                utility.Execute(cmd);
                BindGrid();
                MyMessageBox1.ShowSuccess("Record Deleted Successfully.");
            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Unable to delete record.");
        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    private void BindGrid()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_HomePageServices 'GET'");

            if (dt.Rows.Count > 0)
            {
                gdView.Columns[0].Visible = true;
                gdView.DataSource = dt;
                gdView.DataBind();
                gdView.Columns[0].Visible = false;
                lblmsg.Visible = false;

            }
            else
            {
                gdView.DataSource = null;
                gdView.DataBind();
                lblmsg.Visible = true;

            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Some error occurred, While fetching records.");
        }
    }
}