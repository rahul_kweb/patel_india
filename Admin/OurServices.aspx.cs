﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class OurServices : AdminPage
{
    Utility utility = new Utility();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["OurServices"] != null && Request.QueryString["OurServices"] != "")
        {
            int testInt;
            if (int.TryParse(Request.QueryString["OurServices"], out testInt))
            {
                hdnId.Value = Request.QueryString["OurServices"];
                if (!Page.IsPostBack)
                {
                    Bind();
                }
            }
        }
    }


    private void Bind()
    {

        try
        {
            hdnId.Value = Request.QueryString["OurServices"].ToString();

            dt = utility.Display("EXEC Proc_OurServices 'GET_BY_ID'," + hdnId.Value);

            if (dt.Rows.Count > 0)
            {
                txtDescription.Text = dt.Rows[0]["OurServicesDescription"].ToString();
                lblPageName.Text = dt.Rows[0]["OurServicesHeading"].ToString();
                imgPreview.ImageUrl = string.Format("~/uploads/OurServicesLogo/{0}", dt.Rows[0]["OurServicesLogo"].ToString());
                imgPreview.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

            dt = null;

        }
    }


    protected void btnUpdate_Click(object sender, EventArgs e)
    {


        try
        {

            if (CheckSave())
            {

                try
                {


                    string ext = string.Empty;
                    string MainImg = string.Empty;
                    string Virtualpath = "~/Uploads/OurServicesLogo/";

                    if (fileupload.HasFile)
                    {
                        ext = System.IO.Path.GetExtension(fileupload.FileName).ToLower();
                        if (!utility.IsValidImageFileExtension(ext))
                        {
                            MyMessageBox1.ShowError("Only Image (jpg, jpeg, png, bmp) file allowed.");
                            return; //stop further process
                        }
                        MainImg = utility.GetUniqueName(Virtualpath, "img", ext, this, false);
                        fileupload.SaveAs(Server.MapPath(Virtualpath + MainImg + ext));
                    }




                    using (SqlCommand cmd = new SqlCommand("Proc_OurServices"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PARA", "UPDATE");
                        cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                        cmd.Parameters.AddWithValue("@OurServicesDescription", txtDescription.Text.Trim());
                        cmd.Parameters.AddWithValue("@OurServicesLogo", MainImg + ext);                   
                        dt = utility.Display(cmd);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    utility = null;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {


        }
    }

    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;
        //if (txtDescription.Text.Trim() == string.Empty)
        //{
        //    isOK = false;
        //    message += "Content,";
        //}



        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }
}