﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="AboutUsBoardCommetees.aspx.cs" Inherits="Admin_AboutUsBoardCommetees" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" Runat="Server">

        <div class="page-header">
        Board & Committees
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
       Name<span class="required">*</span> :
        </label>
        <asp:DropDownList ID="ddlPrefix" runat="server" CssClass="dropdown">
            <asp:ListItem Value="0" Text="Select"></asp:ListItem>
            <asp:ListItem>MR.</asp:ListItem>
            <asp:ListItem>MS.</asp:ListItem>
        </asp:DropDownList>
       <asp:TextBox ID="txtName" runat="server" CssClass="textbox"></asp:TextBox>

        <br />
     
    
      
        <label class="control-label">
            Position<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtPosition" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
      <label class="control-label">
            Image<span class="required">*</span> :
        </label>
        <asp:FileUpload ID="FileImage" CssClass="upload-file" runat="server" />

        <asp:Image ID="ImagePreview" runat="server" Text="View Image" visible="false" Target="_blank" Width="200" Height="150"></asp:Image>
        <br />
         <label class="control-label">
            Description<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtDescription" runat="server" CssClass="textbox" textMode="MultiLine" Width="500" Height="150"></asp:TextBox>
        <br />
        <br />
        <br />
        <br />

        <asp:Button ID="btnSave" Text="Save" CssClass="button" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="Cancel" CssClass="button" runat="server" OnClick="btnCancel_Click" />
        <br />

    </div>

     <div align="center">
        <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
    </div>
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
            OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging"
            CssClass="mGrid" PageSize="10" AllowPaging="true">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="ID" />
                <asp:BoundField DataField="Prefix" HeaderText="" />
                <asp:BoundField DataField="Name" HeaderText="Name" />
                <asp:BoundField DataField="Position" HeaderText="Position" />
              <asp:TemplateField HeaderText="Image" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <img src='<%# Eval("Image", "../uploads/bod/{0}") %>' height="150px" width="200px"></img>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Description" HeaderText="Description" />
            
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                            CommandName="Select">
                            <i class="icon-edit"></i> Edit
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                            CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>

</asp:Content>

