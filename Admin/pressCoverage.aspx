﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="pressCoverage.aspx.cs" Inherits="Admin_pressCoverage" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Press Coverage
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 500px;">
        <asp:HiddenField ID="hdnId" runat="server" />


        <label class="control-label">
            ThumbImage <span class="required">*</span> :<small style="font-weight: normal; font-size: 14px;"><br />
                (recommended image size for Movie Image - Width 400px X Height 280px)</small></label>

        <asp:FileUpload ID="FileImages" CssClass="upload-file" runat="server" />
        <asp:Image ID="ImagePreview" runat="server" Text="View Image" Visible="false" Target="_blank" Width="200" Height="150"></asp:Image>
        <br />

        <%--<label class="control-label">PDF/InnerImage</label>--%>

        <%--        <asp:CheckBox ID="chkPdf" runat="server" Text="Pdf"  OnClick="showhidepdf();"  />
        <asp:CheckBox ID="chkBigImage" runat="server" Text="BigImage" />--%>
        <label class="control-label">
            Select Type<span class="required">*</span> :
        </label>
        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatColumns="2">
            <asp:ListItem Text="Pdf" Value="1"></asp:ListItem>
            <asp:ListItem Text="Big Image" Value="2"></asp:ListItem>
        </asp:RadioButtonList>


        <div id="divPdf" style="display: none;">
            <br />
            <label class="control-label">
                Pdf<span class="required">*</span> :
            </label>
            <asp:FileUpload ID="FilePdf" CssClass="upload-file" runat="server" />
            <asp:HyperLink ID="PdfPreview" runat="server" Text="View Pdf" Visible="false" Target="_blank"></asp:HyperLink>
        </div>


        <div id="divImage" style="display: none;">
            <br />
            <label class="control-label">
                Big Image<span class="required">*</span> :
            </label>
            <asp:FileUpload ID="FileInnerImage" CssClass="upload-file" runat="server" />
            <asp:Image ID="InnerImagePreview" runat="server" Text="View InnerImage" Visible="false" Height="150" Width="200" Target="_blank"></asp:Image>
        </div>
        <br />
        <br />

        <asp:Button ID="btnSave" Text="Save" CssClass="button" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="Cancel" CssClass="button" runat="server" OnClick="btnCancel_Click" />

        <br />
        <br />
        <asp:HyperLink ID="hyprresizeimage" runat="server" Text=">Resize your digital images"
            NavigateUrl="http://www.resize2mail.com/advanced.php" Target="_blank" Style="text-decoration: none; font-weight: bold;"></asp:HyperLink>

        <br />
        <br />

    </div>
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
            OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging"
            CssClass="mGrid" PageSize="10" AllowPaging="true">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" />
                <asp:TemplateField HeaderText="Sr No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ImageName" HeaderText="ImageName" Visible="false" />
                <asp:TemplateField HeaderText="Thumbnail Image" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>

                        <img src='<%# Eval("PressCoverageimg", "../uploads/press_coverage/{0}") %>' height="150px" width="200px"></img>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="PDF / Big Image" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>

                        <a href='<%# Eval("pdf", "../uploads/press_coverage/{0}") %>' target="_blank" style='<%#Eval("Format").ToString()=="1"?"display:block":"display:none"%>' >View pdf</a>

                        <img src='<%# Eval("InnerImage", "../uploads/press_coverage/{0}") %>' height="150px" width="200px" style='<%#Eval("Format").ToString()=="2"?"display:block":"display:none"%>'></img>

                    </ItemTemplate>
                </asp:TemplateField>


                <%--<asp:TemplateField HeaderText="InnerImage" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>

                        <a href='<%# Eval("InnerImage", "../uploads/press_coverage/{0}") %>' target="_blank">View InnerImage</a>

                    </ItemTemplate>
                </asp:TemplateField>--%>

                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                            CommandName="Select">
                            <i class="icon-edit"></i> Edit
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                            CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>

    </div>

    <script>
   
        $("[id*=RadioButtonList1] input").live("click", function () {
            var selectedValue = $(this).val();
            //alert(selectedValue);
            if (selectedValue == "1") {
                $('#divPdf').attr('style', 'display:block');
                $('#divImage').attr('style', 'display:none');
            }
            else if (selectedValue == "2") {
                $('#divImage').attr('style', 'display:block');
                $('#divPdf').attr('style', 'display:none');
            }
        });

        $(function () {
            var list = document.getElementById("<%=RadioButtonList1.ClientID%>"); //Client ID of the radiolist
            var inputs = list.getElementsByTagName("input");
            var selected;
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].checked) {
                    selected = inputs[i];
                    break;
                }
            }
            if (selected) {
                //alert(selected.value);
                if (selected.value == "1") {
                    $('#divPdf').attr('style', 'display:block');
                    $('#divImage').attr('style', 'display:none');
                }
                else if (selected.value == "2") {
                    $('#divImage').attr('style', 'display:block');
                    $('#divPdf').attr('style', 'display:none');
                }
            }
            else {
                $('#divPdf').attr('style', 'display:none');
                $('#divImage').attr('style', 'display:none');
            }
        })
    </script>
</asp:Content>

