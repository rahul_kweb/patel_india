﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class Admin_HomePage : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        // SAVE IMAGES
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/uploads/Images/";

        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                if (FileHomePageImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(FileHomePageImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Images", ext, this, false);
                    FileHomePageImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_HomePageBanner"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "ADD");
                    cmd.Parameters.AddWithValue("@HomePageImage", MainImage + ext);
                    cmd.Parameters.AddWithValue("@HomePageDescription",txtHomeDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@Url",txtUrl.Text.Trim());
          

                    

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {

                if (FileHomePageImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(FileHomePageImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Images", ext, this, false);
                    FileHomePageImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_HomePageBanner"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "UPDATE");
                    cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                    cmd.Parameters.AddWithValue("@HomePageImage", MainImage + ext);
                    cmd.Parameters.AddWithValue("@HomePageDescription", txtHomeDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@Url", txtUrl.Text.Trim());

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        if (!FileHomePageImage.HasFile)
        {
            message = "Home page Image ,";
            isOK = false;
        }

        if (txtHomeDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

        if (txtUrl.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Url ";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;

     

        if (txtHomeDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

        if (txtUrl.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Url ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    private void Reset()
    {

        hdnId.Value = string.Empty;

        txtHomeDescription.Text = string.Empty;

       
        txtUrl.Text = string.Empty;
        btnSave.Text = "Save";
        ImagePreview.Visible = false;

    }
    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            int Id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_HomePageBanner 'GET_BY_ID'," + Id);
            hdnId.Value = dt.Rows[0]["Id"].ToString();
           txtHomeDescription.Text = dt.Rows[0]["HomePageDescription"].ToString();
            txtUrl.Text = dt.Rows[0]["Url"].ToString();
              ImagePreview.Visible = true;
              ImagePreview.ImageUrl = string.Format("~/uploads/Images/" + dt.Rows[0]["HomePageImage"].ToString());

            btnSave.Text = "Update";

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("Proc_HomePageBanner"))
            {
                int Id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "DELETE");
                cmd.Parameters.AddWithValue("@Id", Id);
                utility.Execute(cmd);
                BindGrid();
                MyMessageBox1.ShowSuccess("Record Deleted Successfully.");
            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Unable to delete record.");
        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    private void BindGrid()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_HomePageBanner 'GET'");

            if (dt.Rows.Count > 0)
            {
                gdView.Columns[0].Visible = true;
                gdView.DataSource = dt;
                gdView.DataBind();
                gdView.Columns[0].Visible = false;
                lblmsg.Visible = false;

            }
            else
            {
                gdView.DataSource = null;
                gdView.DataBind();
                lblmsg.Visible = true;

            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Some error occurred, While fetching records.");
        }
    }
}