﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="CurrentOpening.aspx.cs" Inherits="Admin_CurrentOpening" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">


    <div class="page-header">
        Current Opening
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            JobTitle<span class="required">*</span> :
        </label>
        <br />
        <asp:DropDownList ID="ddlJobTitle" runat="server" CssClass="dropdown">
            <asp:ListItem Value="0" Text="Select JobTitle"></asp:ListItem>
            <asp:ListItem>CUSTOMER SERVICE EXECUTIVE</asp:ListItem>
            <asp:ListItem>SALES EXECUTIVE</asp:ListItem>
            <asp:ListItem>SOFTWARE DEVELOPER</asp:ListItem>
        </asp:DropDownList>

        <br />
        


        <label class="control-label">
            JobType<span class="required">*</span> :
        </label>
        <br />
        <asp:DropDownList ID="ddlJobType" runat="server" CssClass="dropdown">
            <asp:ListItem Value="0" Text="Select"></asp:ListItem>
            <asp:ListItem >FULL TIME</asp:ListItem>
            <asp:ListItem >HALF TIME</asp:ListItem>

        </asp:DropDownList>
        <br />

        <label class="control-label">
            JobProfile<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtJobProfile" runat="server" CssClass="textbox"></asp:TextBox>
        <br />

        <label class="control-label">
            Qualification<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtQualification" runat="server" CssClass="textbox"></asp:TextBox>
        <br />

        <label class="control-label">
            Candidate Profile<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtCandidateProfile" runat="server" CssClass="textbox"></asp:TextBox>
        <br />

        <label class="control-label">
            Experience<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtExperience" runat="server" CssClass="textbox"></asp:TextBox>
        <br />

        <label class="control-label">
            Location<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtLocation" runat="server" CssClass="textbox"></asp:TextBox>
        <br />


        <asp:Button ID="btnSave" Text="Save" CssClass="button" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="Cancel" CssClass="button" OnClick="btnCancel_Click" runat="server" />
        <br />

    </div>



    <div align="center">
        <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
    </div>
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
            OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging"
            CssClass="mGrid" PageSize="10" AllowPaging="true">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="JobId" HeaderText="JobID" />
                <asp:BoundField DataField="JobTitle" HeaderText="JobTitle" />
                <asp:BoundField DataField="JobProfile" HeaderText="JobProfile" />
                <asp:BoundField DataField="JobType" HeaderText="Zone" />


                <asp:BoundField DataField="Qualification" HeaderText="Qualification" />
                <asp:BoundField DataField="CandidateProfile" HeaderText="CandidateProfile" />
                <asp:BoundField DataField="Experience" HeaderText="Experience" />
                <asp:BoundField DataField="Location" HeaderText="Location" />


                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                            CommandName="Select">
                            <i class="icon-edit"></i> Edit
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                            CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>

</asp:Content>

