﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="HomePageServices.aspx.cs" Inherits="Admin_HomePageServices" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" Runat="Server">
       <div class="page-header">
        Home Page Services
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
           Our Services Logo<span class="required">*</span> :
        </label>
        <asp:FileUpload ID="FileOurServicesLogo" runat="server" CssClass="upload-file" />
        <asp:Image ID="LogoPreview" runat="server" Text="View Image" Visible="false" Target="_blank"
            Width="200" Height="150"></asp:Image>
        <br />

         <label class="control-label">
          Our Services Image<span class="required">*</span> :
        </label>
        <asp:FileUpload ID="FileOurServicesImage" runat="server" CssClass="upload-file" />
        <asp:Image ID="ImagePreview" runat="server" Text="View Image" Visible="false" Target="_blank"
            Width="200" Height="150"></asp:Image>
        <br />

        <label class="control-label">
           Our Services Title<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtOurServicesTitle" runat="server" Width="400px"
             CssClass="textbox"></asp:TextBox>
        <br />

          <label class="control-label">
          Our Services Description<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtOurServicesDescription" runat="server" Width="400px" Height="100px"
            Textmode="MultiLine" CssClass="textbox"></asp:TextBox>
        <br />

        <label class="control-label">
            Our Services Url<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtOurServicesUrl" runat="server" Width="400px" CssClass="textbox"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="btnSave" Text="Save" CssClass="button" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="Cancel" CssClass="button" runat="server" OnClick="btnCancel_Click" />
        <br />
    </div>
    <div align="center">
        <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
    </div>


    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
            OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging"
            CssClass="mGrid" PageSize="10" AllowPaging="true">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="ID" />
                <asp:TemplateField HeaderText="Our Services Logo" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <img src='<%# Eval("OurServicesLogo", "../uploads/Images/{0}") %>' height="150px" width="200px" alt="<%# Eval("OurServicesLogo") %>"></img>
                    </ItemTemplate>
                </asp:TemplateField>

                  <asp:TemplateField HeaderText="Our Services Image" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <img src='<%# Eval("OurServicesImage", "../uploads/Images/{0}") %>' height="150px" width="200px" alt="<%#Eval("OurServicesImage")%>"></img>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="OurServicesTitle" HeaderText="Our Services Title" />
                <asp:BoundField DataField="OurServicesDescription" HeaderText="Our Services Description" />
                <asp:BoundField DataField="OurServicesUrl" HeaderText="Our Services Url" />
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                            CommandName="Select">
                            <i class="icon-edit"></i> Edit
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                            CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>

</asp:Content>

