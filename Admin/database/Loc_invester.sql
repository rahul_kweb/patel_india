
/****** Object:  StoredProcedure [dbo].[Proc_InvestorRelation]    Script Date: 01/03/2018 8:22:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_InvestorRelation]
(
	@PARA		nvarchar(50)='',
	@Id			int=0,
	@PageName	nvarchar(100)='',
	@FromYear	nvarchar(50)='',
	@ToYear		nvarchar(50)='',
	@Title		nvarchar(100)='',
	@Pdf		nvarchar(100)='',
	@TitleDate  Nvarchar(100) =''
)
	
AS
BEGIN
	IF @PARA='Add'
	BEGIN
	insert into InvestorRelation
	(
		PageName,
		FromYear,
		ToYear,
		Title,
		Pdf,
		IsActive,
		AddedOn,
		TitleDate
	 ) 
	values
	(
		@PageName,
		@FromYear,
		@ToYear,
		@Title,
		@Pdf,
		1,
		GETDATE(),
		@TitleDate
	)
	
END

	ELSE IF @PARA='DELETE'
	BEGIN
	UPDATE InvestorRelation SET 
			isactive=0,
			DeletedOn=GETDATE()
			where 
			Id=@Id
	END

	ELSE IF @PARA='UPDATE'
	BEGIN
	if @Pdf<>''
	begin
		UPDATE InvestorRelation set
		Pdf			=	@Pdf
		where
		Id=@Id
	end
	UPDATE InvestorRelation set
		PageName	=	@PageName,
		FromYear	=	@FromYear,
		ToYear		=	@ToYear,
		Title		=	@Title,		
		UpdatedOn	=	GETDATE(),
		TitleDate	=	@TitleDate
		where
		Id=@Id
	END

	ELSE IF @PARA='GET'
	BEGIN
	select Id,PageName,
			FromYear,Toyear,
			Title,Pdf,TitleDate,
			IsActive,
			convert(datetime, [TitleDate], 105) convertedDate
			from InvestorRelation where IsActive=1 
			order by convert(datetime, [TitleDate], 105) desc
	END

	ELSE IF @PARA='GET_BY_ID'
	BEGIN
		SELECT * FROM InvestorRelation 
		WHERE 
		Id=@Id
	END

	ELSE IF @PARA = 'GET_YEAR_BY_PAGENAME'
	BEGIN
		select
			distinct	
			convert(nvarchar(4),CAST(FromYear as datetime),112) AS FromYear,
			convert(nvarchar(4),CAST(ToYear as datetime),112) as ToYear  
			from InvestorRelation 
			where PageName = @PageName
			and
			IsActive = 1
			and
			YEAR(getdate()) -5 <= FromYear
			order by convert(nvarchar(4),CAST(FromYear as datetime),112) desc
	END

	ELSE IF @PARA = 'GET_DETAILS_BY_YEAR_AND_PAGENAME'
	BEGIN
		select * from InvestorRelation
			where PageName = @PageName
			and
			FromYear = @FromYear
			and
			ToYear = @ToYear
			and
			IsActive = 1
		order by convert(datetime, [TitleDate], 105) desc
			
END


			ELSE IF @PARA = 'For_Archives_GET_YEAR_BY_PAGENAME'
	BEGIN
		select
			distinct	
			convert(nvarchar(4),CAST(FromYear as datetime),112) AS FromYear,
			convert(nvarchar(4),CAST(ToYear as datetime),112) as ToYear  
			from InvestorRelation 
			where PageName = @PageName
			and
			IsActive = 1
			and
			YEAR(getdate()) -5 > FromYear
			order by convert(nvarchar(4),CAST(FromYear as datetime),112) desc
	END


	ELSE IF @PARA='FOR_DROPDOWNLIST_ARCHIEVES'
	BEGIN
	select
			distinct	
			convert(nvarchar(4),CAST(FromYear as datetime),112) AS FromYear,
			convert(nvarchar(4),CAST(ToYear as datetime),112) as ToYear ,
			convert(nvarchar(4),CAST(FromYear as datetime),112) +'/'+convert(nvarchar(4),CAST(ToYear as datetime),112) as Archive
			from InvestorRelation 
			where PageName = @PageName
			and
			IsActive = 1
			and
			YEAR(getdate()) -5 > FromYear
			order by convert(nvarchar(4),CAST(FromYear as datetime),112) desc
	END

	END

GO
/****** Object:  StoredProcedure [dbo].[Proc_Location]    Script Date: 01/03/2018 8:22:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_Location]
(
	@PARA					nvarchar(50)='',
	@Id						int=0,
	@Category				nvarchar(100)='',
	@Zone					nvarchar(50)='',
	@City					nvarchar(50)='',
	@ManagerName			nvarchar(1000)='',
	@Address				nvarchar(100)='',
	@Telephone				nvarchar(50)='',
	@Mobile					nvarchar(50)='',
	@Fax					nvarchar(50)='',
	@Email					nvarchar(50)='',
	@CategoryId				int=0,
	@ZoneId					int=0,
	@search					nvarchar(100)='',
	@Branch_Location_Code	nvarchar(100)='',
	@BranchType				nvarchar(1000)='',
	@Ownership				nvarchar(100)='',
	@Email2					nvarchar(50)='',
	@Email3					nvarchar(50)=''

)	

AS
BEGIN

IF @PARA='Add'
	BEGIN
	insert into Location
	(
		Category,
		Zone,
		City,
		ManagerName,
		Address,
		Telephone,
		Mobile,
		Fax,
		Email,	
		CategoryId,
		ZoneId,
		AddedOn,
		IsActive,
		Branch_Location_Code,
		BranchType,
		Ownership,
		Email2,
		Email3
	 ) 
	 values
	 (
	 @Category,
	 @Zone,
	 @City,
	 @ManagerName,
	 @Address,
	 @Telephone,
	 @Mobile,
	 @Fax,
	 @Email,
	 @CategoryId,
	 @ZoneId,
	 getdate(),
	 1,
	 @Branch_Location_Code,
	 @BranchType,
	 @Ownership,
	  @Email2,
	 @Email3
	 )
	END

	ELSE IF @PARA='DELETE'
	BEGIN
	UPDATE Location SET 
			isactive=0,
			DeletedOn=GETDATE()
			where 
			Id=@Id
	END

	ELSE IF @PARA='UPDATE'
	BEGIN
	UPDATE Location set
		Category				=	@Category,
		Zone					=	@Zone,
		City					=	@City,
		ManagerName				=	@ManagerName,	
		Address					=	@Address,
		Telephone				=	@Telephone,
		Mobile					=	@Mobile,
		Fax						=	@Fax,
		Email					=	@Email,
		CategoryId				=	@CategoryId,
		ZoneId					=	@ZoneId,
		UpdatedOn				=	GETDATE(),
	   Branch_Location_Code	    =   @Branch_Location_Code,
		BranchType				=	@BranchType,
		Ownership				=	@Ownership	,
		Email2					=	@Email2,
		Email3					=	@Email3	
		where
		Id=@Id
	END

	ELSE IF @PARA='GET'
	BEGIN
		SELECT * FROM Location 
		WHERE 
		ISACTIVE=1
	END

	ELSE IF @PARA='GET_BY_ID'
	BEGIN
		SELECT * FROM Location 
		WHERE 
		Id=@Id
	END

		ELSE IF @PARA = 'GET_Zone_BY_CATEGORY'
	BEGIN
			select
			Zone
			from Location 
			where Category = @Category
			and
			IsActive = 1

END


		ELSE IF @PARA = 'Get_By_Category_Zone'
	BEGIN
		select * from Location
			where Category = @Category
			and
			Zone = @Zone
			and
			IsActive = 1
	END


	
	ELSE IF @PARA = 'FRONT_GET_CAT_ID'
	BEGIN
		select distinct CategoryId,Category
		from Location 
		where IsActive = 1 order by CategoryId
	END

	ELSE IF @PARA = 'FRONT_GET_ZONE_BY_CAT'
	BEGIN
		select distinct Zone,ZoneId
		from Location 
		where IsActive = 1
		and
		CategoryId = @CategoryId
		order by ZoneId
	END

	ELSE IF @PARA = 'FRONT_GET_DATA_BY_CAT_ZONE'
	BEGIN
		select *
		from Location 
		where IsActive = 1
		and
		CategoryId = @CategoryId
		and
		ZoneId = @ZoneId
	END

	ELSE IF @PARA = 'FRONT_SEARCH_LOCATION'
	BEGIN
		select * from Location
		where
		(
		Zone like '%'+@search+'%' OR
		City like '%'+@search+'%' OR
		ManagerName like '%'+@search+'%' 
		
		)
		and
		IsActive = 1
		and
		CategoryId = @CategoryId
		and
		ZoneId = @ZoneId
	END

	ELSE IF @PARA = 'FRONT_SEARCH_LOCATION_GET_ZONE_BY_CAT'
	BEGIN
		select distinct Zone,ZoneId
		from Location
		where
		(
		Zone like '%'+@search+'%' OR
		City like '%'+@search+'%' OR
		ManagerName like '%'+@search+'%' 
		
		)
		and
		IsActive = 1
		and
		CategoryId = @CategoryId
		order by ZoneId
	END

END


GO
/****** Object:  Table [dbo].[Location]    Script Date: 01/03/2018 8:22:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Location](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](50) NULL,
	[Zone] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[ManagerName] [nvarchar](1000) NULL,
	[Address] [nvarchar](100) NULL,
	[Telephone] [nvarchar](50) NULL,
	[Mobile] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
	[CategoryId] [int] NULL,
	[ZoneId] [int] NULL,
	[Fax] [nvarchar](50) NULL,
	[Branch_Location_Code] [nvarchar](100) NULL,
	[BranchType] [nvarchar](1000) NULL,
	[Ownership] [nvarchar](100) NULL,
	[Email2] [nvarchar](50) NULL,
	[Email3] [nvarchar](50) NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
