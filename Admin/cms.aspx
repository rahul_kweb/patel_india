﻿<%@ Page Title="Patel-india || CMS" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="cms.aspx.cs" Inherits="admin_cms" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header"><asp:Literal ID="lblPageName" runat="server"></asp:Literal></div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <asp:HiddenField ID="hdnId" runat="server" />
   <%-- <asp:FileUpload ID="fileupload" runat="server" />
    
    <asp:Image ID="imgPreview" runat="server" Visible="false"/>--%>
<%--    <br />
    <small style="font-weight: normal; font-size: 14px;">(recommended image size  - Width 289px X Height 257px)</small>--%>
    <br />
    <%--<asp:RequiredFieldValidator ID="rfvalidater" runat="server" ErrorMessage="*" ControlToValidate="fileupload" ForeColor="Red"></asp:RequiredFieldValidator>--%>
    <br />
    <div id="ckid" runat="server">
    <CKEditor:CKEditorControl ID="txtDescription" Text="" runat="server" Width="100%"
        Height="350px"></CKEditor:CKEditorControl>
        </div>
    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="button" OnClick="btnUpdate_Click" />
</asp:Content>
