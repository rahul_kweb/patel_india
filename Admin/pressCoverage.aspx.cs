﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_pressCoverage : AdminPage
{

    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/uploads/press_coverage/";

        string extPdf = string.Empty;
        string MainPdf = string.Empty;
        string VirtualPdf = "~/uploads/press_coverage/";


        string extInnerImage = string.Empty;
        string MainInnerImage = string.Empty;
        string VirtualInnerImage = "~/uploads/press_coverage/";
      


        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                if (FileImages.HasFile)
                {
                    ext = System.IO.Path.GetExtension(FileImages.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "PressCoverageimg", ext, this, false);
                    FileImages.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                if (FilePdf.HasFile)
                {
                    extPdf = System.IO.Path.GetExtension(FilePdf.FileName).ToLower();
                    if (!utility.IsValidPDFFileExtension(extPdf))
                    {
                        MyMessageBox1.ShowError("Only Pdf allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainPdf = utility.GetUniqueName(VirtualPdf, "Pdf", extPdf, this, false);
                    FilePdf.SaveAs(Server.MapPath(VirtualPdf + MainPdf + extPdf));
                }


                if (FileInnerImage.HasFile)
                {
                    extInnerImage = System.IO.Path.GetExtension(FileInnerImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(extInnerImage))
                    {
                        MyMessageBox1.ShowError("Only Image allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainInnerImage = utility.GetUniqueName(VirtualInnerImage, "PressCoverageimg", extInnerImage, this, false);
                    FileInnerImage.SaveAs(Server.MapPath(VirtualInnerImage + MainInnerImage + extInnerImage));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_MediaNewsPressCoverageimages"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "ADD");
                    //cmd.Parameters.AddWithValue("@ImageName", txtImageName.Text);


                    if (MainImage == "")
                    {
                        cmd.Parameters.AddWithValue("@PressCoverageimg", "");
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@PressCoverageimg", MainImage + ext);

                    }



                    cmd.Parameters.AddWithValue("@pdf", MainPdf + extPdf);
                    cmd.Parameters.AddWithValue("@InnerImage", MainInnerImage + extInnerImage);

                    cmd.Parameters.AddWithValue("@Format", RadioButtonList1.SelectedValue);

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {
                if (FileImages.HasFile)
                {
                    ext = System.IO.Path.GetExtension(FileImages.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Images allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "PressCoverageimg", ext, this, false);
                    FileImages.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }


                if (FilePdf.HasFile)
                {
                    extPdf = System.IO.Path.GetExtension(FilePdf.FileName).ToLower();
                    if (!utility.IsValidPDFFileExtension(extPdf))
                    {
                        MyMessageBox1.ShowError("Only Pdf allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainPdf = utility.GetUniqueName(VirtualPdf, "Pdf", extPdf, this, false);
                    FilePdf.SaveAs(Server.MapPath(VirtualPdf + MainPdf + extPdf));
                }

                if (FileInnerImage.HasFile)
                {
                    extInnerImage = System.IO.Path.GetExtension(FileInnerImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(extInnerImage))
                    {
                        MyMessageBox1.ShowError("Only Image allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainInnerImage = utility.GetUniqueName(VirtualInnerImage, "PressCoverageimg", extInnerImage, this, false);
                    FileInnerImage.SaveAs(Server.MapPath(VirtualInnerImage + MainInnerImage + extInnerImage));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_MediaNewsPressCoverageimages"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "UPDATE");
                    cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                    cmd.Parameters.AddWithValue("@PressCoverageimg", MainImage + ext);
                    cmd.Parameters.AddWithValue("@pdf", MainPdf + extPdf);
                    cmd.Parameters.AddWithValue("@InnerImage", MainInnerImage + extInnerImage);
                    cmd.Parameters.AddWithValue("@Format", RadioButtonList1.SelectedValue);

                    if (utility.Execute(cmd))
                    {                    
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        if (!FileImages.HasFile)
        {
            message = "ThumbImage ,";
            isOK = false;
        }

        if (RadioButtonList1.SelectedValue.Equals(""))
        {
            message += " Select Type ,";
            isOK = false;
        }

        if (RadioButtonList1.SelectedValue == "1")
        {
            if (!FilePdf.HasFile)
            {
                message += " Pdf ,";
                isOK = false;
            }
        }

        if (RadioButtonList1.SelectedValue == "2")
        {
            if (!FileInnerImage.HasFile)
            {
                message += " Big Image ,";
                isOK = false;
            }
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {

        bool isOK = true;
        string message = string.Empty;
        if (RadioButtonList1.SelectedValue.Equals(""))
        {
            message += " Select Type ,";
            isOK = false;
        }

        //if (RadioButtonList1.SelectedValue == "1")
        //{
        //    if (!FilePdf.HasFile)
        //    {
        //        message += " Pdf ,";
        //        isOK = false;
        //    }
        //}

        //if (RadioButtonList1.SelectedValue == "2")
        //{
        //    if (!FileInnerImage.HasFile)
        //    {
        //        message += " Big Image ,";
        //        isOK = false;
        //    }
        //}

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }


    private void Reset()
    {
        //txtImageName.Text = string.Empty;
        hdnId.Value = string.Empty;
        btnSave.Text = "Save";
        ImagePreview.Visible = false;
        PdfPreview.Visible = false;
        InnerImagePreview.Visible = false;
        //FileInnerImage.Visible = false;
        //FilePdf.Visible = false;
        RadioButtonList1.SelectedValue = null;

    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
     {
        try
        {
            int Id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_MediaNewsPressCoverageimages 'GET_BY_ID'," + Id);
            hdnId.Value = dt.Rows[0]["Id"].ToString();

            ImagePreview.Visible = true;
            ImagePreview.ImageUrl = string.Format("~/uploads/press_coverage/" + dt.Rows[0]["PressCoverageimg"].ToString());

            RadioButtonList1.SelectedValue = dt.Rows[0]["Format"].ToString();

            if (RadioButtonList1.SelectedValue == "1")
            {
                PdfPreview.Visible = true;
                PdfPreview.NavigateUrl = string.Format("~/uploads/press_coverage/" + dt.Rows[0]["pdf"].ToString());
            }
            else
            {
                InnerImagePreview.Visible = true;
                InnerImagePreview.ImageUrl = string.Format("~/uploads/press_coverage/" + dt.Rows[0]["InnerImage"].ToString());
            }

            btnSave.Text = "Update";

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("Proc_MediaNewsPressCoverageimages"))
            {
                int Id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Delete");
                cmd.Parameters.AddWithValue("@Id", Id);
                utility.Execute(cmd);
                BindGrid();
                MyMessageBox1.ShowSuccess("Record Deleted Successfully.");

                Reset();
            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Unable to delete record.");
        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    private void BindGrid()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_MediaNewsPressCoverageimages 'GET'");

            if (dt.Rows.Count > 0)
            {
                gdView.Columns[0].Visible = true;
                gdView.DataSource = dt;
                gdView.DataBind();
                gdView.Columns[0].Visible = false;             

            }
            else
            {
                gdView.DataSource = null;
                gdView.DataBind();
          

            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Some error occurred, While fetching records.");
        }
    }

      

}
