﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_IepfPdf : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            BindGrid();
        }

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/uploads/Iepf_pdf/";

        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                if (FilePdf.HasFile)
                {
                    ext = System.IO.Path.GetExtension(FilePdf.FileName).ToLower();
                    if (!utility.IsValidPDFFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Pdf file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Pdf", ext, this, false);
                    FilePdf.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_IepfPdf"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "ADD");

                    cmd.Parameters.AddWithValue("@Title", txtTitle.Text);
                    cmd.Parameters.AddWithValue("@Pdf", MainImage + ext);



                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {
                if (FilePdf.HasFile)
                {
                    ext = System.IO.Path.GetExtension(FilePdf.FileName).ToLower();
                    if (!utility.IsValidPDFFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Pdf file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Pdf", ext, this, false);
                    FilePdf.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_IepfPdf"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "UPDATE");
                    cmd.Parameters.AddWithValue("@Id", hdnId.Value);

                    cmd.Parameters.AddWithValue("@Title", txtTitle.Text);
                    cmd.Parameters.AddWithValue("@Pdf", MainImage + ext);



                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }




public bool CheckSave()
{
    bool isOK = true;
    string message = string.Empty;


    if (txtTitle.Text.Trim().Equals(string.Empty))
    {
        isOK = false;
        message += "Title, ";
    }



    if (message.Length > 0)
    {
        message = message.Substring(0, message.Length - 2);
    }
    if (!isOK)
    {
        MyMessageBox1.ShowError("Please fill following fields <br />" + message);
    }
    return isOK;
}

public bool CheckUpdate()
{
    bool isOK = true;
    string message = string.Empty;

    if (txtTitle.Text.Trim().Equals(string.Empty))
    {
        isOK = false;
        message += "Title, ";
    }

    if (message.Length > 0)
    {
        message = message.Substring(0, message.Length - 2);
    }
    if (!isOK)
    {
        MyMessageBox1.ShowError("Please fill following fields <br />" + message);
    }
    return isOK;
}
protected void btnCancel_Click(object sender, EventArgs e)
    {
    Reset();
}


protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
{
    try
    {
        int Id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC Proc_IepfPdf 'GET_BY_ID'," + Id);
        hdnId.Value = dt.Rows[0]["Id"].ToString();
        txtTitle.Text = dt.Rows[0]["Title"].ToString();
        PdfPreview.Visible = true;
        PdfPreview.NavigateUrl = string.Format("~/uploads/press_coverage/" + dt.Rows[0]["Pdf"].ToString());

        btnSave.Text = "Update";

    }
    catch (Exception ex)
    {
        this.Title = ex.Message;
    }
}


protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
{
    try
    {
        using (SqlCommand cmd = new SqlCommand("Proc_IepfPdf"))
        {
            int Id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Para", "DELETE");
            cmd.Parameters.AddWithValue("@Id", Id);
            utility.Execute(cmd);
            BindGrid();
            MyMessageBox1.ShowSuccess("Record Deleted Successfully.");
        }
    }
    catch (Exception ex)
    {
        MyMessageBox1.ShowError("Unable to delete record.");
    }
}
protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
{
    gdView.PageIndex = e.NewPageIndex;
    BindGrid();
}

private void BindGrid()
{
    try
    {
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC Proc_IepfPdf 'GET'");

        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;



        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();


        }
    }
    catch (Exception ex)
    {
        MyMessageBox1.ShowError("Some error occurred, While fetching records.");
    }
}

    private void Reset()
    {

        hdnId.Value = string.Empty;

        txtTitle.Text = string.Empty;
        btnSave.Text = "Save";
        PdfPreview.Visible = false;

    }
}