﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_NewsVideos : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        BindGrid(); 
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {


        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {


                using (SqlCommand cmd = new SqlCommand("Proc_MediaNewsVideos"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "ADD");

                    //cmd.Parameters.AddWithValue("@VideoTitle", txtVideoTitle.Text);
                    cmd.Parameters.AddWithValue("@NewsVideosLink", txtNewsVideo.Text);


                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {

                using (SqlCommand cmd = new SqlCommand("Proc_MediaNewsVideos"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "UPDATE");
                    cmd.Parameters.AddWithValue("@Id", hdnId.Value);

                    //cmd.Parameters.AddWithValue("@VideoTitle", txtVideoTitle.Text);


                    cmd.Parameters.AddWithValue("@NewsVideosLink", txtNewsVideo.Text);


                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }

    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;


       
        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;

       
        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    private void Reset()
    {

        //txtVideoTitle.Text = string.Empty;
        txtNewsVideo.Text = string.Empty;

        btnSave.Text = "Save";
    }
    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            int Id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_MediaNewsVideos 'GET_BY_ID'," + Id);
            hdnId.Value = dt.Rows[0]["Id"].ToString();

            //txtVideoTitle.Text = dt.Rows[0]["VideoTitle"].ToString();
            txtNewsVideo.Text = dt.Rows[0]["NewsVideosLink"].ToString();


           


            btnSave.Text = "Update";

        }
        catch (Exception ex)
        {

        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("Proc_MediaNewsVideos"))
            {
                int Id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Delete");
                cmd.Parameters.AddWithValue("@Id", Id);
     
                utility.Execute(cmd);
                BindGrid();
                MyMessageBox1.ShowSuccess("Record Deleted Successfully.");
            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Unable to delete record.");
        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    private void BindGrid()
    {
        try
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = utility.Display("EXEC Proc_MediaNewsVideos 'GET'");
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Some error occurred, While fetching records.");
        }
    }
   
}