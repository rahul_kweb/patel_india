﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="Location.aspx.cs" Inherits="Admin_Location" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">

    <div class="page-header">
        Location
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Category<span class="required">*</span> :
        </label>
        <br />
        <asp:DropDownList ID="ddlCategory" runat="server" CssClass="dropdown">
            <asp:ListItem Value="0" Text="Select Category"></asp:ListItem>
            <asp:ListItem Value="1" Text="PATEL Roadways Branches"></asp:ListItem>
            <asp:ListItem Value="2" Text="PATEL AIRFREIGHT Branches"></asp:ListItem>
        </asp:DropDownList>
        <br />

        <label class="control-label">
            Zone<span class="required">*</span> :
        </label>
        <br />
        <asp:DropDownList ID="ddlZone" runat="server" CssClass="dropdown">
            <asp:ListItem Value="0" Text="Select"></asp:ListItem>
            <asp:ListItem Value="1" Text="CENTRAL REGION"></asp:ListItem>
            <asp:ListItem Value="2" Text="EAST REGION"></asp:ListItem>
            <asp:ListItem Value="3" Text="NORTH REGION"></asp:ListItem>
            <asp:ListItem Value="4" Text="SOUTH REGION"></asp:ListItem>
            <asp:ListItem Value="5" Text="WEST REGION"></asp:ListItem>
              <asp:ListItem Value="6" Text="GUJARAT  REGION"></asp:ListItem>
           
        </asp:DropDownList>
        <br />


        <label class="control-label">
            City/State<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtCity" runat="server" CssClass="textbox"></asp:TextBox>
        <br />

        <label class="control-label">
            Branch code / Location code<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtBrlLocCode" runat="server" CssClass="textbox"></asp:TextBox>
        <br />

        <label class="control-label">
            Branch Type<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtBrType" runat="server" CssClass="textbox"></asp:TextBox>
        <br />

        <label class="control-label">
            Ownership<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtOwnership" runat="server" CssClass="textbox"></asp:TextBox>
        <br />

        <label class="control-label">
            Contact Person<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtManagerName" runat="server" CssClass="textbox" TextMode="MultiLine"></asp:TextBox>
        <br />

        <label class="control-label">
            Address<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtAddress" runat="server" CssClass="textbox" TextMode="MultiLine"></asp:TextBox>
        <br />

        <label class="control-label">
            Telephone<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtTelephone" runat="server" CssClass="textbox"></asp:TextBox>
        <br />

        <label class="control-label">
            Mobile<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtMobile" runat="server" CssClass="textbox"></asp:TextBox>
        <br />

        <label class="control-label">
            Fax<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtFax" runat="server" CssClass="textbox"></asp:TextBox>
        <br />

        <label class="control-label">
            Email1<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtEmail" runat="server" CssClass="textbox"></asp:TextBox>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w{2,4}([-.]\w{2,4})*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w{2,4}([-.]\w{2,4})*)*" ForeColor="Red" ErrorMessage="Please Enter Valid Email Id" ControlToValidate="txtEmail"></asp:RegularExpressionValidator>
        <br />

        <label class="control-label">
            Email2<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtEmail2" runat="server" CssClass="textbox"></asp:TextBox>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w{2,4}([-.]\w{2,4})*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w{2,4}([-.]\w{2,4})*)*" ForeColor="Red" ErrorMessage="Please Enter Valid Email Id" ControlToValidate="txtEmail2"></asp:RegularExpressionValidator>

        <br />

        <label class="control-label">
            Email3<span class="required">*</span> :
        </label>
        <br />
        <asp:TextBox ID="txtEmail3" runat="server" CssClass="textbox"></asp:TextBox>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w{2,4}([-.]\w{2,4})*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w{2,4}([-.]\w{2,4})*)*" ForeColor="Red" ErrorMessage="Please Enter Valid Email Id" ControlToValidate="txtEmail3"></asp:RegularExpressionValidator>

        <br />
        <br />

        <asp:Button ID="btnSave" Text="Save" CssClass="button" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="Cancel" CssClass="button" OnClick="btnCancel_Click" runat="server" />
        <br />



    </div>



    <div align="center">
        <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
    </div>
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
            OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging"
            CssClass="mGrid" PageSize="10" AllowPaging="true">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="ID" />
                <asp:BoundField DataField="Category" HeaderText="Category" />
                <asp:BoundField DataField="Zone" HeaderText="Zone" />
                <asp:BoundField DataField="City" HeaderText="City" />

                <asp:BoundField DataField="ManagerName" HeaderText="Contact_Person" />
                <asp:BoundField DataField="Address" HeaderText="Address" />
                <asp:BoundField DataField="Telephone" HeaderText="Telephone" />
                <asp:BoundField DataField="Mobile" HeaderText="Mobile" />
                <asp:BoundField DataField="Fax" HeaderText="Fax" />
                <asp:BoundField DataField="Email" HeaderText="Email" />
                <asp:BoundField DataField="Email2" HeaderText="Email2" />
                <asp:BoundField DataField="Email3" HeaderText="Email3" />
                <asp:BoundField DataField="Branch_Location_Code" HeaderText="Branch/Location Code" />
                <asp:BoundField DataField="BranchType" HeaderText="BranchType" />
                <asp:BoundField DataField="Ownership" HeaderText="Ownership" />

                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                            CommandName="Select">
                            <i class="icon-edit"></i> Edit
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                            CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>


</asp:Content>

