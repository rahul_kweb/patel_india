﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_ContactUsDetails : AdminPage
{
    Utility utility = new Utility();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }




    private void BindGrid()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_ContactUs 'GET'");

            if (dt.Rows.Count > 0)
            {
                gdView.Columns[0].Visible = true;
                gdView.DataSource = dt;
                gdView.DataBind();
                gdView.Columns[0].Visible = false;
                lblmsg.Visible = false;

            }
            else
            {
                gdView.DataSource = null;
                gdView.DataBind();
                lblmsg.Visible = true;

            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Some error occurred, While fetching records.");
        }
    }






    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("Proc_ContactUs"))
            {
                int Id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Delete");
                cmd.Parameters.AddWithValue("@Id", Id);
                utility.Execute(cmd);
                BindGrid();
                MyMessageBox1.ShowSuccess("Record Deleted Successfully.");
            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Unable to delete record.");
        }
    }




    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }
}