﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Location : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                using (SqlCommand cmd = new SqlCommand("Proc_Location"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "ADD");
                    cmd.Parameters.AddWithValue("@Category", ddlCategory.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@CategoryId", ddlCategory.SelectedValue);                  
                    cmd.Parameters.AddWithValue("@Zone", ddlZone.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@ZoneId", ddlZone.SelectedValue);
                    cmd.Parameters.AddWithValue("@City", txtCity.Text);
                    cmd.Parameters.AddWithValue("@ManagerName", txtManagerName.Text);
                    cmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                    cmd.Parameters.AddWithValue("@Telephone", txtTelephone.Text);
                    cmd.Parameters.AddWithValue("@Mobile", txtMobile.Text);
                    cmd.Parameters.AddWithValue("@Fax", txtFax.Text);
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                    cmd.Parameters.AddWithValue("@Branch_Location_Code", txtBrlLocCode.Text);
                    cmd.Parameters.AddWithValue("@BranchType", txtBrType.Text);
                    cmd.Parameters.AddWithValue("@Ownership", txtOwnership.Text);
                    cmd.Parameters.AddWithValue("@Email2", txtEmail2.Text);
                    cmd.Parameters.AddWithValue("@Email3", txtEmail3.Text);

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {

                using (SqlCommand cmd = new SqlCommand("Proc_Location"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "UPDATE");
                    cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                    cmd.Parameters.AddWithValue("@Category", ddlCategory.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@CategoryId", ddlCategory.SelectedValue); 
                    cmd.Parameters.AddWithValue("@Zone", ddlZone.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@ZoneId", ddlZone.SelectedValue);
                    cmd.Parameters.AddWithValue("@City", txtCity.Text);
                    cmd.Parameters.AddWithValue("@ManagerName", txtManagerName.Text);
                    cmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                    cmd.Parameters.AddWithValue("@Telephone", txtTelephone.Text);
                    cmd.Parameters.AddWithValue("@Mobile", txtMobile.Text);
                    cmd.Parameters.AddWithValue("@Fax", txtFax.Text);
                    cmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                    cmd.Parameters.AddWithValue("@Branch_Location_Code", txtBrlLocCode.Text);
                    cmd.Parameters.AddWithValue("@BranchType", txtBrType.Text);
                    cmd.Parameters.AddWithValue("@Ownership", txtOwnership.Text);
                    cmd.Parameters.AddWithValue("@Email2", txtEmail2.Text);
                    cmd.Parameters.AddWithValue("@Email3", txtEmail3.Text);

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        if (ddlCategory.SelectedValue == "0")
        {
            isOK = false;
            message += "Category,";
        }

        if (ddlZone.SelectedValue == "0")
        {
            isOK = false;
            message += "Zone, ";
        }

        if (txtCity.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "City, ";
        }

        //if (txtManagerName.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Managername, ";
        //}

        if (txtAddress.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Address, ";
        }

        //if (txtTelephone.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Telephone, ";
        //}

        //if (txtMobile.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Mobile, ";
        //}


        //if (txtFax.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Fax, ";
        //}

        //if (txtEmail.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Email, ";
        //}

        //if (txtBrlLocCode.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Branch/Location code, ";
        //}

        //if ( txtBrType.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Branch Type, ";
        //}

        //if (txtOwnership.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Ownership";
        //}
        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;

        if (ddlCategory.SelectedValue == "0")
        {
            isOK = false;
            message += "Category,";
        }

        if (ddlZone.SelectedValue == "0")
        {
            isOK = false;
            message += "Zone, ";
        }

        if (txtCity.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "City, ";
        }

        //if (txtManagerName.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Managername, ";
        //}

        if (txtAddress.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Address, ";
        }

        //if (txtTelephone.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Telephone, ";
        //}

        //if (txtMobile.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Mobile, ";
        //}


        //if (txtFax.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Fax, ";
        //}

        //if (txtEmail.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Email, ";
        //}

        //if (txtBrlLocCode.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Branch/Location code, ";
        //}

        //if ( txtBrType.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Branch Type, ";
        //}

        //if (txtOwnership.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Ownership";
        //}

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    private void Reset()
    {

        hdnId.Value = string.Empty;

        ddlCategory.SelectedValue = "0";
        ddlZone.SelectedValue = "0";
        txtCity.Text = string.Empty;
        txtAddress.Text = string.Empty;
        txtManagerName.Text = string.Empty;
        txtTelephone.Text = string.Empty;
        txtMobile.Text = string.Empty;
        txtFax.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtBrlLocCode.Text = string.Empty;
        txtBrType.Text = string.Empty;
        txtOwnership.Text = string.Empty;
        txtEmail2.Text = string.Empty;
        txtEmail3.Text = string.Empty;
        btnSave.Text = "Save";


    }
    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            int Id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_Location 'GET_BY_ID'," + Id);
            hdnId.Value = dt.Rows[0]["Id"].ToString();

            //ddlCategory.SelectedItem.Text = dt.Rows[0]["Category"].ToString();
            ddlCategory.SelectedValue = dt.Rows[0]["CategoryId"].ToString();
            ddlZone.SelectedValue = dt.Rows[0]["ZoneId"].ToString();
           

            txtCity.Text = dt.Rows[0]["City"].ToString();
            txtManagerName.Text = dt.Rows[0]["ManagerName"].ToString();
            txtAddress.Text = dt.Rows[0]["Address"].ToString();
            txtTelephone.Text = dt.Rows[0]["Telephone"].ToString();
            txtMobile.Text = dt.Rows[0]["Mobile"].ToString();
            txtFax.Text = dt.Rows[0]["Fax"].ToString();
            txtEmail.Text = dt.Rows[0]["Email"].ToString();
            txtBrlLocCode.Text = dt.Rows[0]["Branch_Location_Code"].ToString();
            txtBrType.Text = dt.Rows[0]["BranchType"].ToString();
            txtOwnership.Text = dt.Rows[0]["Ownership"].ToString();
            txtEmail2.Text = dt.Rows[0]["Email2"].ToString();
            txtEmail3.Text = dt.Rows[0]["Email3"].ToString();
            btnSave.Text = "Update";

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("Proc_Location"))
            {
                int Id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Delete");
                cmd.Parameters.AddWithValue("@Id", Id);
                utility.Execute(cmd);
                BindGrid();
                MyMessageBox1.ShowSuccess("Record Deleted Successfully.");
            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Unable to delete record.");
        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    private void BindGrid()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_Location 'GET'");

            if (dt.Rows.Count > 0)
            {
                gdView.Columns[0].Visible = true;
                gdView.DataSource = dt;
                gdView.DataBind();
                gdView.Columns[0].Visible = false;
                lblmsg.Visible = false;

            }
            else
            {
                gdView.DataSource = null;
                gdView.DataBind();
                lblmsg.Visible = true;

            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Some error occurred, While fetching records.");
        }
    }

}