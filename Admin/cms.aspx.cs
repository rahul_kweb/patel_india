﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class admin_cms : AdminPage
{
    Utility utility = new Utility();
    DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["cms"] != null && Request.QueryString["cms"] != "")
        {
            int testInt;
            if (int.TryParse(Request.QueryString["cms"], out testInt))
            {
                hdnId.Value = Request.QueryString["cms"];
                if (!Page.IsPostBack)
                {
                    Bind();
                }
            }
        }
    }

    private void Bind()
    {
        
        try
        {
            hdnId.Value = Request.QueryString["cms"].ToString();

            dt = utility.Display("EXEC Proc_CMS 'GET_BY_ID'," + hdnId.Value);

            if (dt.Rows.Count > 0)
            {
                
                    txtDescription.Text = dt.Rows[0]["CMS_DESC"].ToString();

                    lblPageName.Text = dt.Rows[0]["CMS_HEADING"].ToString();
               
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            
            dt = null;
            
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        

        try
        {
          
            if (CheckSave())
            {

                try
                {
                  



                    using (SqlCommand cmd = new SqlCommand("Proc_CMS"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PARA","UPDATE");
                        cmd.Parameters.AddWithValue("@CMS_ID", hdnId.Value);
                        cmd.Parameters.AddWithValue("@CMS_DESC", txtDescription.Text.Trim());
                       
                        dt = utility.Display(cmd);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    utility = null;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            

        }
    }

    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;
        //if (txtDescription.Text.Trim() == string.Empty)
        //{
        //    isOK = false;
        //    message += "Content,";
        //}

       

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }
}