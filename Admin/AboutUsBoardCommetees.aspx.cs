﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_AboutUsBoardCommetees : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/uploads/bod/";



        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                if (FileImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(FileImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Image", ext, this, false);
                    FileImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_AboutUsBoardCommetees"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PARA", "ADD");


                    if (MainImage == "")
                    {
                        cmd.Parameters.AddWithValue("@Image", "");
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Image", MainImage + ext);

                    }

                    cmd.Parameters.AddWithValue("@Prefix",ddlPrefix.SelectedItem.Text.Trim());
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Position", txtPosition.Text.Trim());
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {
                if (FileImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(FileImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Images allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Image", ext, this, false);
                    FileImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }


                using (SqlCommand cmd = new SqlCommand("Proc_AboutUsBoardCommetees"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PARA", "UPDATE");
                    cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                    cmd.Parameters.AddWithValue("@Prefix", ddlPrefix.SelectedItem.Text.Trim());
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Position", txtPosition.Text.Trim());
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@Image", MainImage + ext);

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        if (!FileImage.HasFile)
        {
            message = "Image ,";
            isOK = false;
        }


        if (ddlPrefix.SelectedValue == "0")
        {
            isOK = false;
            message += "Prefix, ";
        }
        if (txtName.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Name, ";
        }

        if (txtPosition.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Position, ";
        }

        if (txtPosition.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {

        bool isOK = true;
        string message = string.Empty;
        if (ddlPrefix.SelectedValue == "0")
        {
            isOK = false;
            message += "Prefix, ";
        }
        if (txtName.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Name, ";
        }

        if (txtPosition.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Position, ";
        }

        if (txtPosition.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

       
        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }


    private void Reset()
    {
     
        hdnId.Value = string.Empty;
        btnSave.Text = "Save";
        ImagePreview.Visible = false;
        txtName.Text = string.Empty;
      txtPosition.Text = string.Empty;
     txtDescription.Text = string.Empty;
     ddlPrefix.SelectedValue = "0";
     

    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            int Id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_AboutUsBoardCommetees 'GET_BY_ID'," + Id);
            hdnId.Value = dt.Rows[0]["Id"].ToString();

            ImagePreview.Visible = true;
            ImagePreview.ImageUrl = string.Format("~/uploads/bod/" + dt.Rows[0]["Image"].ToString());
            txtName.Text=dt.Rows[0]["Name"].ToString();
            txtPosition.Text=dt.Rows[0]["Position"].ToString();
            txtDescription.Text=dt.Rows[0]["Description"].ToString();
            ddlPrefix.SelectedValue=dt.Rows[0]["Prefix"].ToString();
            btnSave.Text = "Update";

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("Proc_AboutUsBoardCommetees"))
            {
                int Id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PARA", "DELETE");
                cmd.Parameters.AddWithValue("@Id", Id);
                utility.Execute(cmd);
                BindGrid();
                MyMessageBox1.ShowSuccess("Record Deleted Successfully.");

                Reset();
            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Unable to delete record.");
        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    private void BindGrid()
    {
        try
         {
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_AboutUsBoardCommetees 'GET'");

            if (dt.Rows.Count > 0)
            {
                gdView.Columns[0].Visible = true;
                gdView.DataSource = dt;
                gdView.DataBind();
                gdView.Columns[0].Visible = false;

            }
            else
            {
                gdView.DataSource = null;
                gdView.DataBind();


            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Some error occurred, While fetching records.");
        }
    }

}