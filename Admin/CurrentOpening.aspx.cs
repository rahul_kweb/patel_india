﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_CurrentOpening : AdminPage
{
        Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }

    }

     protected void btnSave_Click(object sender, EventArgs e)
    {

        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                using (SqlCommand cmd = new SqlCommand("Proc_CurrentOpening"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PARA", "ADD");
                    cmd.Parameters.AddWithValue("@JobTitle", ddlJobTitle.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@JobType", ddlJobType.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@JobProfile",txtJobProfile.Text);
                    cmd.Parameters.AddWithValue("@Qualification", txtQualification.Text);
                    cmd.Parameters.AddWithValue("@CandidateProfile", txtCandidateProfile.Text);
                    cmd.Parameters.AddWithValue("@Experience", txtExperience.Text);
                    cmd.Parameters.AddWithValue("@Location", txtLocation.Text);
        
                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {

                using (SqlCommand cmd = new SqlCommand("Proc_CurrentOpening"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PARA", "UPDATE");
                    cmd.Parameters.AddWithValue("@JobId", hdnId.Value);
                    cmd.Parameters.AddWithValue("@JobTitle", ddlJobTitle.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@JobType", ddlJobType.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@JobProfile", txtJobProfile.Text);
                    cmd.Parameters.AddWithValue("@Qualification", txtQualification.Text);
                    cmd.Parameters.AddWithValue("@CandidateProfile",  txtCandidateProfile.Text);
                    cmd.Parameters.AddWithValue("@Experience", txtExperience.Text);
                    cmd.Parameters.AddWithValue("@Location", txtLocation.Text);
        
        

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }


            public bool CheckSave()
            {
                bool isOK = true;
                string message = string.Empty;

                if (ddlJobTitle.SelectedValue == "0")
                {
                    isOK = false;
                    message += "JobTitle,";
                }

                if (ddlJobType.SelectedValue == "0")
                {
                    isOK = false;
                    message += "JobType, ";
                }
                if (txtJobProfile.Text.Trim().Equals(string.Empty))
                {
                    isOK = false;
                    message += "JobProfile, ";
                }

                if (txtQualification.Text.Trim().Equals(string.Empty))
                {
                    isOK = false;
                    message += "Qualification, ";
                }
                if (txtCandidateProfile.Text.Trim().Equals(string.Empty))
                {
                    isOK = false;
                    message += "Candidate Profile, ";
                }

                if (txtExperience.Text.Trim().Equals(string.Empty))
                {
                    isOK = false;
                    message += "Experience, ";
                }

                if (txtLocation.Text.Trim().Equals(string.Empty))
                {
                    isOK = false;
                    message += "Location, ";
                }

                if (message.Length > 0)
                {
                    message = message.Substring(0, message.Length - 2);
                }
                if (!isOK)
                {
                    MyMessageBox1.ShowError("Please fill following fields <br />" + message);
                }
                return isOK;
            }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;

        if (ddlJobTitle.SelectedValue == "0")
        {
            isOK = false;
            message += "JobTitle,";
        }

        if (ddlJobType.SelectedValue == "0")
        {
            isOK = false;
            message += "JobType, ";
        }

        if (txtJobProfile.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "JobProfile, ";
        }

        if (txtQualification.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Qualification, ";
        }
        if (txtCandidateProfile.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Candidate Profile, ";
        }

        if (txtExperience.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Experience, ";
        }

        if (txtLocation.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Location, ";
        }

   


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    private void Reset()
    {
       
        hdnId.Value = string.Empty;

        
        ddlJobTitle.SelectedValue ="0";
        ddlJobType.SelectedValue ="0";
        txtJobProfile.Text = string.Empty;
        txtQualification.Text = string.Empty;
        txtExperience.Text = string.Empty;
        txtCandidateProfile.Text = string.Empty;
        txtLocation.Text = string.Empty;
       
        btnSave.Text = "Save";


    }
    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            int Id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_CurrentOpening 'GET_BY_ID'," + Id);
            hdnId.Value = dt.Rows[0]["JobId"].ToString();
            ddlJobTitle.SelectedValue= dt.Rows[0]["JobTitle"].ToString();
            ddlJobType.SelectedValue = dt.Rows[0]["JobType"].ToString();
            txtJobProfile.Text = dt.Rows[0]["JobProfile"].ToString();

            txtQualification.Text = dt.Rows[0]["Qualification"].ToString();
            txtCandidateProfile.Text = dt.Rows[0]["CandidateProfile"].ToString();
            txtExperience.Text = dt.Rows[0]["Experience"].ToString();
            txtLocation.Text = dt.Rows[0]["Location"].ToString();
   

            btnSave.Text = "Update";

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("Proc_CurrentOpening"))
            {
                int Id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Delete");
                cmd.Parameters.AddWithValue("@JobId", Id);
                utility.Execute(cmd);
                BindGrid();
                MyMessageBox1.ShowSuccess("Record Deleted Successfully.");
            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Unable to delete record.");
        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    private void BindGrid()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC Proc_CurrentOpening 'GET'");

            if (dt.Rows.Count > 0)
            {
                gdView.Columns[0].Visible = true;
                gdView.DataSource = dt;
                gdView.DataBind();
                gdView.Columns[0].Visible = false;
                lblmsg.Visible = false;

            }
            else
            {
                gdView.DataSource = null;
                gdView.DataBind();
                lblmsg.Visible = true;

            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Some error occurred, While fetching records.");
        }
    }

}

