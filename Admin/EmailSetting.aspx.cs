﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

public partial class admin_EmailSetting : AdminPage
{
    private DataTable dt;
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDetails();
        }
    }
    public void BindDetails()
    {
        
        try
        {
            dt = utility.Display("EXEC Proc_Email_Details 'GET'");
            if (dt.Rows.Count > 0)
            {
               
                txtHost.Text = dt.Rows[0]["Host"].ToString();
                txtPort.Text = dt.Rows[0]["Port"].ToString();
                txtUsername.Text = dt.Rows[0]["Username"].ToString();
                //txtPassword.Text = dt.Rows[0]["Password"].ToString();
                txtPassword.Attributes["value"] = dt.Rows[0]["Password"].ToString();
                txtCc.Text = dt.Rows[0]["Cc"].ToString();
                txtTo.Text = dt.Rows[0]["To"].ToString();
                txtBCc.Text = dt.Rows[0]["BCc"].ToString();
                txtPhno.Text = dt.Rows[0]["PhoneNo"].ToString();
                hdnId.Value= dt.Rows[0]["Id"].ToString();
                btnSave.Text = "Update";
            }
            else
            {
                btnSave.Text = "Save";
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
           
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        
        try
        {
            if (CheckSave())
            {


                using (SqlCommand cmd = new SqlCommand("Proc_Email_Details"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "ADD");
                    cmd.Parameters.AddWithValue("@Host", txtHost.Text);   // string.Empty                 
                    cmd.Parameters.AddWithValue("@Port", txtPort.Text);
                    cmd.Parameters.AddWithValue("@Username", txtUsername.Text);
                    cmd.Parameters.AddWithValue("@Password", txtPassword.Text);
                    cmd.Parameters.AddWithValue("@To", txtTo.Text);
                    cmd.Parameters.AddWithValue("@Cc", txtCc.Text);
                    cmd.Parameters.AddWithValue("@BCc", txtBCc.Text);
                    cmd.Parameters.AddWithValue("@PhoneNo", txtPhno.Text);
                    if (hdnId.Value=="")
                    {
                      cmd.Parameters.AddWithValue("@Id", 0);
                    }
                    else
                    {
                    cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                    }
                    cmd.Parameters.AddWithValue("@Added_Updated_By", AdminId);

                    if (utility.Execute(cmd))
                    {
                       // Reset();
                       BindDetails();
                        MyMessageBox1.ShowSuccess("Successfully saved");
                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }


        }
        catch (Exception ex)
        {

        }
    }
    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

     
        if (txtPort.Text.Trim() == string.Empty)
        {
            isOK = false;
            message += "Port, ";
        }
        if (txtHost.Text.Trim() == string.Empty)
        {
            isOK = false;
            message += "Host, ";
        }
        //if (txtUsername.Text.Trim() == string.Empty)
        //{
        //    isOK = false;
        //    message += "User Name, ";
        //}
        if (txtUsername.Text.Trim() == string.Empty || txtUsername.Text.Equals("Email"))
        {
            isOK = false;
            message += "Email, ";
        }
        else if (!Regex.IsMatch(txtUsername.Text, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"))
        {
            isOK = false;
            message += "invalid email address, ";
        }
        if (txtPassword.Text.Trim() == string.Empty)
        {
            isOK = false;
            message += "Password, ";
        }
        if (txtTo.Text.Trim() == string.Empty || txtUsername.Text.Equals("Email"))
        {
            isOK = false;
            message += "To address, ";
        }
        else if (!Regex.IsMatch(txtTo.Text, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"))
        {
            isOK = false;
            message += "invalid To email address, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }


    //protected void btnCancel_Click(object sender, EventArgs e)
    //{
    //    Reset();
    //}
    private void Reset()
    {
       
        txtHost.Text = string.Empty;
        txtPort.Text = string.Empty;
        txtUsername.Text = string.Empty;
        txtPassword.Text = string.Empty;
        txtCc.Text = string.Empty;
        txtTo.Text = string.Empty;
        txtBCc.Text = string.Empty;
    }
}