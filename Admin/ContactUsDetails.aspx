﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="ContactUsDetails.aspx.cs" Inherits="Admin_ContactUsDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" Runat="Server">

    
     <div class="page-header">
       Contact Us Details
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <br />
    <br />
    <br />
    <br />
    <div align="center">
        <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
    </div>
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False"
            OnRowDeleting="gdView_RowDeleting" OnPageIndexChanging="gdView_PageIndexChanging"
            CssClass="mGrid" PageSize="10" AllowPaging="true">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="ID" />
                <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                <asp:BoundField DataField="JobTitle" HeaderText="Job Title" />
              <asp:BoundField DataField="CompanyName" HeaderText="Company Name" />
                  <asp:BoundField DataField="City" HeaderText="City" />
                <asp:BoundField DataField="Pincode" HeaderText="Pincode" />
                <asp:BoundField DataField="Address1" HeaderText="Address1" />
                <asp:BoundField DataField="Address2" HeaderText="Address2" />
                <asp:BoundField DataField="Email" HeaderText="Email" />
                <asp:BoundField DataField="Mobile" HeaderText="Mobile" />
                <asp:BoundField DataField="TimeToContact" HeaderText="Time To Contact" />
                <asp:BoundField DataField="Industry" HeaderText="Industry" />
                <asp:BoundField DataField="Requirement" HeaderText="Requirement" />
                <asp:BoundField DataField="StorageVolume" HeaderText="Storage Volume" />
                 <asp:BoundField DataField="ServiceProvider" HeaderText="Service Provider" />
             
              <%--  <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                            CommandName="Select">
                            <i class="icon-edit"></i> Edit
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>--%>

                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                            CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>

</asp:Content>

