﻿<%@ Page Title="Investor Relations | Logistics Quarterly Results, Dividend, Annual Reports | Patel Integrated Logistics Ltd " Language="C#" MasterPageFile="~/HouseOfPatel.master" AutoEventWireup="true" CodeFile="quaterly_result.aspx.cs" Inherits="quaterly_result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form" runat="server">
        <asp:ScriptManager ID="scrManager" runat="server"></asp:ScriptManager>


        <link rel="stylesheet" href="css/bootstrap-select.min.css" />


        <section>

            <div class="container-fluid">

                <asp:Literal ID="ltrlBanner" runat="server"></asp:Literal>


                <div class="row">

                    <div class="wrapper">

                        <div class="share_holding_pattern_wrp">

                            <asp:Repeater ID="rptQuarterlyResult" runat="server" OnItemDataBound="rptQuarterlyResult_ItemDataBound">
                                <ItemTemplate>
                                    <div>
                                        <div class="share_holding_pattern_content equate">
                                            <div class="inner_title">
                                                <asp:Label ID="hdnFromYear" Text='<%#Eval("FromYear") %>' Style="display: none" runat="server" />
                                                <asp:Label ID="hdnToYear" Text='<%#Eval("ToYear") %>' Style="display: none" runat="server" />

                                                <h2><%#Eval("FromYear") %>/<%#Eval("ToYear") %> </h2>
                                            </div>

                                            <ul class="investor">
                                                <asp:Repeater ID="rptInside" runat="server">
                                                    <ItemTemplate>
                                                        <li>

                                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i><a href="uploads/pdf/<%#Eval("Pdf") %>" target="_blank"><%#Eval("Title") %></a>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </ul>

                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>

                        <div class="clearfix"></div>

                        <div class="archives_area">

                          <div class="form-group select_box">
                               
                              <asp:Label ID="Label1" CssClass="inner_title pull-left" runat="server"><h2>Archives :</h2></asp:Label>

                                 <asp:DropDownList ID="ddlArchieves" runat="server" class="selectpicker form-control no_padding pull-left" OnSelectedIndexChanged="ddlArchieves_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                                  
                                 <div class="clearfix"></div>
                            </div>

                            <asp:UpdatePanel ID="updateArchiveContent" runat="server">
                                <ContentTemplate>
                                    <div class="output text-center">
                                        <asp:Literal ID="ltrArchiveDetails" runat="server"></asp:Literal>
                                    </div>
                                </ContentTemplate>

                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlArchieves" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>

                    </div>

                </div>

            </div>

        </section>

        <script src="js/jquery.min.js"></script>

        <script src="js/bootstrap.min.js"></script>

        <script src="js/slick.min.js"></script>

        <script src="js/rem.min.js"></script>

        <script src="js/equate.js"></script>

        <script src="js/general.js"></script>

        <script src="js/css3-animate-it.js"></script>

        <script src="js/bootstrap-select.min.js"></script>

        <script>
            $(document).ready(function () {
                $('.share_holding_pattern_wrp').slick({
                    infinite: false,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    arrows: true,
                    dits: false,
                    speed: 1000,
                    responsive: [
                   {
                       breakpoint: 1260,
                       settings: {
                           slidesToShow: 3,
                           slidesToScroll: 1,
                           infinite: true,
                           dots: false
                       }
                   },
                   {
                       breakpoint: 792,
                       settings: {
                           slidesToShow: 2,
                           slidesToScroll: 1

                       }
                   },
                   {
                       breakpoint: 531,
                       settings: {
                           slidesToShow: 1,
                           slidesToScroll: 1,
                           adaptiveHeight: true
                       }
                   }
                    ]
                });
            });
        </script>

        <script>
            $(document).ready(function () {
                $('.archives_wrp').slick({
                    infinite: false,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    arrows: true,
                    dits: false,
                    speed: 1000,
                    responsive: [
                   {
                       breakpoint: 1260,
                       settings: {
                           slidesToShow: 3,
                           slidesToScroll: 1,
                           infinite: true,
                           dots: false
                       }
                   },
                   {
                       breakpoint: 792,
                       settings: {
                           slidesToShow: 2,
                           slidesToScroll: 1

                       }
                   },
                   {
                       breakpoint: 531,
                       settings: {
                           slidesToShow: 1,
                           slidesToScroll: 1,
                           adaptiveHeight: true
                       }
                   }
                    ]
                });


            });
        </script>
    </form>
</asp:Content>

