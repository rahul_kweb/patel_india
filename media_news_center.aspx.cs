﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class media_news_center : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        RepeterPressCoverage();
        RepeterNewsVideos();
        BindInnerBanner();
        RepeaterPdf();
    }

    public void RepeterPressCoverage()
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_MediaNewsPressCoverageimages 'GET'");


        if (dt.Rows.Count > 0)
        {
            rptPressCoverage.DataSource = dt;
            rptPressCoverage.DataBind();
        }
        else
        {
            rptPressCoverage.DataSource = null;
            rptPressCoverage.DataBind();
        }
    }


    public void RepeterNewsVideos()
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_MediaNewsVideos 'GET'");


        if (dt.Rows.Count > 0)
        {
            rptNewsVideos.DataSource = dt;
            rptNewsVideos.DataBind();
        }
        else
        {
            rptNewsVideos.DataSource = null;
            rptNewsVideos.DataBind();
        }

    }

    public void BindInnerBanner()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner'GET_BY_ID',1006");

        if (dt.Rows.Count > 0)
        {
            StringBuilder strbuild = new StringBuilder();
            strbuild.Append("<div class=\"row inner_banner_wrp\" style=\"background:url(uploads/Images/" + dt.Rows[0]["InnerBannerImage"].ToString() + ") no-repeat center; background-size:cover;\">");
            strbuild.Append("<div class=\"inner_page_title\">");
            strbuild.Append("<div class=\"inner_page_title_txt text-center\">");
            strbuild.Append("<h1>Media & News Cente</h1>");
            strbuild.Append("<ul class=\"breadcrumb\">");
            strbuild.Append("<li><a href=\"index.aspx\">Home</a></li>");
            strbuild.Append("<li>Media & News Cente</li>");
            strbuild.Append("</ul>");
            strbuild.Append("</div>");
            strbuild.Append("</div>");
            strbuild.Append("</div>");

            ltrlBanner.Text = strbuild.ToString();
        }
    }

    public void RepeaterPdf()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("exec Proc_MediaPdf 'Fornt_Get'");
        if (dt.Rows.Count > 0)
        {
            rptPdf.DataSource = dt;
            rptPdf.DataBind();
        }
        else
        {
            rptPdf.DataSource = null;
            rptPdf.DataBind();
        }
    }

}