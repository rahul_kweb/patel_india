﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class csr : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCsrContent();
        }
    }

    public void BindCsrContent()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'GET_BY_ID',1002");
        if (dt.Rows.Count > 0)
        {
            ltrCsrContent.Text = dt.Rows[0]["CMS_Desc"].ToString();
        }
    }
}