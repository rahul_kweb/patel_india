﻿


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /><link rel="shortcut icon" href="images/fav_icon.png" />
    <!-- css starts -->
    <link rel="stylesheet" href="css/bootstrap.min.css" /><link rel="stylesheet" href="css/font-awesome.min.css" /><link rel="stylesheet" href="css/animations.css" /><link rel="stylesheet" href="css/slick.css" /><link rel="stylesheet" href="css/slick-theme.css" /><link rel="stylesheet" href="css/nav-layout.min.css" /><link rel="stylesheet" href="css/jquery.fancybox.min.css" /><link rel="stylesheet" href="css/general.css" /><link rel="stylesheet" href="css/inner_page.css" />
    <!-- css ends -->

    
<title>
	Locations-PATEL | PAN India Network
</title></head>
<body>
    <header id="myHeader">

        <div class="container-fluid no_padding">

            <div class="row wrapper top_box">

                <div class="logo pull-left">
                    <a href="index.aspx">
                        <img src="images/logo.png" title="Patel Integrated Logistics Ltd." class="logo_on img-responsive">
                    </a>
                </div>

                <div class="header_content_left pull-right">

                    <div class="data pull-left rate">
                        <ul>
                            <li>NSE <span>
                                <img src="images/green_arrow.png" alt=""></span>816.95</li>
                            <li>BSE <span>
                                <img src="images/green_arrow.png" alt=""></span>816.95</li>
                        </ul>
                    </div>

                    <div class="data pull-left" style="margin-top:31px;"><i class="fa fa-volume-control-phone" aria-hidden="true" style="margin-right: 10px;"></i>1800 22 3666</div>

                    <div class="data pull-left social">

                        <a href="https://www.facebook.com/PatelIntegratedLogistics/" target="_blank" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="https://twitter.com/PatelIntegrated" target="_blank" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="https://www.linkedin.com/company/patel-integrated-logistics/" target="_blank" title="LinkedIn"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        <a href="https://www.youtube.com/channel/UClBowcRqT2UUqDBAjk6NycQ" target="_blank" title="Youtube"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                        <a href="https://plus.google.com/u/1/104380753397682066917" target="_blank" title="Google Plus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                        <a href="https://www.instagram.com/patelintegratedlogistics/" target="_blank" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>

                    </div>

                    <div class="data pull-left">

                        <div class="head_search_wrp">
                            <input type="text" placeholder="Search" class="form-control"><span class="text-center fade_anim"><i class="fa fa-search" aria-hidden="true"></i></span>
                        </div>

                    </div>

                </div>

                <div class="clearfix"></div>

            </div>

        </div>

    </header>
    <!-- #BeginLibraryItem "/Library/menu.lbi" -->

    <div class="menu_wrp">

        <a href="#" class="nav-button"></a>

        <nav class="nav">

            <ul class="navigation">

                <li><a href="about_us.aspx">ABOUT US</a></li>

                <li><a href="location.aspx">LOCATIONS</a></li>

                <li class="nav-submenu"><a href="#">OUR SERVICES</a>
                    <ul>
                        <li><a href="patel_roadways.aspx">PATEL ROADWAYS</a></li>
                        <li><a href="patel_express.aspx">PATEL EXPRESS</a></li>
                        <li><a href="patel_airfreight.aspx">PATEL AIRFREIGHT</a></li>
                        <li><a href="patel_warehouse.aspx">PATEL WAREHOUSE</a></li>
                        
                    </ul>
                </li>

                <li><a href="careers.aspx">CAREERS</a></li>

                <li class="nav-submenu"><a href="#">INVESTOR RELATION</a>
                    <ul>
                        <li class="nav-submenu"><a href="#">INVESTOR INFORMATION</a>
                            <ul>
                                <li><a href="share_holding_pattern.aspx">SHARE HOLDING PATTERN</a></li>
                                <li><a href="quaterly_result.aspx">QUARTERLY RESULTS</a></li>
                                <li><a href="annual_reports.aspx">ANNUAL REPORTS</a></li>
                                <li><a href="corporate_announcement.aspx">CORPORATE ANNOUNCEMENTS</a></li>
                                <li><a href="images/pdf/AGM_Chairman_Speech.pdf" target="_blank">CHAIRMAN SPEECH</a></li>
                                <li><a href="unclaimed_dividend.aspx">UNCLAIMED DIVIDEND</a></li>
                            </ul>
                        </li>
                        <li><a href="contacts_for_investor.aspx">CONTACTS FOR INVESTOR</a></li>
                        <li><a href="images/pdf/CodeofConduct.pdf" target="_blank">CODE OF CONDUCT</a></li>
                        <li class="nav-submenu"><a href="#">INSIDER TRADING CODE</a>
                            <ul>
                                <li><a href="images/pdf/PILL-Code-of-Conduct-for-Insider-Trading.pdf" target="_blank">PILL CODE OF CONDUCT FOR INSIDER TRADING</a></li>
                                <li><a href="images/pdf/PILL-Code-of-Fair-Disclosure.pdf" target="_blank">PILL CODE OF FAIR DISCLOSURE</a></li>
                            </ul>
                        </li>
                        <li><a href="images/pdf/NoticeoftheClosureofTradingWindow_n.pdf" target="_blank">CLOSURE OF TRADING WINDOW</a></li>
                    </ul>
                </li>

                <li><a href="media_news_center.aspx">MEDIA & NEWS CENTER</a>

                    <!--<ul>
                    <li><a href="#">PRESS COVERAGE</a></li>
                    <li><a href="#">EVENTS</a></li>
                    <li><a href="#">RECENT NEWS</a></li>
                </ul>-->

                </li>

                <li><a href="contact_us.aspx">CONTACT US</a></li>

            </ul>

            <div class="text-center mobile_call"><i class="fa fa-volume-control-phone" aria-hidden="true" style="margin-right: 10px;"></i>1800 22 3666</div>

            <div class="text-center mobile_social">
                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            </div>

            <div class="head_search_wrp mobile_search">
                <input type="text" placeholder="Search" class="form-control"><span class="text-center fade_anim"><i class="fa fa-search" aria-hidden="true"></i></span>
            </div>

        </nav>

    </div>

    <div>
        




    <section>

        <div class="container-fluid">

            <div class="row inner_banner_wrp map_wrp"></div>

            <div class="row location_wrp wrapper">

                <div class="col-md-12 col-sm-12 col-xs-12 head_off_address row">
                    <div class="inner_title">
                        <h1>HEAD OFFICE</h1>
                    </div>
                    <p>PATEL INTEGRATED LOGISTICS LTD., PATEL HOUSE, 48 Gazdar Bandh, North Avenue Road, Santacruz (West), Mumbai, India – 400 054</p>
                </div>

                <div class="clearfix"></div>

                <div class="regional_wrp_head">

                    <div class="regional_wrp_head_txt pull-left">
                        <h2>Patel Roadways Branches</h2>
                    </div>

                    <div class="address_search pull-right">

                        <input type="text" placeholder="Search" class="form-control"><span class="text-center fade_anim"><i class="fa fa-search" aria-hidden="true"></i></span>

                    </div>

                    <div class="clearfix"></div>

                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 patel_roadways_address_wrp" id="tab1">

                    <div class="row">

                        

                        <div class="clearfix"></div>

                        <ul class="tabs tab_1">
                            <li><a class="active">CENTRAL REGION</a></li>
                            <li><a href="#east_zone">EAST REGION</a></li>
                            <li><a href="#north_zone">NORTH REGION</a></li>
                            <li><a href="#south_zone">SOUTH REGION</a></li>
                            <li><a href="#west_zone">WEST REGION</a></li>
                        </ul>

                        <ul class="tab__content">

                            <li class="active"> 

                                <div class="content__wrapper">
                    
                    			<div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Indore Regional Office</span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  0810</p>

                                    <p> <span class="bl_clr">Branch Type : </span> Regional Office - Godown</p>

                                    <p><span class="bl_clr"> Ownership : </span> OWN Branch</p>

                                    <p><span class="bl_clr"> Contact Person : </span> Mr. Sachin Gour </p>

                                    <p><span class="bl_clr"> Add : </span> Patel Roadways 124/1/1, S.R Compound Dewas Naka, Indore -452010</p>
                        
                                    <p><span class="bl_clr"> Mob. No. : </span> 8358999971</p>

                        
                        
                    </div>
                    
                    			<div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Indore</span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  0185</p>

                                    <p> <span class="bl_clr">Branch Type : </span> Transshipment - Godown</p>

                                    <p><span class="bl_clr"> Ownership : </span> OWN Branch</p>

                                    <p><span class="bl_clr"> Contact Person : </span> Mr. N K Shukla </p>

                                    <p><span class="bl_clr"> Add : </span> Patel Roadways 124/1/1, S.R Compound Dewas Naka, Indore -452010</p>
                        
                                    <p><span class="bl_clr"> Mob. No. : </span> 9981120226</p>
                        
                    </div>
                    
                    			<div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Indore </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  1243</p>

                                    <p> <span class="bl_clr">Branch Type : </span> BOOKING / DELIVERY BRANCH - Godown</p>

                                    <p><span class="bl_clr"> Ownership : </span> OWN Branch</p>

                                    <p><span class="bl_clr"> Contact Person : </span> Mr. Pradeep </p>

                                    <p><span class="bl_clr"> Add : </span> Patel Roadways 124/1/1, S.R Compound Dewas Naka, Indore -452010</p>
                        
                                    <p><span class="bl_clr"> Mob. No. : </span> 9009020083</p>
                        
                    </div>

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Indore </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  1007</p>

                                    <p> <span class="bl_clr">Branch Type : </span> BOOKING - Godown</p>

                                    <p><span class="bl_clr"> Ownership : </span> Business Associate</p>

                                    <p><span class="bl_clr"> Contact Person : </span> Mr. A R Shyju </p>

                                    <p><span class="bl_clr"> Add : </span> Patel Roadways 124/1/1, S.R Compound Dewas Naka, Indore -452010</p>
                        
                                    <p><span class="bl_clr"> Mob. No. : </span> 9826041508</p>
                        
                    </div>

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Bhopal </span>

                                    <p> <span class="bl_clr"> Branch Code :</span> 0154</p>

                                    <p> <span class="bl_clr">Branch Type : </span> BOOKING / DELIVERY BRANCH - Godown</p>

                                    <p><span class="bl_clr"> Ownership : </span> OWN Branch</p>

                                    <p><span class="bl_clr"> Contact Person : </span> Mr. Ashish Tiwari </p>

                                    <p><span class="bl_clr"> Add : </span> Patel Roadways, 40-A Industrial Area Govindpura, Bhopal 462023</p>
                        
                                    <p><span class="bl_clr"> Mob. No. : </span> 9303086637 / 0755-4246165</p>
                        
                    </div>

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Mandideep
 </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  0233
</p>

                                    <p> <span class="bl_clr">Branch Type : </span> BOOKING / DELIVERY BRANCH - Godown</p>

                                    <p><span class="bl_clr"> Ownership : </span> OWN Branch</p>

                                    <p><span class="bl_clr"> Contact Person : </span> Mr. Ashish Tiwari </p>

                                    <p><span class="bl_clr"> Add : </span> Patel Roadways, B228 Indra Nagar, Mandideep 462046</p>
                        
                                    <p><span class="bl_clr"> Mob. No. : </span> 9303086637 / 07480-407494</p>
                        
                    </div>

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Pithampur
 </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  0207</p>

                                    <p> <span class="bl_clr">Branch Type : </span> BOOKING / DELIVERY BRANCH - Godown</p>

                                    <p><span class="bl_clr"> Ownership : </span>Business Associate
</p>

                                    <p><span class="bl_clr"> Contact Person : </span> Mr. Yogesh Jaiswal </p>

                                    <p><span class="bl_clr"> Add : </span> Patel Roadways, Campacola Campus Sector No-01, Main Road, Pithampur</p>
                        
                                    <p><span class="bl_clr"> Mob. No. : </span> 9826476196 / 9300730238</p>
                        
                    </div>

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Pithampur
 </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  0207</p>

                                    <p> <span class="bl_clr">Branch Type : </span> BOOKING / DELIVERY BRANCH - Office</p>

                                    <p><span class="bl_clr"> Ownership : </span>Business Associate
</p>

                                    <p><span class="bl_clr"> Contact Person : </span> Mr. Yogesh Jaiswal </p>

                                    <p><span class="bl_clr"> Add : </span> Patel Roadways, Campacola Campus Sector No-01, Main Road, Pithampur</p>
                        
                                    <p><span class="bl_clr"> Mob. No. : </span> 9826476196 / 9300730238</p>
                        
                    </div>

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Gwalior
 </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  0456</p>

                                    <p> <span class="bl_clr">Branch Type : </span> BOOKING / DELIVERY BRANCH - Godown</p>

                                    <p><span class="bl_clr"> Ownership : </span> Business Associate
</p>

                                    <p><span class="bl_clr"> Contact Person : </span> Mr. Jadon </p>

                                    <p><span class="bl_clr"> Add : </span> Patel Roadways, Bst Transport Near Dargha parking 06, Transport Nagar, Gwalior</p>
                        
                                    <p><span class="bl_clr"> Mob. No. : </span> 9425776218</p>
                        
                    </div>

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Indore Transport Nagar </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  0011</p>

                                    <p> <span class="bl_clr">Branch Type : </span> BOOKING / DELIVERY BRANCH - Godown</p>

                                    <p><span class="bl_clr"> Ownership : </span> OWN Branch</p>

                                    <p><span class="bl_clr"> Contact Person : </span> Mr. Jadon </p>

                                    <p><span class="bl_clr"> Add : </span> Patel Roadways, 211 Transport Nagar, Indore - 452011</p>
                        
                                    <p><span class="bl_clr"> Mob. No. : </span> 9425776218</p>
                        
                    </div>

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Dewas   </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  0213</p>

                                    <p> <span class="bl_clr">Branch Type : </span> BOOKING / DELIVERY BRANCH - Godown</p>

                                    <p><span class="bl_clr"> Ownership : </span> Business Associate
</p>

                                    <p><span class="bl_clr"> Contact Person : </span>Mr. N K Shukla</p>

                                    <p><span class="bl_clr"> Add : </span> Patel Roadways, 111 Station Road, Dewas</p>
                        
                                    <p><span class="bl_clr"> Mob. No. : </span> 9981120226 / 9300132509 </p>
                        
                    </div>

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> GUNA </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  1466</p>

                                   



                                    <p><span class="bl_clr"> Contact Person : </span> Mr. Saurabh Bhargava </p>

                                    <p><span class="bl_clr"> Add : </span> Patel Roadways, 28 Hanuman Colony, Near Shubham Hotel. Guna – 473001 </p>
                        
                                    <p><span class="bl_clr"> Mob. No. : </span> 9425766916 / 9109883584</p>
                        
                    </div>


                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Nasik </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  823</p>

                                    <p> <span class="bl_clr"> Branch Type :</span>  Regional Office</p>
                                
                                    <p><span class="bl_clr"> Add : </span> Plot No.P-39, Behind ITI, Near Radhakrishna Hotel, Satpur MIDC, Nasik-422007
</p>
                        
                        
                                </div>

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Satpur </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  112</p>

                                    <p> <span class="bl_clr"> Branch Type :</span>  Regional Office</p>
                                
                                    <p><span class="bl_clr"> Add : </span> Plot No.P-39, Behind ITI, Near Radhakrishna Hotel, Satpur MIDC, Nasik-422007</p>
                        
                        
                                </div>

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Nasik
 </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  174
</p>

                                    <p> <span class="bl_clr"> Branch Type :</span>  Regional Office</p>
                                
                                    <p><span class="bl_clr"> Add : </span> Plot No.P-39, Behind ITI, Near Radhakrishna Hotel, Satpur MIDC, Nasik-422007</p>
                        
                        
                                </div>

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Nasik
 </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  1455

</p>


                                    <p> <span class="bl_clr"> Branch Type :</span>  Transshipment</p>
                                
                                    <p><span class="bl_clr"> Add : </span> Plot No.P-39, Behind ITI, Near Radhakrishna Hotel, Satpur MIDC, Nasik-422007</p>
                        
                        
                                </div>


                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Sinnar</span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  710</p>

                                    <p><span class="bl_clr"> Add : </span> Plot No.D-23,Opp.Jindal Behind, Telephone Exchange, Malegaon MIDC,Sinnar-422003</p>
                                
                                </div>


                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Malegaon
</span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  490
</p>

                                    <p><span class="bl_clr"> Add : </span> Survey No.18,Plot No.2, Old Agra Road, Ghodewala, Compound Near New Busstand,Opp.
Pila Petrol Pump,Malegaon-413115

</p>
                                
                                </div>


                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Jalgaon

</span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  263

</p>

                                    <p><span class="bl_clr"> Add : </span> C/o Bagul Industries Behind, Hotel Kasturi, Mehrun Road MIDC Area, Jalgaon-425003

</p>
                                
                                </div>

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Dhulia</span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  172</p>

                                    <p><span class="bl_clr"> Add : </span> 14-1, Kotkar Compound Avdhan corner, Bombay Agra Road Avdhan,Dhulia-424311</p>
                                
                                </div>


                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Sangamner</span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  704</p>

                                    <p><span class="bl_clr"> Add : </span> Shop No.3, Akole-Nasik Bypass Road, Near Shramik Mangal Karyala Sangamner-422605</p>
                                
                                </div>


                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> NAGPUR REGIONAL OFFICE</span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  704</p>

                                     <p> <span class="bl_clr"> Branch Type :</span>  Regional Office</p>

                                    <p><span class="bl_clr"> Add : </span> Off. Khadgaon Road, Wadi, Amravati Road, Nagpur. 440023</p>
                                
                                </div>


                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> WADI</span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  386</p>

                                    <p><span class="bl_clr"> Add : </span> Off. Khadgaon Road, Wadi, Amravati Road, Nagpur. 440023</p>
                                
                                </div>


                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> NAGPUR</span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  1245</p>

                                   <p> <span class="bl_clr"> Branch Type :</span> DELIVERY HUB </p>

                                    <p><span class="bl_clr"> Add : </span> Ward No. 2, Khasra No. 282/2, House No. 355, Village Lawa, Nagpur. PIN - 440023</p>
                                
                                </div>

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> NAGPUR </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  372</p>

                                    <p> <span class="bl_clr"> Branch Type :</span>   TRANSSHIPMENT </p>

                                    <p><span class="bl_clr"> Add : </span> Ward No. 2, Khasra No. 282/2, House
No. 355, Village Lawa, Nagpur.
PIN - 440023

</p>
                                
                                </div>


                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Raipur  </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  1328</p>

<p><span class="bl_clr"> Add : </span> Sarvoday colony Gayatri Nagar
Near Chattishgarh public school Hirapur Road 
Raipur -4920001



</p>
                                
                                </div>
                            

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Jabalpur  </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  267</p>

<p><span class="bl_clr"> Add : </span>Plot No.139/12, Main Road
Cherital, Jabalpur 
PIN-482002

</p>
                                
                                </div>

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Akola  </span>

                                    <p> <span class="bl_clr"> Branch Code :</span>  1263</p>

<p><span class="bl_clr"> Add : </span>Chhatrapati Shivaji Maharaj Sankul
Near NH-6 by-pass, Gram Panchyat, Shivar,
Akola  PIN-444164


</p>
                                
                                </div>

                            </div>

                            </li>


                            <li>
                                <div class="content__wrapper">

                                <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
                        
                                    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> KOLKATA
  </span>


<p><span class="bl_clr"> Add : </span>20, GANGADHAR, BABU LANE, OPP. ISLAMIA HOSPITAL, ON CHITTARANJAN AVENUE, KOLKATA - 700012



</p>
                                
                                </div>

                                </div>
                            </li>


                            <li>

                                <div class="content__wrapper">

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Delhi Regional Office  </span>

                                        <p><span class="bl_clr">Branch Code :</span>  0842</p>

                                        <p><span class="bl_clr">Ownership :</span> OWN BRANCH</p>

                                        <p><span class="bl_clr">Add : </span>Caxton House, E-2, Jhandewalan Extension, Rani Jhansi Road, New Delhi - 110055</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Ram Road</span>

                                        <p><span class="bl_clr">Branch Code :</span>  143</p>

                                        <p><span class="bl_clr">Ownership : </span>OWN Branch</p>

                                        <p><span class="bl_clr">Add : </span>E-1 Kailash Park, Kirti Nagar Pin code- 110015</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>UP Border 
                                        </span>

                                        <p>
                                            <span class="bl_clr">Branch Code :</span>  97
                                        </p>

                                        <p><span class="bl_clr">Ownership : </span>OWN Branch</p>

                                        <p><span class="bl_clr">Add : </span>06th Mile Stone, Near Balaji Dharam Kanta Up Border - 201009</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Noida</span>

                                        <p><span class="bl_clr">Branch Code :</span>  0264</p>

                                        <p><span class="bl_clr">Ownership : </span>OWN Branch</p>

                                        <p><span class="bl_clr">Add : </span>f-360 Sector-63, Noida Disst- Gatuam Budh Nagar, Uttar Pradesh </p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Okhla </span>

                                        <p><span class="bl_clr">Branch Code :</span>  0665</p>

                                        <p><span class="bl_clr">Ownership : </span>OWN Branch</p>

                                        <p><span class="bl_clr">Add : </span>X-41, Okhla Indl.Area, Phase - II, New Delhi - 110020</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Gurgaon  </span>

                                        <p><span class="bl_clr">Branch Code :</span>  0131</p>

                                        <p><span class="bl_clr">Ownership : </span>OWN Branch</p>

                                        <p><span class="bl_clr">Add : </span>A-22, Palam Udyog, Plot No.3, Sector - 18,Gurgaon - 122015</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Manesar  </span>

                                        <p><span class="bl_clr">Branch Code :</span>  0784</p>

                                        <p><span class="bl_clr">Branch Type :</span> HUB / TRANSSHIPMENT</p>

                                        <p><span class="bl_clr">Add : </span>Village-Bhora Kalah, Near Bilas pur Chowk Tehsil - Patudi, District - Gurgaon Haryana-122413. </p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Ram Bagh  </span>

                                        <p><span class="bl_clr">Branch Code :</span>  346</p>

                                        <p><span class="bl_clr">Ownership :</span> OWN BRANCH</p>

                                        <p><span class="bl_clr">Add : </span>ground floor 9063 Ram Bagh OLD Rohtak Road Delhi 110007</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>BADLI / SAMAIPUR </span>

                                        <p><span class="bl_clr">Branch Code :</span>  1006</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>F-28, Opp. Vijay Bank, Samaipur Badli, New Delhi – 110042</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>BALLABGARH </span>

                                        <p><span class="bl_clr">Branch Code :</span>  1221</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>20/3, Mathura Road, Near Star Wire, Ballabhgarh, Faridabad 121001</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>BHIWADI </span>

                                        <p><span class="bl_clr">Branch Code :</span>  701</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>Shop No. S-202, Behind Modern Academy School, Near Petrol Pump, Main Rd, Bhiwadi 301001</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>BHIWANI </span>

                                        <p><span class="bl_clr">Branch Code :</span> 736</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>Hallu Bazar, Nr. Old Anaz Mandi, Bhiwani 127021</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>FARIDABAD </span>

                                        <p><span class="bl_clr">Branch Code :</span> 92</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>Plot No. 1 Sectore 15A, 17/6 Mile Stone, Mathura Road, Faridabad 121001</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>GHAZIABAD </span>

                                        <p><span class="bl_clr">Branch Code :</span> 98</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>95, New Arya Nagar, Meerut Rd., Ghaziabad 2011003</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>KAMLA MARKET </span>

                                        <p><span class="bl_clr">Branch Code :</span> 90</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>226 Kamla Mkt. New Delhi – 110002</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>KATRA BARYAN </span>

                                        <p><span class="bl_clr">Branch Code :</span> 94</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>6479 Katra Baryan, Feteh Puri, Delhi – 110006</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>KIRTI NAGAR </span>

                                        <p><span class="bl_clr">Branch Code :</span> 99</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>69, Najafgarh Road, Opp. Sylvania Laxman, Kirti Nagar,  New Delhi – 110015</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>MANGOLPURI INDL AREA </span>

                                        <p><span class="bl_clr">Branch Code :</span> 1211</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>69, B-9, Mangolpuri Industrial Area, Phase – I, Petroleum Mkt. New Delhi – 110083</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>MUNDKA  </span>

                                        <p><span class="bl_clr">Branch Code :</span> 1019</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>428/10, Near Mundka Bus Stand, Main Rohtak Road, Mundka, Delhi – 110041</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>MAYAPURI  </span>

                                        <p><span class="bl_clr">Branch Code :</span> 302</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>S-7, Community Center, Mayapuri Phase I, New Delhi – 110064</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>NANGLOI  </span>

                                        <p><span class="bl_clr">Branch Code :</span> 190</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>C-16, MangolIndl. Area, P.I. Petroleum Mkt. Delhi  - 110083</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>SADAR BAZAR  </span>

                                        <p><span class="bl_clr">Branch Code :</span> 93</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>84, New Qutab Road, Market Teliwara, New Delhi 110006</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>PUNJABI BAUG  </span>

                                        <p><span class="bl_clr">Branch Code :</span> 1083</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>25, Transport Center, Punjabi Bagh, Delhi 110035</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>ROHTAK ROAD  </span>

                                        <p><span class="bl_clr">Branch Code :</span> 84</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>1436/153 Military Road, New Rohtak Rd. Delhi – 110005</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>SAHARANPUR </span>

                                        <p><span class="bl_clr">Branch Code :</span> 495</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>Shivaji Marg, Near Pul Jogain, Kishanpura, Saharanpur 247001</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>ZAKHIRA </span>

                                        <p><span class="bl_clr">Branch Code :</span> 1207</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>Shop No. 20, General Market, Zakhria, New Delhi 110015</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>JHANDEWALAN </span>

                                        <p><span class="bl_clr">Branch Code :</span> 63</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>30/09, Gali No.2, Opp Mother Dairy, Nengipoona, New Delhi - 110036</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>NOIDA PHASE -2 </span>

                                        <p><span class="bl_clr">Branch Code :</span> 1272</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>Sector 67, Sharma Market, Shop No. 5, Noida, UP 201301</p>

                                    </div>


                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>BAWAL </span>

                                        <p><span class="bl_clr">Branch Code :</span> 1274</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>Nr Allahabad Bank, Balipur Chowk, Bawal</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>NARAINA NEW </span>

                                        <p><span class="bl_clr">Branch Code :</span> 1310</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>CB 383/8, Indira Market Ring Road, Naraina</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Kundil</span>

                                        <p><span class="bl_clr">Branch Code :</span> 1379</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p><span class="bl_clr">Add : </span>Opp. Kundli ploice Station, Kundli, Sonipat</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>BHIWANI </span>

                                        <p><span class="bl_clr">Branch Code :</span> 1395</p>

                                        <p><span class="bl_clr">Ownership :</span> Business Associate</p>

                                        <p>
                                            <span class="bl_clr">Add : </span>Shree Bhagwat Road Lines, hallu bazar,goshain Chowk purani Anaj mandi Bhiwani Pin code- 127021
                                        </p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>punjab Regional Ofiice</span>

                                        <p><span class="bl_clr">Branch Code :</span>  827</p>

                                        <p><span class="bl_clr">Branch Type : </span>TRegional OFFICE</p>

                                        <p><span class="bl_clr">Ownership : </span>own</p>

                                        <p><span class="bl_clr">Contact Person : </span>Mr. N K Shukla </p>

                                        <p><span class="bl_clr">Add : </span>Plot No 137 Tpt Nagar Ludhiana -141003</p>

                                        <p><span class="bl_clr">Tel. No. : </span>161-2222852</p>

                                        <p><span class="bl_clr">Mob. No. : </span>7082100475</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Chandigarh </span>

                                        <p><span class="bl_clr">Branch Code :</span> 138</p>

                                        <p><span class="bl_clr">Branch Type : </span>Booking / Delivery</p>

                                        <p><span class="bl_clr">Ownership : </span>own</p>

                                        <p><span class="bl_clr">Contact Person : </span>Mr. N K Shukla </p>

                                        <p><span class="bl_clr">Add : </span>Plot no. 44 New Timber Market Sector 26 Chandigarh-160019</p>

                                        <p><span class="bl_clr">Tel. No. : </span>172-5086616</p>

                                        <p><span class="bl_clr">Mob. No. : </span>7082100473</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Barotiwala </span>

                                        <p><span class="bl_clr">Branch Code :</span> 1233</p>

                                        <p><span class="bl_clr">Branch Type : </span>BOOKING</p>

                                        <p><span class="bl_clr">Ownership : </span>own</p>

                                        <p><span class="bl_clr">Contact Person : </span>Mr. N K Shukla </p>

                                        <p><span class="bl_clr">Add : </span>Shop No. 3 Near Drum Factory Sai Road  Baddi  Distt Solan (HP)- 173205</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9318777449</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Ambala </span>

                                        <p><span class="bl_clr">Branch Code :</span> 436</p>

                                        <p><span class="bl_clr">Branch Type : </span>Transshipment</p>

                                        <p><span class="bl_clr">Ownership : </span>own</p>

                                        <p><span class="bl_clr">Contact Person : </span>Mr. N K Shukla </p>

                                        <p><span class="bl_clr">Add : </span>Godown No.218 Markendey Complex Saina Nagar Near Model Town Crossing Ambala City 134003</p>

                                        <p><span class="bl_clr">Mob. No. : </span>7082100478</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Jammu </span>

                                        <p><span class="bl_clr">Branch Code :</span> 326</p>

                                        <p><span class="bl_clr">Branch Type : </span>Booking / Delivery</p>

                                        <p><span class="bl_clr">Ownership : </span>Business Associate</p>

                                        <p><span class="bl_clr">Contact Person : </span>Mr. N K Shukla </p>

                                        <p><span class="bl_clr">Add : </span>417/18, Narwal Transport Yard no.6, Jammu</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9796222290</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Ambala City </span>

                                        <p><span class="bl_clr">Branch Code :</span> 336</p>

                                        <p><span class="bl_clr">Branch Type : </span>Booking</p>

                                        <p><span class="bl_clr">Ownership : </span>own</p>

                                        <p><span class="bl_clr">Contact Person : </span>Mr. N K Shukla </p>

                                        <p><span class="bl_clr">Add : </span>Godown No.218 Markendey Complex Saina Nagar Near Model Town Crossing Ambala City 134003</p>

                                        <p><span class="bl_clr">Mob. No. : </span>7082100478</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Parwanoo</span>

                                        <p><span class="bl_clr">Branch Code :</span> 488</p>

                                        <p><span class="bl_clr">Branch Type : </span>Booking / Delivery</p>

                                        <p><span class="bl_clr">Ownership : </span>Business Associate</p>

                                        <p><span class="bl_clr">Contact Person : </span>Mr. N K Shukla </p>

                                        <p><span class="bl_clr">Add : </span>7/31 TRANSPORT SITE SECTOR 2 NEAR TRUCK OPERATOR UNION PARWANOO-173220</p>

                                        <p><span class="bl_clr">Tel. No. : </span>01792-232888</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9816000028</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Baddi</span>

                                        <p><span class="bl_clr">Branch Code :</span> 1040</p>

                                        <p><span class="bl_clr">Branch Type : </span>Booking / Delivery</p>

                                        <p><span class="bl_clr">Ownership : </span>Business Associate</p>

                                        <p><span class="bl_clr">Contact Person : </span>Mr. N K Shukla </p>

                                        <p>
                                            <span class="bl_clr">Add : </span>C/O JAI MATA ROADLINESSHOP NO 96 TO 98 MODREN COMPLEX SAI ROAD BADDI-173205 THE NALAGARH DISTT SOLAN HP
                                        </p>

                                        <p><span class="bl_clr">Mob. No. : </span>9816007872</p>


                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Chandigrh</span>

                                        <p><span class="bl_clr">Branch Code :</span> 492</p>

                                        <p><span class="bl_clr">Branch Type : </span>Transshipment</p>

                                        <p><span class="bl_clr">Ownership : </span>own</p>

                                        <p><span class="bl_clr">Contact Person : </span>Mr. N K Shukla </p>

                                        <p><span class="bl_clr">Add : </span>Plot no. 44 New Timber Market Sector 26 Chandigarh-160019</p>

                                        <p><span class="bl_clr">Tel. No. : </span>175-5086616</p>

                                        <p><span class="bl_clr">Mob. No. : </span>7082100473</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Ludhiana</span>

                                        <p><span class="bl_clr">Branch Code :</span> 85</p>

                                        <p><span class="bl_clr">Branch Type : </span>Booking / Delivery</p>

                                        <p><span class="bl_clr">Ownership : </span>own</p>

                                        <p><span class="bl_clr">Contact Person : </span>Mr. N K Shukla </p>

                                        <p><span class="bl_clr">Add : </span>Plot No 137 Tpt Nagar Ludhiana -141003</p>

                                        <p><span class="bl_clr">Mob. No. : </span>7082100472</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Ambala Cant</span>

                                        <p><span class="bl_clr">Branch Code :</span> 137</p>

                                        <p><span class="bl_clr">Branch Type : </span>Booking / Delivery</p>

                                        <p><span class="bl_clr">Ownership : </span>Business Associate</p>

                                        <p><span class="bl_clr">Contact Person : </span>Mr. N K Shukla </p>

                                        <p><span class="bl_clr">Add : </span>1363 Hargualal Road Ambala Cant - HR . 133001</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9896074301</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Kala Amb</span>

                                        <p><span class="bl_clr">Branch Code :</span> 1293</p>

                                        <p><span class="bl_clr">Branch Type : </span>Booking / Delivery</p>

                                        <p><span class="bl_clr">Ownership : </span>own</p>

                                        <p><span class="bl_clr">Contact Person : </span>Mr. N K Shukla </p>

                                        <p><span class="bl_clr">Add : </span>Near Wine Factory Nahan Road Kala Amb - 173030</p>

                                        <p><span class="bl_clr">Mob. No. : </span>7082100477</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Sahnewal</span>

                                        <p><span class="bl_clr">Branch Code :</span> 557</p>

                                        <p><span class="bl_clr">Branch Type : </span>Transshipment</p>

                                        <p><span class="bl_clr">Ownership : </span>own</p>

                                        <p><span class="bl_clr">Contact Person : </span>Mr. N K Shukla </p>

                                        <p><span class="bl_clr">Add : </span>Plot No 137 Tpt Nagar Ludhiana-141003</p>

                                        <p><span class="bl_clr">Mob. No. : </span>7082100472</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Panipat</span>

                                        <p><span class="bl_clr">Branch Code :</span> 1371</p>

                                        <p><span class="bl_clr">Branch Type : </span>Transshipment</p>

                                        <p><span class="bl_clr">Ownership : </span>Business Associate</p>

                                        <p><span class="bl_clr">Contact Person : </span>Mr. N K Shukla </p>

                                        <p><span class="bl_clr">Add : </span>Sector 25 Truck Union Shop No. 259 Near Banshi Ka Daba Panipat Haryana - 132103</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9466423807</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Ambala Cant</span>

                                        <p><span class="bl_clr">Branch Code :</span> 1368</p>

                                        <p><span class="bl_clr">Branch Type : </span>BOOKING</p>

                                        <p><span class="bl_clr">Ownership : </span>Business Associate</p>

                                        <p><span class="bl_clr">Contact Person : </span>Mr. N K Shukla </p>

                                        <p><span class="bl_clr">Add : </span>3706 Hill Road Opp SD Mandir Ambala Cantt Haryana -133001</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9315088515</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Jalandhar Patel chowk</span>

                                        <p><span class="bl_clr">Branch Code :</span> 689</p>

                                        <p><span class="bl_clr">Branch Type : </span>Booking / Delivery</p>

                                        <p><span class="bl_clr">Ownership : </span>Business Associate</p>

                                        <p><span class="bl_clr">Contact Person : </span>Mr. N K Shukla </p>

                                        <p><span class="bl_clr">Add : </span>S-62 Industrial Area Near Leader Factory Jalandhar-144004</p>

                                        <p><span class="bl_clr">Tel. No. : </span>0181-5059887</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9876898327</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Aligarh</span>

                                        <p><span class="bl_clr">Add. : </span>JAIDESH PRASHAD SHAH KAMAL ROAD  ASGARABAD HOUSE DEEN DAYAL SARAI ALIGARH -202001</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9358257828</p>

                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:aligarh_prl@patel-india.com">aligarh_prl@patel-india.com</a></p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>HATHRAS</span>

                                        <p><span class="bl_clr">Add. : </span>sadabad gate HATHRAS-244241</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9358062221</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>BAREILY</span>

                                        <p><span class="bl_clr">Add. : </span>LEECHI BAGH RAMPUR ROAD BAREILY-243001</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9358702198 / 9267524230</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>KANNAUJ</span>

                                        <p><span class="bl_clr">Add. : </span>Makrand Nagar kannauj-209726</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9415472585</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>KANPUR</span>

                                        <p><span class="bl_clr">Add. : </span>PLOT NO 59 DEVKI NAGAR NEAR PRATAP HOTEL YASHODA NAGAR KANPUR -208011</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9935508725 / 9793707725</p>

                                        <p><span class="bl_clr">Email Id : </span><a href="mailto:kanpur_prl@patel-india.com">kanpur_prl@patel-india.com</a> </p>


                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>LUCKNOW</span>

                                        <p><span class="bl_clr">Add. : </span>F 31/308  NEAR PARKING NO 5 TRANSPORT NAGAR LUCKNOW-226008</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9335814168</p>

                                        <p><span class="bl_clr">Email Id : </span><a href="mailto:lucknow_prl@patel-india.com">lucknow_prl@patel-india.com</a> </p>


                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>VARANASI</span>

                                        <p><span class="bl_clr">Add. : </span>D 65/10 NEAR PANI KI  TANKI BAULIA VARANASI-221002</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9956309898</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>RAMPUR</span>

                                        <p><span class="bl_clr">Add. : </span>NEAR YARK HOTEL TAXI STAND RAMPUR-244901</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9897585444 / 9319385444</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>AMROHA</span>

                                        <p><span class="bl_clr">Add. : </span>STATION ROAD DIST J.P. NAGAR AMROHA -244241</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9319510083</p>

                                    </div>


                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>KALPI</span>

                                        <p><span class="bl_clr">Add. : </span>BALA JI TPT  CORP. OPP POST OFFICE KALPI -285204</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9621009090</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>JHANSI</span>

                                        <p><span class="bl_clr">Add. : </span>NH 75 GWALIOR ROAD OPP. GOVT POLYTECHNIC  MAHILA  JHANSI -284001</p>

                                        <p><span class="bl_clr">Mob. No. : </span>09839439849 / 9198753501</p>

                                        <p><span class="bl_clr">Email Id : </span><a href="mailto:jhansi_ba_prl@patel-india.com">jhansi_ba_prl@patel-india.com</a></p>


                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>DADA NAGAR </span>

                                        <p><span class="bl_clr">Add. : </span>125 B SANJAY GANDHI NAGAR NAUBASTA KANPUR-208021</p>

                                        <p><span class="bl_clr">Mob. No. : </span>8853634350</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>AGRA CITY</span>

                                        <p><span class="bl_clr">Add. : </span>Balaji puram, sahdara chungi 100 ft road near nari niketa agra-282006</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9568531526</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>MEERUT</span>

                                        <p><span class="bl_clr">Add. : </span>shop Number 4, Building no 23, b Mandigate, T.P. NAGAR  Meerut -250002</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9927668890</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>AGRA-0002</span>

                                        <p><span class="bl_clr">Add. : </span>21/38 a free ganj agra -282004</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9359956545</p>

                                        <p><span class="bl_clr">Mob. No. : </span>agra_ba@patel-india.com</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>LUCKNOW-CITY</span>

                                        <p><span class="bl_clr">Add. : </span>F 488 transport nagar, kanpur road, lucknow -226008</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9389519906</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>MORADABAD New</span>

                                        <p><span class="bl_clr">Add. : </span>A 63 GANDHI NAGAR NEAR JANTA BLOOD BANK MORADABAD-244001 </p>

                                        <p><span class="bl_clr">Mob. No. : </span>9927081888</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>ROUP</span>

                                        <p><span class="bl_clr">Add. : </span>PLOT NO 59 DEVKI NAGAR NEAR PRATAP HOTEL YASHODA NAGAR KANPUR -208011</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9369930010</p>

                                        <p><span class="bl_clr">Mob. No. : </span><a href="mailto:mohit.verma@patel-india.com">mohit.verma@patel-india.com</a></p>


                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>KANPUR</span>

                                        <p><span class="bl_clr">Branch Type : </span>Transshipment</p>

                                        <p><span class="bl_clr">Add. : </span>PLOT NO 59 DEVKI NAGAR NEAR PRATAP HOTEL YASHODA NAGAR KANPUR -208011</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9369319215 / 9369930006</p>

                                        <p><span class="bl_clr">Email Id : </span><a href="mailto:kanpur_trm@patel-india.com">kanpur_trm@patel-india.com</a></p>


                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>ALLAHABAD</span>

                                        <p><span class="bl_clr">Add. : </span>667 transport nagar Allahabad -211002</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9415443098</p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>UTTARAKHAND REGIONAL OFFICE</span>

                                        <p><span class="bl_clr">Add. : </span>Shop No. B -62,Naveen Mandi Sathal Kicha Road,  Bhagwara, Rudrapur(UK)-263153</p>

                                        <p><span class="bl_clr">Tel. No. : </span>5944-244905</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9258300003 / 09888886879</p>

                                        <p><span class="bl_clr">Email Id: </span><a href="mailto:arvind.rai@patel-india.com">arvind.rai@patel-india.com </a></p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>RUDRAPUR</span>

                                        <p><span class="bl_clr">Branch Code : </span>1220</p>

                                        <p><span class="bl_clr">Branch Type : </span>Booking / Delivery</p>

                                        <p><span class="bl_clr">Ownership : </span>Own</p>

                                        <p><span class="bl_clr">Add. : </span>Shop No. B-62  Bagwara  Mandi Kicha   Road Rudarpur - 263153</p>

                                        <p><span class="bl_clr">Tel. No. : </span>05944-244905</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9258300003 / 9568790009</p>

                                        <p>
                                            <span class="bl_clr">Email Id: </span><a href="mailto:arvind.rai@patel-india.com">arvind.rai@patel-india.com & <a href="mailto:rudrapur@patel-india.com">rudrapur@patel-india.com</a>
                                            </a>
                                        </p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>RUDRAPUR</span>

                                        <p><span class="bl_clr">Branch Code : </span>1431</p>

                                        <p>
                                            <span class="bl_clr">Branch Type : </span>Transshipment/p>

                                    <p><span class="bl_clr">Ownership : </span>Own</p>

                                            <p><span class="bl_clr">Add. : </span>OPP. HONDA FACTORY RAMNAGAR KICHA ROAD RUDRAPUR - 263153</p>


                                            <p><span class="bl_clr">Mob. No. : </span>9690019560</p>

                                            <p><span class="bl_clr">Email Id: </span><a href="mailto:rudrapur.tr@patel-india.com">rudrapur.tr@patel-india.com</a> </p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>DEHRADUN</span>

                                        <p><span class="bl_clr">Branch Code : </span>248197</p>

                                        <p>
                                            <span class="bl_clr">Branch Type : </span>Booking / Delivery/p>

                                    <p><span class="bl_clr">Ownership : </span>Own</p>

                                            <p><span class="bl_clr">Add. : </span>Near Old Post Office NH -72 Selaqui Dehradun-248197</p>

                                            <p><span class="bl_clr">Mob. No. : </span>9690019507</p>

                                            <p><span class="bl_clr">Email Id: </span><a href="mailto:dehradun_prl@patel-india.com">dehradun_prl@patel-india.com</a> </p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>HARIDWAR</span>

                                        <p><span class="bl_clr">Branch Code : </span>1214</p>

                                        <p>
                                            <span class="bl_clr">Branch Type : </span>Booking / Delivery/p>

                                    <p><span class="bl_clr">Ownership : </span>Own</p>

                                            <p><span class="bl_clr">Add. : </span>Sarswati Vihar, Brahampur Road, Jamalpur Khurd, Bahadrabad Distt Haridwar-249402</p>

                                            <p><span class="bl_clr">Mob. No. : </span>9690019562</p>

                                            <p><span class="bl_clr">Email Id: </span><a href="mailto:kameshwar.chaturvedi@patel-india.com">kameshwar.chaturvedi@patel-india.com</a> </p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>HARIDWAR</span>

                                        <p><span class="bl_clr">Branch Code : </span>1353</p>

                                        <p>
                                            <span class="bl_clr">Branch Type : </span>Booking / Delivery/p>

                                    <p><span class="bl_clr">Ownership : </span>Own</p>

                                            <p><span class="bl_clr">Add. : </span>Sarswati Vihar, Brahampur Road, Jamalpur Khurd, Bahadrabad Distt Haridwar-249402</p>

                                            <p><span class="bl_clr">Mob. No. : </span>9690019561</p>

                                            <p><span class="bl_clr">Email Id: </span><a href="mailto:haridwar_br_prl@patel-india.com">haridwar_br_prl@patel-india.com</a> </p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Kashipur</span>

                                        <p><span class="bl_clr">Branch Code : </span>1409</p>

                                        <p>
                                            <span class="bl_clr">Branch Type : </span>Booking / Delivery/p>

                                    <p><span class="bl_clr">Ownership : </span>Own</p>

                                            <p><span class="bl_clr">Add. : </span>Opposite Jeshpur Bus Stand Kashipur - 241347</p>

                                            <p><span class="bl_clr">Mob. No. : </span>9690019563</p>

                                            <p><span class="bl_clr">Email Id: </span><a href="mailto:cse.uk@patel-india.com">cse.uk@patel-india.com</a> </p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>RUDRAPUR</span>

                                        <p><span class="bl_clr">Branch Code : </span>1452</p>

                                        <p><span class="bl_clr">Branch Type : </span>BA</p>

                                        <p><span class="bl_clr">Ownership : </span>Own</p>

                                        <p><span class="bl_clr">Add. : </span>181/70 LIG AWAS VIKASH NEAR SHIV SHAKTI MANDIR - 263153</p>

                                        <p><span class="bl_clr">Mob. No. : </span>8475002020</p>

                                        <p><span class="bl_clr">Email Id: </span><a href="mailto:sidcul.ba@patel-india.com">sidcul.ba@patel-india.com</a> </p>

                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>RUDRAPUR</span>

                                        <p><span class="bl_clr">Branch Code : </span>1457</p>

                                        <p><span class="bl_clr">Branch Type : </span>Booking / Delivery</p>

                                        <p><span class="bl_clr">Ownership : </span>Own</p>

                                        <p><span class="bl_clr">Add. : </span>Shop No. B-51  Bagwara  Mandi Kicha   Road Rudarpur - 263153</p>

                                        <p><span class="bl_clr">Mob. No. : </span>9258300003</p>

                                        <p><span class="bl_clr">Email Id: </span><a href="mailto:cse.uk@patel-india.com">cse.uk@patel-india.com</a> </p>

                                    </div>

                                </div>

                            </li>

                            <li>   

                                <div class="content__wrapper">

                                    

  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>BANGALORE Regional Office</span>
    <p><span class="bl_clr"> Branch Code :</span>0854</p>
    <p><span class="bl_clr"> Ownership : </span>OWN BRANCH</p>
    <p><span class="bl_clr"> Add : </span>No.2, New    Mission Road, Bangalore - 560027</p>
    <p><span class="bl_clr"> Mob. No. : </span>080-22243004/22222189</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Bangalore </span>
    <p><span class="bl_clr"> Branch Code :</span>005</p>
    <p><span class="bl_clr"> Ownership : </span>OWN BRANCH - BOOKING</p>
    <p><span class="bl_clr"> Add : </span>No.2,    New Mission Road, Bangalore - 560027</p>
    <p><span class="bl_clr"> Mob. No. : </span>9620310251</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Bangalore</span>
    <p><span class="bl_clr"> Branch Code :</span>0548</p>
    <p><span class="bl_clr"> Ownership : </span> OWN BRANCH - DELIVERY</p>
    <p><span class="bl_clr"> Add : </span>No.2,    New Mission Road, Bangalore - 560027</p>
    <p><span class="bl_clr"> Mob. No. : </span>9379219509</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Bangalore</span>
    <p><span class="bl_clr"> Branch Code :</span>0034</p>
    <p><span class="bl_clr"> Ownership : </span>OWN BRANCH -  TRANSSHIPMENT </p>
    <p><span class="bl_clr"> Add : </span>Sy    No:69/5 Budhihal Village,Kasba Hobli, Nelamangala Taluk,Bangalore-562123</p>
    <p><span class="bl_clr"> Mob. No. : </span>9341216411/9353020006</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Bommasandra</span>
    <p><span class="bl_clr"> Branch Code :</span>0498</p>
    <p><span class="bl_clr"> Ownership : </span>OWN BRANCH</p>
    <p><span class="bl_clr"> Add : </span>Shed    No:H ,50/5 , Thimmareddy Ind. Estate, Bommasandra Village,Bangalore-560099</p>
    <p><span class="bl_clr"> Mob. No. : </span>9379219503</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>K.R.Puram</span>
    <p><span class="bl_clr"> Branch Code :</span>0525</p>
    <p><span class="bl_clr"> Ownership : </span>OWN BRANCH</p>
    <p><span class="bl_clr"> Add : </span>Shed No:    P-12,Jayveecons,Devasandra Ind. Estate,Mahadevapura,Whitefield    Road,Bangalore-560048</p>
    <p><span class="bl_clr"> Mob. No. : </span>9325918393</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Peenya-2</span>
    <p><span class="bl_clr"> Branch Code :</span>1322</p>
    <p><span class="bl_clr"> Ownership : </span>OWN BRANCH</p>
    <p><span class="bl_clr"> Add : </span>Shed    No:V-50/1 6th Main Road,Peenya-2nd Stage Bangalore-560058</p>
    <p><span class="bl_clr"> Mob. No. : </span>9886985601/9590202721</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Hoskote</span>
    <p><span class="bl_clr"> Branch Code :</span>1053</p>
    <p><span class="bl_clr"> Ownership : </span>OWN BRANCH</p>
    <p><span class="bl_clr"> Add : </span>No:239,Site    No:7 Chokkahalli Village Kasaba Hobli HoskoteTaluk Bangalore-562114</p>
    <p><span class="bl_clr"> Mob. No. : </span>9353020010</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Peenya</span>
    <p><span class="bl_clr"> Branch Code :</span>0048</p>
    <p><span class="bl_clr"> Ownership : </span>BUSINESS ASSOCIATE</p>
    <p><span class="bl_clr"> Add : </span>No. 35, Jai    Maruthi Indl. Estate, Tumkur Road, Peenya, Bangalore - 560 058</p>
    <p><span class="bl_clr"> Mob. No. : </span>9448309135</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Hosur</span>
    <p><span class="bl_clr"> Branch Code :</span>0105</p>
    <p><span class="bl_clr"> Ownership : </span>BUSINESS ASSOCIATE</p>
    <p><span class="bl_clr"> Add : </span>Plot    No:4/56/2/175,Narasimha Colony,Seetharamed,Near Hero Honda    Showroom,Hosur-635109</p>
    <p><span class="bl_clr"> Mob. No. : </span>8971988478</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Bommanahalli</span>
    <p><span class="bl_clr"> Branch Code :</span>0790</p>
    <p><span class="bl_clr"> Ownership : </span>BUSINESS ASSOCIATE</p>
    <p><span class="bl_clr"> Add : </span>No:26,Bilal    Mazjid Road,6th Cross,M.G Palya Main Road,Bangalore-560068</p>
    <p><span class="bl_clr"> Mob. No. : </span>7411299707</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Shivajinagar</span>
    <p><span class="bl_clr"> Branch Code :</span>0617</p>
    <p><span class="bl_clr"> Ownership : </span>BUSINESS ASSOCIATE</p>
    <p><span class="bl_clr"> Add : </span>No:1/1,Amstrong    Road Cross,Shivaji Nagar-Bangalore-560 001</p>
    <p><span class="bl_clr"> Mob. No. : </span>9945076554</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Kamakshipalya</span>
    <p><span class="bl_clr"> Branch Code :</span>0502</p>
    <p><span class="bl_clr"> Ownership : </span>BUSINESS ASSOCIATE</p>
    <p><span class="bl_clr"> Add : </span>56/3,    Chikkapillappa, Indl. Est. Kamakshipalyam, Magadi Road, Bangalore - 560 079</p>
    <p><span class="bl_clr"> Mob. No. : </span>9341666263</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Mangalore</span>
    <p><span class="bl_clr"> Branch Code :</span>0033</p>
    <p><span class="bl_clr"> Ownership : </span>BUSINESS ASSOCIATE</p>
    <p><span class="bl_clr"> Add : </span>Door    No:23-9-595/2, IK Buidling,Mangalanagara Layout,Monkey Stand,Mangalore-575001</p>
    <p><span class="bl_clr"> Mob. No. : </span>7353178699</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Mysore</span>
    <p><span class="bl_clr"> Branch Code :</span>0104</p>
    <p><span class="bl_clr"> Ownership : </span>BUSINESS ASSOCIATE</p>
    <p><span class="bl_clr"> Add : </span>No:2203/5,New    Suntepet,Chamaraj Mohalla,Mysore-570001</p>
    <p><span class="bl_clr"> Mob. No. : </span>0821-4264426</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Kanakpura Road</span>
    <p><span class="bl_clr"> Branch Code :</span>0666</p>
    <p><span class="bl_clr"> Ownership : </span>BUSINESS ASSOCIATE</p>
    <p>16/17    Ramakrishna Nagar 3rd Cross,Ring Road 3rd Cross,Sarakki 6th Phase J.P Nagar,    Bangalore-560 078</p>
    <p><span class="bl_clr"> Mob. No. : </span>9901925563</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Bannerghata    Road</span>
    <p><span class="bl_clr"> Branch Code :</span>1088</p>
    <p><span class="bl_clr"> Ownership : </span>BUSINESS ASSOCIATE</p>
    <p><span class="bl_clr"> Add : </span>No.97/500,    2nd Cross, Bole Khalli Layout, Opp : Kalyani Kala mandir, Bannerghatta Main    Road, Bangalore - 562 076</p>
    <p><span class="bl_clr"> Mob. No. : </span>9980644541</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Tumkur</span>
    <p><span class="bl_clr"> Branch Code :</span>0476</p>
    <p><span class="bl_clr"> Ownership : </span>BUSINESS ASSOCIATE</p>
    <p><span class="bl_clr"> Add : </span> No.1896,    Ist Main Road, New Mandipet, Tumkur - 572 101</p>
    <p><span class="bl_clr"> Mob. No. : </span> 9008247521</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>KERALA REGIONAL OFFICE</span>
    <p><span class="bl_clr"> Branch Code :</span>0858</p>
    <p><span class="bl_clr">Branch Type : </span> REGIONAL OFFICE</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Umesh bhardwaj</p>
    <p><span class="bl_clr"> Add : </span> Embees Building,    Market Road, Kombara, Cochin, Ernakulam - 682018.</p>
    <p><span class="bl_clr"> Tel No. : </span>0484-2394159</p>
    <p><span class="bl_clr"> Mob No. : </span>7907630562</p>
    <p><a href="mailto:umesh.bhardwaj@patel-india.com">umesh.bhardwaj@patel-india.com</a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>ALLEPPEY</span>
    <p><span class="bl_clr"> Branch Code :</span>0042</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Premnadh</p>
    <p><span class="bl_clr"> Add : </span> CCNB Road, Near    Kotwal Chavadi Building, Alleppey -688001.</p>
    <p><span class="bl_clr"> Tel No. : </span> 0477-2245154</p>
    <p><span class="bl_clr"> Mob No. : </span>9847539747</p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:alleppey@patel-india.com"> alleppey@patel-india.com </a> </p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>ALWAYE</span>
    <p><span class="bl_clr"> Branch Code :</span>0073</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Balakrishnan</p>
    <p><span class="bl_clr"> Add : </span> 117/16, Edakkatil    Sastha Deavasom Temple Builing, Ernakulam Road, Alwaye - 683101.</p>
    <p><span class="bl_clr"> Tel No. : </span>0484- / 2623546/    8281811026</p>
    <p><span class="bl_clr"> Mob No. : </span>9446285107</p>
    <p><span class="bl_clr"> Email Id : </span><a href="mailto:alwaye@patel-india.com">alwaye@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>CALICUT</span>
    <p><span class="bl_clr"> Branch Code :</span>0365</p>
    <p> <span class="bl_clr">Branch Type : </span> TRANSSHIPMENT</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Kunhiraman</p>
    <p><span class="bl_clr"> Add : </span> 8/60, Kailas    Building, Opp Fire Station, Near Corporation Office, Calicut -673032.</p>
    <p><span class="bl_clr"> Tel No. : </span>0495-2365783 /2365953</p>
    <p><span class="bl_clr"> Mob No. : </span>9349118171</p>
    <p><span class="bl_clr"> Email Id : </span><a href="mailto:calicut@patel-india.com">calicut@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>CALICUT</span>
    <p><span class="bl_clr"> Branch Code :</span>0046</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Kunhiraman</p>
    <p><span class="bl_clr"> Add : </span> 8/60, Kailas    Building, Opp Fire Station, Near Corporation Office, Calicut -673032.</p>
    <p><span class="bl_clr"> Tel No. : </span>0495-2365783 /2365953</p>
    <p><span class="bl_clr"> Mob No. : </span>9349118171</p>
    <p><span class="bl_clr"> Email Id : </span><a href="mailto:calicut@patel-india.com">calicut@patel-india.com</a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>CANNANNORE </span>
    <p><span class="bl_clr"> Branch Code :</span>0414</p>
    <p><span class="bl_clr">Branch Type : </span> TRANSSHIPMENT</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Prakasan</p>
    <p><span class="bl_clr"> Add : </span>Arafa Complex , Arat    Road , Kannure -670001</p>
    <p><span class="bl_clr"> Tel No. : </span>0497-2768725 /    9495807088</p>
    <p><span class="bl_clr"> Mob No. : </span>9497145268</p>
    <p><span class="bl_clr"> Email Id : </span><a href="mailto:kannur@patel-india.com ">kannur@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>CHALAKUDY</span>
    <p><span class="bl_clr"> Branch Code :</span>0106</p>
    <p><span class="bl_clr"> Contact Person : </span>Ms.Leena</p>
    <p><span class="bl_clr"> Add : </span>20/500, Main Road ,    Chalakudy - 680307.</p>
    <p><span class="bl_clr"> Mob No. : </span>9495752163</p>
      </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>CHANGANACHERY</span>
    <p><span class="bl_clr"> Branch Code :</span>0110</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Moni Sam</p>
    <p><span class="bl_clr"> Add : </span> Panachikal Building,    Market Road, Changanacherry -686101.</p>
    <p><span class="bl_clr"> Tel No. : </span>0481-2420586 </p>
    <p><span class="bl_clr"> Mob No. : </span>9447359520</p>
    <p><span class="bl_clr"> Email Id : </span><a href="mailto:changanacherry_prl@patel-india.com">changanacherry_prl@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>EDAPPALLY</span>
    <p><span class="bl_clr"> Branch Code :</span>0360</p>
    <p> <span class="bl_clr">Branch Type : </span> TRANSSHIPMENT</p>
    <p><span class="bl_clr">Contact Person : </span>Mr.Muarlidharan</p>
    <p><span class="bl_clr">Add : </span> 51/3A2, Ward No    50/127a, Chettiparambil Road ,On Edapally Railway Station Road, Edappally    -682041.</p>
    <p><span class="bl_clr">Tel No. : </span> 0484-2801970 /    9349608300</p>
    <p><span class="bl_clr">Mob No. : </span>9446046504</p>
    <p><span class="bl_clr">Email Id : </span> <a href="mailto:edapally_trm@patel-india.com">edapally_trm@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>IRINJALAKUDA</span>
    <p><span class="bl_clr"> Branch Code :</span>0201</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Paul</p>
    <p><span class="bl_clr"> Add : </span> 15/15, Brotheren    Misson Road, Behind  Govt Hospital,    Irinjalkuda- 680121.</p>
    <p><span class="bl_clr"> Tel No. : </span>0480-2823350</p>
    <p><span class="bl_clr"> Mob No. : </span>9447441919</p>
    <p><span class="bl_clr"> Email Id : </span><a href="mailto:irinjalakuda_prl@patel-india.com"> irinjalakuda_prl@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>KALAMASSERY</span>
    <p><span class="bl_clr"> Branch Code :</span>0069</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Murali</p>
    <p><span class="bl_clr"> Add : </span> 816/v , Near    Pathampius Chruch, EMS  Road, North    Kalamassery -683109.</p>
    <p><span class="bl_clr"> Tel No. : </span>0484- 2541636</p>
    <p><span class="bl_clr"> Mob No. : </span>9947177616</p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:kalamassery_prl@patel-india.com">kalamassery_prl@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>KANJIKODE</span>
    <p><span class="bl_clr"> Branch Code :</span>0329</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Vinayan</p>
    <p><span class="bl_clr"> Add : </span> 15/86/161, Kottayil    Building, Opp Tata Telecom Building, Kanjikode West , Palakkad -678623.</p>
    <p><span class="bl_clr"> Tel No. : </span>0491-2566057    -9447942558</p>
    <p><span class="bl_clr"> Mob No. : </span>9495866056</p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:kanjikode_prl@patel-india.com ">kanjikode_prl@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>KANJIRAPALLY</span>
    <p><span class="bl_clr"> Branch Code :</span>0296</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Romy Sebastian</p>
    <p><span class="bl_clr"> Add : </span> VI/ 556, Kedison    Building Kanjirapally - 686 507.</p>
    <p><span class="bl_clr"> Tel No. : </span>0482- 8202383</p>
    <p><span class="bl_clr"> Mob No. : </span>9447230703</p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:kanjirapally_prl@patel-india.com">kanjirapally_prl@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>KANNUR</span>
    <p><span class="bl_clr"> Branch Code :</span>0071</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Prakashan</p>
    <p>Arafa Complex , Arat    Road , Kannure -670001</p>
    <p><span class="bl_clr"> Tel No. : </span>0497-2768725 /    9495807088</p>
    <p><span class="bl_clr"> Mob No. : </span>9497145268</p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:kannur@patel-india.com"> kannur@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>KOTTAYAM</span>
    <p><span class="bl_clr"> Branch Code :</span>0051</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Unnikrishnan</p>
    <p><span class="bl_clr"> Add : </span> Sudarsana Building,    Temple Road, Thirunakkara, Kottayam -686001.</p>
    <p><span class="bl_clr"> Tel No. : </span>0481- 2582680 /    2582540</p>
    <p><span class="bl_clr"> Mob No. : </span>9447483155</p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:kottayam@patel-india.com"> kottayam@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>KOCHI</span>
    <p><span class="bl_clr"> Branch Code :</span>1400</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Preejith Nair</p>
    <p><span class="bl_clr"> Add : </span> Embees Building,    Market Road, Kombara, Cochin, Ernakulam - 682018.</p>
    <p><span class="bl_clr"> Tel No. : </span>0484-2394159 /    2801970</p>
    <p><span class="bl_clr"> Mob No. : </span>9349680004/9349608302</p>
    <p><a href="mailto:kochi@patel-india.com">kochi@patel-india.com</a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>MAHEE</span>
    <p><span class="bl_clr"> Branch Code :</span>0782</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Pradeepan</p>
    <p><span class="bl_clr"> Add : </span> C/o, Alleppey Parcel    Service, Near Mahe Sports Ground, Mahee -673310.</p>
    <p><span class="bl_clr"> Tel No. : </span>0497-2761751</p>
    <p><span class="bl_clr"> Mob No. : </span>9746455897</p>
    <p>NIL</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>MARTHANDAM</span>
    <p><span class="bl_clr"> Branch Code :</span>0114</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr. Ranjith</p>
    <p><span class="bl_clr"> Add : </span> 4/35-C, Pammam  Main Road, Marthandam - 629165.</p>
    <p><span class="bl_clr"> Tel No. : </span>04652-277978</p>
    <p><span class="bl_clr"> Mob No. : </span>9367525311</p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:nagercoil_prl@patel-india.com"> nagercoil_prl@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>NAGERCOIL </span>
    <p><span class="bl_clr"> Branch Code :</span>1217</p>
    <p> <span class="bl_clr">Branch Type : </span> TRANSSHIPMENT</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr. Ranjith</p>
    <p><span class="bl_clr"> Add : </span> 298A, Awai Shanmugham    Salai, Ozhuhinaseri, Nagercoil -629001.</p>
    <p><span class="bl_clr"> Tel No. : </span>04652-277978</p>
    <p><span class="bl_clr"> Mob No. : </span>9367525311</p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:nagercoil_prl@patel-india.com"> nagercoil_prl@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>NAGERCOIL</span>
    <p><span class="bl_clr"> Branch Code :</span>0109</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr. Ranjith</p>
    <p><span class="bl_clr"> Tel No. : </span>04652-277978</p>
    <p><span class="bl_clr"> Mob No. : </span>9367525311</p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:nagercoil_prl@patel-india.com"> nagercoil_prl@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PALAKKAD</span>
    <p><span class="bl_clr"> Branch Code :</span>0062</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Rajendran</p>
    <p><span class="bl_clr"> Add : </span>Metherath building    ,Vandikkara street , Big Bazr , Palakkad - 678014</p>
    <p><span class="bl_clr"> Tel No. : </span>0491-2535310</p>
    <p><span class="bl_clr"> Mob No. : </span>9249715220</p>
    <p><a href="mailto:palakkad.ba@patel-india.com">palakkad.ba@patel-india.com</a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PALARIVATTOM</span>
    <p><span class="bl_clr"> Branch Code :</span>0494</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Rajeevan</p>
    <p><span class="bl_clr"> Add : </span>40/2499 a joy's Lane.    Near Corporation Office , Anchumana ,     Mamangalam Road 682024</p>
    <p><span class="bl_clr"> Tel No. : </span>0484-2332525</p>
    <p><span class="bl_clr"> Mob No. : </span>9447500335</p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:palarivattom_prl@patel-india.com"> palarivattom_prl@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PERUMBAVOOR</span>
    <p><span class="bl_clr"> Branch Code :</span>1458</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Girshkumar</p>
    <p><span class="bl_clr"> Add : </span>Fish Market Road ,    Near Sanjoe Convent , Perumbavoor P O , 683542</p>
    <p><span class="bl_clr"> Tel No. : </span>4842525404</p>
    <p><span class="bl_clr"> Mob No. : </span>9388475600</p>
    <p><a href="mailto:perumbavoor.ba@patel-india.com">perumbavoor.ba@patel-india.com</a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>QUILON</span>
    <p><span class="bl_clr"> Branch Code :</span>0032</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Nazeer</p>
    <p><span class="bl_clr"> Add : </span>Mulamkadakom ,    Sreekrishna Automobiles Compound,Thirumullavaram P O , Kollam -  691012</p>
    <p><span class="bl_clr"> Tel No. : </span>8089969833</p>
    <p><span class="bl_clr"> Mob No. : </span>9746969833</p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:quilon@patel-india.com"> quilon@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>RAVIPURAM</span>
    <p><span class="bl_clr"> Branch Code :</span>0388</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Shivan</p>
    <p><span class="bl_clr"> Add : </span>39/5613, Kurishupally    Road,Ravipuram, Ernakulam-682015.</p>
    <p><span class="bl_clr"> Tel No. : </span>0484-2356808</p>
    <p><span class="bl_clr"> Mob No. : </span>9388418772</p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:ravipuram_prl@patel-india.com"> ravipuram_prl@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>SHERTALAI</span>
    <p><span class="bl_clr"> Branch Code :</span>0205</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Varghese</p>
    <p><span class="bl_clr"> Add : </span>CMC  II. Door No. 2/200, West Manorma Junction,    Main Road,  Behind Aided Primary    Teacher Society , Shertalai -688524</p>
    <p><span class="bl_clr"> Tel No. : </span>9895413538</p>
    <p><span class="bl_clr"> Mob No. : </span>9895413538</p>

  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>TELLICHERRY</span>
    <p><span class="bl_clr"> Branch Code :</span>0101</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.pradeepan</p>
    <p><span class="bl_clr"> Add : </span>Door    no . 47/ 1434,Thayalangadi, Tellicherry - 670101 Tellicherry, KERALA</p>
    <p><span class="bl_clr"> Tel No. : </span>0497-2761751</p>
    <p><span class="bl_clr"> Mob No. : </span>9746455897</p>
   
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>TIRUVALLA</span>
    <p><span class="bl_clr"> Branch Code :</span>0198</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Louis</p>
    <p><span class="bl_clr"> Add : </span>Door    no 448/2, Periyalath Near YMCA  Railway    Station Road , Thiruvalla - 689101.<br /></p>
    <p><span class="bl_clr"> Tel No. : </span>0469-3201011</p>
    <p><span class="bl_clr"> Mob No. : </span>9446921953</p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:thiruvalla_ba_prl@patel-india.com"> thiruvalla_ba_prl@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>TRICHUR</span>
    <p><span class="bl_clr"> Branch Code :</span>0067</p>
    <p><span class="bl_clr"> Contact Person : </span>Mrs.Sheebajigish</p>
    <p><span class="bl_clr"> Add : </span> 10/158, KSK Comples,Erinchery Angadi, Trichur - 680001 Trichur, KERALA</p>
    <p><span class="bl_clr"> Tel No. : </span>0487- 2424695</p>
    <p><span class="bl_clr"> Mob No. : </span>8296763277</p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:trichur@patel-india.com"> trichur@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>TRIVANDRUM</span>
    <p><span class="bl_clr"> Branch Code :</span>0039</p>
    <p><span class="bl_clr"> Contact Person : </span>Mr.Vishnu</p>
    <p><span class="bl_clr"> Add : </span>Injekkal    Vallakadave  P O ,Trivandrum - 698008</p>
    <p><span class="bl_clr"> Tel No. : </span>9074323687</p>
    <p><span class="bl_clr"> Mob No. : </span>9074323687</p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:trivandrum_fr_prl@patel-india.com"> trivandrum_fr_prl@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Tirupur    &amp; Tirupur</span>
    <p><span class="bl_clr"> Branch Code :</span>0070 &amp; 0664</p>
    <p><span class="bl_clr"> Branch Type :</span>TRANSSHIPMENT</p>
    <p> <span class="bl_clr"> Ownership :</span> own branch</p>
    <p><span class="bl_clr"> Add. :</span> New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
    <p><span class="bl_clr"> Mob. No. :</span>9894017050 /    0421-2201684/2244617</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Sivakasi</span>
    <p><span class="bl_clr"> Branch Code :</span>0120</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>5B-14/T, Anna Road, Parasakthi    Colony,Sivakasi-626123</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9789981999 /    04562-221520 </p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Pondicherry</span>
    <p><span class="bl_clr"> Branch Code :</span>0056</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>Plot No.77-81, IIIrd cross, Ranga    Nagar, Oulgaret, Puducherry - 605 010</p>
    <p><span class="bl_clr"> Tel. No. :</span> 2608022991 /    0413-2293007</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Chennai Tr</span>
    <p><span class="bl_clr"> Branch Code :</span>0362</p>
    <p><span class="bl_clr"> Branch Type :</span> TRANSSHIPMENT</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>2/199, Kundrathur Main Road,    Sikkarayapuram, Kundrathur,Chennai – 600069</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9003288258 /    9944144425 / 9025533545</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Poonamallee</span>
    <p><span class="bl_clr"> Branch Code :</span>1242</p>
    <p><span class="bl_clr"> Branch Type :</span> DELIVERY BRANCH</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>2/199, Kundrathur Main Road,    Sikkarayapuram, Kundrathur,Chennai – 600069</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9003288258 /    9944144425 / 9025533545</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Poonamallee</span>
    <p><span class="bl_clr"> Branch Code :</span>1377</p>
    <p><span class="bl_clr"> Branch Type :</span> BOOKING BRANCH</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>2/199, Kundrathur Main Road,    Sikkarayapuram, Kundrathur,Chennai – 600069</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9003288258 /    9944144425 / 9025533545</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Padi</span>
    <p><span class="bl_clr"> Branch Code :</span>0460</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>11, Reddy Street, korattur, opp road    of Brittania industries chennai 600080</p>
    <p> <span class="bl_clr"> Tel. No. :</span>044-26523599 /    26520771 /</p> 
    <p><span class="bl_clr"> Mob. No. :</span>  9840951545 / 9841904559</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Guindy</span>
    <p><span class="bl_clr"> Branch Code :</span>0055</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p>S9 Vasant Complex Thiru-vi-ka Indl    Estate,Guindy Chennai-600 032</p>
    <p><span class="bl_clr"> Mob. No. :</span> /    9442333385 / 9791025740</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Chennai</span>
    <p><span class="bl_clr"> Branch Code :</span>0852</p>
    <p><span class="bl_clr"> Branch Type :</span> Regional office    (Accounts)</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>S9 Vasant Complex Thiru-vi-ka Indl    Estate,Guindy Chennai-600 032</p>
    <p><span class="bl_clr"> Tel. No. :</span>044 - 45532320</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Chennai</span>
    <p><span class="bl_clr"> Branch Code :</span>0014 </p>
    <p><span class="bl_clr"> Branch Type :</span>BOOKING BRANCH</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>30/52,SALAI VINAYAGAR KOIL STREET,    MUTHIALPET, CHENNAI 600001</p>
    <p><span class="bl_clr"> Tel. No. :</span> 044-43567666 /    9840755633</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Chennai</span>
    <p><span class="bl_clr"> Branch Code :</span>0648</p>
    <p><span class="bl_clr"> Branch Type :</span> DELIVERY BRANCH</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>30/52,SALAI VINAYAGAR KOIL STREET,    MUTHIALPET, CHENNAI 600001</p>
    <p><span class="bl_clr"> Mob. No. :</span> 044-43567666 /    9840755633</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Maraimalainagar</span>
    <p><span class="bl_clr"> Branch Code :</span>0574</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span> 10/33, Singaravelan Street, NH-2, Opp.    Fire station, MM Nagar 603209</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9841783605 /    044-27454237</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Coimbatore</span>
    <p><span class="bl_clr"> Branch Code :</span>0007 &amp; 0320</p>
    <p><span class="bl_clr"> Branch Type :</span> BOOKING / DELIVERY    BRANCH</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>44 /2 , Kings Complex, Sungam Ukkadam    Bye Pass Road, Coimbatore -641045</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9894032012 /    04222313012</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Coimbatore </span>
    <p><span class="bl_clr"> Branch Code :</span>0361 &amp; 1252</p>
    <p><span class="bl_clr"> Branch Type :</span> TRANSSHIPMENT /    CENTRALISED DELIVERY</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>354/2 MARKET ROAD, NEAR LAKSHMI    THEATRE ,IRUGUR, COIMBATORE 641103</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9994144615 /    0422-2632477 / 9894638967 / 9025140361</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Coimbatore</span>
    <p><span class="bl_clr"> Branch Code :</span>0361</p>
    <p><span class="bl_clr"> Branch Type :</span> TRANSSHIPMENT</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>354/2 MARKET ROAD, NEAR LAKSHMI    THEATRE ,IRUGUR, COIMBATORE 641103</p>
    <p><span class="bl_clr"> Mob. No. :</span>9994144615 /    0422-2632477 / 9894638967 / 9025140361</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Coimbatore </span>
    <p><span class="bl_clr"> Branch Code :</span>0361</p>
    <p><span class="bl_clr"> Branch Type :</span> TRANSSHIPMENT</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>354/2 MARKET ROAD, NEAR LAKSHMI    THEATRE ,IRUGUR, COIMBATORE 641103</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9994144615 /    0422-2632477 / 9894638967 / 9025140361</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Coimbatore</span>
    <p><span class="bl_clr"> Branch Code :</span>0361</p>
    <p><span class="bl_clr"> Branch Type :</span> TRANSSHIPMENT</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>354/2 MARKET ROAD, NEAR LAKSHMI    THEATRE ,IRUGUR, COIMBATORE 641103</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9994144615 /    0422-2632477 / 9894638967 / 9025140361</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Sriperumbudur</span>
    <p><span class="bl_clr"> Branch Code :</span>1244</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>No.12, Ambethkar Salai,    Irungattukottai , Sriperumbudur - 602117</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9677006663 /    8220801838</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Mount Road</span>
    <p><span class="bl_clr"> Branch Code :</span>0053</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>No.6,Swamy Achari street, Pudupakkam    ,Royapettah, Chennai – 600014</p>
    <p>9791098139 /    044-28606702</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Cuddalore</span>
    <p><span class="bl_clr"> Branch Code :</span>0298</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>48/1-2 IMPERIAL ROAD,    PACHAYANKUPPAM,  CUDDALORE -    607005 </p>
    <p> <span class="bl_clr"> Mob. No. :</span> 8682880172 /    04142-239991 / 8428582306</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Madurai &amp;    MADURAI </span>
    <p><span class="bl_clr"> Branch Code :</span>0016 &amp; 363</p>
    <p><span class="bl_clr"> Branch Type :</span> TRANSSHIPMENT</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>8b, Yanaikal, Kalpalam    Road,Madurai-625001</p>
    <p><span class="bl_clr"> Mob. No. :</span> 0452-2621143 /    9994384466</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Taramani</span>
    <p><span class="bl_clr"> Branch Code :</span>1397</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>No;398,Kpk Nagar North, 9th Street,    Kottiwakkam, Chennai- 600041</p>
    <p><span class="bl_clr"> Mob. No. :</span> 95403030883 /    9841917091</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>DINDUGAL</span>
    <p><span class="bl_clr"> Branch Code :</span>0257</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>No.25 Aranmanaikulam,Near Sivan Koil ,    Madurai Road,  Dindigul- 624001</p>
    <p><span class="bl_clr"> Mob. No. :</span> 8508499275 /    0451-2427112 / 8925195426</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>KARUR </span>
    <p><span class="bl_clr"> Branch Code :</span>0078</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>No : 16, Ramakrishnapuram , Karur -    639001</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9500979591 /    04324-231364-334246</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>SALEM &amp;    SALEM</span>
    <p><span class="bl_clr"> Branch Code :</span>0047 &amp;553</p>
    <p><span class="bl_clr"> Branch Type :</span> TRANSSHIPMENT</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>121/18 , Sivanar Street, Gugai, Near    BSNL Telephone Exchage, Salem - 636 006</p>
    <p><span class="bl_clr"> Mob. No. :</span> 0427-2469604 /    9443212635</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PALLAVARAM </span>
    <p><span class="bl_clr"> Branch Code :</span>0468</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span> NO- 10 ANGALAAMMAKOVIL    2 STREET NAGALKENI CHENNAI-44</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9840146800</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PONDY IND    ESTATE</span>
    <p><span class="bl_clr"> Branch Code :</span>0769</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>21, Kamaraj Street,Kadhirkamam,Pondicherry – 605 009</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9345018614</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> ERODE AG &amp; ERODE</span>
    <p><span class="bl_clr"> Branch Code :</span>0076 &amp; 552</p>
    <p><span class="bl_clr"> Branch Type :</span> TRANSSHIPMENT</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>844,Park Road, Erode-638003</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9443680192 /    0424-2213706</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> KARAIKAL </span>
    <p><span class="bl_clr"> Branch Code :</span>0504</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>203, Church Street, Karaikal - 609 602</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9842325254 /    04368-222839</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>TRICHY &amp;    TRICHY</span>
    <p><span class="bl_clr"> Branch Code :</span>0103 &amp; 0366</p>
    <p><span class="bl_clr"> Branch Type :</span> TRANSSHIPMENT</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>No.21, Eda Street, Trichy – 620008</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9941744455 /    0431-2203738</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> RANIPET </span>
    <p><span class="bl_clr"> Branch Code :</span>0147</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>34, H-3, Ammoor Road,    Muthukadai,Ranipet—632401</p>
    <p><span class="bl_clr"> Mob. No. :</span> 04172/270736/    9092314727 / 9443394983</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>BODINAYANKUR</span>
    <p><span class="bl_clr"> Branch Code :</span>0358</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>B4, Prasad Complex, Near Town Police    Station,Bodinayakanur-626513</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9843117887 /    9943476636 / 04546-282664</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PERUNGUDI</span>
    <p><span class="bl_clr"> Branch Code :</span>0271</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>No. 1 / 168 , Kamaraj street,    Opp.Perumal Temple, Thoraipakam , Chennai - 600 096</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9383165369</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;TIRUNELVELI&nbsp;</span>
    <p><span class="bl_clr"> Branch Code :</span>0221</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>103,Sankaran Koil Road, Tatchanallur,    Tirunelveli-627358</p>
    <p><span class="bl_clr"> Mob. No. :</span> 94443427664</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>TUTICORIN</span>
    <p><span class="bl_clr"> Branch Code :</span>0220</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>84/3, South Raja    Street,Tuticorin-628001</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9944715725 /    0461-2322993</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>SINGANALLUR</span>
    <p><span class="bl_clr"> Branch Code :</span>0470</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>71,ANAIYANGADU ROAD,Opp to Vasanth    Mills, SINGANALLUR, COIMBATORE 641005</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9865433693</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PERINAYANKPALAYAM</span>
    <p><span class="bl_clr"> Branch Code :</span>0534</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>707-M-10,LMW ROAD, PERINAYANKANPALAYAM    641020</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9443369304 /    0422-2693043</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>TIRUPATI</span>
    <p><span class="bl_clr"> Branch Code :</span>1401</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>No: 354, Tilak Road, Opp Ioc Petrol    bunk, Near Sridevi Complex, Tirupati - 517501</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9908538277</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PANRUTI</span>
    <p><span class="bl_clr"> Branch Code :</span>1421</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>No:33/A Annai Indira Gandhi Salai,    Panruti - 607106</p>
    <p><span class="bl_clr"> Mob. No. :</span> 8015931947</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>THIRUKKANUR</span>
    <p><span class="bl_clr"> Branch Code :</span>1426</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Ownership :</span>No:13, Saraswathi Nagar, Thirukkanur,    Pondicherry - 605501</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9443287314</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>VILLUPURAM</span>
    <p><span class="bl_clr"> Branch Code :</span>1427</p>
    <p><span class="bl_clr"> Ownership :</span>own branch</p>
    <p><span class="bl_clr"> Add. :</span>No:199, Thiruvennainallur Road,    Alanguppam, Arasur Post, Villupuram - 607107</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9786341880</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Tirupur</span>
    <p><span class="bl_clr"> Branch Code :</span>0070</p>
    <p> <span class="bl_clr"> Branch Type :</span> BOOKING/DELIVERY</p>
  
    <p>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Sivakasi</span>
    <p><span class="bl_clr"> Branch Code :</span>0120</p>
    <p><span class="bl_clr"> Branch Type :</span> BOOKING/DELIVERY</p>

    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>

  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Pondicherry</span>
    <p><span class="bl_clr"> Branch Code :</span>0056</p>
    <p><span class="bl_clr"> Branch Type :</span> BOOKING/DELIVERY</p>

    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>

  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Chennai</span>
    <p><span class="bl_clr"> Branch Code :</span>0362</p>
    <p><span class="bl_clr"> Branch Type :</span> TRANSSHIPMENT</p>

    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>

  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Poonamallee</span>
    <p><span class="bl_clr"> Branch Code :</span>1242</p>
    <p><span class="bl_clr"> Branch Type :</span> DELIVERY</p>

    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>

  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Poonamallee</span>
    <p><span class="bl_clr"> Branch Code :</span>1377</p>
    <p><span class="bl_clr"> Branch Type :</span> BOOKING</p>

    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>

  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Padi</span>
    <p><span class="bl_clr"> Branch Code :</span>0460</p>
    <p><span class="bl_clr"> Branch Type :</span> BOOKING/DELIVERY</p>

    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>

  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Guindy</span>
    <p><span class="bl_clr"> Branch Code :</span>0055</p>
    <p><span class="bl_clr"> Branch Type :</span> BOOKING/DELIVERY</p>

    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>

  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Chennai    Regional Office</span>
    <p><span class="bl_clr"> Branch Code :</span>0852</p>
    <p> <span class="bl_clr"> Branch Type :</span> REGIONAL OFFICE</p>

    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>

  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Chennai</span>
    <p><span class="bl_clr"> Branch Code :</span>0014 </p>
    <p><span class="bl_clr"> Branch Type :</span> BOOKING</p>

    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>

  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Chennai</span>
    <p><span class="bl_clr"> Branch Code :</span>0648</p>
    <p><span class="bl_clr"> Branch Type :</span> DELIVERY</p>

    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>

  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Maraimalainagar</span>
    <p><span class="bl_clr"> Branch Code :</span>0574</p>
    <p><span class="bl_clr"> Branch Type :</span> BOOKING/DELIVERY</p>

    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>

  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Coimbatore</span>
    <p><span class="bl_clr"> Branch Code :</span>0007 &amp; 0320</p>
    <p><span class="bl_clr"> Branch Type :</span> BOOKING/DELIVERY</p>

    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>

  </div> 
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Sriperumbudur</span>
    <p><span class="bl_clr"> Branch Code :</span>1244</p>
    <p> <span class="bl_clr"> Branch Type :</span> BOOKING/DELIVERY</p>

    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>

  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Mount Road</span>
    <p><span class="bl_clr"> Branch Code :</span>0053</p>
    <p> <span class="bl_clr"> Branch Type :</span> BOOKING/DELIVERY</p>

    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Cuddalore</span>
    <p><span class="bl_clr"> Branch Code :</span>0298</p>
    <p> <span class="bl_clr"> Branch Type :</span> BOOKING/DELIVERY</p>
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Madurai</span>
    <p><span class="bl_clr"> Branch Code :</span>0016</p>
    <p> <span class="bl_clr"> Branch Type :</span> BOOKING/DELIVERY</p>
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Taramani</span>
    <p><span class="bl_clr"> Branch Code :</span>1397</p>
    <p> <span class="bl_clr"> Branch Type :</span> BOOKING/DELIVERY</p>
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>AGENCIES</span>
    
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>DINDUGAL</span>
    <p><span class="bl_clr"> Branch Code :</span>0257</p>
    
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>KARUR </span>
    <p><span class="bl_clr"> Branch Code :</span>0078</p>
   
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>SALEM</span>
    <p><span class="bl_clr"> Branch Code :</span> 0047</p>
   
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PALLAVARAM </span>
    <p><span class="bl_clr"> Branch Code :</span>0468</p>
  
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PONDY IND    ESTATE</span>
    <p><span class="bl_clr"> Branch Code :</span>0769</p>
 
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> IDAPPADI</span>
    <p><span class="bl_clr"> Branch Code :</span> 0731</p>
 
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> ERODE AG</span>
    <p><span class="bl_clr"> Branch Code :</span> 0076</p>

    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> KARAIKAL </span>
    <p><span class="bl_clr"> Branch Code :</span> 0504</p>
    
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>TRICHY </span>
    <p><span class="bl_clr"> Branch Code :</span> 0103</p>
   
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> RANIPET </span>
    <p><span class="bl_clr"> Branch Code :</span> 0147</p>
  
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>BODINAYANKUR</span>
    <p><span class="bl_clr"> Branch Code :</span> 0358</p>
  
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</span>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PERUNGUDI</span>
    <p><span class="bl_clr"> Branch Code :</span> 0271</p>
   
    <p><span class="bl_clr"> Add. :</span> New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;TIRUNELVELI&nbsp;</span>
    <p><span class="bl_clr"> Branch Code :</span> 0221</p>
 
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>TUTICORIN</span>
    <p><span class="bl_clr"> Branch Code :</span> 0220</p>

    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>SINGANALLUR</span>
    <p><span class="bl_clr"> Branch Code :</span>0470</p>
   
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PERINAYANKPALAYAM</span>
    <p><span class="bl_clr"> Branch Code :</span> 0534</p>
    
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>THIRUBHUVANAI</span>
    <p><span class="bl_clr"> Branch Code :</span> 0512</p>
 
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>TIRUPATI</span>
    <p><span class="bl_clr"> Branch Code :</span> 1401</p>
    
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
   
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PANRUTI</span>
    <p><span class="bl_clr"> Branch Code :</span> 1421</p>
   
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>THIRUKKANUR</span>
    <p><span class="bl_clr"> Branch Code :</span> 1426</p>
    
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>VILLUPURAM</span>
    <p> <span class="bl_clr"> Branch Code :</span> 1427</p>
    
    <p><span class="bl_clr"> Add. :</span>New No. 130  Old No.182, Union Mills Road, Geethanjali    Complex, Opp: Sangeetha theatre, Tirupur-641601</p>
  </div>

  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Patancheru</span>
    <p><span class="bl_clr"> Add. : </span>26-80,old plot    no-16/12(plot no:-A/16/26), Near santhoshimata temple,Patencheru,Telangana,    A.P-502319</p>
    <p><span class="bl_clr"> Mob. No. : </span> 8455240101</p>
    <p><span class="bl_clr"> Email Id. : </span><a href="mailto:pattancheru_prl@patel-india.com"> pattancheru_prl@patel-india.com </a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Kompally/TR/APRO</span>
    <p><span class="bl_clr"> Add. : </span>Survey.98,NH7,Opp to runway9/Big    bazaar,Kompally village, Adjacent to bantia furnitures,quthbullapur    mandal,  Kompally,Hyderabad,    Telangana-500014.</p>
    <p><span class="bl_clr"> Mob. No. : </span> 8790005601 / 8790005616 / 8790005609 / 8790005610 / 8790005611  </p>
    <p><span class="bl_clr"> Email Id. : </span> <a href="mailto:n.srinivasa@patel-india.com"> n.srinivasa@patel-india.com </a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Secundrabad</span>
    <p><span class="bl_clr"> Add. : </span>Shop:2, plot no.24,rasoolpura,    chandanagar, Ground floor, Secundrabad,Telangana-500003</p>
    <p><span class="bl_clr"> Mob. No. : </span> 8790005602 / 040-27905516</p>
    <p><span class="bl_clr"> Email Id. : </span> <a href="mailto:secunderabad_dly@patel-india.com"> secunderabad_dly@patel-india.com </a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Prashantnagar</span>
    <p><span class="bl_clr"> Add. : </span>D.no:5-5-35/22,Plot no:119/A,    Prasanthnagar,Moosapet,Hyd,Telangana-500072</p>
    <p><span class="bl_clr"> Mob. No. : </span> 8790005607 / 040-23075389</p>
    <p><span class="bl_clr"> Email Id. : </span> <a href="mailto:prashantnagar_prl@patel-india.com"> prashantnagar_prl@patel-india.com </a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Kakinda</span>
    <p><span class="bl_clr"> Add. : </span>D.No:42-1-20,Vallbhai Street, Opp    swapna theatre, Kakinada,Andhra Pradesh -533001</p>
    <p><span class="bl_clr"> Mob. No. : </span> 9959688199</p>
    <p><a href="mailto:kakinada@patel-india.com%20/">kakinada@patel-india.com </a>   /  <a href="mailto:drameshbabu265@gmail.com"> drameshbabu265@gmail.com</a></p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Nacharam</span>
    <p><span class="bl_clr"> Add. : </span>Plot no:7-10/48,New Raghavendranagar    Colony, St.Peters School Lane, Nacharam, Hyd,Telangana-500076</p>
    <p><span class="bl_clr"> Mob. No. : </span> 9704135991</p>
    <p><span class="bl_clr"> Email Id. : </span><a href="mailto:nacharam_prl@patel-india.com"> nacharam_prl@patel-india.com </a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Dwarapudi</span>
    <p><span class="bl_clr"> Add. : </span>Surveyno:197,D.No.    5-265/2, New Cloth Market, Main Road, Near Delhi Rajasthan Transport &amp;    Siddhartha  convent,Dwarapudi,Andhra    Pradesh-533341</p>
    <p><span class="bl_clr"> Mob. No. : </span>9491445967</p>
    <p><span class="bl_clr"> Email Id. : </span> <a href="mailto:rajahmundry@patel-india.com"> rajahmundry@patel-india.com </a> </p>
  </div>
  
 	<div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Guntur</span>
    <p><span class="bl_clr"> Add. : </span>Dno:19-11-10,sangadigunta,Lanchester    Road,  Guntur,AndhraPradesh 522003</p>
    <p><span class="bl_clr"> Mob. No. : </span> 9440885759</p>
    <p><span class="bl_clr"> Email Id. : </span><a href="mailto:bsrao2000guntur@gmail.com"> bsrao2000guntur@gmail.com </a> </p>
  </div>
  
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Vijayawada</span>
    <p><span class="bl_clr"> Add. : </span>plot-43,Phase-3,SRMT road,    Autonagar,Vijayawada,Andhra Pradesh-520007</p>
    <p><span class="bl_clr"> Tel. No. : </span> 0866-2543299</p>
    <p><span class="bl_clr"> Mob. No. : </span> 8790005603</p>
    <p><span class="bl_clr"> Email Id. : </span><a href="mailto:vijaywada@patel-india.com"> vijaywada@patel-india.com </a></p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Diwandevdi</span>
    <p><span class="bl_clr"> Add. : </span>22-7-270,Dewandevidi,NizamBagh,    Hyderabad, Telangana-500002</p>
    td><span class="bl_clr"> Tel. No. : </span> 040-24520892</p>
    <p><span class="bl_clr"> Mob. No. : </span> 9059057946</p>
    <p><span class="bl_clr"> Email Id. : </span> <a href="mailto:diwandevdi_dly@patel-india.com"> diwandevdi_dly@patel-india.com </a> </p>
  </div>
 <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Vizag(Gajuwaka)</span>
    <p><span class="bl_clr"> Add. : </span> D.No:-7-8-35/2,Block-B,IDA,    Autonagar,Opp.power station,Gajuwaka,Andhra Pradesh-530026</p>
    <p><span class="bl_clr"> Mob. No. : </span> 9298955363 / 9581479514</p>
    <p><a href="mailto:vishakhapatnam@patel-india.com%20/">vishakhapatnam@patel-india.com </a> / <a href="mailto:gajuwaka_br_prl@patel-india.com"> gajuwaka_br_prl@patel-india.com</a></p>
  </div>

</div>
                            </li>


                            <li>

                                <div class="content__wrapper">

                                    

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Mapusa,    Goa</span>
                                        <p><span class="bl_clr">Branch Code :</span>653</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Ramdas Vithu Fatarpekar</p>
                                        <p><span class="bl_clr">Add : </span>Jewel Plaza,    Bldg.,Shop no- 3 &amp; 15, Near Government ITI, Padem, Mapusa, Goa - 403507</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9822187155</p>
                                    </div>


                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Panjim, Goa</span>
                                        <p><span class="bl_clr">Branch Code :</span>652</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Nitant Sazu Walwaikar</p>
                                        <p><span class="bl_clr">Add : </span>PATEL    ROAD HOS 39,PANVEL RAIBANDAR, PANJIM GOA 403006</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9850212651</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Ahmednagar</span>
                                        <p><span class="bl_clr">Branch Code :</span>135</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Sagar Thipse</p>
                                        <p><span class="bl_clr">Add : </span>Plot    No.P39,MIDC Area,Opp.Chakan Oil Mills, Ahmednagar - 414111</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9922595903</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:ahmednagar@patel-india.com">ahmednagar@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Baramati</span>
                                        <p><span class="bl_clr">Branch Code :</span>752</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Ravindra Vishnu Shinde</p>
                                        <p><span class="bl_clr">Add : </span>Opp. Shriram Nagar, Bhigwan Road, Baramati -    413102</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9822963785</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:baramati_ba_prl@patel-india.com">baramati_ba_prl@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Chakan</span>
                                        <p><span class="bl_clr">Branch Code :</span>283</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE - BOOKING</p>
                                        <p><span class="bl_clr">Contact Person : </span>Mukund Randive</p>
                                        <p><span class="bl_clr">Add : </span>Gat    No. 410/411,Medankar Complex, Medankar Wadi, Pune Nasik Highway,  Chakan, Pune - 410501</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9326826356</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:chakan_ba_prl@patel-india.com">chakan_ba_prl@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Khedshivapur</span>
                                        <p><span class="bl_clr">Branch Code :</span>768</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Atul laxman Shivtare</p>
                                        <p><span class="bl_clr">Add : </span>Near  Shivam Hardware , Behind  Sree     Swami  Samarth Hotel ,  At Post Velu, Veluphata, Tal. Bhor Pune    412205</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9822108408 /    9552075778</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:khedshivapur_prl@patel-india.com">khedshivapur_prl@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Shirwal</span>
                                        <p><span class="bl_clr">Branch Code :</span>1210</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Atul laxman Shivtare</p>
                                        <p><span class="bl_clr">Add : </span>Patel    Roadways         GAT NO 433/B Opp  Lawkim  LTD       AT/P Shindewadi  Tal  – Khandala        DIST  - Satara  412801</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9822108408 / 9657565778</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Punecity</span>
                                        <p><span class="bl_clr">Branch Code :</span>1286</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE - BOOKING</p>
                                        <p><span class="bl_clr">Contact Person : </span>Sandeep Gadgil</p>
                                        <p><span class="bl_clr">Add : </span>Plot    No. 1,  Survey no-581/5C, near Gulmohar    hsg sty, opp. NK Ashiyana, Marketyard, Pune 411037</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9822045236</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:poonacity_bkg@patel-india.com ">poonacity_bkg@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Phursungi</span>
                                        <p><span class="bl_clr">Branch Code :</span>340</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Anil Suryawanshi</p>
                                        <p><span class="bl_clr">Add : </span>Pune Saswad Road,mantarwadi phata,    Phursungi, Pune-412308</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9623362200</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:phursungi_prl@patel-india.com" title="mailto:phursungi_prl@patel-india.com">phursungi_prl@patel-india.com</a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Pirangut</span>
                                        <p><span class="bl_clr">Branch Code :</span>449</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Nikhil Jadhav</p>
                                        <p><span class="bl_clr">Add : </span>Shinde    Wadi, Kasar Amboli, Post Ambarvet, &nbsp;PUNE&nbsp;&nbsp;412108&nbsp;</p>
                                        <p><span class="bl_clr">Mob. No. : </span>8308373991</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:pirangut.ba@patel-india.com">pirangut.ba@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Sholapur</span>
                                        <p><span class="bl_clr">Branch Code :</span>175</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Subhash Ghodse</p>
                                        <p><span class="bl_clr">Add : </span>Bihind    Shreeji Industry A/P Kondi, Tal- North Sholapur, Dist- Sholapur - 413006</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9823417450</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:solapur.ba@patel-india.com">solapur.ba@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Ranjangaon</span>
                                        <p><span class="bl_clr">Branch Code :</span>1219</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Jagarnath Rawani</p>
                                        <p><span class="bl_clr">Add : </span>Opp.    Mahaganpati Mandir, Pune-Nagar Highway, Ranjangaon-412220</p>
                                        <p><span class="bl_clr">Mob. No. : </span>7040786325</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Urulikanchan</span>
                                        <p><span class="bl_clr">Branch Code :</span>597</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Sambhaji Darekar</p>
                                        <p><span class="bl_clr">Add : </span>Muktangan    Complex, opp:Kasturi Garden , pune-sholapur road, Urulikanchan, Tal-Haveli,    Dist.: Pune-412202</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9422331609</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:urulikanchan_ba@patel-india.com">urulikanchan_ba@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Wagholi</span>
                                        <p><span class="bl_clr">Branch Code :</span>117</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Aashish Kotadia</p>
                                        <p><span class="bl_clr">Add : </span>Guru    Ramdas Warehouse, Opp. Parijat Dhaba, Pune Nagar Road, Gala No.4,    Wagholi,Pune-412207.</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9225511311,    9960967786</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:wagholi@patel-india.com">wagholi@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Hubli </span>
                                        <p><span class="bl_clr">Branch Code :</span>229</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>S.H.Yargatti</p>
                                        <p><span class="bl_clr">Add : </span>Patel    Roadways,  Shed no 10, KSCMF    Compound,  Vikas Nagar, Hubli-580021</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9448363436</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:hubli@patel-india.com">hubli@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Koregaon bhima</span>
                                        <p><span class="bl_clr">Branch Code :</span>583</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Shivaji S Jadhav</p>
                                        <p><span class="bl_clr">Add : </span>At    Post Sanaswadi, Near Telephone Exchange, Tal. Shirur, &nbsp;PUNE&nbsp;    &nbsp;412207&nbsp;</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9404244101</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:koregaon_ba_prl@patel-india.com">koregaon_ba_prl@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Lonavala</span>
                                        <p><span class="bl_clr">Branch Code :</span>209</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Ralph D'silva</p>
                                        <p><span class="bl_clr">Add : </span>Maharaja    Complex, Shop No. 11, Nangargaon, Lonavala -410401</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9421017052</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Karad</span>
                                        <p><span class="bl_clr">Branch Code :</span>275</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Sanjay Mohanlala Shaha</p>
                                        <p><span class="bl_clr">Add : </span>C/o    Shakti Transport Co., MArket Yard  Gate    no - 5,  near Militry canteen , Karad,    Dist -Satara-415110</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9822052856</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Satara</span>
                                        <p><span class="bl_clr">Branch Code :</span>238</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Nihal Mhetre</p>
                                        <p><span class="bl_clr">Add : </span>&quot;Bhartiya&quot;,G-21,    Old MIDC,Near BSNL, SATARA - 415 004 </p>
                                        <p><span class="bl_clr">Mob. No. : </span>9673941739</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:satara_fr@patel-india.com">satara_fr@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Gokul    shirgaon</span>
                                        <p><span class="bl_clr">Branch Code :</span>1253</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Mangesh B. Kulkarni</p>
                                        <p><span class="bl_clr">Add : </span>Plot    No.R-10,Opp.IFM Electronics ,Gokul Circle, MainRoad ,MIDC Gokul    Shirgaon,DIST-Kolhapur,Pin:416234</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9422414214/    9371178273</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:gokul_ba_prl@patel-india.com">gokul_ba_prl@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Kurkumbh</span>
                                        <p><span class="bl_clr">Branch Code :</span>1248</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Sunil Shitole</p>
                                        <p><span class="bl_clr">Add : </span>Plot    no. SS-3; MIDC KURKUMBH, Opp. Honour Lab Ltd,     Behind-Shivratna Hotel,Tal-Daund. Dist- Pune-413802.</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9922747604</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:kurkumbh_ba_prl@patel-india.com">kurkumbh_ba_prl@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Chakan  MIDC</span>
                                        <p><span class="bl_clr">Branch Code :</span>1278</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Arun S.Lohangade</p>
                                        <p><span class="bl_clr">Add : </span>Office    No.10, Navaratna Complex, chakan, Tal-Khed, Pune-410501    </p>
                                        <p><span class="bl_clr">Mob. No. : </span>9881031234</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:chakanmidc_fr_prl@patel-india.com">chakanmidc_fr_prl@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Bhosarigaon</span>
                                        <p><span class="bl_clr">Branch Code :</span>1289</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Vaibhav Bhimrao Khandage</p>
                                        <p><span class="bl_clr">Add : </span>Plot    No. 240, Sector no.7 , PCNTDA, Bhosari, Pune-411026</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9860304528</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:bhosarigaon@patel-india.com">bhosarigaon@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Wadgaon Budruk</span>
                                        <p><span class="bl_clr">Branch Code :</span>777</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Mangesh Kadu</p>
                                        <p><span class="bl_clr">Add : </span>S.No.    25/5,Office No. 1, Swami Samarth Apt.., Opp. Savali Dhaba, Pune Sinhagad    Road,  Nanded Phata, Wadgaon Budruk,    Pune-411041</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9604179809 / 9527147779</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:wadgaon_fr_prl@patel-india.com">wadgaon_fr_prl@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Verna</span>
                                        <p><span class="bl_clr">Branch Code :</span>651</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>DEEPAK VISHWAKARMA</p>
                                        <p><span class="bl_clr">Add : </span>C/o    Anandi Logistics, S -136, PHASE III B, Verna , Industrial Estate Verna – Goa    403722</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9371365215</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:verna@patel-india.com">verna@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Pimpri-Chinchwad</span>
                                        <p><span class="bl_clr">Branch Code :</span>242</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>VIJAY PARASKAR</p>
                                        <p><span class="bl_clr">Add : </span>Sr.    No. 64, Opp. Ador Welding, Near Aishwaryam Bldg., Near Datta Mandir, Akurdi    Lane, Chinchwad - 411019.</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9049525128</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:poonamain_bkg@patel-india.com">poonamain_bkg@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>MAPUSA</span>
                                        <p><span class="bl_clr">Branch Code :</span>0653</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Pratik Marathe</p>
                                        <p><span class="bl_clr">Add : </span>Jewel    Plaza, Bldg.,Shop no- 3 &amp; 15, Near Government ITI, Padem, Mapusa, Goa -    403507</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9371365217</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:prl.mapusa@patel-india.com">prl.mapusa@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>WALUJ</span>
                                        <p><span class="bl_clr">Branch Code :</span>337</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Sushant / Vijay</p>
                                        <p><span class="bl_clr">Add : </span>PLOT    NO. D-13/1, WALUJ INDUSTRIAL AREA,MIDC WALUJ, AURANGABAD 431133</p>
                                        <p><span class="bl_clr">Tel. No. : </span>02402555311 </p>
                                        <p><span class="bl_clr">Mob. No. : </span>9371365218</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:waluj@patel-india.com">waluj@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>BELGAUM </span>
                                        <p><span class="bl_clr">Branch Code :</span>288</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Archana</p>
                                        <p><span class="bl_clr">Add : </span>Plot    No. 703/1, Patravali Compound industrial estate Udyambagh, Belgaum - 590008</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9379219511</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:belgaummain@patel-india.com">belgaummain@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>KURULI (CHAKAN TR)</span>
                                        <p><span class="bl_clr">Branch Code :</span>1050</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Jagannath /    Sandeep Gadekar</p>
                                        <p><span class="bl_clr">Add : </span>Gat    No.81, Village Kuruli, Opp. Sadguru Petrol Pump, Behind New Babetta Dhaba,    Pune Nashik Highway, Tal Khed, Chakan, Pune-410501</p>
                                        <p><span class="bl_clr">Mob. No. : </span>9371365202</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:chakan_tr@patel-india.com">chakan_tr@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>AURANGABAD</span>
                                        <p><span class="bl_clr">Branch Code :</span>140</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Nilesh Karale /    Vikas Sole</p>
                                        <p><span class="bl_clr">Add : </span>Plot    No.40 &amp; 41, Service Industries, Cidco, Chikalthana, Aurangabad-431210)</p>
                                        <p><span class="bl_clr">Mob. No. :  </span>9371365207</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:aurangabad@patel-india.com">aurangabad@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>TALEGAON</span>
                                        <p><span class="bl_clr">Branch Code :</span>721</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Sravan</p>
                                        <p><span class="bl_clr">Add : </span>Opp.    Sheetal Hotel, Savitrabai Niwas, Mahasalkar Colony, Old Mumbai-Pune Highway,    Pune.</p>
                                        <p><span class="bl_clr">Mob. No. :  </span>8605309352</p>
                                        <p><span class="bl_clr">Email. Id. : </span><a href="mailto:talegaon_ba_prl@patel-india.com">talegaon_ba_prl@patel-india.com </a></p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>TATHAWADE</span>
                                        <p><span class="bl_clr">Branch Code :</span>1025</p>
                                        <p><span class="bl_clr">Ownership : </span>BUSINESS ASSOCIATE</p>
                                        <p><span class="bl_clr">Contact Person : </span>Kuldip</p>
                                        <p><span class="bl_clr">Add : </span>Yelwande    Wasti, Hinjawadi-Marunji Road, Hinjawadi, Tal- Mulshi, Pune-411057</p>
                                        <p><span class="bl_clr">Mob. No. :  </span>9325993346</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            GUJARAT R. O.</span>
                                        <p><span class="bl_clr">Branch Type : </span>Regional Office</p>
                                        <p><span class="bl_clr">Add : </span>A/4,    Jaimangal House, Nr. Gandhigram Railway station, Opp. Sakar – 1, Ashram road,    ahmedabad - 380009.</p>
                                    </div>

                                    

                                    

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>ASLALI</span>
                                        <p><span class="bl_clr">Branch Type : </span>Transhipment /    Booking Office</p>
                                        <p><span class="bl_clr">Add : </span>Block    no. 499, harsidhi estate, nr. Lalji mulji transport, daskroi, aslali,    ahmedabad - 382427</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Vapi</span>
                                        <p><span class="bl_clr">Branch Type : </span>Transhipment </p>
                                        <p><span class="bl_clr">Add : </span>Survey    #23/ 4, Village Valwada,Ta; Umbergaon, Dist : Valsad,Gujarat ,396105</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>UNN</span>
                                        <p><span class="bl_clr">Branch Type : </span>Transhipment /    Booking Office</p>
                                        <p><span class="bl_clr">Add : </span>Godown    No.B/4, Madhuram Estate, Sachin Palasana Road, opp. Mulla dyeing, Near T.C.I    Godown, surat - navsari highway, Sachin,Surat - 394220</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            UDHANA</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch</p>
                                        <p><span class="bl_clr">Add : </span>Plot.No    A/15, Sardar Patel Road, Road No.9, Opp. Hindustan Fashion Ltd., Udyognagar,    Udhna - 394210, surat.</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            DAMAN</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch</p>
                                        <p><span class="bl_clr">Add : </span>Shop    no 5 , R B &amp; SONS&nbsp; H.P PETROL BUNK, NEAR USTAV    HOTEL , Daman Vapi Main Road, Dhabel Daman 396210</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            SILVASSA</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch</p>
                                        <p><span class="bl_clr">Add : </span>KESHARI PLAZA , VAPI SILVASSA MAIN ROAD    ,LAVACHHA VIDEO CINEMA ,NR. PATEL WEIGHT BRIDGE -LAVACHHA – 396195</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            ANKLESHWAR</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch</p>
                                        <p><span class="bl_clr">Add : </span>4792,    G.I.D.C., Opp.Last Water Tank, Ankleshwar - 393002,GUJARAT&nbsp;</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            F. NAGAR</span>
                                        <p><span class="bl_clr">Branch Type : </span>Transhipment /    Booking Office/ Delivery</p>
                                        <p><span class="bl_clr">Add : </span>Block    no.443, godown no.1, national highway no.8, opp.fertilizer nagar gate,    dashrath-390008, vadodara,Gujarat.</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            HALOL</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch</p>
                                        <p><span class="bl_clr">Add : </span>Mahakali    Estate, Halol-Godhara Road, Halol - 389350.</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            MANJUSAR</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch</p>
                                        <p><span class="bl_clr">Add : </span>5,    Shankarbhai Patel Estate, Savali Road, Manjusar, Tal.: Savli, Dist.: Vadodara    - 391775.</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            MAKARPURA</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch</p>
                                        <p><span class="bl_clr">Add : </span>Shed    No.200,G.I.D.C., Makarpura,Baroda-390010,GUJARAT</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            SANAND</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch</p>
                                        <p><span class="bl_clr">Add : </span>Plot    No.1, Kailash Industrial Estate, Iyava, Tal.: Sanand, Dist.: Ahmedabad –    382172.</p>
                                    </div>


                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            GUJARAT GINNING</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch</p>
                                        <p><span class="bl_clr">Add : </span>88,    (218 / 13 / 1 &amp; 2), Gujarat Ginning Mills Compound, Outside Prem Darwaja,    Idga Road, Ahmedabad - 380001</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            NAWAGAM</span>
                                        <p><span class="bl_clr">Branch Type : </span>Transhipment /    Booking Office/ Delivery</p>
                                        <p><span class="bl_clr">Add : </span>Lalpari    Road,Anandpur,Nawagam-360003,Dist.- Rajkot</p>
                                    </div>


                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Vapi</span>
                                        <p><span class="bl_clr">Branch Type : </span>Area office / BA    Booking </p>
                                        <p>160/2B,2ND Phase, Near Dadrawala    Paper Mills...396195</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Bhavnagar</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>OLD    BUNDER ROAD NEAR MARU  PETROL PUMP F/44    GODOWN NO- A &quot;10&quot; BHAVNAGAR - 364001</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Naroda</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>672/114, Naroda    Road,Opp. Hindustan Oxygen Ltd.,Near Thakkar Bapu    Ashram,Naroda,Ahmedabad-382330,GUJARAT&nbsp;</p>
                                    </div>


                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Changodar </span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>Near shivam food    suply , Oppsotie Harishidhi way bridge , Sanathal -changodar road . Changodar    , Ahmedabad-382213</p>
                                    </div>


                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Sarkhej </span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>No 14, Sahkar    Estate,Behind Hotel Sukh Sagar,Sarkhej-Bavla Highway,Sarkhej, Ahmedabad -    382210, GUJARAT&nbsp;</p>
                                    </div>


                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Surendranagar</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp; Delivery    Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>Shop No 20, Opp. Bhavna    Waybridge,, Nr. Olcare Lab, GIDC, Wadhwan-363035. Surendranagar</p>
                                    </div>





                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Narol </span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>Godown No.10, Shreeji    Warehousing Estate, opp. Caliconagar, narol sarkhej road, narol,    ahmedabad.382405</p>
                                    </div>


                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Odhav </span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>No.6 Ishawar    Estate,G.I.D.C.,Odhav Road, Odhav,Ahmedabad,GUJARAT&nbsp;,382415</p>
                                    </div>



                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            UMARGOAN</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>Plot No.72/2,Near    Metal Beds,GIDC,Umargaon,GUJARAT&nbsp;396155</p>
                                    </div>



                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Gandhidham</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>Shop No.9, Ward 12B,    Collector Road, Raviraj Complex, Gandhidham-372201.</p>
                                    </div>



                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Vatva</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>E/9/A    SHIV SHAKTI INDUSTRIYAL ESTATE NR, JAIPUR GOLDEN TRANSPORT,KIRAN    INDUSTRIES,PHASE -1GIDC VATVA, AHMEDABAD-382445</p>
                                    </div>



                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            &nbsp;SANTEJ&nbsp;</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>Plot No.    795/2&nbsp;,Patel Estate,Opp. Rallies Pharmaceuticals,Santej,    Rakanpur,Gandhinagar.382721</p>
                                    </div>


                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Shapar</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>Shop No.6, Ravi Kiran    Complex, Road No.2, Shapar,Rajkot.360024</p>
                                    </div>



                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Jamnagar</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>GIDC-2,Plot no.137,    Opp&nbsp;Kaveri&nbsp;Dairy,Dared,JAMNAGAR-361005</p>
                                    </div>



                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            &nbsp;POR RAMANGAMADI&nbsp;</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>Koodalmaa House,    4/88/3 , Shreenath plaza, N.H. No.8, Por Ramangamadi, Dist.- Vadodara.391243</p>
                                    </div>



                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Naroda GIDC</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking Delivery Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>Godown no 2 , Opp new samrat namkeen, Near Toton Pharma, Phase 2 gidc naroda, Ahmedabad -382330</p>
                                    </div>


                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Gandhinagar</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>1. Plot No.    906/22/4  Sector No. 28, GIDCE ngg.    Estate Near Hyundai Show room, Gandhinagar Gujarat   Pin No.382018</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                            VV Nagar</span>
                                        <p><span class="bl_clr">Branch Type : </span>Booking &amp;    Delivery Branch (BA)</p>
                                        <p><span class="bl_clr">Add : </span>shop    no.1_GIDC,Vallabh vidya nagar,Near SB Engineering, plot no.160 -Anand ,    Gujarat-388001</p>
                                    </div>

                                    

                                    

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>hiwandi </span>
                                        <p><span class="bl_clr">Branch Code :</span>212</p>
                                        <p><span class="bl_clr">Add : </span>Gala No. 1,2,3, Bldg No. 6,    Arihant Compound, Koper, Purna Village, Bhiwandi 421 302</p>
                                        <p><span class="bl_clr">Mob. No. :</span>7777 0428 34/35/18/01</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Andheri </span>
                                        <p><span class="bl_clr">Branch Code :</span>43</p>
                                        <p><span class="bl_clr">Add : </span>Unit No.2, Ground Floor, Sarita Estate,90 feet Road, Near Uma Maheshwari Temple, Zarimari, Andheri - Kurla    Road, Mumbai 400 072</p>
                                        <p><span class="bl_clr">Mob. No. :</span>77777 0428 08/31</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Chakala</span>
                                        <p><span class="bl_clr">Branch Code :</span>1055/0001</p>
                                        <p><span class="bl_clr">Add : </span>Gala No. 4,, Kurla Street, Dana    Bunder, Opp. K Galli,Musjid East, Mumbai 400 009</p>
                                        <p><span class="bl_clr">Mob. No. :</span>77777 0428 06/07</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Vashi</span>
                                        <p><span class="bl_clr">Branch Code :</span>261</p>
                                        <p><span class="bl_clr">Add : </span>Plot No. C 226, MIDC, Turbhe    Industrial Area, Navi Mumbai 400 750</p>
                                        <p><span class="bl_clr">Mob. No. :</span>79930 4602 09</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Vasai</span>
                                        <p><span class="bl_clr">Branch Code :</span>1336</p>
                                        <p><span class="bl_clr">Add : </span>Shop No6, Sheetal Swapna Industrial Eastet, Sativali Road,Gokhiware, Vasai East 401 208</p>
                                        <p><span class="bl_clr">Mob. No. :</span>77777 0428 32/25</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Palghar </span>
                                        <p><span class="bl_clr">Branch Code :</span>1350</p>
                                        <p><span class="bl_clr">Add : </span>Shop No. 8A, Near Ayappa Mandir, Dist    Palghar, 401 404</p>
                                        <p><span class="bl_clr">Mob. No. :</span>77777 0428 16/21</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Tarapur</span>
                                        <p><span class="bl_clr">Branch Code :</span>247</p>
                                        <p><span class="bl_clr">Add : </span>Shop No. 18 &amp; 26, Bldg. No.5,    Mahavir Chember, Navapur Road, Boisar 401 501</p>
                                        <p><span class="bl_clr">Mob. No. :</span>77777 0428 05/04/21</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Khopoli</span>
                                        <p><span class="bl_clr">Branch Code :</span>663</p>
                                        <p><span class="bl_clr">Add : </span>Gala No. 5 &amp;6, Shree Apartment,    Vard No. 18,Mauje Hal BK, Khopoli, Dist Raigad 410 203</p>
                                        <p><span class="bl_clr">Mob. No. :</span>77045 6545 78</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Taloja</span>
                                        <p><span class="bl_clr">Branch Code :</span>383</p>
                                        <p><span class="bl_clr">Add : </span>Plot PAP 27, Opp. Sanghi Orgam, Near    Nawada Petrol Pump, Taloja Indstrial Area, MIDC Taloja 410 208</p>
                                        <p><span class="bl_clr">Mob. No. :</span>77777 0428 03/20</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Bhandup</span>
                                        <p><span class="bl_clr">Branch Code :</span>163</p>
                                        <p><span class="bl_clr">Add : </span>Shop No.32, B1, Anjali Kumar Bldg,    Mhada Colony, Subhash Nagar, Bhandup West 400 078</p>
                                        <p><span class="bl_clr">Mob. No. :</span>77777 0428 30</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>BHAW TRM</span>
                                        <p><span class="bl_clr">Branch Code :</span>364</p>
                                        <p><span class="bl_clr">Add : </span>Gala No. 1 to 7, A8,A9, Shree Raj    Rajeshwari Logistic Park, At Bhatale Village Vehale, Bhiwandi 421 302</p>
                                        <p><span class="bl_clr">Mob. No. :</span>78433 9535 43/46/47</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Dombivali </span>
                                        <p><span class="bl_clr">Branch Code :</span>1420</p>
                                        <p><span class="bl_clr">Add : </span>Gala No.3, Bhalla Bhavan, Kalyan -    Shil Road, Near Lalit Kata, manpada, Dombivali east 421 204</p>
                                        <p><span class="bl_clr">Mob. No. :</span>77777 0428 33</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Regional Office</span>
                                        <p><span class="bl_clr">Branch Code :</span>816</p>
                                        <p><span class="bl_clr">Add : </span>101-105, First floor, Parijat Garden    Commercial Complex, Carnation Plaza, Bldg. No.1, Near Hyper City,GB Road,    Kasarwadavali, Thane West 400 615</p>
                                        <p><span class="bl_clr">Mob. No. :</span>7022 25972883/84/85</p>
                                    </div>


                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Lote</span>
                                        <p><span class="bl_clr">Branch Code :</span>771</p>
                                        <p><span class="bl_clr">Branch Type :</span>Business Associates</p>
                                        <p><span class="bl_clr">Add : </span>House No 403, Lote    Parshuram MIDC, Mumbai – Goa Highway, Tal – Khed , Dist- Ratnagiri 415722</p>
                                        <p><span class="bl_clr">Mob. No. :</span>7(02356)272345 &nbsp;/    9822982257</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Ambernath</span>
                                        <p><span class="bl_clr">Branch Code :</span>538</p>
                                        <p><span class="bl_clr">Branch Type :</span>Business Associates</p>
                                        <p><span class="bl_clr">Add : </span>Gala NO. B 3/1,    Behind MSEB, Near ShivSena Office, Morivali Village M I D C, Ambernath (west)    421501</p>
                                        <p><span class="bl_clr">Mob. No. :</span>79422 4752    09/9322739602</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Sativali</span>
                                        <p><span class="bl_clr">Branch Code :</span>429</p>
                                        <p><span class="bl_clr">Branch Type :</span>Business Associates</p>
                                        <p><span class="bl_clr">Add : </span>Gala No.3, Gangotri Bldg No.1, Tungareshwar    Industrial Complex, Sativali Kaman Road, Sativali, Vasai East, Thane 401 202</p>
                                        <p><span class="bl_clr">Mob. No. :</span>77777 0428    29/9921607884/9270092447</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Mira Road</span>
                                        <p><span class="bl_clr">Branch Code :</span>1271</p>
                                        <p><span class="bl_clr">Branch Type :</span>Business Associates</p>
                                        <p><span class="bl_clr">Add : </span>Shop No. D/18, Rajmandir Complex, Hatkesh Udyog Nagar, Mira    Road - East. Thane - 401 107</p>
                                        <p><span class="bl_clr">Mob. No. :</span>79004 8582 01</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Ratnagiri</span>
                                        <p><span class="bl_clr">Branch Code :</span>659</p>
                                        <p><span class="bl_clr">Branch Type :</span>Business Associates</p>
                                        <p><span class="bl_clr">Add : </span>Plot No 21, Near TRP, MIDC mirjole Ratnagiri 415639</p>
                                        <p><span class="bl_clr">Mob. No. :</span>79665299815</p>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

                                        <span class="bl_clr uppercase"><i class="fa fa-map-marker" aria-hidden="true"></i>Goregoan</span>
                                        <p><span class="bl_clr">Branch Code :</span>791</p>
                                        <p><span class="bl_clr">Branch Type :</span>Business Associates</p>
                                        <p><span class="bl_clr">Add : </span>New ashok nagar services road, &nbsp;    W e highway Goregaon ( E ) Mumbai -4000063</p>
                                        <p><span class="bl_clr">Mob. No. :</span>7022-26865243/ 09320526041</p>
                                    </div>

                                    


                                </div>

                             </li>

                        </ul>

                    </div>

                </div>

                <div class="clearfix"></div>

                <div class="col-md-12 col-sm-12 col-xs-12 patel_roadways_address_wrp pull-left" id="Div1">

                    <div class="row">

                        <div class="patel_roadways_address_wrp_head pull-left">patel airfreight Branches</div>

                        <div class="clearfix"></div>

                        <ul class="pobc_tabs tabs">
                            <li><a class="active_1">EAST REGION</a></li>
                            <li><a href="#north_zone">NORTH REGION</a></li>
                            <li><a href="#south_zone">SOUTH REGION</a></li>
                            <li><a href="#west_zone">WEST REGION</a></li>
                        </ul>

                        <ul class="pobc_tab__content tab__content kalpesh">



                        
                                <div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>EAST    REGIONAL OFFICE</span>
    <p><span class="bl_clr"> Location Code : </span> ERO</p>
    <p><span class="bl_clr"> Branch Type : </span> Regional office</p>
    <p><span class="bl_clr"> Add : </span>B12, CHINAR    PARK, KOLKATA - 700 157</p>
    <p><span class="bl_clr"> Tel NO. : 033-25160330 / 46000517</span></p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>CAMAC STREET</span>
    <p><span class="bl_clr"> location Code : </span>CCU-CSO</p>
    <p><span class="bl_clr"> Branch Type : </span>Own Branch</p>
    <p><span class="bl_clr"> Add. : </span> 8 CAMAC STREET , SHANTINIKETAN    BUILDING, ROOM NO - 65 KOLKATA - 700 017</p>
    <p><span class="bl_clr"> Tel No. : </span>033-24551979</p>
    <p><span class="bl_clr"> Mob No. : </span>9619983950</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>BARABAZAR</span>
    <p><span class="bl_clr"> Add. : </span>CCU-BBO</p>
    <p><span class="bl_clr"> Branch Type : </span>Franchisee Branch</p>
    <p><span class="bl_clr"> Add. : </span> 161/1, ROOM NO 12A, BANGUR BUILDING</p>
    <p><span class="bl_clr"> Tel. No. : </span> 033-22712169 / 22738658</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>SILIGURI</span>
    <p><span class="bl_clr"> Location Code : </span>SIL</p>
    <p><span class="bl_clr"> Branch Type : </span>Franchisee Branch</p>
    <p><span class="bl_clr"> Add. : </span> ASHA PURNA SARANI, PRODHAN NAGAR</p>
    <p><span class="bl_clr"> Mob. No. : </span> 9093017141/9093974490</p>
  </div>
 
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>GUWAHATI</span>
    <p><span class="bl_clr"> Location Code : </span> GAU</p>
    <p><span class="bl_clr"> Branch Type : </span> Franchisee Branch</p>
    <p><span class="bl_clr"> Add. : </span> HOUSE NO 9, PALTAN BAZAR, A K AZAD    ROAD, NEAR ST JOHN SCHOOL, REHABARI, GUWAHATI - 781 008, ASSAM</p>
    <p><span class="bl_clr"> Mob. No. : </span> 9331006674 / 9830124919</p>
  </div>
  
  
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>IMPHAL</span>
    <p><span class="bl_clr"> Location Code : </span>IMF</p>
    <p><span class="bl_clr"> Branch Type : </span>Franchisee Branch</p>
    <p><span class="bl_clr">Add.</span>CHANGANGEI,OPP TULIHAL AIRPORT  , KHUNDRAKPAM BUILDING, IMPHAL WEST, PS LAMPHEL MANIPUR - 795140</p>
    <p><span class="bl_clr">Mob No.</span>8257004700</p>
  </div>
  
  
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>BHUBANESWAR</span>
    <p><span class="bl_clr">Location COde</span>BBI</p>
    <p><span class="bl_clr">Branch Type</span>Franchisee Branch</p>
    <p><span class="bl_clr">Add. :</span>10 KHARVEL NAGAR, STATION    SQUARE, BHUBANESWAR - 751 001 </p>
    <p><span class="bl_clr">Tel No.</span>0674-2534829</p>
    <p><span class="bl_clr">Mob. No.</span>9437035669</p>
  </div>
  
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>CUTTACK</span>
    <p><span class="bl_clr"> Location Code : </span>CTK</p>
    <p><span class="bl_clr"> Branch Type : </span>Franchisee Branch</p>
    <p><span class="bl_clr">Add. : </span>KAMALPUR, NEAR PALA MANDIR BADAMBARI, CUTTACK</p>
    <p><span class="bl_clr"> Tel No. : </span> 0674-2534829</p>
    <p><span class="bl_clr"> Mob. No. : </span>9437035669</p>
  </div>
  
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>AGARTALA</span>
    <p><span class="bl_clr"> Add. : </span>IXA</p>
    <p><span class="bl_clr"> Branch Type : </span>Franchisee Branch</p>
    <p><span class="bl_clr">Add. </span>DISTRICT - WEST TRIPURA, PS -    AGARTALA WEST, MOUZA - AGRTALA SIT NO - 16, PGS -    AGARTALA</p>
    <p><span class="bl_clr">Mob. No. :</span> 9862978337/8337090385</p>
  </div>
                                </div>
                                
                                <div>
                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> NORTH REGIONAL OFFICE</span>
    <p> <span class="bl_clr"> Location Code :</span>  NRO</p>
    <p><span class="bl_clr"> Branch Type : </span> Regional Office</p>
    <p> <span class="bl_clr"> Contact Person :</span> Mr. Anil Kumar Tiwari  - Regional Manager - 7290054865 / Mr. Ratanlal Goel - Regional Finance Manager - 7290054846 / Mr. Kuldeep Singh Rawat - Executive IT - 8750724730</p>
    <p><span class="bl_clr"> Add :</span> 251 - F,  A Block,  Extn Road No.6, Near Hotel OM Place, 2nd Floor, Mahipalpur, New Delhi - 110 037  (DL)</p>
    <p><span class="bl_clr"> Tel. No. : </span>  011 -26783987 (Dir) / 011 -26786762 / 26783982 </p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:anilkumart@patel-india.com"> anilkumart@patel-india.com  </a> /  <a href="mailto:r_goel@patel-india.com"> r_goel@patel-india.com </a> / <a href="mailto:k_rawat@patel-india.com">k_rawat@patel-india.com </a></p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>DELHI HUB DL-HU</span>
    <p><span class="bl_clr"> Location Code :</span> NRO</p>
    <p><span class="bl_clr"> Branch Type :</span> Hub</p>
    <p><span class="bl_clr"> Contact person :</span> Mr. Anupam Srivastava - Regional Operations Manager -  shift Operations Customer Service Executives </p>
    <p><span class="bl_clr"> Add :</span> 251 - F,  A Block,  Extn Road No.6, Near Hotel OM Place, Ground Floor Mahipalpur, New Delhi - 110 037  (DL)</p>
     <p><span class="bl_clr"> Tel. No. :</span>7290054863 / 7290080518 / 011 - 26786725</p>
    <p><span class="bl_clr"> Mob. No. :</span>7290054861 / 7290054862</p>
    <p><span class="bl_clr"> Email Id : </span> <a href="mailto:a_srivastava@patel-india.com"> a_srivastava@patel-india.com </a> /  <a href="mailto:pobc_del_ops@patel-india.com"> pobc_del_ops@patel-india.com </a> / <a href="mailto:cs_del@patel-india.com">cs_del@patel-india.com </a></p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PAHARGANJ
(DEL-PGO)</span>
    <p><span class="bl_clr"> Location Code :</span> DEL - PGO
</p>
    <p><span class="bl_clr"> Branch Type :</span> Booking Branch
</p>
    <p><span class="bl_clr"> Contact person :</span> Mr. Dahiya - 9971173500 / Mr. Diwan Singh - 9311972198 / Ms. Nitika - 7859863434 / 7065588441 / 443</p>
    <p><span class="bl_clr"> Add :</span> G - 51, Vardhman Diamond Plaza, Motiya Khan, D. B. Gupta Road, Paharganj, New Delhi - 110 055  (DL) </p>
     <p><span class="bl_clr"> Tel. No. :</span>011 - 23535565</p>
    <p><span class="bl_clr"> Email id :</span> <a href="mailto:cs_del@patel-india.com">cs_del@patel-india.com </a> / <a href="mailto:paharganj_fr_pobc@patel-india.com">paharganj_fr_pobc@patel-india.com </a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>CHANDNI CHOWK (DEL-CCO)</span>
    <p><span class="bl_clr"> Location Code :</span> DEL - CCO</p>
    <p><span class="bl_clr"> Branch Type :</span> Booking Branch
</p>
    <p><span class="bl_clr"> Contact person :</span> Mr. Dahiya - 9971173500 / Mr.Ranvir Kumar / Kamal Goshwami</p>
    <p><span class="bl_clr"> Add :</span> Shop No. 4900, Kucha Ustada Dag, Chandni Chowk, New Delhi - 110 006  (DL)</p>
     <p><span class="bl_clr"> Tel. No. :</span>011 - 23973486</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>NEHRU PLACE(DEL-NPO)</span>
    <p><span class="bl_clr"> Location Code :</span> DEL - NPO</p>
    <p><span class="bl_clr"> Branch Type :</span> Booking Branch</p>
    <p><span class="bl_clr"> Contact person :</span> Mr. Pratap Kumar Beura - 9313105163 / 9810757027</p>
    <p><span class="bl_clr"> Add :</span> B - 46, Basement Kalkaji, New Delhi - 110019  (DL)</p>
    <p><span class="bl_clr"> Tel. No. :</span>011 - 26235710  /  9871870276</p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:nehruplace_ba_pobc@patel-india.com">  nehruplace_ba_pobc@patel-india.com </a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>CONNAUGHT PLACE(DEL-CPO)</span>
    <p><span class="bl_clr"> Location Code :</span> DEL - CPO</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Sanjeev Thakral - 9811813556 / 9891655870</p>
    <p><span class="bl_clr"> Add :</span> SHOP no # 40 shankar market near super bazar opposite mayur bhavan Connaught Place, New Delhi - 110 001  (DL)</p>
    <p><span class="bl_clr"> Tel. No. :</span>011 - 65151335 / 23414708 / 01123414708</p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:connaught_fr_pobc@patel-india.com">  connaught_fr_pobc@patel-india.com </a> </p>
  </div>
  
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>JAIPUR(JAI)</span>
    <p><span class="bl_clr"> Location Code :</span> JAI</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Siddharth Shukla - 9335299749 / 9935359749 / Mr. Ajeet - 9839068473</p>
    <p><span class="bl_clr"> Add :</span> E -1, Kanti Chandra Road, Bani Park, Jaipur - 302 016, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Tel. No. :</span> 0141 - 4010339  /  9214444620 </p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:cust_ser_jai@patel-india.com">  Email : cust_ser_jai@patel-india.com</a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>JODHPUR(JDH)</span>
    <p><span class="bl_clr"> Location Code :</span> JDH</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Durgesh Sodani - 9314204616 </p>
    <p><span class="bl_clr"> Add :</span> Shop No. 25, Gulab Bhavan, Chopasni Road, Jodhpur - 342 001, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Tel. No. :</span> 0291 - 2622962 </p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:jdh_ba@patel-india.com">  Email : jdh_ba@patel-india.com</a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PALI (PLI)</span>
    <p><span class="bl_clr"> Location Code :</span> PLI</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Durgesh Sodani - 9314204616 </p>
    <p><span class="bl_clr"> Add :</span> Shop No. 109, Jangiwada, Pali - 306 401, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Tel. No. :</span> 0291 - 2622962 </p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:jdh_ba@patel-india.com">  Email : jdh_ba@patel-india.com</a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PALI (PLI)</span>
    <p><span class="bl_clr"> Location Code :</span> PLI</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Durgesh Sodani - 9314204616 </p>
    <p><span class="bl_clr"> Add :</span> Shop No. 109, Jangiwada, Pali - 306 401, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Tel. No. :</span> 0291 - 2622962 </p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:jdh_ba@patel-india.com">  Email : jdh_ba@patel-india.com</a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>BHILWARA (BHL)</span>
    <p><span class="bl_clr"> Location Code :</span> BHL</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Vijay Sodani - 9214485455 / 9351658355 </p>
    <p><span class="bl_clr"> Add :</span> Shop No. 3, Khabya Market, LNT Road, Bhilwara - 311 001, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:v_sodani@patel-india.com">  Email : v_sodani@patel-india.com</a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>BHILWARA (BHL)</span>
    <p><span class="bl_clr"> Location Code :</span> BHL</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Vijay Sodani - 9214485455 / 9351658355 </p>
    <p><span class="bl_clr"> Add :</span> Shop No. 3, Khabya Market, LNT Road, Bhilwara - 311 001, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:v_sodani@patel-india.com">  Email : v_sodani@patel-india.com</a> </p>
  </div>
  
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>AJMER   (AJM)</span>
    <p><span class="bl_clr"> Location Code :</span> AJM</p>
    <p><span class="bl_clr"> Branch Type :</span> Delivery Branch</p>
    <p><span class="bl_clr"> Contact person :</span>DELIVERY POINT - 9335299749 / 9352077700 </p>
    
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>AMBALA (ABA)</span>
    <p><span class="bl_clr"> Location Code :</span> ABA</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Subodh Balodi - 9315438016 / Mr. Rajesh Balodi - 8950092448 / 9355223891 </p>
     <p><span class="bl_clr"> Add :</span>Shop No.111,  Railway Road,
Near Football Chowk,
Ambala Cantt - 133 001 ,
Haryana  (HR)</p>
<p><span class="bl_clr"> Tel. No. :</span>0171 - 4003945</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto:cs_ambala@patel-india.com"> cs_ambala@patel-india.com  </a> / <a href="mailto:paf_fr_ambala@patel-india.com"> paf_fr_ambala@patel-india.com </a> </p>

  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PANIPAT (PNP)</span>
    <p><span class="bl_clr"> Location Code :</span> PNP</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Dhrambir Singh - 9068763876 / 70156-49321 /9017822337 </p>
     <p><span class="bl_clr"> Add :</span>Shop # 33, BMK Market Behind Hive Hotel G.T.Road
Panipat - 132 103,  Haryana  (HR)
</p>
<p><span class="bl_clr"> Tel. No. :</span>0171 - 4003945</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: pnp_ba@patel-india.com">  pnp_ba@patel-india.com  </a> </p>

  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>LUDHIANA (LUH)</span>
    <p><span class="bl_clr"> Location Code :</span> LUH</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Kanwal Jit Singh  - 9316818372 / 8146005400 </p>
     <p><span class="bl_clr"> Add :</span>820,  Industrial  Aera - B,  
Near Allahabad Bank  Partap Chowk 
Ludhiana - 141 003
Punjab  (PB)

</p>
<p><span class="bl_clr"> Tel. No. :</span> 0161 - 4638372</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: cs_lud_paf@patel-india.com">  cs_lud_paf@patel-india.com </a> / <a href="mailto: ludhiana_ba@patel-india.com">  ludhiana_ba@patel-india.com </a></p>

  </div>
  
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>JALANDHAR
(QJU)
</span>
    <p><span class="bl_clr"> Location Code :</span> QJU
</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Manjit singh  - 9780335518 </p>
     <p><span class="bl_clr"> Add :</span>Hotel Diamond Basement SCO.1 SARDAR COMPLEX ,
NEHRU GARDEN ROAD
JALANDHAR- 144 010
Punjab (PB)


</p>
<p><span class="bl_clr"> Tel. No. :</span> 0161 - 4638372</p>
<p><span class="bl_clr"> Mob. No. :</span>9316818372 / 8146005400</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: cs_lud_paf@patel-india.com">  cs_lud_paf@patel-india.com </a> / <a href="mailto: ludhiana_ba@patel-india.com">  ludhiana_ba@patel-india.com </a></p>

  </div>
 
 
 <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PHAGBARA
(PHG)

(QJU)
</span>
    <p><span class="bl_clr"> Location Code :</span> QJU
</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Kamal  - 9356609746 </p>
     <p><span class="bl_clr"> Add :</span>KHALSA SCHOOL BUILDING.
MODEL TOWN
PHAGWARA.-144 001
Punjab (PB)


</p>
<p><span class="bl_clr"> Tel. No. :</span> 0161 - 4638372</p>
<p><span class="bl_clr"> Mob. No. :</span>9316818372 / 8146005400</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: cs_lud_paf@patel-india.com">  cs_lud_paf@patel-india.com </a> / <a href="mailto: ludhiana_ba@patel-india.com">  ludhiana_ba@patel-india.com </a></p>

  </div>

<div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>AMRITSAR
(ATQ)

</span>
    <p><span class="bl_clr"> Location Code :</span> ATQ

</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Paramjeet Singh  - 9356132613 </p>
     <p><span class="bl_clr"> Add :</span>KHALSA SCHOOL BUILDING.
MODEL TOWN
PHAGWARA.-144 001
Punjab (PB)


</p>
<p><span class="bl_clr"> Tel. No. :</span> 0183 - 5050136</p>
<p><span class="bl_clr"> Mob. No. :</span>9815576872</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: atq_ba@patel-india.com">  atq_ba@patel-india.com </a></p>

  </div>
  
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>CHANDIGARH
(IXC)


</span>
    <p><span class="bl_clr"> Location Code :</span> IXC


</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mrs. Taruna Ahuja / Mr. Rajeev Kumar  - 9356132613 </p>
     <p><span class="bl_clr"> Add :</span>SCO 1092 & 1093, Cabin No.5,
Sector 22 B, 1st Floor,
Opp. Bus Stand, 
Chandigarh - 160 022  (CH)



</p>
<p><span class="bl_clr"> Tel. No. : 0172 - 5026842</span>  </p>
<p><span class="bl_clr"> Mob. No. :</span>9217856842 / 9988837645</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: ixc_ba@patel-india.com">  ixc_ba@patel-india.com </a></p>

  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>BADDI



</span>
    <p><span class="bl_clr"> Location Code :</span> BDI



</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mrs. Taruna Ahuja / Mr. Rajeev Kumar  - 9217856842 </p>
     <p><span class="bl_clr"> Add :</span>G - 7, City Square Mall
Sai Road, Baddi
Himachal Pradesh (HP)




</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: ixc_ba@patel-india.com">  ixc_ba@patel-india.com </a></p>

  </div>
  
  
   <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>JAMMU(IXJ)</span>
    <p><span class="bl_clr"> Location Code :</span> IXJ</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Ved Pal Gandotra  - 9419187580 </p>
     <p><span class="bl_clr"> Add :</span>Panama Chowk, Near Raghu Garage, Near Railway Station,  Jammu - 180 004 Jammu & Kashmir  (JK) </p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: baixj@patel-india.com"> baixj@patel-india.com </a></p>

  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>LUCKNOW (LKO)</span>
    <p><span class="bl_clr"> Location Code :</span> LKO</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Siddharth Shukla  - 9335299749 </p>
     <p><span class="bl_clr"> Add :</span>Shop No. GF - 1 & 2,
P. L. Complex, 12/66 Cantt Road,
Murli Nagar, Lucknow - 226 001
Uttar Pradesh  (UP)
 </p>
 <p><span class="bl_clr"> Tel No. :</span> 0522 - 4105573</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: baixj@patel-india.com"> pobc_lko@patel-india.com </a></p>

  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>LUCKNOW (LKO)</span>
    <p><span class="bl_clr"> Location Code :</span> LKO</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Siddharth Shukla  - 9335299749 </p>
     <p><span class="bl_clr"> Add :</span>Shop No. GF - 1 & 2,
P. L. Complex, 12/66 Cantt Road,
Murli Nagar, Lucknow - 226 001
Uttar Pradesh  (UP)
 </p>
 <p><span class="bl_clr"> Tel No. :</span> 0522 - 4105573</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: pobc_lko@patel-india.com"> pobc_lko@patel-india.com </a></p>

  </div>
  
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>GORAKHPUR
(GOP)
</span>
    <p><span class="bl_clr"> Location Code :</span> GOP
</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr Sushil  - 9889331467 / 9335299749 </p>
     <p><span class="bl_clr"> Add :</span>C/127/163 Dilzadpur Near Meenka Cinema  Gorakhpur ,  Uttar Pradesh  (UP)

 </p>
 <p><span class="bl_clr"> Tel No. :</span> 0522 - 4105573</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: pobc_lko@patel-india.com"> pobc_lko@patel-india.com </a></p>

  </div>
  
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>MATHURA
(MAT)


</span>
    <p><span class="bl_clr"> Location Code :</span> MAT

</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Siddharth Shukla  - 9335299749 / 8923510294 / 7906758145 / Mr. Sarthak - 8126914083 </p>
     <p><span class="bl_clr"> Add :</span>21,Briz Nagar enclave clony,sonkh adda
Junction roar,Mathura
Mathura  (UP)


 </p>
 <p><span class="bl_clr"> Mob No. :</span> 7037152033</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: arga_ba@patel-india.com"> arga_ba@patel-india.com</a></p>

  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PATNA(PAT)</span>
    <p><span class="bl_clr"> Location Code :</span> PAT

</p>
    <p><span class="bl_clr"> Branch Type :</span> Own Branch
</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Wakeel Ahmed  - 9334313971 / 7484848201 </p>
     <p><span class="bl_clr"> Add :</span>Hotel Swagatam, Near Petrol Pump,
213/A, Plot No.722, Ground Floor,
East Exhibition Road,
Patna - 800 001,  Bihar  (BR)
</p>

<p><span class="bl_clr"> Email Id :</span> <a href="mailto: wakeel.ahmed@patel-india.com"> wakeel.ahmed@patel-india.com</a></p>

  </div>
  
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>RANCHI
(IXR)</span>
    <p><span class="bl_clr"> Location Code :</span> IXR


</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch

</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Vivek Bhasin  - 9835151510 / Mr. Ajay - Mr. Ajeet - 7677103316 / 7677103315 </p>
     <p><span class="bl_clr"> Add :</span>Plot No. 2002/F,
East of Play Ground,
Harmu Housing Colony,
Ranchi - 834 002, Jharkhand  (JH)

</p>
<p><span class="bl_clr"> Tel No. :</span> 0651 - 2245913</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: wakeel.ahmed@patel-india.com"> wakeel.ahmed@patel-india.com</a></p>

  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> GWALIOR
(GWL)
 </span>
    <p><span class="bl_clr"> Location Code :</span> GWL



</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch

</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. M. S. Panwar  - 7771007120 / 9301234577 / 8818818882 </p>
     <p><span class="bl_clr"> Add :</span>Plot No. 670, Ground Floor,
Mata Wali Gali, Sinde Ki Chawani,
Gwalior  - 474 009,  
Madhya Pradesh  (MP)


</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: wakeel.ahmed@patel-india.com"> mspanwar@patel-india.com</a> / <a href="mailto: cust_ser_gwl@patel-india.com"> cust_ser_gwl@patel-india.com</a></p>

  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> JHANSI
(JNI)

 </span>
    <p><span class="bl_clr"> Location Code :</span> JNI




</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch


</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Virendra Kumar Savita  -  9336621212 / 8423399589</p>
     <p><span class="bl_clr"> Add :</span>Side 539, Chaman Ganj,  Sipri Bazar,
Jhansi - 248 003, Madhya Pradesh  (MP)

</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto:pobc_jhn@patel-india.com"> pobc_jhn@patel-india.com</a> </p>

  </div>
                                </div>


         

                                <div>
                                      <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>HYDERABAD</span>
    <p> <span class="bl_clr"> Location Code :</span> HYD</p>
    <p><span class="bl_clr"> Branch Type :</span> Own Branch</p>
    <p><span class="bl_clr"> Add :</span> H No    1-10-27/5/1, Prakash Nagar, Near Prakash Nagar Post Office, Begumpet,    Hyderabad - 500 016</p>
    <p><span class="bl_clr"> Tel. No. :</span>040-48500161</p>
    <p><span class="bl_clr"> Mob. No. :</span>9395536611 </p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>HYDERABAD</span>
    <p> <span class="bl_clr"> Location Code :</span> HYD</p>
    <p><span class="bl_clr"> Branch Type :</span>Own    Branch</p>
    <p><span class="bl_clr"> Add :</span>No    - 4-6-137 to 140, Pan Bazar, Secunderabad - 500 003</p>
    <p><span class="bl_clr"> Tel. No. :</span>040-65641164</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>GUNTUR</span>
    <p><span class="bl_clr"> Location Code :</span> GNT</p>
    <p><span class="bl_clr"> Branch Type :</span>Franchisee    Branch</p>
    <p><span class="bl_clr"> Add :</span>12/1    A, Arandalpet, 6th Cross Road,Guntur, Andhra Pradesh - 522 002</p>
    <p><span class="bl_clr"> Mob. No. : </span> 9246489077 / 9347376272</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>VIJAYAWADA</span>
    <p> <span class="bl_clr"> Location Code :</span> VGA</p>
    <p><span class="bl_clr"> Branch Type :</span>Franchisee    Branch</p>
    <p><span class="bl_clr"> Add :</span>H.No. 14-9-11,Kotal Naga Bhushanan Street,Shankermutt Complex, Hanuman    Pet,  Vijaywada, Andhra Pradesh- 520    003</p>
    <p><span class="bl_clr"> Tel No. :</span> 0866 - 2575376</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9347376272 / 9000323784</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>VISAKHAPATNAM</span>
    <p> <span class="bl_clr"> Location Code :</span> VTZ</p>
    <p><span class="bl_clr"> Branch Type :</span>Franchisee    Branch</p>
    <p><span class="bl_clr"> Add :</span>Door No. 43-15/12/2, Subalaxmi Nagar, Railway New Colony, Visakhapatnam, Andhra Pradesh - 530 016</p>
    <p><span class="bl_clr"> Tel. No. :</span> 0891 - 2541327</p>
    <p><span class="bl_clr"> Mob. No. :</span> 9100265757 / 9393554649 / 9347376272 </p>
  </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Chennai - Airport</span>
    <p> <span class="bl_clr"> Location Code :</span> MAA</p>
    <p> <span class="bl_clr"> Branch Type :</span> Regional Office</p>
    <p> <span class="bl_clr"> Add :</span> New No.185, Old    No. 35, Medavakkam Main Road, Adambakkam, Chennai - 600 088.</p>
    <p> <span class="bl_clr"> Tel. No. :</span> 044-23461011 / 12 / 13 / 14</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Egmore Office</span>
    <p> <span class="bl_clr"> Location Code :</span> MAA - EGO</p>
    <p> <span class="bl_clr"> Branch Type :</span> Own Branch</p>
    <p> <span class="bl_clr"> Add :</span>No. 84/1, Poosala Gengu Reddy Street, Egmore, Chennai 600 008.</p>
    <p> <span class="bl_clr"> Tel. No. :</span> 044-23461010</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Parsn Office</span>
    <p> <span class="bl_clr"> Location Code :</span> MAA - PSO</p>
    <p> <span class="bl_clr"> Branch Type :</span> Own Branch</p>
    <p> <span class="bl_clr"> Add :</span> A-11,    Basement Floor, Gemini Parsn Commercial Complex, No.1, Kodambakkam High Road,    Chennai - 600 006.</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Parrys Office</span>
    <p> <span class="bl_clr"> Location Code :</span> MAA - PRS</p>
    <p> <span class="bl_clr"> Branch Type :</span> Own Branch</p>
    <p> <span class="bl_clr"> Add :</span> No.216, Rattan Bazaar, Chennai - 600 003.</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Salem - HUB</span>
    <p> <span class="bl_clr"> Location Code :</span> SLM - HUB</p>
    <p> <span class="bl_clr"> Branch Type :</span> Hub</p>
    <p> <span class="bl_clr"> Add :</span> PMC Complex, No.134, Post Office Road, Suramangalam, Salem - 636005.</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Salem</span>
    <p> <span class="bl_clr"> Location Code :</span> SLM</p>
    <p> <span class="bl_clr"> Branch Type :</span> Own Branch</p>
    <p> <span class="bl_clr"> Add :</span> No.67-B,    Khadar Kan Street, 2nd Floor. Rathna Building, Opp Railway Junction, Suramangalam, Salem - 636005</p>
    <p> <span class="bl_clr"> Tel. No. :</span> 0427-2446604  / 9362103126</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Central TN Office, Coimbatore</span>
    <p> <span class="bl_clr"> Location Code :</span> CTN - AO</p>
    <p> <span class="bl_clr"> Branch Type :</span> Area Office</p>
    <p> <span class="bl_clr"> Add :</span> 34,    Anna Nagar, Opp OM Sakthi Temple, Sowripalayam Pirivu, Ramanathapuram,    Coimbatore - 641045</p>
    <p><span class="bl_clr"> Tel. No. :</span> 0422 - 4396394, 4950814.</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Coimbatore</span>
    <p> <span class="bl_clr"> Location Code :</span> CJB - HUB</p>
    <p> <span class="bl_clr"> Branch Type :</span> Hub</p>
    <p> <span class="bl_clr"> Add :</span> No    : 4B, Alagu Nagar, Civil Aerodrome Post, Coimbatore - 641014</p>
    <p><span class="bl_clr"> Tel. No. :</span>  9363121090, 0422 - 4396394</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Andhra Pradesh Area Office</span>
    <p> <span class="bl_clr"> Location Code :</span> AP - AO</p>
    <p> <span class="bl_clr"> Branch Type :</span> Area Office</p>
    <p> <span class="bl_clr"> Add :</span> H    No 1-10-27/5/1, Prakash Nagar, Near Prakash Nagar Post Office, Begumpet,    Hyderabad - 500 016</p>
    <p> <span class="bl_clr"> Tel. No. :</span> 040-48500161</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Pan Bazar (HYD PBO)</span>
    <p> <span class="bl_clr"> Location Code :</span> HYD - PBO</p>
    <p> <span class="bl_clr"> Branch Type :</span> Own Branch</p>
    <p> <span class="bl_clr"> Add :</span> No - 4-6-137 to 140, Pan Bazar, Secunderabad - 500 003</p>
    <p> <span class="bl_clr"> Tel. No. :</span> 040-65641164</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Karnataka Area Office </span>
    <p> <span class="bl_clr"> Location Code :</span> KA - AO</p>
    <p> <span class="bl_clr"> Branch Type :</span> Area Office</p>
    <p> <span class="bl_clr"> Add :</span> No 2, New Mission Road  (opp to Mahavir Jain college,Behind Total Gas Bunk) Bangalore-560 027</p>
    <p> <span class="bl_clr"> Tel. No. :</span> 080-22222794</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Chickpet (BLR - CKT)</span>
    <p> <span class="bl_clr"> Location Code :</span> BLR CKT</p>
    <p> <span class="bl_clr"> Branch Type :</span> Own Branch</p>
    <p> <span class="bl_clr"> Add :</span> No  33,3rd Cross NMC Lane,Rangaswamy Temple Street Bangalore-560 053</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Pondicherry </span>
    <p> <span class="bl_clr"> Location Code :</span> PNY</p>
    <p> <span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p> <span class="bl_clr"> Add :</span> Swift    Express, 4, Ayyanar Koil Street, Ayyanar Nagar, Pondicherry - 605 013 (PY)</p>
    <p> <span class="bl_clr"> Tel. No. :</span> 0413    - 2243491</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Madurai(IXM)</span>
    <p> <span class="bl_clr"> Location Code :</span> IXM</p>
    <p> <span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p> <span class="bl_clr"> Add :</span> 2,    2A, 1st Floor, West Perumal Maistry Street, Madurai - 625 001,</p>
    <p> <span class="bl_clr"> Tel. No. :</span> 0452    - 4381618 / 2344827</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Trichy (TRZ)</span>
    <p> <span class="bl_clr"> Location Code :</span> TRZ</p>
    <p> <span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p> <span class="bl_clr"> Add :</span> No.6/1,    10 Birds Road, Cantontment, Trichy - 620 001,</p>
    <p> <span class="bl_clr"> Tel. No. :</span> 0431    - 4000622</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Tirupur </span>
    <p> <span class="bl_clr"> Location Code :</span> TRP</p>
    <p> <span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p> <span class="bl_clr"> Add :</span> 22    Rayapandaram Street, Avinashi Road, Tirupur - 641 602</p>
    <p> <span class="bl_clr"> Tel. No. :</span> 9894022461, 0421-2246160</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Karur (KRR) </span>
    <p> <span class="bl_clr"> Location Code :</span> KRR</p>
    <p> <span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p>  <span class="bl_clr"> Add :</span> No.    30, Sengundapuram, 7th Cross Street, Karur,</p>
    <p> <span class="bl_clr"> Tel. No. :</span> 04324    - 234246</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Erode (EDE) </span>
    <p> <span class="bl_clr"> Location Code :</span> EDE</p>
    <p> <span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p> <span class="bl_clr"> Add :</span> No.    844, Park Road, Erode - 638 003,</p>
    <p> <span class="bl_clr"> Tel. No. :</span> 0424-223970</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Guntur </span>
    <p> <span class="bl_clr"> Location Code :</span> GNT</p>
    <p> <span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p> <span class="bl_clr"> Add :</span> 12/1    A, Arandalpet, 6th Cross Road,Guntur, Andhra Pradesh - 522 002</p>
    <p> <span class="bl_clr"> Tel. No. :</span> 9246489077,    9347376272</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Vijayawada </span>
    <p> <span class="bl_clr"> Location Code :</span> VGA</p>
    <p> <span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p>  <span class="bl_clr"> Add :</span> H. No. 14-9-11,Kotal Naga Bhushanan Street,Shankermutt Complex, Hanuman    Pet,  Vijaywada, Andhra Pradesh- 520    003</p>
    <p> <span class="bl_clr"> Tel. No. :</span> 0866 - 2575376</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

     <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Vishakapatnam </span>
    <p> <span class="bl_clr"> Location Code :</span> VTZ</p>
    <p> <span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p> <span class="bl_clr"> Add :</span> Door    No. 43-15/12/2, Subalaxmi Nagar, Railway New Colony, Visakhapatnam, Andhra    Pradesh - 530 016</p>
    <p> <span class="bl_clr"> Tel. No. :</span> 0891 - 2541327</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i> Mysore </span>
    <p> <span class="bl_clr"> Location Code :</span> MYQ</p>
    <p> <span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p> <span class="bl_clr"> Add :</span> M/s. S. S. Associates, 1466 Narayana Sashtri Road, Opp. Udupi Krishna Mandir,    Devaraja Mohalla, Mysore - 570 001, Karnataka (KA)</p>
    <p> <span class="bl_clr"> Tel. No. :</span> 0821 - 4245963</p>
  </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Calicut</span>
    <p> <span class="bl_clr"> Location Code :</span>CCJ</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Add :</span> 17/1463 Room No.A1, Alameen Building, Rammohan Road, Chinthavalappu, Puthiyar P.O., Calicut 673004</p>
    <p><span class="bl_clr">Tel. No.</span>0495-2727402 / 4015705</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Cannanore</span>
    <p> <span class="bl_clr"> Location Code :</span>CNR</p>
    <p><span class="bl_clr"> Branch Type :</span>Franchisee Branch</p>
    <p><span class="bl_clr"> Add :</span>Elite Building, CW2795 E2, Netaji Road, Kannur 670001</p>
    <p><span class="bl_clr">Mob. No.</span> 9349155705</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Kerala    Area Office</span>
    <p><span class="bl_clr"> Location Code :</span>KL-AO</p>
    <p><span class="bl_clr"> Branch Type :</span>Area Office</p>
    <p><span class="bl_clr"> Add :</span><span class="bl_clr"> Add :</span>43/1458 A1, ST.Bendict Square, ST.Bendict Road, Cochin-682018.</p>
    <p><span class="bl_clr">Tel. No.</span> 0484-2393590,2395732</p>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">

    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>Trivandrum</span>
    <p><span class="bl_clr"> Location Code :</span>TRV</p>
    <p><span class="bl_clr"> Branch Type :</span>Own Branch</p>
    <p><span class="bl_clr"> Add :</span>No.23/334, Kochar Road, Chalai, Trivandrum-695036,</p>
    <p><span class="bl_clr">Tel. No.</span>0471-2476194</p>
  </div>



                                </div>

               

                   

                                <div>
                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>CHANDNI CHOWK (DEL-CCO)</span>
    <p><span class="bl_clr"> Location Code :</span> DEL - CCO</p>
    <p><span class="bl_clr"> Branch Type :</span> Booking Branch
</p>
    <p><span class="bl_clr"> Contact person :</span> Mr. Dahiya - 9971173500 / Mr.Ranvir Kumar / Kamal Goshwami</p>
    <p><span class="bl_clr"> Add :</span> Shop No. 4900, Kucha Ustada Dag, Chandni Chowk, New Delhi - 110 006  (DL)</p>
     <p><span class="bl_clr"> Tel. No. :</span>011 - 23973486</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>NEHRU PLACE(DEL-NPO)</span>
    <p><span class="bl_clr"> Location Code :</span> DEL - NPO</p>
    <p><span class="bl_clr"> Branch Type :</span> Booking Branch</p>
    <p><span class="bl_clr"> Contact person :</span> Mr. Pratap Kumar Beura - 9313105163 / 9810757027</p>
    <p><span class="bl_clr"> Add :</span> B - 46, Basement Kalkaji, New Delhi - 110019  (DL)</p>
    <p><span class="bl_clr"> Tel. No. :</span>011 - 26235710  /  9871870276</p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:nehruplace_ba_pobc@patel-india.com">  nehruplace_ba_pobc@patel-india.com </a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>CONNAUGHT PLACE(DEL-CPO)</span>
    <p><span class="bl_clr"> Location Code :</span> DEL - CPO</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Sanjeev Thakral - 9811813556 / 9891655870</p>
    <p><span class="bl_clr"> Add :</span> SHOP no # 40 shankar market near super bazar opposite mayur bhavan Connaught Place, New Delhi - 110 001  (DL)</p>
    <p><span class="bl_clr"> Tel. No. :</span>011 - 65151335 / 23414708 / 01123414708</p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:connaught_fr_pobc@patel-india.com">  connaught_fr_pobc@patel-india.com </a> </p>
  </div>
  
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>JAIPUR(JAI)</span>
    <p><span class="bl_clr"> Location Code :</span> JAI</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Siddharth Shukla - 9335299749 / 9935359749 / Mr. Ajeet - 9839068473</p>
    <p><span class="bl_clr"> Add :</span> E -1, Kanti Chandra Road, Bani Park, Jaipur - 302 016, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Tel. No. :</span> 0141 - 4010339  /  9214444620 </p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:cust_ser_jai@patel-india.com">  Email : cust_ser_jai@patel-india.com</a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>JODHPUR(JDH)</span>
    <p><span class="bl_clr"> Location Code :</span> JDH</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Durgesh Sodani - 9314204616 </p>
    <p><span class="bl_clr"> Add :</span> Shop No. 25, Gulab Bhavan, Chopasni Road, Jodhpur - 342 001, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Tel. No. :</span> 0291 - 2622962 </p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:jdh_ba@patel-india.com">  Email : jdh_ba@patel-india.com</a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PALI (PLI)</span>
    <p><span class="bl_clr"> Location Code :</span> PLI</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Durgesh Sodani - 9314204616 </p>
    <p><span class="bl_clr"> Add :</span> Shop No. 109, Jangiwada, Pali - 306 401, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Tel. No. :</span> 0291 - 2622962 </p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:jdh_ba@patel-india.com">  Email : jdh_ba@patel-india.com</a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PALI (PLI)</span>
    <p><span class="bl_clr"> Location Code :</span> PLI</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Durgesh Sodani - 9314204616 </p>
    <p><span class="bl_clr"> Add :</span> Shop No. 109, Jangiwada, Pali - 306 401, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Tel. No. :</span> 0291 - 2622962 </p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:jdh_ba@patel-india.com">  Email : jdh_ba@patel-india.com</a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>BHILWARA (BHL)</span>
    <p><span class="bl_clr"> Location Code :</span> BHL</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Vijay Sodani - 9214485455 / 9351658355 </p>
    <p><span class="bl_clr"> Add :</span> Shop No. 3, Khabya Market, LNT Road, Bhilwara - 311 001, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:v_sodani@patel-india.com">  Email : v_sodani@patel-india.com</a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>BHILWARA (BHL)</span>
    <p><span class="bl_clr"> Location Code :</span> BHL</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Vijay Sodani - 9214485455 / 9351658355 </p>
    <p><span class="bl_clr"> Add :</span> Shop No. 3, Khabya Market, LNT Road, Bhilwara - 311 001, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:v_sodani@patel-india.com">  Email : v_sodani@patel-india.com</a> </p>
  </div>
  
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>AJMER   (AJM)</span>
    <p><span class="bl_clr"> Location Code :</span> AJM</p>
    <p><span class="bl_clr"> Branch Type :</span> Delivery Branch</p>
    <p><span class="bl_clr"> Contact person :</span>DELIVERY POINT - 9335299749 / 9352077700 </p>
    
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>AMBALA (ABA)</span>
    <p><span class="bl_clr"> Location Code :</span> ABA</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Subodh Balodi - 9315438016 / Mr. Rajesh Balodi - 8950092448 / 9355223891 </p>
     <p><span class="bl_clr"> Add :</span>Shop No.111,  Railway Road,
Near Football Chowk,
Ambala Cantt - 133 001 ,
Haryana  (HR)</p>
<p><span class="bl_clr"> Tel. No. :</span>0171 - 4003945</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto:cs_ambala@patel-india.com"> cs_ambala@patel-india.com  </a> / <a href="mailto:paf_fr_ambala@patel-india.com"> paf_fr_ambala@patel-india.com </a> </p>

  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PANIPAT (PNP)</span>
    <p><span class="bl_clr"> Location Code :</span> PNP</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Dhrambir Singh - 9068763876 / 70156-49321 /9017822337 </p>
     <p><span class="bl_clr"> Add :</span>Shop # 33, BMK Market Behind Hive Hotel G.T.Road
Panipat - 132 103,  Haryana  (HR)
</p>
<p><span class="bl_clr"> Tel. No. :</span>0171 - 4003945</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: pnp_ba@patel-india.com">  pnp_ba@patel-india.com  </a> </p>

  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>LUDHIANA (LUH)</span>
    <p><span class="bl_clr"> Location Code :</span> LUH</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Kanwal Jit Singh  - 9316818372 / 8146005400 </p>
     <p><span class="bl_clr"> Add :</span>820,  Industrial  Aera - B,  
Near Allahabad Bank  Partap Chowk 
Ludhiana - 141 003
Punjab  (PB)

</p>
<p><span class="bl_clr"> Tel. No. :</span> 0161 - 4638372</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: cs_lud_paf@patel-india.com">  cs_lud_paf@patel-india.com </a> / <a href="mailto: ludhiana_ba@patel-india.com">  ludhiana_ba@patel-india.com </a></p>

  </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>CHANDNI CHOWK (DEL-CCO)</span>
    <p><span class="bl_clr"> Location Code :</span> DEL - CCO</p>
    <p><span class="bl_clr"> Branch Type :</span> Booking Branch
</p>
    <p><span class="bl_clr"> Contact person :</span> Mr. Dahiya - 9971173500 / Mr.Ranvir Kumar / Kamal Goshwami</p>
    <p><span class="bl_clr"> Add :</span> Shop No. 4900, Kucha Ustada Dag, Chandni Chowk, New Delhi - 110 006  (DL)</p>
     <p><span class="bl_clr"> Tel. No. :</span>011 - 23973486</p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>NEHRU PLACE(DEL-NPO)</span>
    <p><span class="bl_clr"> Location Code :</span> DEL - NPO</p>
    <p><span class="bl_clr"> Branch Type :</span> Booking Branch</p>
    <p><span class="bl_clr"> Contact person :</span> Mr. Pratap Kumar Beura - 9313105163 / 9810757027</p>
    <p><span class="bl_clr"> Add :</span> B - 46, Basement Kalkaji, New Delhi - 110019  (DL)</p>
    <p><span class="bl_clr"> Tel. No. :</span>011 - 26235710  /  9871870276</p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:nehruplace_ba_pobc@patel-india.com">  nehruplace_ba_pobc@patel-india.com </a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>CONNAUGHT PLACE(DEL-CPO)</span>
    <p><span class="bl_clr"> Location Code :</span> DEL - CPO</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Sanjeev Thakral - 9811813556 / 9891655870</p>
    <p><span class="bl_clr"> Add :</span> SHOP no # 40 shankar market near super bazar opposite mayur bhavan Connaught Place, New Delhi - 110 001  (DL)</p>
    <p><span class="bl_clr"> Tel. No. :</span>011 - 65151335 / 23414708 / 01123414708</p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:connaught_fr_pobc@patel-india.com">  connaught_fr_pobc@patel-india.com </a> </p>
  </div>
  
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>JAIPUR(JAI)</span>
    <p><span class="bl_clr"> Location Code :</span> JAI</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Siddharth Shukla - 9335299749 / 9935359749 / Mr. Ajeet - 9839068473</p>
    <p><span class="bl_clr"> Add :</span> E -1, Kanti Chandra Road, Bani Park, Jaipur - 302 016, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Tel. No. :</span> 0141 - 4010339  /  9214444620 </p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:cust_ser_jai@patel-india.com">  Email : cust_ser_jai@patel-india.com</a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>JODHPUR(JDH)</span>
    <p><span class="bl_clr"> Location Code :</span> JDH</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Durgesh Sodani - 9314204616 </p>
    <p><span class="bl_clr"> Add :</span> Shop No. 25, Gulab Bhavan, Chopasni Road, Jodhpur - 342 001, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Tel. No. :</span> 0291 - 2622962 </p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:jdh_ba@patel-india.com">  Email : jdh_ba@patel-india.com</a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PALI (PLI)</span>
    <p><span class="bl_clr"> Location Code :</span> PLI</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Durgesh Sodani - 9314204616 </p>
    <p><span class="bl_clr"> Add :</span> Shop No. 109, Jangiwada, Pali - 306 401, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Tel. No. :</span> 0291 - 2622962 </p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:jdh_ba@patel-india.com">  Email : jdh_ba@patel-india.com</a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PALI (PLI)</span>
    <p><span class="bl_clr"> Location Code :</span> PLI</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Durgesh Sodani - 9314204616 </p>
    <p><span class="bl_clr"> Add :</span> Shop No. 109, Jangiwada, Pali - 306 401, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Tel. No. :</span> 0291 - 2622962 </p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:jdh_ba@patel-india.com">  Email : jdh_ba@patel-india.com</a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>BHILWARA (BHL)</span>
    <p><span class="bl_clr"> Location Code :</span> BHL</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Vijay Sodani - 9214485455 / 9351658355 </p>
    <p><span class="bl_clr"> Add :</span> Shop No. 3, Khabya Market, LNT Road, Bhilwara - 311 001, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:v_sodani@patel-india.com">  Email : v_sodani@patel-india.com</a> </p>
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>BHILWARA (BHL)</span>
    <p><span class="bl_clr"> Location Code :</span> BHL</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Vijay Sodani - 9214485455 / 9351658355 </p>
    <p><span class="bl_clr"> Add :</span> Shop No. 3, Khabya Market, LNT Road, Bhilwara - 311 001, Rajasthan  (RJ)</p>
    <p><span class="bl_clr"> Email Id :</span> <a href="mailto:v_sodani@patel-india.com">  Email : v_sodani@patel-india.com</a> </p>
  </div>
  
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>AJMER   (AJM)</span>
    <p><span class="bl_clr"> Location Code :</span> AJM</p>
    <p><span class="bl_clr"> Branch Type :</span> Delivery Branch</p>
    <p><span class="bl_clr"> Contact person :</span>DELIVERY POINT - 9335299749 / 9352077700 </p>
    
  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>AMBALA (ABA)</span>
    <p><span class="bl_clr"> Location Code :</span> ABA</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Subodh Balodi - 9315438016 / Mr. Rajesh Balodi - 8950092448 / 9355223891 </p>
     <p><span class="bl_clr"> Add :</span>Shop No.111,  Railway Road,
Near Football Chowk,
Ambala Cantt - 133 001 ,
Haryana  (HR)</p>
<p><span class="bl_clr"> Tel. No. :</span>0171 - 4003945</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto:cs_ambala@patel-india.com"> cs_ambala@patel-india.com  </a> / <a href="mailto:paf_fr_ambala@patel-india.com"> paf_fr_ambala@patel-india.com </a> </p>

  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>PANIPAT (PNP)</span>
    <p><span class="bl_clr"> Location Code :</span> PNP</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Dhrambir Singh - 9068763876 / 70156-49321 /9017822337 </p>
     <p><span class="bl_clr"> Add :</span>Shop # 33, BMK Market Behind Hive Hotel G.T.Road
Panipat - 132 103,  Haryana  (HR)
</p>
<p><span class="bl_clr"> Tel. No. :</span>0171 - 4003945</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: pnp_ba@patel-india.com">  pnp_ba@patel-india.com  </a> </p>

  </div>
  
  <div class="col-md-4 col-sm-6 col-xs-12 office_address_txt equate">
    <span class="bl_clr uppercase"> <i class="fa fa-map-marker" aria-hidden="true"></i>LUDHIANA (LUH)</span>
    <p><span class="bl_clr"> Location Code :</span> LUH</p>
    <p><span class="bl_clr"> Branch Type :</span> Franchisee Branch</p>
    <p><span class="bl_clr"> Contact person :</span>Mr. Kanwal Jit Singh  - 9316818372 / 8146005400 </p>
     <p><span class="bl_clr"> Add :</span>820,  Industrial  Aera - B,  
Near Allahabad Bank  Partap Chowk 
Ludhiana - 141 003
Punjab  (PB)

</p>
<p><span class="bl_clr"> Tel. No. :</span> 0161 - 4638372</p>
<p><span class="bl_clr"> Email Id :</span> <a href="mailto: cs_lud_paf@patel-india.com">  cs_lud_paf@patel-india.com </a> / <a href="mailto: ludhiana_ba@patel-india.com">  ludhiana_ba@patel-india.com </a></p>

  </div>
                                </div>

                   

                        </ul>

                    </div>

                </div>

                <div class="clearfix"></div>

            </div>





        </div>

        </li>

	</div>

    </section>

    <!-- js starts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/rem.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQbt8fBXZu0RHPqPCRM3GPMRklOt-9Als&callback=initMap"></script>
    <script src="js/mapmarker.jquery.js"></script>
    <!--<script src="js/equate.js"></script>-->

    <script src="js/general.js"></script>
    <!--<script src="js/css3-animate-it.js"></script>-->

    <script type="text/javascript">
        $(document).ready(function () { var a = { markers: [{ latitude: "19.078018", longitude: "72.828999", icon: "images/pointer.png", baloon_text: "<strong> PATEL HOUSE </strong> <p> 48, Gazdar Bandh, North Avenue Road,<br> Santacruz (West), Mumbai, India – 400 054 </p>"}] }; $(".map_wrp").mapmarker({ zoom: 17, center: "19.078018, 72.828999", markers: a }) });


        jQuery(".patel_roadways_address_wrp .tabs li a").click(function (event) {

            $(".patel_roadways_address_wrp .tabs li a").removeClass('active');
            $(this).addClass('active');





            /*setTimeout(function(){
            equalheight('.equate');
            },50);*/

        });

        jQuery(".pobc_tabs li a").click(function (event) {

            $(".pobc_tabs li a").removeClass('active_1');
            $(this).addClass('active_1');





            /*setTimeout(function(){
            equalheight('.equate');
            },50);*/

        });




    </script>

    

    <style type="text/css">
        .office_address_txt {
            min-height: 396px;
        }
    </style>

    <script src="js/index.js"></script>

    <!-- js ends -->


    </div>
    <footer>

        <div class="container-fluid footer_wrp">

            <div class="row wrapper">

                <div class="col-xs-12 footer_box_1">

                    <div class="col-md-5 col-sm-12 no_padding foot_about yellow_line">

                        <div class="foot_head about_foot_head">About Us <i class="fa fa-angle-down" aria-hidden="true"></i></div>

                        <p class="foot_dropdown">Started as a ‘One man’ ‘One truck’ company Est. 1959; Inc. 1962. PATEL is a trusted name in the world of logistics. One of the pioneers of logistics in India. An ISO 9001:2008 company listed with premier stock exchanges like the National Stock Exchange and Bombay Stock Exchange. Today, PATEL has a well deserved reputation for strong business values, quality services & unflagging corporate excellence.</p>

                    </div>

                    <div class="col-md-6 col-md-offset-1 col-sm-12 no_padding">

                        <div class="col-md-4 col-sm-4 no_padding yellow_line">

                            <div class="foot_head">OUR SERVICES <i class="fa fa-angle-down" aria-hidden="true"></i></div>

                            <ul class="foot_dropdown">
                                <li><a href="patel_roadways.aspx">PATEL Roadways</a></li>
                                <li><a href="patel_express.aspx">PATEL Express</a> </li>
                                <li><a href="patel_airfreight.aspx">PATEL Airfreight</a></li>
                                <li><a href="patel_warehouse.aspx">PATEL Warehouse</a></li>
                             
                            </ul>

                        </div>

                        <div class="col-md-4 col-sm-4 no_padding yellow_line">

                            <div class="foot_head">QUICK LINKS <i class="fa fa-angle-down" aria-hidden="true"></i></div>

                            <ul class="foot_dropdown">
                                <li><a href="careers.aspx">Careers</a> </li>
                                <li><a href="share_holding_pattern.aspx">Investor Relation</a></li>
                                <li><a href="media_news_center.aspx">Media & News Center</a></li>
                            </ul>

                        </div>

                        <div class="col-md-4 col-sm-4 no_padding yellow_line">

                            <div class="foot_head support">SUPPORT <i class="fa fa-angle-down" aria-hidden="true"></i></div>

                            <ul class="foot_dropdown">
                                <li><a href="location.aspx">Locations</a></li>
                                <!--<li><a href="#">FAQ</a></li>-->
                                
                                <li><a href="contact_us.aspx">Contact Us</a></li>
                                <li><a href="#">Sitemap</a></li>
                            </ul>

                        </div>

                    </div>

                </div>

                <div class="col-xs-12 footer_box_2 no_padding">

                    <div class="col-md-6 col-sm-12 col-xs-12 no_padding copyright">
                        &copy; 2018  PATEL INTEGRATED LOGISTICS LTD. All rights reserved.
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12 no_padding foot_link">
                        
                        <a href="https://kwebmaker.com/" target="_blank" class="pull-right">kwebmaker &trade;</a>
                    </div>

                </div>

            </div>

        </div>

    </footer>
    <!-- #EndLibraryItem -->
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"></a>

</body>
</html>
