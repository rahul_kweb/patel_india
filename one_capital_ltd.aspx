﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HouseOfPatel.master" AutoEventWireup="true" CodeFile="one_capital_ltd.aspx.cs" Inherits="one_capital_ltd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%--    <link rel="shortcut icon" href="images/fav_icon.png">
<!-- css starts -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/animations.css">
<link rel="stylesheet" href="css/slick.css">
<link rel="stylesheet" href="css/slick-theme.css">
<link rel="stylesheet" href="css/nav-layout.min.css">
<link rel="stylesheet" href="css/jquery.fancybox.min.css">
<link rel="stylesheet" href="css/general.css">
<link rel="stylesheet" href="css/inner_page.css">--%>
    <section>
	
    <div class="service_box_content_text container-fluid">
    	
        <div class="row inner_banner_wrp" style="background:url(images/inner_banner/one_capital.jpg) no-repeat center; background-size:cover;">
        	
            <div class="inner_page_title">
            	<div class="inner_page_title_txt text-center">
               		<h1>One Capital Ltd</h1>
                  	<ul class="breadcrumb">
                  		<li><a href="index.aspx">Home</a></li>
                        <li>Our Services</li>
                    </ul>
                </div>
            </div>
        </div>
        
      	<div class="row services_wrp wrapper">
        
        	<div class="services_wrp_logo"><img src="images/one_capital_logo.jpg" alt="" class="logo_border"></div>
            
            <div class="row patel_express_txt">
        	
                <div class="col-md-6 col-sm-6 col-xs-12">
                    
                    <p>M/s One Capitall Limited was incorporated on 11th April, 2008 under the Indian Companies Act, 1956 as M/s One Capital Private Limited vide Certificate of Incorporation issued on 11th April, 2008. The name from M/s One Capital Private Limited was changed to M/s One Capitall Private Limited vide fresh Incorporation Certificate dated 01st July, 2009. Further, M/s One Capitall Private Limited was converted into a Public Limited Company on 9th June, 2010 as M/s One Capitall Limited vide Certificate of Incorporation dated 09th June, 2010. The Company is a Non-Banking Finance Company registered with Reserve Bank of India under certificate No. N-13.01921</p>

<p>M/s One Capitall Limited being an NBFC provides finance by way of Loans, Inter-Corporate Deposits, and Structured Loans to Corporate, Partnership Firms, Individuals, etc. The finance is mainly provided for business purpose to Small and Medium Enterprises, HNI’s against security of immovable and moveable assets.</p>

<p>The geographical area of business operation is in and around the city of Mumbai. However, in the long term, the Company would be considering to expand to other Metros as well.</p>
				
                </div>
                
                <div class="col-md-6 col-sm-6 col-xs-12 one_capital_right">
                    
                    <p>The different segments and Products which the Company presently offers is as follows:</p>

					<div class="inner_title"><h2>SME Financing</h2></div>
                    
                    <div class="arrowlistmenu"> 
       
       				<div class="main_accord"> 
            			
                    	<div class="menuheader expandable openheader" headerindex="0h">
                        	<span class="accordprefix"></span>Loan Against Property
                            <span class="accordsuffix"></span>
                        </div>
                            
                      	<div class="categoryitems accord_detail" contentindex="0c" style="">
                        
                       	 	<p>The ‘loan against property’ product enables customers to obtain loans against their residential or commercial property. Loans offered under this product may be utilized towards different Business purposes including Business Expansion, purchase of plant & machinery and debt consolidation with ease of payment for a longer tenor, creating infrastructure for a new project etc.</p>
                        
                        </div>
                            
       					<div class="menuheader expandable" headerindex="1h">
                        	<span class="accordprefix"></span>Capex / Working Capital Loans 
                            <span class="accordsuffix"></span>
                        </div>
             			
                        <div class="categoryitems accord_detail" contentindex="1c" style="display: none;">
                    		
                        	<p>Through the above product the company caters to the borrowing requirements of entities for various business needs. These loans are provided to meet working capital, expansion or other business uses which are typically shorter tenor usages or bridge finances. We have the flexibility to provide tailor made solutions also to suit particular requirements of the clients e.g., secured loans for working capital requirements, secured term loans for capital expenditure and expansion purposes. The Company has also developed a product for financing educational institutes for their expansion / long term investment needs.</p>
                            
               			</div>
                
              			<div class="menuheader expandable" headerindex="2h">
                        	<span class="accordprefix"></span>Consumer Financing
                            <span class="accordsuffix"></span>
                        </div>
                        
             			<div class="categoryitems accord_detail" contentindex="2c" style="display: none;">
                    		
                            <p>This scheme cater to all types of users including Individuals, Proprietorship concerns, HUFs, Firms, Corporate and other enterprises for their different type of needs which include vehicle loan, equipment loan, Personal loan etc. backed by collateral security.</p>
                            
               			</div>
                
                	</div>
 				
                </div>
                
                </div>
            
            </div>
       
       </div>
     
     </div>

</section>

    <script src="js/jquery.min.js"></script>

<script src="js/bootstrap.min.js"></script>

<script src="js/rem.min.js"></script>

<script src="js/ddaccordion.js"></script>

<script src="js/general.js"></script>

<script src="js/css3-animate-it.js"></script>

<script>

    ddaccordion.init({ headerclass: "expandable", contentclass: "categoryitems", revealtype: "click", mouseoverdelay: 200, collapseprev: !0, defaultexpanded: [0], onemustopen: !1, animatedefault: !0, persiststate: !1, toggleclass: ["", "openheader"], togglehtml: ["prefix", "", ""], animatespeed: "fast", oninit: function (e, a) { }, onopenclose: function (e, a, t, n) { } });

</script>

</asp:Content>

